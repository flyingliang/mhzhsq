/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.lzxuni.common.exception;

import com.lzxuni.common.utils.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * 异常处理器
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2016-10-27
 */
@ControllerAdvice
public class LzxExceptionHandler {
	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 处理自定义异常
	 */
	@ExceptionHandler(LzxException.class)
	public String handleRRException(LzxException e, HttpServletRequest request){
		R r = R.error(e.getMessage());
		String requestURI = request.getRequestURI();
		r.put("requestURI", requestURI);
		return handle(r,e,request);
	}
	@ExceptionHandler(NullPointerException.class)
	public String handleRRException(NullPointerException e, HttpServletRequest request){
		R r = R.error(e.getMessage());
		String requestURI = request.getRequestURI();
		r.put("requestURI", requestURI);
		return handle(r,e,request);
	}

	@ExceptionHandler(DuplicateKeyException.class)
	public String handleDuplicateKeyException(DuplicateKeyException e, HttpServletRequest request){
		R r = R.error("数据库中已存在该记录");
		return handle(r,e,request);
	}

	@ExceptionHandler(Exception.class)
	public String handleException(Exception e, HttpServletRequest request){
		R r = R.error(e.getMessage());
		return handle(r,e,request);
	}

	private String handle(R r, Exception e, HttpServletRequest request) {
		logger.error(e.getMessage(), e);
		request.setAttribute("javax.servlet.error.status_code",500);
		request.setAttribute("r",r);
		return "forward:/error";
	}
}
