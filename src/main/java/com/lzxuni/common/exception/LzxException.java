package com.lzxuni.common.exception;

/**
 * 自定义异常
 * 
 * @author 孙志强

 * @date 2016年10月27日 下午10:11:27
 */
public class LzxException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
    private String message;
    private int code = 500;

	public LzxException(Throwable cause) {
		super(cause);
	}

	public LzxException(String message) {
		super(message);
		this.message = message;
	}
	
	public LzxException(String message, Throwable e) {
		super(message, e);
		this.message = message;
	}
	
	public LzxException(String message, int code) {
		super(message);
		this.message = message;
		this.code = code;
	}
	
	public LzxException(String message, int code, Throwable e) {
		super(message, e);
		this.message = message;
		this.code = code;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}
