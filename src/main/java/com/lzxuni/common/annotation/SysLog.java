package com.lzxuni.common.annotation;

import java.lang.annotation.*;

/**
 * 系统日志注解
 * 
 * @author 孙志强

 * @date 2017年3月8日 上午10:19:56
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

	//分类
	int categoryId() ;
	//功能模块
	String module() ;
	//操作类型
	String operateType() ;

}
