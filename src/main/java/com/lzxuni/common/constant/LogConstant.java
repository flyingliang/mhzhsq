package com.lzxuni.common.constant;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author:孙志强
 * @create:2019-01-28 10:36
 * @Modified BY:
 **/

public class LogConstant {
	public static final int LOGINID = 1; //登陆
	public static final int VIEWID = 2;  //访问
	public static final int OPERATEID = 3;//操作-增删改
	public static final int EXCEPTIONID = 4; //异常

	public static final String LOGIN = "登陆";
	public static final String VIEW = "访问";
	public static final String INSERT = "增加";
	public static final String DELETE = "删除";
	public static final String UPDATE = "修改";
	public static final String IMPORT = "导入";
	public static final String EXPORT = "导出";

}
