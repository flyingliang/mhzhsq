package com.lzxuni.common.constant;

/**
 * web常量
 */
public abstract class Constants {
	
	/**
	 * 静态资源分隔符，孙志强加，用在静态资源过滤，匹配url规则是否包含此字符串，包含则用流读文件返回
	 * 此变量全局唯一，别的模块不能包含此关键字
	 */
	public static final String STATIC_RESOURCE_BASE_PATH = "resource";
	/**
	 * 路径分隔符
	 */
	public static final String SPT = "/";
	/**
	 * 索引页
	 */
	public static final String INDEX = "index";
	/**
	 * 手机索引页
	 */
	public static final String INDEX_MOBILE = "indexMobile";
	/**
	 * 索引页
	 */
	public static final String INDEX_HTML = "/index.html";
	/**
	 * 手机索引页
	 */
	public static final String INDEX_HTML_MOBILE = "/indexMobile.html";
	/**
	 * 默认模板
	 */
	public static final String DEFAULT = "default";
	/**
	 * UTF-8编码
	 */
	public static final String UTF8 = "UTF-8";
	/**
	 * 提示信息
	 */
	public static final String MESSAGE = "message";
	/**
	 * cookie中的JSESSIONID名称
	 */
	public static final String JSESSION_COOKIE = "JSESSIONID";
	/**
	 * url中的jsessionid名称
	 */
	public static final String JSESSION_URL = "jsessionid";
	/**
	 * HTTP POST请求
	 */
	public static final String POST = "POST";
	/**
	 * HTTP GET请求
	 */
	public static final String GET = "GET";
	
	public static final String ADMIN_SUFFIX = "admin/index.html";
	
	
}
