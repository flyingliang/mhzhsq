package com.lzxuni.common.constant;

import com.lzxuni.common.utils.UuidUtil;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author:孙志强
 * @create:2018-12-18 17:11
 * @Modified BY:
 **/

public class ConstantEnum {

	public enum VersionEnum {
		COMPANYDATA("company", UuidUtil.get32UUID()) ,
		DEPTDATA("dept", UuidUtil.get32UUID()) ,
		ITEMDATA("itemdata", UuidUtil.get32UUID()) ;

		VersionEnum(String module,String ver){
			this.module = module;
			this.ver = ver;
		}
		private String module;
		private String ver;

		public String getModule() {
			return module;
		}

		public void setModule(String module) {
			this.module = module;
		}

		public String getVer() {
			return ver;
		}

		public void setVer(String ver) {
			this.ver = ver;
		}
	}

	/**
	 * 功能描述: 系统日志常量<br>
	 *
	 * @Author 孙志强
	 * @date 2018/12/19 14:51
	 */
	public enum LogEnum {
		VISIT(2),
		INSERT(2),
		UPDATE(2),
		DELETE(2),
		;

		LogEnum(int categoryId) {
			this.categoryId = categoryId;
		}

		private int categoryId;



	}

	public enum OSSEnum {
		ACCESS_ID("id", "LTAIXRDasRv07e1d") ,
		ACCESS_KEY("key", "XSRWAvt65mg5WehxWjirEZiCDFt4lV") ,
		BUCKET_NAME("name", "netloan2") ,
		OSS_ENDPOINT("url", "http://oss-cn-shenzhen.aliyuncs.com") ;

		OSSEnum(String parameter,String value){
			this.parameter = parameter;
			this.value = value;
		}

		private String parameter;
		private String value;

		public String getParameter() {
			return parameter;
		}

		public void setParameter(String parameter) {
			this.parameter = parameter;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}


}
