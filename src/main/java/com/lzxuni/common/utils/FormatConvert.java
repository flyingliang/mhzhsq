package com.lzxuni.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormatConvert {

	/**
     * 名字的转化
     * 1.规则：姓+*
     * **/
    public static String replaceNameX(String str){
        String reg = ".{1}";
        StringBuffer sb = new StringBuffer();
        Pattern p = Pattern.compile(reg);
        Matcher m = p.matcher(str);
        int i = 0;
        while(m.find()){
            i++;
            if(i==1)
                continue;
            m.appendReplacement(sb, "*");
        }
        m.appendTail(sb);
        return sb.toString();
    }

    /**
     * 格式化金额，并将金额保留*号
     */
    public static String moneyFormat(Double v) {
        DecimalFormat formater = new DecimalFormat("0.00");
        formater.setMaximumFractionDigits(2);
        formater.setGroupingSize(3);
        formater.setRoundingMode(RoundingMode.FLOOR);
        formater.setGroupingUsed(true);
        String temp = formater.format(v.doubleValue());
        String result = temp.substring(0,temp.lastIndexOf(","));
        return result = result +",***.**";
    }

	/** name && address **/
    public static String getClAddress(String content, int frontNum, int endNum) {
        if(StringUtils.isEmpty(content)){
            return content;
        }
        if (frontNum >= content.length() || frontNum < 0) {
            return content;
        }
        if (endNum >= content.length() || endNum < 0) {
            return content;
        }
        if (frontNum + endNum >= content.length()) {
            return content;
        }
        String starStr = "";
        for (int i = 0; i < (content.length() - frontNum - endNum); i++) {
            starStr = starStr + "*";
        }
        return content.substring(0, frontNum) + starStr
                + content.substring(content.length() - endNum, content.length());
    }

    /** phone **/
    public static String replacePhoneNumber(String phoneNumber){
        if(StringUtils.isNotEmpty(phoneNumber)) {
            phoneNumber = phoneNumber.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
        }
        return phoneNumber;
    }

    /**
     * 身份证
     * 规则：后四位****
     * **/
    public static String idcard(String idcard){
        if (!idcard.isEmpty()) {
            idcard = idcard.replaceAll(idcard.substring(idcard.length()-4),"****");
        }
        return idcard;
    }
    /**
     * 银行卡
     * 规则：后六位******
     * **/
    public static String bankCard(String bankCard){
        if (!bankCard.isEmpty()) {
            bankCard = bankCard.replaceAll(bankCard.substring(bankCard.length()-6),"******");
        }
        return bankCard;
    }

    /**
     * 格式化合作社信息
     */
    public static String substring(String str){
        if (StringUtils.isNotEmpty(str)&& str.contains("县")) {
            str =  str.substring(0, str.indexOf("县"))+"县******合作社";
        }else if (StringUtils.isNotEmpty(str)&& str.contains("市")) {
            str =  str.substring(0, str.indexOf("市"))+"市******合作社联社";
        }else{
            str =  str.substring(0, 2)+"******合作社";
        }

        return str;
    }

    /**
     * 格式化金钱
     */
    public static String FormatMoney(String str){
        String money = "0.00";
        if (StringUtils.isNotEmpty(str) && !str.equals("0.00")) {
            if (str.indexOf(",") > 0) {

            }else{
                str = BigDecimalUtils.moneyFormat(new BigDecimal(str).doubleValue());
            }
            money = str.substring(0, str.lastIndexOf(","));
            money += ",***.**";
        }
        return money;
    }

}
