package com.lzxuni.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author:孙志强
 * @create:2019-02-22 15:24
 * @Modified BY:
 **/
public class BigDecimalUtils {

	//默认除法运算精度
	private static final int DEF_DIV_SCALE = 10;

	//所有方法均用静态方法实现，不允许实例化
	private BigDecimalUtils() {}

	/**
	 * 实现浮点数的加法运算功能
	 *
	 * @param v1 加数1
	 * @param v2 加数2
	 * @return v1+v2的和
	 */
	public static double add(BigDecimal v1, BigDecimal v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		return b1.add(b2).doubleValue();
	}

	/**
	 * 实现浮点数的减法运算功能
	 *
	 * @param v1 被减数
	 * @param v2 减数
	 * @return v1-v2的差
	 */
	public static double sub(BigDecimal v1, BigDecimal v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		return b1.subtract(b2).doubleValue();
	}

	/**
	 * 实现浮点数的乘法运算功能
	 *
	 * @param v1 被乘数
	 * @param v2 乘数
	 * @return v1×v2的积
	 */
	public static double multi(BigDecimal v1, BigDecimal v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		return b1.multiply(b2).doubleValue();
	}

	/**
	 * 实现浮点数的除法运算功能
	 * 当发生除不尽的情况时，精确到小数点以后DEF_DIV_SCALE位(默认为10位)，后面的位数进行四舍五入。
	 *
	 * @param v1 被除数
	 * @param v2 除数
	 * @return v1/v2的商
	 */
	public static double div(BigDecimal v1, BigDecimal v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		return b1.divide(b2, DEF_DIV_SCALE, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	/**
	 * 实现浮点数的除法运算功能
	 * 当发生除不尽的情况时，精确到小数点以后scale位，后面的位数进行四舍五入。
	 *
	 * @param v1    被除数
	 * @param v2    除数
	 * @param scale 表示需要精确到小数点以后几位
	 * @return v1/v2的商
	 */
	public static double div(BigDecimal v1, BigDecimal v2, int scale) {
		if (scale < 0) {
			throw new IllegalArgumentException(
					"The scale must be a positive integer or zero");
		}
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	/**
	 * 提供精确的小数位四舍五入功能
	 *
	 * @param v     需要四舍五入的数字
	 * @param scale 小数点后保留几位
	 * @return 四舍五入后的结果
	 */
	public static double round(Double v, int scale) {
		if (scale < 0) {
			throw new IllegalArgumentException(
					"The scale must be a positive integer or zero");
		}
		BigDecimal b = new BigDecimal(Double.toString(v));
		BigDecimal one = new BigDecimal("1");
		return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	/**
	 * 格式化金额。带千位符
	 *
	 * @param v
	 * @return
	 */
	public static String moneyFormat(Double v) {
		DecimalFormat formater = new DecimalFormat("0.00");
		formater.setMaximumFractionDigits(2);
		formater.setGroupingSize(3);
		formater.setRoundingMode(RoundingMode.FLOOR);
		formater.setGroupingUsed(true);
		return formater.format(v.doubleValue());
	}


}
