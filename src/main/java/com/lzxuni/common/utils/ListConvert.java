package com.lzxuni.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.date.DateUtil;
import com.lzxuni.modules.organization.entity.User;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.lzxuni.modules.shiro.ShiroUtils.getUser;

/**
 * 〈一句话功能简述〉<br>
 *
 * 本工具类暂时没用
 * @author:szq
 * @create:2019-03-13 16:06
 * @Modified BY:
 * 本方法用来将传进来的list集合进行处理将里面含有的字符串进行带*的转换
 * 要求的转换规则
 * 1.结算数据里面
 *   人名：姓+*
 *   合作社：县+*+合作社
 *   身份证：后四位*
 *   利率是*
 *
 *   注意事项
 *      用银行往来账户做的案例：
 *         （1.本类适用于页面中的显示
 *           2.对于导出的功能不太适用（因为我们的导出功能做了数据的加和处理,而且转回成对象也很麻烦，到不如直接更改导出对象属性的值）
 *           3.对于页面的显示我觉得还是用笨方法比较容易（循环list集合单独更改某个类的属性）
 *                对于特殊的类型我们给出一个不可能的值然后在页面进行判断
 *                 后端：
 *                 loan1.setInterestRate(new BigDecimal("-1"));
 *                 try {
 *                     loan1.setApplyDate(new SimpleDateFormat("yyyy-MM-dd").parse("1900-01-01"));
 *                 } catch (ParseException e) {
 *                     e.printStackTrace();
 *                 }
 *                 前端：
 *                 formatter: function(d) {
 *                             if("1900-01-01 00:00:00" !== d){
 *                                 return b.formatDate(d, "yyyy-MM-dd")
 *                             }else{
 *                                 return "****-**-**"
 *                             }
 *                         }
 *               这样一个笨方法（适用于少数的），一个使用工具类（适用于整体量大的情况）的方法就全了
 *           ）
 *      1.static list变量不能更改，更改后需重启服务器
 *      2.当我们更改一个类的其他属性，但是不更改日期属性，但是页面中又显示了日期的时候
 *          a.如果不更改日期类型，页面返回的是num类型（我们需要转换）   此时分为两种类型给不同的角色应用
 *          b.如果更改日期类型,页面返回的是String类型,这个类型又分为两种一种是正常的日期类型
 *          另外的就是带*的类型（我们自己设定的）  此时也分为两种类型给不同的角色应用
 *          formatter: function(d) {
 *                             if(typeof (d) === "string"){
 *                                 if(d.indexOf("*") == -1 ){
 *                                     return b.formatDate(d, "yyyy-MM-dd")
 *                                 }else {
 *                                     return d
 *                                 }
 *                             }
 *                             if(typeof (d) ==="number"){
 *                                 return  b.formatDate(new Date(d), "yyyy-MM-dd");
 *                             }
 *                         }
 *
 **/

public class ListConvert {
	/**
	 * 正常的角色需要调用的方法
	 *
	 * @param listOld
	 * @return
	 */
	public static List listConvert(List listOld) {
		//将list转换成JSONArray
		JSONArray array = JSONArray.parseArray(JSON.toJSONString(listOld));
		for (int i = 0; i < array.size(); i++) {
			//将JSONArray 中的每一项进行遍历，是一个list里面是一个个键值对
			JSONObject jsonObject = (JSONObject) array.get(i);
			/**
			 * 已还款用到格式化还款金额
			 * 1.如果key值等于repayMoney
			 * 2.我们就把该key对应的值进行格式化的操作
			 * 3.格式化成带逗号的
			 */
			if (jsonObject.get("repayMoney") != null) {
				jsonObject.put("repayMoney", BigDecimalUtils.moneyFormat(Double.valueOf(String.valueOf(jsonObject.get("repayMoney")))));
			}
			/**
			 * 未还款需要格式化：贷款金额 ，贷款时间
			 */
			if (jsonObject.get("allowMoney") != null) {
				jsonObject.put("allowMoney", BigDecimalUtils.moneyFormat(Double.valueOf(String.valueOf(jsonObject.get("allowMoney")))));
			}
			if (jsonObject.get("allowDate") != null) {
				//将long类型格式化成一个时间的字符串返回到页面中
				jsonObject.put("allowDate", DateUtil.DateToString(new Date((long)jsonObject.get("allowDate")), "yyyy-MM-dd"));
			}
			/**
			 * 刷卡数据：刷卡金额
			 */
			if (jsonObject.get("fkje") != null) {
				jsonObject.put("fkje", BigDecimalUtils.moneyFormat(Double.valueOf(String.valueOf(jsonObject.get("fkje")))));
			}
			/**
			 * 结算管理第一层用到的,结算金额
			 */
			if (jsonObject.get("settlementAmount") != null) {
				jsonObject.put("settlementAmount", BigDecimalUtils.moneyFormat(Double.valueOf(String.valueOf(jsonObject.get("settlementAmount")))));
			}


		}
		return JSONObject.parseArray(array.toJSONString(), Map.class);
	}

	/**
	 * 演示的角色需要调用的方法
	 *
	 * @param listOld
	 * @return
	 */
	public static String[] list = {"name", "customRealName", "customCode", "fullName", "repayMoney","allowMoney", "interestRate","yqkCard","hzs","fkje","settlementAmount"};

	public static List getList(List listOld) {
		//将list转换成JSONArray
		JSONArray array = JSONArray.parseArray(JSON.toJSONString(listOld));
		for (int i = 0; i < array.size(); i++) {
			//将JSONArray 中的每一项进行遍历，应该是一个个的键值对
			JSONObject jsonObject = (JSONObject) array.get(i);
			for (int j = 0; j < list.length; j++) {
				/**
				 * 我们将这个键值对用key的方式看看是否包含集合中的数据
				 * 如果这个对象的属性中含有list集合中的名字，并且这个属性值等于name的时候我们就进行下面的操作将name的值格式化成
				 * 先根据属性key值获取该属性值然后将这个格式化一下就可以了
				 **/
				if (jsonObject.containsKey(list[j])) {
					if ("name".equals(list[j])) {
						jsonObject.put(list[j], FormatConvert.replaceNameX((String) jsonObject.get(list[j])));
					}
					/**
					 * 已还款模块：customRealName ，customCode，fullName，repayMoney
					 */
					if ("customRealName".equals(list[j])) {
						jsonObject.put(list[j], FormatConvert.replaceNameX((String) jsonObject.get(list[j])));
					}
					if ("customCode".equals(list[j])) {
						jsonObject.put(list[j], FormatConvert.idcard((String) jsonObject.get(list[j])));
					}
					if ("fullName".equals(list[j])) {
						jsonObject.put(list[j], FormatConvert.substring((String) jsonObject.get(list[j])));
					}
					if ("repayMoney".equals(list[j])) {
						jsonObject.put(list[j], FormatConvert.moneyFormat((Double.valueOf(String.valueOf(jsonObject.get(list[j]))))));
					}
					/**
					 * 未还款模块：贷款金额，贷款时间，利率
					 */
					if ("allowMoney".equals(list[j])) {
						jsonObject.put(list[j], FormatConvert.moneyFormat((Double.valueOf(String.valueOf(jsonObject.get(list[j]))))));
					}
					if ("interestRate".equals(list[j])) {
						jsonObject.put(list[j], "****");
					}
					/**
					 * 园区卡充值数据
					 */
					if ("yqkCard".equals(list[j])) {
						jsonObject.put(list[j], FormatConvert.bankCard((String) jsonObject.get(list[j])));
					}
					/**
					 * 刷卡数据：合作社，刷卡金额
					 */
					if ("hzs".equals(list[j])) {
						jsonObject.put(list[j], FormatConvert.substring((String) jsonObject.get(list[j])));
					}
					if ("fkje".equals(list[j])) {
						jsonObject.put(list[j], FormatConvert.moneyFormat((Double.valueOf(String.valueOf(jsonObject.get(list[j]))))));
					}
					/**
					 * 结算管理第一层用到的，结算金额
					 */
					if ("settlementAmount".equals(list[j])) {
						jsonObject.put(list[j], FormatConvert.moneyFormat((Double.valueOf(String.valueOf(jsonObject.get(list[j]))))));
					}






				}
			}
			/**
			 * 未结算,充值数据：用到日志转换
			 * jsonObject 直接把日期转换
			 */
			if (jsonObject.get("allowDate") != null) {
				//将long类型格式化成一个时间的字符串返回到页面中
				jsonObject.put("allowDate", DateUtil.DateToString(new Date((long)jsonObject.get("allowDate")), "yyyy-MM-dd"));
			}
		}
		return JSONObject.parseArray(array.toJSONString(), Map.class);
	}

	/**
	 * 封装当用户的角色分成演示或者不演示的时候，在Controller中调用的一个方法
	 */
	public static PageInfo PageInfoConvert(PageInfo pageInfo) {
		User user = getUser();
		if (user != null && user.hashRole("演示")) {
			/**
			 * 1.演示角色要把几个字段变成*
			 *   姓名，身份证，所属合作社，还款金额
			 */
			pageInfo.setList(ListConvert.getList(pageInfo.getList()));
		} else {
			/**
			 * 1.正常角色我们也要将上面的
			 * 2.由于演示角色的时候，我们要将list都转换成map的形式所以我们就直接不管什么角色都返回map
			 */
			pageInfo.setList(ListConvert.listConvert(pageInfo.getList()));
		}
		return pageInfo;
	}
}










