package com.lzxuni.common.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author:孙志强
 * @create:2018-12-18 17:36
 * @Modified BY:
 **/

public class VersionUtil {

	public static String getVersion(HttpServletRequest request, String module){
		String ver = (String) request.getServletContext().getAttribute(module);
		if(StringUtils.isEmpty(ver)){
			ver = UuidUtil.get32UUID();
		}
		return ver;
	}
	public static String updateVersion(HttpServletRequest request, String module){
		String ver = UuidUtil.get32UUID();
		request.getServletContext().setAttribute(module, ver);
		return ver;
	}
}
