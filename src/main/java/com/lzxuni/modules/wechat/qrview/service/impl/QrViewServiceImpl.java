package com.lzxuni.modules.wechat.qrview.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.aged.organization.entity.AgedOrganization;
import com.lzxuni.modules.wechat.config.entity.WechatUserInfo;
import com.lzxuni.modules.wechat.qrview.mapper.QrViewMapper;
import com.lzxuni.modules.wechat.qrview.service.QrViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 微信预览页 服务实现类
 * Created by 潘云明
 * 2019/7/8 14:58
 */
@Service
public class QrViewServiceImpl  implements QrViewService {
    @Autowired
    private QrViewMapper mapper;

    @Override
    public String queryOne(String sql){
       List<String> list= mapper.queryOne(sql);
        return list.get(0);
    }
}
