package com.lzxuni.modules.wechat.qrview.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.aged.organization.entity.AgedOrganization;
import com.lzxuni.modules.wechat.config.entity.WechatUserInfo;

import java.util.List;
import java.util.Map;

/**
 * 微信预览页 服务类
 * Created by 潘云明
 * 2019/7/5 16:08
 */
public interface QrViewService {
    String queryOne(String sql);

}
