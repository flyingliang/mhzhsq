package com.lzxuni.modules.wechat.qrview.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.wechat.config.entity.WechatUserInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 微信预览页 Mapper 接口
 * Created by 潘云明
 * 2019/7/8 15:01
 */
public interface QrViewMapper  {
    List<String> queryOne(@Param("sql") String sql);
}
