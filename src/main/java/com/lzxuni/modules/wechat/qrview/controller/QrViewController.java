package com.lzxuni.modules.wechat.qrview.controller;

import com.github.pagehelper.util.StringUtil;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import com.lzxuni.modules.wechat.qrview.service.QrViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 微信预览页
 * Created by 潘云明
 * 2019/7/16
 */
@Controller
@RequestMapping("/wechat")
public class QrViewController extends WeChatController {
    @Autowired
    private QrViewService qrViewService;

    @RequestMapping("/qrView")
    public ModelAndView personage(HttpServletRequest req, String type, String id) {
        String sql = "";
        ModelAndView mv = new ModelAndView("/wechat/qrview/index");
        if (StringUtil.isEmpty(type) || StringUtil.isEmpty(id) || id.length() != 32) {
            return mv;
        }
        //首页-政务公开
        if (type.equals("Publicity")) {   //政务发布
            sql = "select content from tbl_home_publicity where id='" + id+"'";
        }else if (type.equals("style")){   //党建作风建设
            sql = "select content from tbl_home_party_style where id='" + id+"'";
        }else if (type.equals("activity")){   //志愿者活动管理
            sql = "select content from tbl_home_volunteer_activity where id='" + id+"'";
        }else if (type.equals("tissue")){   //志愿者组织管理
            sql = "select introduce from tbl_home_volunteer_tissue where id='" + id+"'";
        }else if (type.equals("mediate")){   //矛盾调解案例
            sql = "select content from tbl_home_lawmanage_mediate where id='" + id+"'";
        }else if (type.equals("aidcase")){   //法律援助案例
            sql = "select content from tbl_home_lawmanage_aidcase where id='" + id+"'";
        }else if (type.equals("comtraining")){   //警务室警训
            sql = "select content from tbl_home_lawmanage_comtraining where id='" + id+"'";
        }else if (type.equals("createpeace")){   //平安创建管理
            sql = "select content from tbl_home_lawmanage_createpeace where id='" + id+"'";
        }else if (type.equals("correct")){   //社区矫正管理
            sql = "select content from tbl_home_lawmanage_correct where id='" + id+"'";
        }else if (type.equals("policeroom")){   //社区警务室管理
            sql = "select content from tbl_home_lawmanage_policeroom a\n" +
                    "LEFT JOIN tbl_home_lawmanage_comtraining b on a.communityid = b.communityid\n" +
                    "where a.id='" + id+"'";
        }else if (type.equals("regulations")){   //法律法规管理
            sql = "select content from tbl_home_lawmanage_regulations where id='" + id+"'";
        }else if (type.equals("onlineconsulation")){   //在线咨询管理
            sql = "select content from tbl_home_lawmanage_onlineconsultation where id='" + id+"'";
        }else if (type.equals("lookafter")){   //日间照料管理
            sql = "select content from tbl_home_aged_lookafter where id='" + id+"'";
        }else if (type.equals("organization")){   //养老机构管理
            sql = "select content from tbl_home_aged_organization where id='" + id+"'";
        }else if (type.equals("guidancenew")){   //政务大厅办事指南
            sql = "select content from tbl_home_life_guidance_new where id='" + id+"'";
        }else if (type.equals("shop")){   //商家管理
            sql = "select content from tbl_home_life_shop where id='" + id+"'";
        }else if (type.equals("medicine")){   //医疗卫生管理
            sql = "select content from tbl_home_life_medicine where id='" + id+"'";
        }else if (type.equals("affair")){   //政务大厅管理
            sql = "select content from tbl_home_life_affair where id='" + id+"'";
        }else if (type.equals("study")){   //在线学习管理
            sql = "select content from tbl_home_party_study where id='" + id+"'";
        }else if (type.equals("elegance")){   //群团风采管理
            sql = "select content from tbl_home_party_elegance where id='" + id+"'";
        }else if (type.equals("notice")){   //群团风采管理
            sql = "select content from tbl_home_notice where id='" + id+"'";
        }

        mv.addObject("content", qrViewService.queryOne(sql));
        mv.addObject("jsApiConfig", getJsAPiConfig(getAddress(req)));
        return mv;
    }

}
