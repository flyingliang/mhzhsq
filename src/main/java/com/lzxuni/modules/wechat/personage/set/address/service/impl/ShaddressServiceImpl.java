package com.lzxuni.modules.wechat.personage.set.address.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.wechat.personage.set.address.entity.Shaddress;
import com.lzxuni.modules.wechat.personage.set.address.mapper.ShaddressMapper;
import com.lzxuni.modules.wechat.personage.set.address.service.ShaddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 基础管理_地址管理_收货地址 服务实现类
 * Created by 潘云明
 * 2019/7/11
 */
@Service
public class ShaddressServiceImpl extends ServiceImpl<ShaddressMapper, Shaddress> implements ShaddressService {
    @Autowired
    private ShaddressMapper mapper;

    public void updateDef(String id, String openid) {
        mapper.updateDef(id,openid);
    }

    @Override
    public PageInfo<Map<String,Object>> selectListByCondition(PageParameter pageParameter, String openId,String id) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList =mapper.selectListByCondition(openId,id);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }
}
