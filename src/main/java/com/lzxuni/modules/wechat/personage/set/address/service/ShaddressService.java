package com.lzxuni.modules.wechat.personage.set.address.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.wechat.personage.set.address.entity.Shaddress;

import java.util.Map;

/**
 * 基础管理_地址管理_收货地址 服务类
 * Created by 潘云明
 * 2019/7/11
 */
public interface ShaddressService extends IService<Shaddress> {
    //设置默认
    void updateDef(String id,String openid);


    //微信
    PageInfo<Map<String, Object>> selectListByCondition(PageParameter pageParameter, String openId,String id);

}
