package com.lzxuni.modules.wechat.personage.set;


import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.pccontrol.basics.real.entity.RealName;
import com.lzxuni.modules.pccontrol.basics.real.service.RealNameService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 实名认证 前端控制层
 * Created by 潘云明
 * 2019/8/02
 */
@RestController
@RequestMapping("/wechat/personage/set/realname")
public class WxRealNameController extends WeChatController {
    @Autowired
    private  RealNameService realNameService;
    /*
    实名认证
     */
    @RequestMapping("/Index")
    public ModelAndView Index(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("/wechat/personage/set/realname");
        String url = PortUrl + req.getRequestURI();
        mv.addObject("jsApiConfig", getJsAPiConfig(url));
        return mv;
    }

    @RequestMapping("/insert")
    public Object insert(HttpServletRequest req, RealName entity, String uploadImgUrl1,String uploadImgUrl2,String uploadImgUrl3) {
        entity.setOpenid(getOpenId(req));
        entity.setNickname(getNickName(req));
        String ywId = UuidUtil.get32UUID();
        entity.setId(ywId);
        realNameService.save(entity);
        getTempFile(uploadImgUrl1, "实名认证_身份证正面", "realName1", ywId, "image");
        getTempFile(uploadImgUrl2, "实名认证_身份证正面", "realName2", ywId, "image");
        getTempFile(uploadImgUrl3, "实名认证_手持身份证", "realName3", ywId, "image");

        return R.ok("提交成功");
    }

}



