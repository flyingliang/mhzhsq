package com.lzxuni.modules.wechat.personage.mallorder;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.mall.entity.MallOrder;
import com.lzxuni.modules.pccontrol.mall.service.MallOrderService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 个人中心订单
 * Created by 潘云明
 * 2019/8/02
 */
@Controller
@RequestMapping("/wechat/personage")
public class WxMallOrderController extends WeChatController {
    @Autowired
    private MallOrderService mallOrderService;

    //个人中心订单
    @RequestMapping("/order_list")
    public ModelAndView personage_dingD() {
        ModelAndView mv = new ModelAndView("/wechat/personage/personage_order_list");
        return mv;
    }


    //订单列表

    @ResponseBody
    @RequestMapping("/GetRecordList")
    public Object GetRecordList(HttpServletRequest req, String pagination, MallOrder entity) {
        entity.setIsdel("1");
        entity.setOpenid(getOpenId(req));
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(mallOrderService.selectListByConditionAndImg(pageParameter, entity));
        return R.ok().put("data", pageData);
    }


    @RequestMapping("/order_Info")
    public ModelAndView order_Info(HttpServletRequest req,String id) {
        MallOrder entity=new MallOrder();
        entity.setIsdel("1");
        entity.setId(id);
        entity.setOpenid(getOpenId(req));
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        ModelAndView mv = new ModelAndView("/wechat/personage/personage_order_info");
        mv.addObject("orderInfo",getPageData(mallOrderService.selectListByConditionAndImg(pageParameter, entity)));
        return mv;
    }



    @ResponseBody
    @RequestMapping("/isVoer")
    public Object isVoer(HttpServletRequest req, String id) {
        MallOrder mallOrder=new MallOrder();
        mallOrder.setId(id);
        mallOrder.setStatus("3");
        mallOrderService.updateById(mallOrder);
        return R.ok("收货完成");
    }


}
