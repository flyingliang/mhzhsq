package com.lzxuni.modules.wechat.personage.collection.controller;

import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.pccontrol.home.life.shop.entity.Shop;
import com.lzxuni.modules.pccontrol.home.life.shop.service.ShopService;
import com.lzxuni.modules.wechat.personage.collection.entity.Collection;
import com.lzxuni.modules.wechat.personage.collection.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/14
 * 收藏
 */
@Controller
@RequestMapping("/wechat/personage/collection")
public class CollectionController {
    @Autowired
    private CollectionService collectionService;
    @Autowired
    private ShopService tblHomeLifeShopService;
    //收藏--存储
    @ResponseBody
    @RequestMapping("/savecollection")
    public Object saveWenda(HttpServletRequest req, String  shopid) {
        Collection co=new Collection();
        co.setId(UuidUtil.get32UUID());
        co.setOpenid((String)req.getSession().getAttribute("openid"));
        co.setShopid(shopid);
       // String boole="提交成功";
        collectionService.SaveCollection(co);

        return R.ok().put("data", "ok");
    }

    //收藏--存储
    @ResponseBody
    @RequestMapping("/selectListbyopenid")
    public Object selectListbyopenid(HttpServletRequest req) {
        Collection co=new Collection();
        co.setOpenid((String)req.getSession().getAttribute("openid"));
        List<Collection> list=collectionService.selectListbyopenid(co);
        List shoplist=new ArrayList();
        Shop ss=new Shop();
        ss.setYwtype("sj");
        for(Collection cc:list){
            Shop shop=tblHomeLifeShopService.selectshopbyid(ss.setId(cc.getShopid()));
            shoplist.add(shop);

        }

        return R.ok().put("data", shoplist);
    }

    //收藏--存储
    @ResponseBody
    @RequestMapping("/delcollection")
    public Object delcollection(HttpServletRequest req, String  shopid) {
        Collection co=new Collection();
        co.setOpenid((String)req.getSession().getAttribute("openid"));
        co.setShopid(shopid);

        // String boole="提交成功";
        int yon=collectionService.deletebyopenid(co);

        return R.ok().put("data", yon);
    }


}
