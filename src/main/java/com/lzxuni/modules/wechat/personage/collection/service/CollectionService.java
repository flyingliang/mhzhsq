package com.lzxuni.modules.wechat.personage.collection.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.wechat.personage.collection.entity.Collection;

import java.util.List;

public interface CollectionService extends IService<Collection> {

    //保存
    void SaveCollection(Collection demo);
    List<Collection> selectListbyopenid(Collection demo);

    int deletebyopenid(Collection demo);



}
