package com.lzxuni.modules.wechat.personage.tinywish;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.party.wish.entity.Wish;
import com.lzxuni.modules.pccontrol.home.party.wish.service.WishService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 *  功能：</b>党建-->在线学习 <br>
 *  作者：</b>任东方<br>
 *  日期：</b>2019年8月23日
 */

@RestController
@RequestMapping("/wechat/personage")
public class WxTinyWishController extends WeChatController {

    @Autowired
    private WishService wishService;

    @RequestMapping("/personage_WXY")
    public ModelAndView personage_WXY() {
        ModelAndView mv = new ModelAndView("/wechat/personage/personage_WXY");
        return mv;
    }

    @RequestMapping("/personage_WXY_list")
    public Object personage_WXY_list(HttpServletRequest req,String pagination) {
        Wish wish =new Wish();
        String ywType="wish";
        wish.setIsdel("1");
        wish.setIsshow("1");
        wish.setOpenid(getOpenId(req));
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(wishService.sxqueryPageYinyWish(pageParameter,wish,ywType));
        return R.ok().put("data", pageData);
    }

}
