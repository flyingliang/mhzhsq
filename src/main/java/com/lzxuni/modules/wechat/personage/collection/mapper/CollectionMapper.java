package com.lzxuni.modules.wechat.personage.collection.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.wechat.personage.collection.entity.Collection;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface CollectionMapper extends BaseMapper<Collection> {

    //保存
    void saveCollection(Collection demo);

    List<Collection> selectListbyopenid(Collection demo);

    int deletebyopenid(Collection demo);


}
