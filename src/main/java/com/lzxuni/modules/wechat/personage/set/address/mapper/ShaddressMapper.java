package com.lzxuni.modules.wechat.personage.set.address.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.wechat.personage.set.address.entity.Shaddress;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 基础管理_地址管理_收货地址 Mapper 接口
 * Created by 潘云明
 * 2019/7/11
 */
public interface ShaddressMapper extends BaseMapper<Shaddress> {
    void updateDef(@Param("id") String id,@Param("openid")String openid);


    List<Map<String,Object>> selectListByCondition(@Param("openid") String openid,@Param("id") String id);
}
