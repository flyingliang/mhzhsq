package com.lzxuni.modules.wechat.personage.set.address.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 基础管理_地址管理_收货地址
 * Created by 潘云明
 * 2019/8/19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_basics_address_sh")
public class Shaddress implements Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1L;
    @TableId
    /** 主键，uuid */
    private String id;

    /** 姓名 */
    private String name;

    /** 电话 */
    private String phone;

    /** 省id */
    private String provinceid;

    /** 市id */
    private String cityid;

    /** 区id */
    private String regionid;

    /** 具体位置 */
    private String info;

    /** 是否默认 */
    private String isdef;

    /** openid */
    private String openid;

    /** 微信名 */
    private String nickname;

    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除 */
    private String isdel;

}
