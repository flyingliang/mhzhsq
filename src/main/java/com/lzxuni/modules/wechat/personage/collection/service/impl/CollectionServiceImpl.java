package com.lzxuni.modules.wechat.personage.collection.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.wechat.personage.collection.entity.Collection;
import com.lzxuni.modules.wechat.personage.collection.mapper.CollectionMapper;
import com.lzxuni.modules.wechat.personage.collection.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollectionServiceImpl extends ServiceImpl<CollectionMapper, Collection> implements CollectionService {
    @Autowired
    private CollectionMapper collectionMapper;


    @Override
    public void SaveCollection(Collection demo) {
        collectionMapper.saveCollection(demo);

    }

    @Override
    public  List<Collection> selectListbyopenid(Collection demo) {
        return  collectionMapper.selectListbyopenid(demo);
    }

    @Override
    public  int deletebyopenid(Collection demo) {
        return  collectionMapper.deletebyopenid(demo);
    }




}
