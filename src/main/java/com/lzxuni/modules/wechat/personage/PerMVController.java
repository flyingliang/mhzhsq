package com.lzxuni.modules.wechat.personage;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.real.entity.RealCar;
import com.lzxuni.modules.pccontrol.basics.real.entity.RealName;
import com.lzxuni.modules.pccontrol.basics.real.service.RealCarService;
import com.lzxuni.modules.pccontrol.basics.real.service.RealNameService;
import com.lzxuni.modules.pccontrol.home.culture.production.entity.CultureProduction;
import com.lzxuni.modules.pccontrol.home.culture.production.service.CultureProductionService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 个人中心
 * Created by Lhl
 * 2019/8/02
 */
@Controller
@RequestMapping("/wechat/personage")
public class PerMVController extends WeChatController {
    @Autowired
    private RealNameService realNameService;

    @Autowired
    private RealCarService realCarService;
    @Autowired
    private CultureProductionService cultureProductionService;

    //个人中心设置
    @RequestMapping("/per_set_index")
    public ModelAndView per_set_index(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("/wechat/personage/set/per_set_index");
        RealName realName = realNameService.getOne(new QueryWrapper<RealName>().eq("openid", getOpenId(req)).orderByDesc("date").last("limit 1"));
        RealCar realCar= realCarService.getOne(new QueryWrapper<RealCar>().eq("openid", getOpenId(req)).orderByDesc("date").last("limit 1"));
        mv.addObject("realName", JSON.toJSONString(realName));
        mv.addObject("realCar", JSON.toJSONString(realCar));
        return mv;
    }


    @RequestMapping("/personage_SZ")
    public String personage_SZ() {
        return "/wechat/personage/personage_SZ";
    }

    @RequestMapping("/personage_SZ_nc")
    public String personage_SZ_nc() {
        return "/wechat/personage/personage_SZ_nc";
    }

    @RequestMapping("/personage_SZ_shdiz")
    public String personage_SZ_shdiz() {
        return "/wechat/personage/personage_SZ_shdiz";
    }

    @RequestMapping("/personage_SZ_smrz")
    public String personage_SZ_smrz() {
        return "/wechat/personage/personage_SZ_smrz";
    }




    @RequestMapping("/personage_GZSC")
    public String personage_GZSC() {
        return "/wechat/personage/personage_GZSC";
    }

    @RequestMapping("/personage_WJRZZ")
    public String personage_WJRZZ() {
       // return "/wechat/personage/personage_WJRZZ";
        return "/wechat/personage/personage_list";
    }

//    @RequestMapping("/personage_WXY")
//    public String personage_WXY() {
//        return "/wechat/personage/personage_WXY";
//    }

    @RequestMapping("/personage_WDWT")
    public String personage_WDWT() {
        return "/wechat/personage/personage_WDWT";
    }
//
//    @RequestMapping("/personage_SZ")
//    public String personage_SZ() {
//        return "/wechat/personage/personage_SZ";
//    }

    //个人中心-我的作品
    @ResponseBody
    @RequestMapping("/gerenzuopinlist")
    public Object GetList(HttpServletRequest req,String pagination) {
        CultureProduction cultureProduction =new CultureProduction();
        String ywType="production";
        cultureProduction.setIsdel("1");
        cultureProduction.setIsshow("1");
        cultureProduction.setOpenid((String)req.getSession().getAttribute("openid"));
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(cultureProductionService.queryPageAndImg(pageParameter, cultureProduction,ywType));
        return R.ok().put("data", pageData);
    }




}
