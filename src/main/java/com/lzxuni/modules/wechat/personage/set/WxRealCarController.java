package com.lzxuni.modules.wechat.personage.set;


import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.pccontrol.basics.real.entity.RealCar;
import com.lzxuni.modules.pccontrol.basics.real.service.RealCarService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 车辆认证 前端控制层
 * Created by 潘云明
 * 2019/8/02
 */
@RestController
@RequestMapping("/wechat/personage/set/realcar")
public class WxRealCarController extends WeChatController {
    @Autowired
    private RealCarService realCarService;
    /*
    车辆认证
     */
    @RequestMapping("/Index")
    public ModelAndView Index(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("/wechat/personage/set/realcar");
        String url = PortUrl + req.getRequestURI();
        mv.addObject("jsApiConfig", getJsAPiConfig(url));
        return mv;
    }

    @RequestMapping("/insert")
    public Object insert(HttpServletRequest req, RealCar entity, String uploadImgUrl1, String uploadImgUrl2, String uploadImgUrl3) {
        entity.setOpenid(getOpenId(req));
        entity.setNickname(getNickName(req));
        String ywId = UuidUtil.get32UUID();
        entity.setId(ywId);
        realCarService.save(entity);
        getTempFile(uploadImgUrl1, "车辆认证_身份证正面", "realCar1", ywId, "image");
        getTempFile(uploadImgUrl2, "车辆认证_身份证正面", "realCar2", ywId, "image");
        getTempFile(uploadImgUrl3, "车辆认证_手持驾驶证", "realCar3", ywId, "image");
        getTempFile(uploadImgUrl3, "车辆认证_手持行驶证", "realCar4", ywId, "image");

        return R.ok("提交成功");
    }

}



