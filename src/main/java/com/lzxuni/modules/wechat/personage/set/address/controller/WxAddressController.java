package com.lzxuni.modules.wechat.personage.set.address.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.address.service.CityService;
import com.lzxuni.modules.pccontrol.basics.address.service.ProvinceService;
import com.lzxuni.modules.pccontrol.basics.address.service.RegionService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import com.lzxuni.modules.wechat.personage.set.address.entity.Shaddress;
import com.lzxuni.modules.wechat.personage.set.address.service.ShaddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 基础管理_地址管理_收货地址 前端控制层
 * Created by 潘云明
 * 2019/8/02
 */
@RestController
@RequestMapping("/wechat/personage/set/address")
public class WxAddressController extends WeChatController {
    //省
    @Autowired
    private ProvinceService provinceService;

    //市
    @Autowired
    private CityService cityService;
    //区
    @Autowired
    private RegionService regionService;

    //收货地址
    @Autowired
    private ShaddressService shaddressService;

    /*
    收货地址列表
     */
    @RequestMapping("/Index")
    public ModelAndView Index() {
        ModelAndView mv = new ModelAndView("/wechat/personage/set/address/address_list");
        return mv;
    }

    @RequestMapping("/Info")
    public ModelAndView Form(String id) {

        ModelAndView mv = new ModelAndView("/wechat/personage/set/address/address_Info");
        mv.addObject("ProvinceList", JSON.toJSONString(provinceService.queryList()));
        mv.addObject("CityList", JSON.toJSONString(cityService.queryList()));
        mv.addObject("RegionList", JSON.toJSONString(regionService.queryList()));
        if (StringUtils.isNotEmpty(id)) {
            System.out.println("##" + JSON.toJSONString(provinceService.getById(id)));
            mv.addObject("id",id);
            mv.addObject("shaddress", JSON.toJSONString(shaddressService.getById(id)));
        }else{
            mv.addObject("shaddress", "''");
        }
        return mv;
    }

    //发布
    @RequestMapping("/Form")
    public Object insert(HttpServletRequest req, Shaddress entity) {
        String id = "";
        if (StringUtils.isNotEmpty(entity.getId())) {
            id = entity.getId();
        } else {
            id = UuidUtil.get32UUID();
        }
        if (StringUtils.isNotEmpty(entity.getIsdef()) && entity.getIsdef().equals("1")) {
            shaddressService.updateDef(id, getOpenId(req));
        } else {
            entity.setIsdef("0");
        }
        System.out.println("##" + entity.getIsdef());
        if (StringUtils.isEmpty(entity.getId())) {
            entity.setOpenid(getOpenId(req));
            entity.setNickname(getNickName(req));
            entity.setId(id);
            shaddressService.save(entity);
            return R.ok("添加成功");
        } else {
            shaddressService.updateById(entity);
            return R.ok("修改成功");
        }
    }
    @RequestMapping("/Del")
    public Object Del(HttpServletRequest req, String id) {
        shaddressService.removeById(id);

        return R.ok("删除成功");
    }

    //
    @RequestMapping("/List")
    public Object GetList(HttpServletRequest req, String pagination, String type) {

        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(shaddressService.selectListByCondition(pageParameter, getOpenId(req),null));
        return R.ok().put("data", pageData);
    }

}



