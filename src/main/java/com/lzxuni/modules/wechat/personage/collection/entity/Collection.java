package com.lzxuni.modules.wechat.personage.collection.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/14
 * 个人中心--收藏
 */

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_personage_collection")
public class Collection {

    private static final long serialVersionUID = 1L;
    @TableId
    /** uuid 主键 */
    private String id;

    /**微信id */
    private String openid;

    /** 商家id */
    private String shopid;

    /** 建立时间 */
    private Date date;
}
