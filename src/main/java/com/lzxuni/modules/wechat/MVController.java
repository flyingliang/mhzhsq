package com.lzxuni.modules.wechat;

import com.alibaba.fastjson.JSON;
import com.lzxuni.modules.pccontrol.forum.service.SignInService;
import com.lzxuni.modules.pccontrol.home.publicity.entity.Notice;
import com.lzxuni.modules.pccontrol.home.publicity.entity.Publicity;
import com.lzxuni.modules.pccontrol.home.publicity.service.NoticeService;
import com.lzxuni.modules.pccontrol.home.publicity.service.PublicityService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 微信端跳也
 * Created by 潘云明
 * 2019/7/16
 */
@Controller
@RequestMapping("/wechat")
public class MVController extends WeChatController {
    @Autowired
    private PublicityService tblHomePublicityService;
    @Autowired
    private NoticeService noticeService;
    @Autowired
    private SignInService signInService;
    //首页
    @RequestMapping("/index")
    public ModelAndView foot() {
        ModelAndView mv = new ModelAndView("/wechat/index");
        Publicity pc = new Publicity();
        List list = tblHomePublicityService.selectListfour(pc.setThistype("0"));
        Notice no=new Notice();
        List list2 = noticeService.selectListfour(no);
       // System.out.println("*********" + JSON.toJSONString(list));
        mv.addObject("data", JSON.toJSONString(list));
        mv.addObject("data2", JSON.toJSONString(list2));
        return mv;
    }

    //论坛
    @RequestMapping("/forum")
    public String forum() {
        return "/wechat/forum/index";
    }

    //个人中心
    @RequestMapping("/personage")
    public ModelAndView personage(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("/wechat/personage");
        mv.addObject("countNum", signInService.quertUseCountNum(getOpenId(req)));
        return mv;
    }

}
