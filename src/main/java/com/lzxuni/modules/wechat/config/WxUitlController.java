package com.lzxuni.modules.wechat.config;

import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.service.FileEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 主页_党建_心愿 前端控制器
 * Created by 潘云明
 * 2019/7/17
 */

@RestController
@RequestMapping("/wechat/util")
public class WxUitlController extends BaseController {


    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/queryImgUrl")
    public Object queryImgUrl(String keyValue, String ywType) throws Exception {
        System.out.println("查询图片列表###参数列表：keyValue：" + keyValue + ",ywType:" + ywType);
        if (StringUtils.isNotEmpty(keyValue)) {//返回图片数量
            FileEntity fileBeanCustom = new FileEntity();
            fileBeanCustom.setYwId(keyValue);
            fileBeanCustom.setYwType(ywType);
            return R.ok().put("data", fileEntityService.queryListByFileEntity(fileBeanCustom));
        }

        return null;
    }

}
