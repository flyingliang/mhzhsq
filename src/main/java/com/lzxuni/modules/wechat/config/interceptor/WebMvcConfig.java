package com.lzxuni.modules.wechat.config.interceptor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 注册拦截器
 * Created by 潘云明
 * 2019/7/26 14:26
 */

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Value("${lzxuni.wechat.URL}")
    public String URL;
    @Bean
    public WeChatInterceptor weChatInterceptor(){
        return new  WeChatInterceptor(URL);//有参构造方法进行属性赋值
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        //addPathPattern后跟拦截地址，excludePathPatterns后跟排除拦截地址
        registry.addInterceptor(weChatInterceptor()).addPathPatterns("/wechat/**").excludePathPatterns("/wechat/login").excludePathPatterns("/wechat/login_do").excludePathPatterns("/wechat/qrView");
    }
}
