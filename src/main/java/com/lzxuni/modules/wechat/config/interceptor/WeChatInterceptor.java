package com.lzxuni.modules.wechat.config.interceptor;

import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.wechat.config.entity.WechatUserInfo;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 拦截器
 * Created by 潘云明
 * 2019/7/26 14:20
 */

public class WeChatInterceptor implements HandlerInterceptor {

    private String URL;
    public WeChatInterceptor(String URL) {
        this.URL = URL;
    }




    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        HttpSession session = request.getSession();
        String openid = (String) session.getAttribute("openid");
        WechatUserInfo userInfo=(WechatUserInfo)session.getAttribute("userInfo");
        if (StringUtils.isNotEmpty(openid)&&userInfo!=null&&openid.equals(userInfo.getOpenid())) {
//            System.out.println("已经登录"+openid);
            return true;
        } else {
//            System.out.println("未登录" + openid);
            response.sendRedirect(URL+"/wechat/login");
            return false;
        }
    }

}
