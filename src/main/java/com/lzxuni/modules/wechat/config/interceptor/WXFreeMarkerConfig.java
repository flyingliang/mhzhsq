package com.lzxuni.modules.wechat.config.interceptor;


import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.annotation.PostConstruct;

@Slf4j
@Configuration
@Data
public class WXFreeMarkerConfig {
    @Autowired
    private freemarker.template.Configuration configuration;


    private InternalResourceViewResolver resourceViewResolver;

    @Value("${lzxuni.wechat.URL}")
    private String wxURl;

    @Value("${lzxuni.wechat.PortUrl}")
    private String PortUrl;


    // Spring 初始化的时候加载配置
    @PostConstruct
    public void setConfigure() throws Exception {

        // 加载html的资源路径
        configuration.setSharedVariable("wxURl", wxURl);
        configuration.setSharedVariable("wxPortURl", PortUrl);

    }
}
