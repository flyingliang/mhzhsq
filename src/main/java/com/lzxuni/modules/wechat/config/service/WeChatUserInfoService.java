package com.lzxuni.modules.wechat.config.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.wechat.config.entity.WechatUserInfo;

/**
 * 微信_用户 服务类
 * Created by 潘云明
 * 2019/7/5 16:08
 */
public interface WeChatUserInfoService extends IService<WechatUserInfo> {
    PageInfo<WechatUserInfo> queryPage(PageParameter pageParameter);
	//保存
    WechatUserInfo SaveOrUpdate(WechatUserInfo entity);

}
