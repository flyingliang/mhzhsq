package com.lzxuni.modules.wechat.config.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.party.map.entity.HomePartyMap;
import com.lzxuni.modules.wechat.config.entity.WechatUserInfo;

/**
 * 微信_用户 Mapper 接口
 * Created by 潘云明
 * 2019/7/8 15:01
 */
public interface WeChatUserInfoMapper extends BaseMapper<WechatUserInfo> {
}
