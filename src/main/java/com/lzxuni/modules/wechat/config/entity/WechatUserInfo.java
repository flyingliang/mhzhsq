package com.lzxuni.modules.wechat.config.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_wechat_userinfo")
public class WechatUserInfo {

    /** 版本号 */
    private static final long serialVersionUID = 1L;
    @TableId
    private String openid;

    /** 用户昵称 */
    private String nickname;

    /** 用户的性别 */
    private String sex;

    /** 省份 */
    private String province;

    /** 城市 */
    private String city;

    /** 国家 */
    private String country;

    /** 头像 */
    private String headimgurl;

    /** 是否禁用 */
    private String isboodbye;

}
