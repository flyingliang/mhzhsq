package com.lzxuni.modules.wechat.config.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.wechat.config.entity.WechatUserInfo;
import com.lzxuni.modules.wechat.config.mapper.WeChatUserInfoMapper;
import com.lzxuni.modules.wechat.config.service.WeChatUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 微信_用户 服务实现类
 * Created by 潘云明
 * 2019/7/8 14:58
 */
@Service
public class WeChatUserInfoServiceImpl extends ServiceImpl<WeChatUserInfoMapper, WechatUserInfo> implements WeChatUserInfoService {
    @Autowired
    private WeChatUserInfoMapper mapper;

    @Override
    public WechatUserInfo SaveOrUpdate(WechatUserInfo entity) {
        try {
            if (this.getById(entity.getOpenid()) == null) {
               this.save(entity);
            } else {
                this.updateById(entity);
            }
        } catch (Exception e) {
            entity.setNickname("无法识别");
            if (this.getById(entity.getOpenid()) == null) {
                this.save(entity);
            } else {
                this.updateById(entity);
            }
        }
        return entity;
    }


    public PageInfo<WechatUserInfo> queryPage(PageParameter pageParameter) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<WechatUserInfo> signInList = mapper.selectList(new QueryWrapper<WechatUserInfo>());
        PageInfo<WechatUserInfo> pageInfo = new PageInfo<>(signInList);
        return pageInfo;
    }
}
