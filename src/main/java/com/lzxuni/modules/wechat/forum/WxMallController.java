package com.lzxuni.modules.wechat.forum;


import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.forum.service.SignInService;
import com.lzxuni.modules.pccontrol.mall.entity.JfGoods;
import com.lzxuni.modules.pccontrol.mall.entity.MallOrder;
import com.lzxuni.modules.pccontrol.mall.service.JfGoodsService;
import com.lzxuni.modules.pccontrol.mall.service.MallOrderService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import com.lzxuni.modules.wechat.personage.set.address.service.ShaddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 论坛
 * Created by Lhl
 * 2019/8/02
 */
@RestController
@RequestMapping("/wechat/forum/mall")
public class WxMallController extends WeChatController {

    @Autowired
    private JfGoodsService goodsService;
    //积分
    @Autowired
    private SignInService signInService;

    //收货地址
    @Autowired
    private ShaddressService shaddressService;
    //订单
    @Autowired
    private MallOrderService mallOrderService;


    /*主跳页*/
    @RequestMapping("/index")
    public ModelAndView index() {
        ModelAndView mv = new ModelAndView("/wechat/forum/mall_index");
        return mv;
    }

    // 兑换说明
    @RequestMapping("/tips")
    public ModelAndView tips() {
        ModelAndView mv = new ModelAndView("/wechat/forum/mall_tips");
        return mv;
    }

    //兑换记录
    @RequestMapping("/record")
    public ModelAndView record() {
        ModelAndView mv = new ModelAndView("/wechat/forum/mall_record");
        return mv;
    }

    //兑换规则
    @RequestMapping("/rule")
    public ModelAndView rule() {
        ModelAndView mv = new ModelAndView("/wechat/forum/mall_rule");
        return mv;
    }

    //商品详情页
    @RequestMapping("/goodsinfo")
    public ModelAndView goodsinfo(HttpServletRequest req, String id) {
        ModelAndView mv = new ModelAndView("/wechat/forum/mall_goodsinfo");
        JfGoods entity = new JfGoods();
        String ywType = "goodspic";
        entity.setId(id);
        entity.setIsdel("1");
        entity.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        mv.addObject("goodsinfo", getPageData(goodsService.selectListByConditionAndImg(pageParameter, entity, ywType)));
        mv.addObject("countNum", signInService.quertUseCountNum(getOpenId(req)));
        return mv;
    }

    @RequestMapping("/GetGoodsList")
    public Object GetGoodList(HttpServletRequest req, String pagination, JfGoods entity) {
        String ywType = "goodspic";
        entity.setIsdel("1");
        entity.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(goodsService.selectListByConditionAndImg(pageParameter, entity, ywType));
        return R.ok().put("data", pageData);
    }

    //商品详情页
    @RequestMapping("/mall_order_add")
    public ModelAndView mall_order_add(HttpServletRequest req, String id) {
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"10000\",sidx:\"\",sord:\"\"}", PageParameter.class);
        ModelAndView mv = new ModelAndView("/wechat/forum/mall_order_add");
        //1.Json转换成实体类
        mv.addObject("goodsid", id);
        mv.addObject("shaddress", JSON.toJSONString(getPageData(shaddressService.selectListByCondition(pageParameter, getOpenId(req), null))));
        return mv;
    }

/*创建订单*/
    @RequestMapping("/insert")
    public Object insert(HttpServletRequest req, String goodsid, String shaddress, String content) {
//获取地址信息
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        PageInfo<Map<String, Object>> pageInfo = shaddressService.selectListByCondition(pageParameter, getOpenId(req), shaddress);
        Map<String, Object> map = pageInfo.getList().get(0);
        String address = map.get("name") + " " + map.get("phone") + " " + map.get("provincename") + map.get("cityname") + map.get("regionname") + map.get("info");
//可用积分
        int useNum = Integer.parseInt(signInService.quertUseCountNum(getOpenId(req)));
//商品积分
        int goodnum = Integer.parseInt(goodsService.getById(goodsid).getNum());
        if (useNum >= goodnum) {
            MallOrder entity = new MallOrder();
            entity.setAddress(address);
            entity.setStatus("1");
            entity.setGoodsid(goodsid);
            entity.setContent(content);
            entity.setOpenid(getOpenId(req));
            entity.setNickname(getNickName(req));
            entity.setId(UuidUtil.get32UUID());
            mallOrderService.cutNum(goodsid);
            mallOrderService.save(entity);
            return R.ok("提交成功");
        } else {
            return R.ok("积分不足");
        }
    }

    @RequestMapping("/GetRecordList")
    public Object GetRecordList(HttpServletRequest req, String pagination, MallOrder entity) {
        entity.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(mallOrderService.selectListByConditionAndImg(pageParameter, entity));
        return R.ok().put("data", pageData);
    }








}
