package com.lzxuni.modules.wechat.forum;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.pccontrol.forum.entity.SignIn;
import com.lzxuni.modules.pccontrol.forum.service.SignInService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 论坛
 * Created by 潘云明
 * 2019/8/02
 */
@RestController
@RequestMapping("/wechat/forum/signin")
public class WxSignInController extends WeChatController {

    @Autowired
    private SignInService signInService;
    @Value("${lzxuni.wechat.maxNum}")
    public int maxNum;

    @Value("${lzxuni.wechat.minNum}")
    public int minNum;

    /*
    签到跳页
     */
    @RequestMapping("/index")
    public ModelAndView cityFriend(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("/wechat/forum/signin_index");
        SignIn signIn = new SignIn();
        signIn.setOpenid(getOpenId(req));
        System.out.println("##" + signInService.queryDate(signIn));
        mv.addObject("dateArr", signInService.queryDate(signIn));
        mv.addObject("num", (int) (Math.random() * (maxNum - minNum) + minNum));
        return mv;
    }

    //签到添加
    @RequestMapping("/add")
    public Object posts_insert(HttpServletRequest req, Long num) {
        String ywId = UuidUtil.get32UUID();
        SignIn signIn = new SignIn();

        signIn.setId(ywId);
        signIn.setNum(num);
        signIn.setOpenid(getOpenId(req));
        signIn.setNickname(getNickName(req));
        signInService.save(signIn);
        return R.ok("发布成功");
    }


    /*
   签到跳页
    */
    @RequestMapping("/list")
    public ModelAndView list(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("/wechat/forum/signin_list");
        SignIn signIn = new SignIn();
        signIn.setOpenid(getOpenId(req));
//        System.out.println("##" + signInService.queryDate(signIn));
        mv.addObject("countInfo", signInService.queryCountNum(signIn));
        mv.addObject("countList", JSON.toJSONString(signInService.queryCountList()));
        return mv;
    }


}



