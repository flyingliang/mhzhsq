package com.lzxuni.modules.wechat.forum;


import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.forum.entity.Comment;
import com.lzxuni.modules.pccontrol.forum.entity.Good;
import com.lzxuni.modules.pccontrol.forum.entity.Posts;
import com.lzxuni.modules.pccontrol.forum.service.CommentService;
import com.lzxuni.modules.pccontrol.forum.service.GoodService;
import com.lzxuni.modules.pccontrol.forum.service.PostsService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 论坛
 * Created by 潘云明
 * 2019/8/02
 */
@RestController
@RequestMapping("/wechat/forum")
public class WxForumController extends WeChatController {
    @Autowired
    private CommentService service;

    @Autowired
    private GoodService goodService;

    @Autowired
    private PostsService postsService;

    /*
    同城交友跳页
     */
    @RequestMapping("/cityFriend")
    public ModelAndView cityFriend() {
        ModelAndView mv = new ModelAndView("/wechat/forum/cityFriend_list");
        return mv;
    }

    /*
    大事小情跳页
     */
    @RequestMapping("/bingAndSmall")
    public ModelAndView bingAndSmall() {
        ModelAndView mv = new ModelAndView("/wechat/forum/bigAndSmall_list");
        return mv;
    }

    /*
    新鲜随拍跳页
     */
    @RequestMapping("/takePhoto")
    public ModelAndView takePhoto() {
        ModelAndView mv = new ModelAndView("/wechat/forum/takePhoto_list");
        return mv;
    }

    /*
       求职招聘跳页
        */
    @RequestMapping("/findJob")
    public ModelAndView findJob() {
        ModelAndView mv = new ModelAndView("/wechat/forum/findJob_list");
        return mv;
    }

    /*
       物品买卖跳页
        */
    @RequestMapping("/goodBuy")
    public ModelAndView goodBuy() {
        ModelAndView mv = new ModelAndView("/wechat/forum/goodBuy_list");
        return mv;
    }

    /*
      房屋租售跳页
       */
    @RequestMapping("/houseBuy")
    public ModelAndView houseBuy() {
        ModelAndView mv = new ModelAndView("/wechat/forum/houseBuy_list");
        return mv;
    }

    /*
      居民求助跳页
       */
    @RequestMapping("/peopleHelp")
    public ModelAndView peopleHelp() {
        ModelAndView mv = new ModelAndView("/wechat/forum/peopleHelp_list");
        return mv;
    }


    /*
    发布新帖跳页
     */

    @RequestMapping("/add")
    public ModelAndView add(HttpServletRequest req, String type) {
        ModelAndView mv = new ModelAndView("/wechat/forum/forum_add");
        String url = PortUrl + req.getRequestURI() + "?" + req.getQueryString();
        mv.addObject("jsApiConfig", getJsAPiConfig(url));
        mv.addObject("type", type);

        return mv;
    }

    //发布新帖
    @RequestMapping("/insert")
    public Object insert(HttpServletRequest req, Comment entity, String uploadImgUrls) {
        entity.setOpenid(getOpenId(req));
        entity.setNickname(getNickName(req));
        String typeName = "发帖图片";
        String ywType = "addForumPic";
        String ywId = UuidUtil.get32UUID();
        entity.setId(ywId);
        service.save(entity);
        System.out.println("####论坛上传图片####" + uploadImgUrls);
        getTempFile(uploadImgUrls, typeName, ywType, ywId, "image");
        return R.ok("发帖成功");
    }

    //帖子列表
    @RequestMapping("/list")
    public Object GetList(HttpServletRequest req, String pagination, String type) {
        Comment entity = new Comment();
        String ywType = "addForumPic";
        entity.setIsdel("1");
        entity.setIsshow("1");
        entity.setType(type);

        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(service.selectListByConditionAndImg(pageParameter, entity, ywType, getOpenId(req)));
        return R.ok().put("data", pageData);
    }


    //查看帖子
    @RequestMapping("/fornum_info")
    public Object fornum_info(HttpServletRequest req, String id) {
        ModelAndView mv = new ModelAndView("/wechat/forum/forum_info");
//        ModelAndView mv = new ModelAndView("/wechat/shareNul");
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        Comment entity = new Comment();
        String ywType = "addForumPic";
        entity.setId(id);
        PageInfo<Map<String, Object>> pageInfo = service.selectListByConditionAndImg(pageParameter, entity, ywType, getOpenId(req));

        if (pageInfo.getList().size() == 0) {
            mv.setViewName("/wechat/shareNull");
            return mv;
        }
        String isshow = pageInfo.getList().get(0).get("isshow").toString();
        if (!isshow.equals("1")) {
            mv.setViewName("/wechat/shareNull");
            return mv;
        }
        String isdel = pageInfo.getList().get(0).get("isdel").toString();
        if (isdel.equals("0")) {
            mv.setViewName("/wechat/shareNull");
            return mv;
        }
        mv.addObject("comment", getPageData(pageInfo));
        mv.addObject("jsApiConfig", getJsAPiConfig(getAddress(req)));
        return mv;
    }

    //点赞
    @RequestMapping("/zan")
    public Object zan(HttpServletRequest req, String id, String zan) {
        if (StringUtils.isEmpty(id) || StringUtils.isEmpty(zan)) {
            return R.error();
        }
        Good good = new Good();
        good.setId(UuidUtil.get32UUID());
        good.setOpenid(getOpenId(req));
        good.setNickname(getNickName(req));
        good.setCommentid(id);
        if (zan.equals("1")) {
            //保存点赞
            goodService.save(good);
            service.addGoodNum(id);

        } else if (zan.equals("-1")) {
            //删除点赞
            goodService.removeByCondition(good);
            service.cutGoodNum(id);
        }
        //1.Json转换成实体类
        return R.ok();
    }


    //发布回复跳页
    @RequestMapping("/posts_add")
    public ModelAndView posts_add(String id) {
        ModelAndView mv = new ModelAndView("/wechat/forum/posts_add");
        mv.addObject("id", id);
        return mv;
    }

    //发布回复
    @RequestMapping("/posts_insert")
    public Object posts_insert(HttpServletRequest req, String id, String content) {
        String ywId = UuidUtil.get32UUID();
        Posts posts = new Posts();

        posts.setId(ywId);
        posts.setCommentid(id);
        posts.setPostscontent(content);
        posts.setOpenid(getOpenId(req));
        posts.setNickname(getNickName(req));
        postsService.save(posts);
        return R.ok("发布成功");
    }


    //评论列表
    @RequestMapping("/posts_list")
    public Object posts_list(String pagination, String id) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(postsService.selectListByCondition(pageParameter, id));
        return R.ok().put("data", pageData);
    }

    //发布回复跳页
    @RequestMapping("/my_forum")
    public ModelAndView my_forum(String id) {
        ModelAndView mv = new ModelAndView("/wechat/forum/my_forum");
        return mv;
    }

    //我的帖子列表
    @RequestMapping("/my_list")
    public Object my_list(HttpServletRequest req, String pagination, String type) {
        Comment entity = new Comment();
        String ywType = "addForumPic";
        entity.setIsdel("1");
        entity.setIsshow("1");
        entity.setType(type);
        entity.setOpenid(getOpenId(req));
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(service.selectListByConditionAndImg(pageParameter, entity, ywType, getOpenId(req)));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/delForum")
    public Object delForum(String id) {
        Comment entity = new Comment();
        entity.setIsdel("0");
        entity.setId(id);
        //1.Json转换成实体类
        service.updateById(entity);
        return R.ok();
    }


}



