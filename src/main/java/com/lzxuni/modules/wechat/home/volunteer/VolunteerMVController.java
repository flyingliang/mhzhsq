package com.lzxuni.modules.wechat.home.volunteer;

import com.lzxuni.modules.common.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 首页--志愿服务
 * Created by Lhl
 * 2019/8/02
 */
@Controller
@RequestMapping("/wechat/home")
public class VolunteerMVController extends BaseController {

    @RequestMapping("/volunteer")
    public String party() {
        return "/wechat/home/volunteer/volunteer";
    }


}
