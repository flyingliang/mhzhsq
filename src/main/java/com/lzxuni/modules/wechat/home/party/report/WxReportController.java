package com.lzxuni.modules.wechat.home.party.report;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.basics.community.entity.Community;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import com.lzxuni.modules.pccontrol.basics.village.entity.Village;
import com.lzxuni.modules.pccontrol.basics.village.service.VillageService;
import com.lzxuni.modules.pccontrol.home.life.company.entity.LifeCompany;
import com.lzxuni.modules.pccontrol.home.life.company.service.LifeCompanyService;
import com.lzxuni.modules.pccontrol.home.party.campaign.entity.PartyCampaign;
import com.lzxuni.modules.pccontrol.home.party.campaign.service.PartyCampaignService;
import com.lzxuni.modules.pccontrol.home.party.ceactivity.entity.PartyCeactivity;
import com.lzxuni.modules.pccontrol.home.party.ceactivity.service.PartyCeactivityService;
import com.lzxuni.modules.pccontrol.home.party.report.entity.PartyReport;
import com.lzxuni.modules.pccontrol.home.party.report.service.PartyReportService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 *  功能：</b>党建-->共驻共建模块 <br>
 *  作者：</b>任东方<br>
 *  日期：</b>2019年8月19日
 */

@RestController
@RequestMapping("/wechat/home")
public class WxReportController extends WeChatController {

    @Autowired
    private CommunityService communityService;
    @Autowired
    private VillageService villageService;
    @Autowired
    private LifeCompanyService lifeCompanyService;
    @Autowired
    private PartyReportService partyReportService;
    @Autowired
    private PartyCeactivityService partyCeactivityService;
    @Autowired
    private FileEntityService fileEntityService;
    @Autowired
    private PartyCampaignService partyCampaignService;

    @RequestMapping("/party")
    public ModelAndView party(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("/wechat/home/party/party");
        return mv;
    }

    @RequestMapping("/party/report/report_index")
    public ModelAndView report_index(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("/wechat/home/party/report/report_index");
            String openid = getOpenId(req);
            PartyReport report = partyReportService.getOne(new QueryWrapper<PartyReport>().eq("openid", openid));
            if(report != null){
                String isshow = report.getIsshow();
                mv.addObject("isshow", isshow);
            }else{
                mv.addObject("isshow", "0") ;
            }
        return mv;
    }

    //社区报到
    @RequestMapping("/party/report/report_Sqbd")
    public ModelAndView report_Sqbd(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("/wechat/home/party/report/report_Sqbd");
        String openid = getOpenId(req);
        mv.addObject("openid", openid);
        return mv;
    }

    //社区报到保存
    @RequestMapping("/party/report/saveForm")
    public Object saveForm(HttpServletRequest req, PartyReport partyReport) throws Exception {
        String uuid = UuidUtil.get32UUID();
        partyReport.setId(uuid);
        partyReport.setDate(new Date());
        partyReport.setIsshow("0");
        partyReport.setIsdel("1");
        partyReport.setOpenid(getOpenId(req));
        partyReportService.save(partyReport);
        return R.ok().put("data", "保存成功！");
    }

    //验证社区报到
    @RequestMapping("/party/report/ajax_shequ_baodao")
    public Object shequbaodao(String idnumber) {
        int msg = partyReportService.count(new QueryWrapper<PartyReport>().eq("idnumber", idnumber));
        return msg;
    }

    //参加活动
    @RequestMapping("/party/report/report_list")
    public ModelAndView report_list() {
        ModelAndView mv = new ModelAndView("/wechat/home/party/report/report_list");
        return mv;
    }
    //参加活动分页列表
    @RequestMapping("/party/report/report_list_fenye")
    public Object GetList(String pagination) {
        PartyCeactivity partyCeactivity =new PartyCeactivity();
        String ywType="partyCeactivityPic";
        partyCeactivity.setIsdel("1");
        partyCeactivity.setIsshow("1");
//        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(partyCeactivityService.queryPageAndImg(pageParameter,partyCeactivity,ywType));
        return R.ok().put("data", pageData);
    }
    //参加活动详细页
    @RequestMapping("/party/report/report_info")
    public ModelAndView report_info(String id) throws Exception {
        ModelAndView mv = new ModelAndView("/wechat/home/party/report/report_info");
        List<PartyCeactivity> partyCeaList = partyCeactivityService.queryListAll(id);
        if (partyCeaList != null && partyCeaList.size() > 0) {
                for (int i = 0; i < partyCeaList.size(); i++) {
                    mv.addObject("ceactivityid", partyCeaList.get(i).getId());
                    mv.addObject("status", partyCeaList.get(i).getStatus());
                }
        }
            mv.addObject("partylist", partyCeaList);
            return mv;
    }

    //参加活动-我要参加
    @RequestMapping("/party/report/report_join")
    public ModelAndView report_join(String id) {
        ModelAndView mv = new ModelAndView("/wechat/home/party/report/report_join");
        List<Village> villList = villageService.list(new QueryWrapper<Village>());
        mv.addObject("villList", villList);
        mv.addObject("ceactivityid", id);
        return mv;
    }

    //参加活动报名保存
    @RequestMapping("/party/report/report_join_saveForm")
    public Object report_join_saveForm(PartyCampaign partyCampaign) throws Exception {
        String uuid2 = UuidUtil.get32UUID();
        partyCampaign.setId(uuid2);
        partyCampaign.setIsshow("1");
        partyCampaign.setIsdel("1");
        partyCampaign.setDate(new Date());
        partyCampaignService.save(partyCampaign);
        return R.ok().put("data", "保存成功！");
    }

    //验证活动报名
    @RequestMapping("/party/report/ajax_huodong_baoming")
    public Object huodong_baoming(PartyCampaign partyCampaign) {
        String campaignname = partyCampaign.getCampaignname();
        String campaignphone = partyCampaign.getCampaignphone();
        String villageid = partyCampaign.getVillageid();
        int msg = partyCampaignService.count(new QueryWrapper<PartyCampaign>().eq("campaignname",campaignname).eq("campaignphone",campaignphone).eq("villageid",villageid));
        return msg;
    }

    //创建活动
    @RequestMapping("/party/report/report_Chj")
    public ModelAndView report_Chj(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("/wechat/home/party/report/report_Chj");
        List<Community> commList = communityService.list(new QueryWrapper<Community>());
        String url = PortUrl + req.getRequestURI() ;
        mv.addObject("jsApiConfig", getJsAPiConfig(url));
        mv.addObject("commList", commList);
        return mv;
    }
    //创建活动保存
    @RequestMapping("/party/report/report_saveForm")
    public Object saveForm(HttpServletRequest req,PartyCeactivity partyCeactivity, String uploadImgUrls) throws Exception {
        String uuid1 = UuidUtil.get32UUID();
        partyCeactivity.setId(uuid1);
        partyCeactivity.setDate(new Date());
        partyCeactivity.setOpenid(getOpenId(req));
        partyCeactivity.setIsdel("1");
        partyCeactivity.setIsshow("1");
        String typeName = "创建活动图片";
        String ywType = "partyCeactivityPic";
        String ywId = uuid1;
        partyCeactivityService.save(partyCeactivity);
        getTempFile(uploadImgUrls, typeName, ywType, ywId, "image");
        return R.ok().put("data", "保存成功！");
    }

    //社区联动
    @RequestMapping("/party/report/ajax_shequ")
    public Object shequ(String pcode) {
        List<Community> commList = communityService.list(new QueryWrapper<Community>());
        return R.ok().put("data",commList);
    }
    //小区联动
    @RequestMapping("/party/report/ajax_xiaoqu")
    public Object xiaoqu(String pcode) {
        List<Village> villageList = villageService.list(new QueryWrapper<Village>().eq("communityid",pcode));
        return R.ok().put("data",villageList);
    }
    //单位联动
    @RequestMapping("/party/report/ajax_danwei")
    public Object danwei(String pcode) {
        List<LifeCompany> lifeList = lifeCompanyService.list(new QueryWrapper<LifeCompany>().eq("villageid",pcode));
        return R.ok().put("data",lifeList);
    }

}
