package com.lzxuni.modules.wechat.home.life;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.shop.entity.Shop;
import com.lzxuni.modules.pccontrol.home.life.shop.service.ShopService;
import com.lzxuni.modules.pccontrol.home.life.shoptype.entity.Shoptype;
import com.lzxuni.modules.pccontrol.home.life.shoptype.service.ShoptypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.SQLException;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/12
 * 送货上门-
 */

@Controller
@RequestMapping("/wechat/home/life/shsm")
public class LifeShsmController {
    @Autowired
    private ShoptypeService tblHomeLifeShoptypeService;
    @Autowired
    private ShopService tblHomeLifeShopService;

    //跳转医疗卫生
    @RequestMapping("/affair_SHSM")
    public String affair_YLWS() {
        return "/wechat/home/life/shsm/affair_SHSM";
    }


    //商店list
    @ResponseBody
    @RequestMapping("/findshopList")
    public Object findshopList(String shoptype,String sname,String pagination) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);

        Shop shop=new Shop();
        if(StringUtils.isNotEmpty(shoptype)){
            shop.setShoptypeid(shoptype);
        }
         if(StringUtils.isNotEmpty(sname)){
            shop.setSname(sname);
        }
        shop.setYwtype("sj");
        shop.setContract("已签约");
        PageData pageData = getPageData( tblHomeLifeShopService.selectshopList(pageParameter,shop));
        return R.ok().put("data", pageData);
    }



    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
