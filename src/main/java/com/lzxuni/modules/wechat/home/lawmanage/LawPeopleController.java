package com.lzxuni.modules.wechat.home.lawmanage;

import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.pccontrol.home.lawmanage.communityper.entity.Communityper;
import com.lzxuni.modules.pccontrol.home.lawmanage.communityper.service.CommunityperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/19
 * 综治-各类人员列表
 */

@Controller
@RequestMapping("/wechat/home/lawmanage/people")
public class LawPeopleController {

    @Autowired
    private CommunityperService communityperService;


    //所有
    @ResponseBody
    @RequestMapping("/peopleList")
    public Object peopleList(HttpServletRequest req, String type,String communityid) {
        Communityper ct = new Communityper();
        ct.setYwtype("sj");
        ct.setNumberid(type);
        ct.setCommunityid(communityid);
        List list = communityperService.selectpeopleAll(ct);
        return R.ok().put("data", list);
    }

//所有
    @ResponseBody
    @RequestMapping("/peopleallList")
    public Object peopleallList(HttpServletRequest req, String type) {
        Communityper ct = new Communityper();
        ct.setYwtype("sj");
        ct.setNumberid(type);
        List list = communityperService.selectpeopleAll2(ct);
        return R.ok().put("data", list);
    }







    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
