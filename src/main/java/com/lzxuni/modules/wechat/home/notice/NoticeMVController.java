package com.lzxuni.modules.wechat.home.notice;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import com.lzxuni.modules.pccontrol.home.publicity.entity.Notice;
import com.lzxuni.modules.pccontrol.home.publicity.service.NoticeService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 首页--政务公开
 * Created by Lhl
 * 2019/8/02
 */
@Controller
@RequestMapping("/wechat/home/notice")
public class NoticeMVController extends WeChatController {
    @Autowired
    private NoticeService noticeService;
    @Autowired
    private CommunityService communityService;


    @RequestMapping("/tongzhigogngao")
    public String zhengWuFaBu() {
        return "/wechat/home/notice/tongzhigogngao";
    }

    @RequestMapping("/tongzhigogngao_xx")
    public ModelAndView zhengWuFaBu_xx(HttpServletRequest request,String id) {
        ModelAndView mv = new ModelAndView("/wechat/home/notice/tongzhigonggao_xx");
        Notice no=new Notice();
        no.setId(id);
        Notice notice=noticeService.getById(no);
        noticeService.pageviewPlusOne(notice);
        if(StringUtils.isNotEmpty(notice.getIsshow())&&!notice.getIsshow().equals("1")){
            mv.setViewName("/wechat/shareNull");
            return mv;
        }
        if(StringUtils.isNotEmpty(notice.getIsdel())&&notice.getIsdel().equals("0")){
            mv.setViewName("/wechat/shareNull");
            return mv;
        }
        mv.addObject("notice", notice);
        mv.addObject("jsApiConfig", getJsAPiConfig(getAddress(request)));


        return mv;
    }



    //首页四条
    @ResponseBody
    @RequestMapping("/selectListfour")
    public Object selectListfour(HttpServletRequest req, String communityid) {
        Notice no=new Notice();
        List list =noticeService.selectListfour(no);
        return R.ok().put("data", list);
    }


    //所有
    @ResponseBody
    @RequestMapping("/selectListall")
    public Object selectListall(HttpServletRequest req, String pagination) {
        Notice no=new Notice();
        //1.Json转换成实体类
        System.out.println(pagination);
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);

        PageData pageData = getPageData( noticeService.selectListAll(pageParameter,no));
        return R.ok().put("data", pageData);
    }







    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
