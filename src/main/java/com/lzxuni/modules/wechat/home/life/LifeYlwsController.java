package com.lzxuni.modules.wechat.home.life;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.medicine.entity.Medicine;
import com.lzxuni.modules.pccontrol.home.life.medicine.service.MedicineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/12
 * 医疗服务-
 */

@Controller
@RequestMapping("/wechat/home/life/ylws")
public class LifeYlwsController {

    @Autowired
    private MedicineService tblHomeLifeMedicineService;

    //跳转医疗卫生
    @RequestMapping("/affair_YLWS")
    public String affair_YLWS() {
        return "/wechat/home/life/ylws/affair_YLWS";
    }

    //跳转我要咨询
    @RequestMapping("/wyzx")
    public ModelAndView wyzx() {
        ModelAndView mv = new ModelAndView("/wechat/home/life/ylws/ylws_wyzx");

        return mv;
    }

    //我要咨询--存储
    @ResponseBody
    @RequestMapping("/saveWenda")
    public Object saveWenda(HttpServletRequest req, Medicine me) {
        // System.out.println(aff);
        me.setId(UuidUtil.get32UUID());
        me.setOpenid((String)req.getSession().getAttribute("openid"));
        me.setNickname((String)req.getSession().getAttribute("nickname"));
        String boole="提交成功";
        try {
            tblHomeLifeMedicineService.save(me);
        }catch (Exception e){
            boole="提交失败";
        }
        return R.ok().put("data", boole);
    }


    //我的问题
    @ResponseBody
    @RequestMapping("/myWenda")
    public Object myWenda(HttpServletRequest req, Medicine me,String pagination) {

        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        me.setOpenid((String)req.getSession().getAttribute("openid"));
        me.setYwtype("kefu");

        PageData pageData = getPageData( tblHomeLifeMedicineService.queryPage2(pageParameter,me));
        return R.ok().put("data", pageData);
    }

    //所有问题
    @ResponseBody
    @RequestMapping("/allWenda")
    public Object allWenda(HttpServletRequest req, Medicine me,String pagination) {

        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //aff.setOpenid((String)req.getSession().getAttribute("openid"));
        me.setYwtype("kefu");

        PageData pageData = getPageData( tblHomeLifeMedicineService.queryPage3(pageParameter,me));
        return R.ok().put("data", pageData);
    }








    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
