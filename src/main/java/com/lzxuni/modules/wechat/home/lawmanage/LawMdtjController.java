package com.lzxuni.modules.wechat.home.lawmanage;

import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.pccontrol.home.lawmanage.conflictmediation.entity.Conflictmediation;
import com.lzxuni.modules.pccontrol.home.lawmanage.conflictmediation.service.ConflictmediationService;
import com.lzxuni.modules.pccontrol.home.lawmanage.mediate.entity.Mediates;
import com.lzxuni.modules.pccontrol.home.lawmanage.mediate.service.MediatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/19
 * 矛盾调解-
 */

@Controller
@RequestMapping("/wechat/home/lawmanage/mdtj")
public class LawMdtjController {

    @Autowired
    private MediatesService mediateService;
    @Autowired
    private ConflictmediationService tblHomeLawmanageConflictmediationService;

    //跳转
    @RequestMapping("/lawmanage_MDTJ")
    public String lawmanage_MDTJ() {
        return "/wechat/home/lawmanage/mdtj/lawmanage_MDTJ";
    }

    //跳转
    @RequestMapping("/lawmanage_MDTJ_xx")
    public ModelAndView lawmanage_MDTJ_xx(String id) {
        ModelAndView mv = new ModelAndView("wechat/home/lawmanage/mdtj/lawmanage_MDTJ_xx");

        return mv;
    }

    //跳转
    @RequestMapping("/lawmanage_MDTJ_xx_FB")
    public ModelAndView lawmanage_MDTJ_xx_FB(String id) {
        ModelAndView mv = new ModelAndView("wechat/home/lawmanage/mdtj/lawmanage_MDTJ_xx_FB");

        return mv;
    }

    //跳转
    @RequestMapping("/lawmanage_MDTJ_al")
    public ModelAndView lawmanage_MDTJ_al(String id) {
        ModelAndView mv = new ModelAndView("wechat/home/lawmanage/mdtj/lawmanage_MDTJ_al");
        Mediates mediates = mediateService.queryBymcId(new Mediates().setId(id));
        mv.addObject("mediates",mediates);
        return mv;
    }

    //查找最新6条案例
    @ResponseBody
    @RequestMapping("/querylist6")
    public Object querylist6(HttpServletRequest req) {
        Mediates ct = new Mediates();
        List list = mediateService.querylist6(ct);
        return R.ok().put("data", list);
    }

    //申请矛盾调解
    @ResponseBody
    @RequestMapping("/saveForm")
    public Object saveForm(HttpServletRequest req, Conflictmediation conflictmediation) {
        String ywId = UuidUtil.get32UUID();
        conflictmediation.setId(ywId);
        tblHomeLawmanageConflictmediationService.save(conflictmediation);
        return R.ok("保存成功");
    }






    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
