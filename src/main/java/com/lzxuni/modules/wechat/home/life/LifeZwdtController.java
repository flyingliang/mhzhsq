package com.lzxuni.modules.wechat.home.life;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import com.lzxuni.modules.pccontrol.home.life.affair.entity.Affair;
import com.lzxuni.modules.pccontrol.home.life.affair.service.AffairService;
import com.lzxuni.modules.pccontrol.home.life.guidance.entity.GuidanceNew;
import com.lzxuni.modules.pccontrol.home.life.guidance.service.GuidanceNewService;
import com.lzxuni.modules.pccontrol.home.life.guidance.service.GuidanceService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/12
 * 政务大厅-
 */

@Controller
@RequestMapping("/wechat/home/life/zwdt")
public class LifeZwdtController extends WeChatController {
    @Autowired
    private GuidanceService tblHomeLifeGuidanceService;
     @Autowired
    private GuidanceNewService guidanceNewService;
    @Autowired
    private CommunityService communityService;
    @Autowired
    private AffairService homeLifeAffairService;

    //跳转政务大厅
    @RequestMapping("/affair_ZWDT")
    public String affair_ZWDT() {
        return "/wechat/home/life/zwdt/affair_ZWDT";
    }

    //办事指南list
    @ResponseBody
    @RequestMapping("/findGuideList")
    public Object findGuideList() {
        List guide = guidanceNewService.findGuideList();
        return R.ok().put("data", guide);
    }

    //办事指南详情
    @RequestMapping("/affair_ZWDT_bszn")
    public ModelAndView affair_ZWDT_bszn(HttpServletRequest req,String id) {
      //  ModelAndView mv = new ModelAndView("/wechat/home/life/zwdt/affair_ZWDT_bszn");
        ModelAndView mv = new ModelAndView("/wechat/home/life/zwdt/affair_ZWDT_bszn_new");
        GuidanceNew gui=new GuidanceNew();
        gui.setId(id);
        GuidanceNew gg=guidanceNewService.getById(gui);
        if(gg==null||(StringUtils.isNotEmpty(gg.getIsshow())&&!gg.getIsshow().equals("1"))){
            mv.setViewName("/wechat/shareNull");
            return mv;
        }
        if(StringUtils.isNotEmpty(gg.getIsdel())&&gg.getIsdel().equals("0")){
            mv.setViewName("/wechat/shareNull");
            return mv;
        }
        mv.addObject("guide", gg);
        mv.addObject("content", delHTMLTag(gg.getContent()));
        mv.addObject("jsApiConfig", getJsAPiConfig(getAddress(req)));
        return mv;
    }

    //跳转我要咨询
    @RequestMapping("/wyzx")
    public ModelAndView wyzx() {
        ModelAndView mv = new ModelAndView("/wechat/home/life/zwdt/zwdt_wyzx");

        return mv;
    }


    //社区list
    @ResponseBody
    @RequestMapping("/findShequList")
    public Object findShequList() {
        List shequ = communityService.queryLList();
        return R.ok().put("data", shequ);
    }
    //我要咨询--存储
    @ResponseBody
    @RequestMapping("/saveWenda")
    public Object saveWenda(HttpServletRequest req, Affair aff) {
       // System.out.println(aff);
        aff.setId(UuidUtil.get32UUID());
        aff.setOpenid((String)req.getSession().getAttribute("openid"));
        aff.setNickname((String)req.getSession().getAttribute("nickname"));
        String boole="提交成功";
        try {
            homeLifeAffairService.save(aff);
        }catch (Exception e){
            boole="提交失败";
        }
        return R.ok().put("data", boole);
    }

    //我的问题
    @ResponseBody
    @RequestMapping("/myWenda")
    public Object myWenda(HttpServletRequest req, Affair aff,String pagination) {

        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        aff.setOpenid((String)req.getSession().getAttribute("openid"));
        aff.setYwtype("kefu");

        PageData pageData = getPageData( homeLifeAffairService.queryPage2(pageParameter,aff));
        return R.ok().put("data", pageData);
    }

    //所有问题
    @ResponseBody
    @RequestMapping("/allWenda")
    public Object allWenda(HttpServletRequest req, Affair aff,String pagination) {

        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //aff.setOpenid((String)req.getSession().getAttribute("openid"));
        aff.setYwtype("kefu");

        PageData pageData = getPageData( homeLifeAffairService.queryPage3(pageParameter,aff));
        return R.ok().put("data", pageData);
    }















    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
