package com.lzxuni.modules.wechat.home.volunteer.activity;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import com.lzxuni.modules.pccontrol.home.volunteer.vractivity.entity.VolunteerActivity;
import com.lzxuni.modules.pccontrol.home.volunteer.vractivity.service.VolunteerActivityService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 主页_文化_文化作品 前端控制器
 * Created by fcd
 * 2019/7/16
 */

@RestController
@RequestMapping("/wechat/home/volunteer/activity")
public class WxActivityController extends WeChatController {
    @Autowired
    private VolunteerActivityService volunteerActivityService;



    @RequestMapping("/Index")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/wechat/home/volunteer/activity/activity_list");
        return mv;
    }

    @RequestMapping("/list")
    public Object GetList(String pagination) {
        VolunteerActivity volunteerActivity =new VolunteerActivity();
        String ywType="vractivity";
        volunteerActivity.setIsdel("1");
        volunteerActivity.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(volunteerActivityService.queryPageAndImg(pageParameter, volunteerActivity,ywType));
       return R.ok().put("data", pageData);
    }

    @RequestMapping("/activity_info")
    public Object style_info(String id) {
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        String ywType="vractivity";
        VolunteerActivity volunteerActivity =new VolunteerActivity();
        volunteerActivity.setId(id);
        ModelAndView mv = new ModelAndView("/wechat/home/volunteer/activity/activity_info");
        mv.addObject("volunteerActivity",getPageData(volunteerActivityService.queryPageAndImg(pageParameter,volunteerActivity,ywType)));
        return mv;
    }
}