package com.lzxuni.modules.wechat.home.life;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.shop.entity.Shop;
import com.lzxuni.modules.pccontrol.home.life.shop.service.ShopService;
import com.lzxuni.modules.pccontrol.home.life.shoptype.entity.Shoptype;
import com.lzxuni.modules.pccontrol.home.life.shoptype.service.ShoptypeService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/12
 * 全城商家-
 */

@Controller
@RequestMapping("/wechat/home/life/qcsj")
public class LifeQcsjController extends WeChatController {
    @Autowired
    private ShoptypeService tblHomeLifeShoptypeService;
    @Autowired
    private ShopService tblHomeLifeShopService;

    //跳转全城商家
    @RequestMapping("/affair_QCSJ")
    public ModelAndView affair_YLWS(HttpServletRequest request,String type,String shopName) throws UnsupportedEncodingException {
        ModelAndView mv =new ModelAndView("/wechat/home/life/qcsj/affair_QCSJ");
        mv.addObject("jsApiConfig", getJsAPiConfig(getAddress(request)));
            mv.addObject("type", type);
        if(StringUtils.isEmpty(shopName)){
            shopName="";
        }
            mv.addObject("shopName",URLDecoder.decode(shopName, "UTF-8"));
            System.out.println("==="+URLDecoder.decode(shopName, "UTF-8"));
        return mv;
    }


    //商店类型list
    @ResponseBody
    @RequestMapping("/findshoptypeList")
    public Object findshoptypeList() {
        Shoptype st = new Shoptype();
        st.setYwtype("sj");
        List<Shoptype> list = null;
        try {
            list = tblHomeLifeShoptypeService.queryShopTypeList(st);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return R.ok().put("data", list);
    }

    //商店list
    @ResponseBody
    @RequestMapping("/findshopList")
    public Object findshopList(String shoptype, String sname, String pagination) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);

        Shop shop = new Shop();
        if (StringUtils.isNotEmpty(shoptype)) {
            shop.setShoptypeid(shoptype);
        }
        if (StringUtils.isNotEmpty(sname)) {
            shop.setSname(sname);
        }
        shop.setYwtype("sj");

        PageData pageData = getPageData(tblHomeLifeShopService.selectshopList(pageParameter, shop));
        return R.ok().put("data", pageData);
    }

    //跳转商家详情
    @RequestMapping("/affair_QCSJ_XQ")
    public ModelAndView sqsjxq(HttpServletRequest request, String id) {
       // ModelAndView mv = new ModelAndView("/wechat/home/life/qcsj/QCSJ_XQ");
        ModelAndView mv = new ModelAndView("/wechat/home/life/qcsj/shangjiaxiangqing");
        Shop ss = new Shop();
        ss.setId(id);
        ss.setYwtype("sj");
        Shop shop = tblHomeLifeShopService.selectshopbyid(ss);

        if(shop==null||(StringUtils.isNotEmpty(shop.getIsdel())&&shop.getIsdel().equals("0"))){
            mv.setViewName("/wechat/shareNull");
            return mv;
        }
        mv.addObject("mapJson",JSON.toJSON(shop).toString());
        mv.addObject("shop", shop);
        mv.addObject("jsApiConfig", getJsAPiConfig(getAddress(request)));
        if(StringUtils.isNotEmpty(shop.getIsdel())&&shop.getIsdel().equals("0")){
            mv.setViewName("/wechat/shareNull");
        }
        return mv;
    }
    //跳转地图页
    @RequestMapping("/affair_QCSJ_XQ_MAP")
    public ModelAndView tiaozhuanmap(HttpServletRequest request, String id) {
        // ModelAndView mv = new ModelAndView("/wechat/home/life/qcsj/QCSJ_XQ");
        ModelAndView mv = new ModelAndView("/wechat/home/life/qcsj/map_shangjia");
        Shop ss = new Shop();
        ss.setId(id);
        ss.setYwtype("sj");
        Shop shop = tblHomeLifeShopService.selectshopbyid(ss);
        mv.addObject("shop", shop);
        mv.addObject("mapJson",JSON.toJSON(shop).toString());

        return mv;
    }

    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
