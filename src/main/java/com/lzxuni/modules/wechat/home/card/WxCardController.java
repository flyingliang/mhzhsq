package com.lzxuni.modules.wechat.home.card;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import com.lzxuni.modules.pccontrol.home.card.entity.Card;
import com.lzxuni.modules.pccontrol.home.card.entity.CardZXBZ;
import com.lzxuni.modules.pccontrol.home.card.service.CardService;
import com.lzxuni.modules.pccontrol.home.card.service.CardZXBZService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 首页--在现办理
 * Created by Lhl
 * 2019/8/02
 */
@Controller
@RequestMapping("/wechat/home/card")
public class WxCardController extends WeChatController {
    //社区
    @Autowired
    private CommunityService communityService;

    //办证基础
    @Autowired
    private CardService cardService;
    //在线办证
    @Autowired
    private CardZXBZService cardZXBZService;

    @RequestMapping("/index")
    public String index() {
        return "/wechat/home/card/card_index";
    }

    //独生子女
    @RequestMapping("/data/card_DSZN")
    public String card_DSZN() {
        return "/wechat/home/card/data/card_DSZN";
    }


    //在线办证
    @RequestMapping("/data/card_ZXBZ")
    public ModelAndView card_ZXBZ(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("/wechat/home/card/data/card_ZXBZ");
        String url = PortUrl + req.getRequestURI();
        mv.addObject("jsApiConfig", getJsAPiConfig(url));
        mv.addObject("community", JSON.toJSONString(communityService.queryLList()));
        return mv;
    }

    //在线办证添加
    @ResponseBody
    @RequestMapping("/data/card_ZXBZ_add")
//    public Object card_ZXBZ_add(HttpServletRequest req, String uploadImgUrls, String communityid, String type, String name, String phone, String idnum, String address, String cardinfo) {
    public Object card_ZXBZ_add(HttpServletRequest req, String uploadImgUrls, Card card, CardZXBZ cardZXBZ) {
        String typeName = "在线办证_在线开证明图片";
        String ywType = "card_ZXBZ_Pic";
        String ywId = UuidUtil.get32UUID();
        card.setId(ywId);
        cardZXBZ.setId(ywId);
        cardService.save(card);
        cardZXBZService.save(cardZXBZ);
        getTempFile(uploadImgUrls, typeName, ywType, ywId, "image");
        return R.ok("提交成功");
    }
    @ResponseBody
    @RequestMapping("/GetEntity")
    public Object  GetEntity(String name,String idnum){
        Card entity=new Card();
        entity.setIsdel("1");
        entity.setIdnum(idnum);
        entity.setName(name);
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"10000\",sidx:\"\",sord:\"\"}", PageParameter.class);
        System.out.println("###"+JSON.toJSONString(getPageData(cardService.queryPage(pageParameter,entity))));
        return R.ok().put("data",JSON.toJSONString(getPageData(cardService.queryPage(pageParameter,entity))));
    }

    @RequestMapping("/data/card_CX")
    public String zaiXianBanLi_CX() {
        return "/wechat/home/card/data/card_CX";
    }
//

//
//    @RequestMapping("/onlingProcessing/data/zaiXianBanLi_LNYD")
//    public String zaiXianBanLi_LNYD() {
//        return "/wechat/home/onlingProcessing/data/zaiXianBanLi_LNYD";
//    }
//
//    @RequestMapping("/onlingProcessing/data/zaiXianBanLi_SYDJ")
//    public String zaiXianBanLi_SYDJ() {
//        return "/wechat/home/onlingProcessing/data/zaiXianBanLi_SYDJ";
//    }
//
//    @RequestMapping("/onlingProcessing/data/zaiXianBanLi_SYDJ_2")
//    public String zaiXianBanLi_SYDJ_2() {
//        return "/wechat/home/onlingProcessing/data/zaiXianBanLi_SYDJ_2";
//    }
//
//    @RequestMapping("/onlingProcessing/data/zaiXianBanLi_SYDJ_3")
//    public String zaiXianBanLi_SYDJ_3() {
//        return "/wechat/home/onlingProcessing/data/zaiXianBanLi_SYDJ_3";
//    }
//
//    @RequestMapping("/onlingProcessing/data/zaiXianBanLi_SYDJ_4")
//    public String zaiXianBanLi_SYDJ_4() {
//        return "/wechat/home/onlingProcessing/data/zaiXianBanLi_SYDJ_4";
//    }

}
