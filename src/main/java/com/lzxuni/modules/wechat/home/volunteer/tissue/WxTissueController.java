package com.lzxuni.modules.wechat.home.volunteer.tissue;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.volunteer.tissue.entity.VolunteerTissue;
import com.lzxuni.modules.pccontrol.home.volunteer.tissue.service.VolunteerTissueService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 主页_文化_志愿组织 前端控制器
 * Created by fcd
 * 2019/7/16
 */

@RestController
@RequestMapping("/wechat/home/volunteer/tissue")
public class WxTissueController extends WeChatController {

    @Autowired
    private VolunteerTissueService volunteerTissueService;


    @RequestMapping("/Index")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/wechat/home/volunteer/tissue/tissue_list");
        return mv;
    }

    @RequestMapping("/list")
    public Object GetList(String pagination) {
        VolunteerTissue volunteerTissue =new VolunteerTissue();
        String ywType="tissue";
        volunteerTissue.setIsdel("1");
        volunteerTissue.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(volunteerTissueService.queryPageAndImg(pageParameter, volunteerTissue,ywType));
       return R.ok().put("data", pageData);
    }

    @RequestMapping("/tissue_info")
    public Object style_info(String id) {
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        String ywType="tissue";
        VolunteerTissue volunteerTissue =new VolunteerTissue();
        volunteerTissue.setId(id);
        ModelAndView mv = new ModelAndView("/wechat/home/volunteer/tissue/tissue_info");
        mv.addObject("volunteerTissue",getPageData(volunteerTissueService.queryPageAndImg(pageParameter,volunteerTissue,ywType)));
        return mv;
    }
}