package com.lzxuni.modules.wechat.home.volunteer.mission;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.common.utils.web.OkHttpUtil;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.volunteer.join.service.VolunteerJoinService;
import com.lzxuni.modules.pccontrol.home.volunteer.mien.entity.VolunteerMien;
import com.lzxuni.modules.pccontrol.home.volunteer.mien.service.VolunteerMienService;
import com.lzxuni.modules.pccontrol.home.volunteer.vractivity.entity.VolunteerActivity;
import com.lzxuni.modules.pccontrol.home.volunteer.vractivity.service.VolunteerActivityService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import com.lzxuni.modules.wechat.config.entity.WechatUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.UUID;

/**
 * 主页_文化_文化作品 前端控制器
 * Created by fcd
 * 2019/7/16
 */

@RestController
@RequestMapping("/wechat/home/volunteer/mission")
public class WxMissionController extends WeChatController {

    @Autowired
    private VolunteerActivityService volunteerActivityService;


    @Autowired
    private VolunteerMienService volunteerMienService;

    @RequestMapping("/Index")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/wechat/home/volunteer/mission/mission_list");
        return mv;
    }
    @RequestMapping("/list1")
    public Object GetList1(String pagination,String openid) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(volunteerActivityService.selectTasks(pageParameter,openid));
       return R.ok().put("data", pageData);
    }
    @RequestMapping("/list2")
    public Object GetList2(String pagination) {
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(volunteerMienService.queryList(pageParameter));
        return R.ok().put("data", pageData);
    }
    @RequestMapping("/list3")
    public Object GetList3(String pagination) {
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(volunteerMienService.queryListTwo(pageParameter));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/receive")
    public ModelAndView receive(String title,Integer award,String ayid,String openid) {
        ModelAndView mv = new ModelAndView("/wechat/home/volunteer/mission/mission_list");
        String ywId = UuidUtil.get32UUID();
        VolunteerMien volunteerMien=new VolunteerMien();
        volunteerMien.setOpenid(openid);
        volunteerMien.setId(ywId);
        volunteerMien.setTitle(title);
        volunteerMien.setAward(award);
        volunteerMien.setAyid(ayid);
        volunteerMien.setDate(new Date());
        volunteerMien.setIsdel("1");
        volunteerMien.setState("1");
        volunteerMienService.save(volunteerMien);

        //领取活动
        VolunteerActivity volunteerActivity=new VolunteerActivity();
        volunteerActivity.setId(ayid);
        volunteerActivity.setState("1");
        volunteerActivityService.updateById(volunteerActivity);
        return mv;
    }

    @RequestMapping("/mission_info")
    public Object style_info(String id) {
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        String ywType="vractivity";
        VolunteerActivity volunteerActivity =new VolunteerActivity();
        volunteerActivity.setId(id);
        ModelAndView mv = new ModelAndView("/wechat/home/volunteer/activity/activity_info");
        mv.addObject("volunteerActivity",getPageData(volunteerActivityService.queryPageAndImg(pageParameter,volunteerActivity,ywType)));
        return mv;
    }
//    title="+items.title+"&openid="+oid+"&award="+items.award+"
    @RequestMapping("/mission_infoto")
    public Object style_infoto(String id,String title,String openid,String award,String ayid) {
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        String ywType="vractivity";
        VolunteerActivity volunteerActivity =new VolunteerActivity();
        volunteerActivity.setId(id);
        ModelAndView mv = new ModelAndView("/wechat/home/volunteer/mission/activity_infoto");
        mv.addObject("volunteerActivity",getPageData(volunteerActivityService.queryPageAndImg(pageParameter,volunteerActivity,ywType)));
        String title1=title;
        String openid1 = openid;
        String award1 = award;
        String infotoid = ayid;
        System.out.println("************ "+title1+" ** "+openid1+" ** "+award1+" ** "+infotoid);
        mv.addObject("titles",title1);
        mv.addObject("openids",openid1);
        mv.addObject("awards1",award1);
        mv.addObject("infotoid",infotoid);
        return mv;
    }
}