package com.lzxuni.modules.wechat.home.culture;

import com.lzxuni.modules.common.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 首页--生活
 * Created by Lhl
 * 2019/8/02
 */
@Controller
@RequestMapping("/wechat/home")
 public class CultureMVController extends BaseController {

    @RequestMapping("/culture")
    public String party() {
        return "/wechat/home/culture/culture";
    }

    @RequestMapping("/culture/serve/serve_bszn")
    public String serve_bszn() {
        return "/wechat/home/culture/serve/serve_bszn";
    }

    @RequestMapping("/culture/serve/serve_SQDR")
    public String serve_SQDR() {
        return "/wechat/home/culture/serve/serve_SQDR";
    }


    @RequestMapping("/culture/serve/affair_QCSJ")
    public String affair_QCSJ() {
        return "/wechat/home/culture/serve/affair_QCSJ";
    }

    @RequestMapping("/culture/serve/serve_WHZP")
    public String serve_WHZP() {
        return "/wechat/home/culture/serve/serve_WHZP";
    }

    @RequestMapping("/culture/serve/serve_WHZP_FB")
    public String serve_WHZP_FB() {
        return "/wechat/home/culture/production/production_add";
    }

    @RequestMapping("/culture/serve/serve_WYTT")
    public String serve_WYTT() {
        return "/wechat/home/culture/serve/serve_WYTT";
    }

    @RequestMapping("/culture/serve/serve_WYTT_FB")
    public String serve_WYTT_FB() {
        return "/wechat/home/culture/serve/serve_WYTT_FB";
    }

    @RequestMapping("/culture/serve/serve_particular")
    public String serve_particular() {
        return "/wechat/home/culture/serve/serve_particular";
    }



}
