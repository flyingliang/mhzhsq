package com.lzxuni.modules.wechat.home.aged;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.basics.community.entity.Community;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import com.lzxuni.modules.pccontrol.home.aged.lookafter.entity.AgedLookafter;
import com.lzxuni.modules.pccontrol.home.aged.lookafter.service.AgedLookafterService;
import com.lzxuni.modules.pccontrol.home.aged.organization.entity.AgedOrganization;
import com.lzxuni.modules.pccontrol.home.aged.organization.service.AgedOrganizationService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *  功能：</b>养老模块 <br>
 *  作者：</b>任东方<br>
 *  日期：</b>2019年8月9日
 */
@RestController
@RequestMapping("/wechat/home")
public class WxAgedController extends WeChatController {

    @Autowired
    private FileEntityService fileEntityService;
    @Autowired
    private AgedOrganizationService agedOrganizationService;
    @Autowired
    private CommunityService communityService;
    @Autowired
    private AgedLookafterService agedLookafterService;

    @RequestMapping("/aged")
    public ModelAndView aged() {
        ModelAndView mv = new ModelAndView("/wechat/home/aged/aged");
        return mv;
    }

    @RequestMapping("/aged/care/care_list")
    public ModelAndView carelist() {
        ModelAndView mv = new ModelAndView("/wechat/home/aged/care/care_list");
//        List<AgedOrganization> organList = agedOrganizationService.list(new QueryWrapper<AgedOrganization>().orderByDesc("date").last("limit 4"));
//        mv.addObject("organList", organList);
        return mv;
    }

    //分页
    @RequestMapping("/aged/care/list")
    public Object GetList(String pagination) {
        AgedOrganization aged =new AgedOrganization();
        String ywType="organization";
        aged.setIsdel("1");
        aged.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(agedOrganizationService.queryPageAndImg(pageParameter,aged,ywType));
        return R.ok().put("data", pageData);
    }

    //详细页
    @RequestMapping("/aged/care_detail")
    public Object style_info(HttpServletRequest request, String id) throws Exception {
        ModelAndView mv = new ModelAndView("/wechat/home/aged/care/care_particular");
        List<AgedOrganization> agedList = agedOrganizationService.list(new QueryWrapper<AgedOrganization>().eq("id", id));
        if(agedList != null && agedList.size() >0){
            for (int i = 0; i < agedList.size(); i++) {
//                agedList.get(i).setContent(StringUtils.removeHtml(agedList.get(i).getContent()));
                FileEntity fileEntity = new FileEntity();
                Community community = new Community();
                String idd = agedList.get(i).getId();
                String communityid = agedList.get(i).getCommunityid();
                fileEntity.setYwId(idd);
                fileEntity.setYwType("organization");
                List<FileEntity> list1 = fileEntityService.queryListByFileEntity(fileEntity);
                List<Community> list = communityService.list(new QueryWrapper<Community>().eq("id", communityid));
                agedList.get(i).setFileEntities(list1);
                if (list !=null && list.size() >0){
                    agedList.get(i).setCommunityname(list.get(i).getCommunityname());
                }
            }
        }

        if(agedList==null||(StringUtils.isNotEmpty(agedList.get(0).getIsshow())&&!agedList.get(0).getIsshow().equals("1"))){
            mv.setViewName("/wechat/shareNull");
            return mv;
        }
        if(StringUtils.isNotEmpty(agedList.get(0).getIsdel())&&agedList.get(0).getIsdel().equals("0")){
            mv.setViewName("/wechat/shareNull");
            return mv;
        }
        mv.addObject("jsApiConfig", getJsAPiConfig(getAddress(request)));
        mv.addObject("agedList", agedList);
        return mv;
    }

    //领办待办
    @RequestMapping("/aged/care/care_daiBan")
    public ModelAndView care_daiBan() {
        ModelAndView mv = new ModelAndView("/wechat/home/aged/care/care_daiBan");
        return mv;
    }

    //日间照料
    @RequestMapping("/aged/care/care_riJianZhaoLiao")
    public ModelAndView care_riJianZhaoLiao() {
        ModelAndView mv = new ModelAndView("/wechat/home/aged/care/care_riJianZhaoLiao");
        return mv;
    }
    //日间照料分页
    @RequestMapping("/aged/care/care_riJianZhaoLiao_list")
    public R care_riJianZhaoLiao_list(String pagination) throws Exception {
        AgedLookafter agedLookafter = new AgedLookafter();
        String ywType="lookafter";
        agedLookafter.setIsdel("1");
        agedLookafter.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(agedLookafterService.queryPageAndImg(pageParameter,agedLookafter,ywType));
        return R.ok().put("data", pageData);
    }

    //详细页
    @RequestMapping("/aged/care/care_riJianZhaoLiao_xx")
    public ModelAndView care_riJianZhaoLiao_xx(String id) throws Exception {
        ModelAndView mv = new ModelAndView("/wechat/home/aged/care/care_riJianZhaoLiao_xx");
        List<AgedLookafter> agedLLookist = agedLookafterService.list(new QueryWrapper<AgedLookafter>().eq("id", id));
       if(agedLLookist != null && agedLLookist.size() >0){
            for(int i=0;i<agedLLookist.size();i++){
               agedLLookist.get(i).setContent((agedLLookist.get(i).getContent()));
               FileEntity fileEntity = new FileEntity();
               String id2 = agedLLookist.get(i).getId();
               fileEntity.setYwId(id2);
               fileEntity.setYwType("lookafter");
               List<FileEntity> list2 = fileEntityService.queryListByFileEntity(fileEntity);
               agedLLookist.get(i).setFileEntities(list2);
           }
       }
        mv.addObject("agedLLookist", agedLLookist);
        return mv;
    }

}
