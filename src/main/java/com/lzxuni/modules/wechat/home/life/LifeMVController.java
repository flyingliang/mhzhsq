package com.lzxuni.modules.wechat.home.life;

import com.lzxuni.modules.common.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 首页--生活
 * Created by Lhl
 * 2019/8/02
 */
@Controller
@RequestMapping("/wechat/home")
public class LifeMVController extends BaseController {

    @RequestMapping("/life")
    public String party() {
        return "/wechat/home/life/life";
    }




    @RequestMapping("/life/affair/affair_XXGQ")
    public String affair_XXGQ() {
        return "/wechat/home/life/affair/affair_XXGQ";
    }

}
