package com.lzxuni.modules.wechat.home.open;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.community.entity.Community;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import com.lzxuni.modules.pccontrol.home.publicity.entity.Publicity;
import com.lzxuni.modules.pccontrol.home.publicity.service.PublicityService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 首页--政务公开
 * Created by Lhl
 * 2019/8/02
 */
@Controller
@RequestMapping("/wechat/home/open")
public class OpenMVController extends WeChatController {
    @Autowired
    private PublicityService tblHomePublicityService;
    @Autowired
    private CommunityService communityService;


    @RequestMapping("/zhengWuFaBu")
    public String zhengWuFaBu() {
        return "/wechat/home/open/zhengWuFaBu";
    }

    @RequestMapping("/zhengWuFaBu_xx")
    public ModelAndView zhengWuFaBu_xx(HttpServletRequest request,String id) {
        ModelAndView mv = new ModelAndView("/wechat/home/open/zhengWuFaBu_xx");
        Publicity pc=new Publicity();
        pc.setId(id);
        Publicity publicity=tblHomePublicityService.getById(pc);
        tblHomePublicityService.pageviewPlusOne(publicity);

        if(StringUtils.isNotEmpty(publicity.getIsshow())&&!publicity.getIsshow().equals("1")){
            mv.setViewName("/wechat/shareNull");
        }
        if(StringUtils.isNotEmpty(publicity.getIsdel())&&publicity.getIsdel().equals("0")){
            mv.setViewName("/wechat/shareNull");
        }
        mv.addObject("publicity", publicity);
        mv.addObject("content", delHTMLTag(publicity.getContent()));

        Community community= communityService.getById(new Community().setId(publicity.getCommunityid()));
//        mv.addObject("communityname", community.getCommunityname());
        mv.addObject("jsApiConfig", getJsAPiConfig(getAddress(request)));


        return mv;
    }



    //首页四条
    @ResponseBody
    @RequestMapping("/selectListfour")
    public Object selectListfour(HttpServletRequest req, String communityid) {
        Publicity pc=new Publicity();
        pc.setThistype("0");
        List list =tblHomePublicityService.selectListfour(pc);
        return R.ok().put("data", list);
    }


    //所有
    @ResponseBody
    @RequestMapping("/selectListall")
    public Object selectListall(HttpServletRequest req, String pagination) {
        Publicity pc=new Publicity();
        pc.setThistype("0");
        //1.Json转换成实体类
        System.out.println(pagination);
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);

        PageData pageData = getPageData( tblHomePublicityService.selectListAll(pageParameter,pc));
        return R.ok().put("data", pageData);
    }







    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
