package com.lzxuni.modules.wechat.home.culture.intelligent;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.service.CultureIntelligentService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * 主页_文化_文化作品 前端控制器
 * Created by fcd
 * 2019/7/16
 */

@RestController
@RequestMapping("/wechat/home/culture/intelligent")
public class WxIntelligentController extends WeChatController {
    @Autowired
    private CultureIntelligentService cultureIntelligentService;

    @Autowired
    private FileEntityService fileEntityService;


    @RequestMapping("/Index")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/wechat/home/culture/intelligent/intelligent_list");
        return mv;
    }

    @RequestMapping("/list")
    public Object GetList(String pagination) {
        CultureIntelligent cultureIntelligent =new CultureIntelligent();
        String ywType="intelligent";
        cultureIntelligent.setIsdel("1");
        cultureIntelligent.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(cultureIntelligentService.queryPageAndImg(pageParameter, cultureIntelligent,ywType));
       return R.ok().put("data", pageData);
    }

    @RequestMapping("/intelligent_info")
    public Object style_info(String id) {
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        String ywType="intelligent";
        CultureIntelligent cultureIntelligent=new CultureIntelligent();
        cultureIntelligent.setId(id);
        ModelAndView mv = new ModelAndView("/wechat/home/culture/intelligent/intelligent_info");
        mv.addObject("cultureIntelligent",getPageData(cultureIntelligentService.queryPageAndImg(pageParameter,cultureIntelligent,ywType)));
        return mv;
    }

    @RequestMapping("/add")
    public ModelAndView add(HttpServletRequest request) {
        ModelAndView mv = new ModelAndView("/wechat/home/culture/intelligent/intelligent_add");
        String url = PortUrl + request.getRequestURI() ;
        mv.addObject("jsApiConfig", getJsAPiConfig(url));
        List<Tree> companyList = cultureIntelligentService.getTree();
        mv.addObject("companyList",companyList);
        return mv;
    }

    //发布信息
    @RequestMapping("/insert")
    public Object insert(HttpServletRequest request,CultureIntelligent cultureIntelligent, String uploadImgUrls) {
        cultureIntelligent.setOpenid(getOpenId(request));
        cultureIntelligent.setNickname(getNickName(request));
        String typeName = "达人图片";
        String ywType = "intelligent";
        String ywId = UuidUtil.get32UUID();
        cultureIntelligent.setId(ywId);
        cultureIntelligent.setIsdel("1");
        cultureIntelligent.setIsshow("1");
        cultureIntelligent.setDate(new Date());
        cultureIntelligentService.save(cultureIntelligent);
        getTempFile(uploadImgUrls, typeName, ywType, ywId, "image");
        return R.ok("添加成功");
    }
}