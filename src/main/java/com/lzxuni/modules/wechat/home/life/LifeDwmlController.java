package com.lzxuni.modules.wechat.home.life;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.company.entity.LifeCompany;
import com.lzxuni.modules.pccontrol.home.life.company.service.LifeCompanyService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/12
 * 单位名录-
 */

@Controller
@RequestMapping("/wechat/home/life/dwml")
public class LifeDwmlController extends WeChatController {
    @Autowired
    private LifeCompanyService tblHomeLifeCompanyService;

    //跳转医疗卫生
    @RequestMapping("/affair_DWML")
    public String affair_YLWS() {
        return "/wechat/home/life/dwml/affair_DWML";
    }

    //商店list
    @ResponseBody
    @RequestMapping("/findcompanList")
    public Object findcompanList(String compantype,String pagination) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);

        LifeCompany lc=new LifeCompany();
        if(StringUtils.isNotEmpty(compantype) && !compantype.equals("全部")){
            lc.setCompanytypeid(compantype);
        }

        lc.setYwtype("company");

        PageData pageData = getPageData( tblHomeLifeCompanyService.queryPage2(pageParameter,lc));
        return R.ok().put("data", pageData);
    }


    //跳转单位详情
    @RequestMapping("/affair_DWML_XQ")
    public ModelAndView sqsjxq(HttpServletRequest request, String id) {

        ModelAndView mv = new ModelAndView("/wechat/home/life/dwml/danweixiangqing");
        LifeCompany ss = new LifeCompany();
        ss.setId(id);
        ss.setYwtype("company");
        LifeCompany company = tblHomeLifeCompanyService.selectcompanybyid(ss);

        if(company==null||(StringUtils.isNotEmpty(company.getIsdel())&&company.getIsdel().equals("0"))){
            mv.setViewName("/wechat/shareNull");
            return mv;
        }
        mv.addObject("mapJson",JSON.toJSON(company).toString());
        mv.addObject("company", company);
        mv.addObject("jsApiConfig", getJsAPiConfig(getAddress(request)));
        if(StringUtils.isNotEmpty(company.getIsdel())&&company.getIsdel().equals("0")){
            mv.setViewName("/wechat/shareNull");
        }
        return mv;
    }

    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
