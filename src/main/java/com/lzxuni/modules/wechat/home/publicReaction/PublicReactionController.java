package com.lzxuni.modules.wechat.home.publicReaction;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.basics.village.entity.Village;
import com.lzxuni.modules.pccontrol.basics.village.service.VillageService;
import com.lzxuni.modules.pccontrol.home.publicreaction.entity.Publicreaction;
import com.lzxuni.modules.pccontrol.home.publicreaction.service.PublicreactionService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 功能：</b>社情反应<br>
 *作者：</b>任东方<br>
 *日期：</b>2019年8月23日
 */

@RestController
@RequestMapping("/wechat/home/publicReaction")
public class PublicReactionController extends WeChatController{

    @Autowired
    private VillageService villageService;
    @Autowired
    private PublicreactionService publicreactionService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/sheQingFanYing")
    public ModelAndView sheQingFanYing(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("/wechat/home/publicReaction/sheQingFanYing");
        List<Village> villageList = villageService.list(new QueryWrapper<Village>());
        String url = PortUrl + req.getRequestURI() ;
        mv.addObject("jsApiConfig", getJsAPiConfig(url));
        mv.addObject("villageList", villageList);
        return mv;
    }

    //社情反应保存
    @RequestMapping("/sheQingFanYing_saveForm")
    public Object saveForm(HttpServletRequest req, Publicreaction publicreaction, String uploadImgUrls) throws Exception {
        String uuid1 = UuidUtil.get32UUID();
        publicreaction.setId(uuid1);
        publicreaction.setDate(new Date());
        publicreaction.setOpenid(getOpenId(req));
        publicreaction.setNickname(getNickName(req));
        publicreaction.setIsshow("1");
        publicreaction.setIsdel("1");
        String typeName = "社情反映图片";
        String ywType = "publicreactionPic";
        String ywId = uuid1;
        publicreactionService.save(publicreaction);
        getTempFile(uploadImgUrls, typeName, ywType, ywId, "image");
        return R.ok("保存成功！");
    }

    //我的反应
    @RequestMapping("/sheQingFanYing_list")
    public ModelAndView sheQingFanYing_list(HttpServletRequest req,String flag) throws Exception {
        ModelAndView mv = new ModelAndView("/wechat/home/publicReaction/sheQingFanYing_list");
        String openid = getOpenId(req);
        List<Publicreaction> pubList=new ArrayList<>();
        if(flag!=null&&flag.equals("1")){
            pubList = publicreactionService.list(new QueryWrapper<Publicreaction>().eq("openid",openid));
        }else if(flag!=null&&flag.equals("2")){
            pubList = publicreactionService.list(new QueryWrapper<Publicreaction>());
        }
        if(pubList != null && pubList.size()>0){
            for(int i=0;i<pubList.size();i++){
                FileEntity fileEntity = new FileEntity();
                String ywid = pubList.get(i).getId();
                fileEntity.setYwId(ywid);
                fileEntity.setYwType("publicreactionPic");
                List<FileEntity> fileList = fileEntityService.queryListByFileEntity(fileEntity);
                if(fileList != null && fileList.size() >0){
                    pubList.get(i).setUrlPath(fileList.get(0).getUrlPath());
                }
            }
        }
        mv.addObject("pubList", pubList);
        return mv;
    }

    @RequestMapping("/sheQingFanYing_list_xx")
    public ModelAndView sheQingFanYing_list_xx(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("/wechat/home/publicReaction/sheQingFanYing_list_xx");
        return mv;
    }

}
