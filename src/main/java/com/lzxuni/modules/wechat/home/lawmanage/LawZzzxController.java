package com.lzxuni.modules.wechat.home.lawmanage;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.entity.Managementcenter;
import com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.service.ManagementcenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/19
 * 综治中心-
 */

@Controller
@RequestMapping("/wechat/home/lawmanage/zzzx")
public class LawZzzxController {

    @Autowired
    private ManagementcenterService tblHomeLawmanageManagementcenterService;

    @Autowired
    private CommunityService communityService;
    @Autowired
    private FileEntityService fileEntityService;

    //跳转综治中心
    @RequestMapping("/lawmanage_ZZZZ")
    public String lawmanage_ZZZZ() {
        return "/wechat/home/lawmanage/zzzx/lawmanage_ZZZZ";
    }

    //跳转
    @RequestMapping("/lawmanage_ZZZZ_xx")
    public ModelAndView lawmanage_ZZZZ_xx(String id) {
        ModelAndView mv = new ModelAndView("wechat/home/lawmanage/zzzx/lawmanage_ZZZZ_xx");
        Managementcenter mt=new Managementcenter();
        mt.setId(id);
        mt.setYwtype("sj");
        Managementcenter mc=tblHomeLawmanageManagementcenterService.queryBymcId(mt);
        System.out.println(mc.toString());
        mv.addObject("mc", mc);


        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(mc.getId());
        fileBeanCustom.setYwType("lc");
        try {
            mv.addObject("picNum", fileEntityService.queryListByFileEntity(fileBeanCustom).get(0).getUrlPath());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mv;
    }

    //所有
    @ResponseBody
    @RequestMapping("/allList")
    public Object allWenda(HttpServletRequest req, String pagination) {
        Managementcenter mt=new Managementcenter();
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //aff.setOpenid((String)req.getSession().getAttribute("openid"));
        mt.setYwtype("sj");

       List list =tblHomeLawmanageManagementcenterService.queryPage3(mt);
        return R.ok().put("data", list);
    }








    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
