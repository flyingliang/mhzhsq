package com.lzxuni.modules.wechat.home.lawmanage;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.community.entity.Community;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import com.lzxuni.modules.pccontrol.home.lawmanage.createpeace.entity.Createpeace;
import com.lzxuni.modules.pccontrol.home.lawmanage.createpeace.service.CreatepeaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/19
 * 平安创建-
 */

@Controller
@RequestMapping("/wechat/home/lawmanage/pacj")
public class LawPacjController {
    @Autowired
    private CreatepeaceService tblHomeLawmanageCreatepeaceService;

    @Autowired
    private CommunityService communityService;

    //跳转平安创建
    @RequestMapping("/lawmanage_PACJ")
    public String lawmanage_PACJ() {
        return "/wechat/home/lawmanage/pacj/lawmanage_PACJ";
    }

    //跳转List
    @RequestMapping("/lawmanage_PACJ_list")
    public String lawmanage_SQJZ_list() {
        return "/wechat/home/lawmanage/pacj/lawmanage_PACJ_list";
    }



    //跳转
    @RequestMapping("/lawmanage_PACJ_xx")
    public ModelAndView lawmanage_SQJZ_xx(String id) {
        ModelAndView mv = new ModelAndView("wechat/home/lawmanage/pacj/lawmanage_PACJ_xx");
        Createpeace cc=new Createpeace();
        cc.setId(id);
        //mt.setYwtype("sj");
        Createpeace cp=tblHomeLawmanageCreatepeaceService.getById(cc);
        Community community= communityService.getById(new Community().setId(cp.getCommunityid()));
        cp.setCommunityname(community.getCommunityname());
        mv.addObject("createpeace", cp);
        return mv;
    }

    //所有
    @ResponseBody
    @RequestMapping("/selectListthree")
    public Object selectListthree(HttpServletRequest req, String pagination) {
        Createpeace cc=new Createpeace();
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);

        cc.setYwtype("sj");

       List list =tblHomeLawmanageCreatepeaceService.selectListthree(cc);
        return R.ok().put("data", list);
    }

    //所有
    @ResponseBody
    @RequestMapping("/selectListall")
    public Object selectListall(HttpServletRequest req, String pagination) {
        Createpeace cc=new Createpeace();
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);

        cc.setYwtype("sj");
        PageData pageData = getPageData( tblHomeLawmanageCreatepeaceService.selectListall(pageParameter,cc));
        return R.ok().put("data", pageData);
    }






    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
