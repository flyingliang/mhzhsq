package com.lzxuni.modules.wechat.home.lawmanage;

import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.pccontrol.home.lawmanage.aidcase.entity.Aidcase;
import com.lzxuni.modules.pccontrol.home.lawmanage.aidcase.service.AidcaseService;
import com.lzxuni.modules.pccontrol.home.lawmanage.legalaid.entity.Legalaid;
import com.lzxuni.modules.pccontrol.home.lawmanage.legalaid.service.LegalaidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/19
 * 法律援助-
 */

@Controller
@RequestMapping("/wechat/home/lawmanage/flyz")
public class LawFlyzController {
    @Autowired
    private AidcaseService aidcaseService;
    @Autowired
    private LegalaidService tblHomeLawmanageLegalaidService;

    //跳转
    @RequestMapping("/lawmanage_FLYZ")
    public String lawmanage_FLYZ() {
        return "/wechat/home/lawmanage/flyz/lawmanage_FLYZ";
    }

    //跳转
    @RequestMapping("/lawmanage_FLYZ_xx")
    public ModelAndView lawmanage_FLYZ_xx(String id) {
        ModelAndView mv = new ModelAndView("wechat/home/lawmanage/flyz/lawmanage_FLYZ_xx");

        return mv;
    }

    //跳转
    @RequestMapping("/lawmanage_FLYZ_xx_FB")
    public ModelAndView lawmanage_FLYZ_xx_FB(String id) {
        ModelAndView mv = new ModelAndView("wechat/home/lawmanage/flyz/lawmanage_FLYZ_xx_FB");

        return mv;
    }

    //跳转
    @RequestMapping("/lawmanage_FLYZ_al")
    public ModelAndView lawmanage_FLYZ_al(String id) {
        ModelAndView mv = new ModelAndView("wechat/home/lawmanage/flyz/lawmanage_FLYZ_al");
        Aidcase aidcase = aidcaseService.queryBymcId(new Aidcase().setId(id));
        mv.addObject("aidcase",aidcase);

        return mv;
    }

    //查找最新6条案例
    @ResponseBody
    @RequestMapping("/querylist6")
    public Object querylist6(HttpServletRequest req) {
        Aidcase ct = new Aidcase();
        List list = aidcaseService.querylist6(ct);
        return R.ok().put("data", list);
    }

    //申请法律援助
    @ResponseBody
    @RequestMapping("/saveForm")
    public Object saveForm(HttpServletRequest req, Legalaid tblHomeLawmanageLegalaid) {
        String ywId = UuidUtil.get32UUID();
        tblHomeLawmanageLegalaid.setId(ywId);
        tblHomeLawmanageLegalaidService.save(tblHomeLawmanageLegalaid);
        return R.ok("保存成功");
    }


    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
