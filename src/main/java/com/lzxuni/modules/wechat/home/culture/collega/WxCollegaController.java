package com.lzxuni.modules.wechat.home.culture.collega;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.culture.college.entity.CultureCollege;
import com.lzxuni.modules.pccontrol.home.culture.college.service.CultureCollegeService;
import com.lzxuni.modules.pccontrol.home.party.style.entity.Style;
import com.lzxuni.modules.pccontrol.home.party.style.service.StyleService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 主页_文化_市民学院 前端控制器
 * Created by fcd
 * 2019/7/16
 */

@RestController
@RequestMapping("/wechat/home/culture/college")
public class WxCollegaController extends WeChatController {
    @Autowired
    private CultureCollegeService cultureCollegeService;

    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/wechat/home/culture/college/college_list");
        return mv;
    }

    @RequestMapping("/list")
    public Object GetList(String pagination) {
        CultureCollege cultureCollege =new CultureCollege();
        String ywType="college";
        cultureCollege.setIsdel("1");
        cultureCollege.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(cultureCollegeService.queryPageAndImg(pageParameter, cultureCollege,ywType));
       return R.ok().put("data", pageData);
    }

    @RequestMapping("/college_info")
    public Object style_info(String id) {
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        String ywType="college";
        CultureCollege cultureCollege=new CultureCollege();
        cultureCollege.setId(id);
        ModelAndView mv = new ModelAndView("/wechat/home/culture/college/college_info");
        mv.addObject("cultureCollege",getPageData(cultureCollegeService.queryPageAndImg(pageParameter,cultureCollege,ywType)));
        return mv;
    }


}