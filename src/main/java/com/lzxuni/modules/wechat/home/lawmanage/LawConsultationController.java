package com.lzxuni.modules.wechat.home.lawmanage;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.lawmanage.onlineconsultation.entity.Onlineconsultation;
import com.lzxuni.modules.pccontrol.home.lawmanage.onlineconsultation.service.OnlineconsultationService;
import com.lzxuni.modules.pccontrol.home.life.medicine.entity.Medicine;
import com.lzxuni.modules.pccontrol.home.life.medicine.service.MedicineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/19
 * 在线咨询-
 */

@Controller
@RequestMapping("/wechat/home/lawmanage/zxzx")
public class LawConsultationController {

    @Autowired
    private MedicineService tblHomeLifeMedicineService;
    @Autowired
    private OnlineconsultationService tblHomeLawmanageOnlineconsultationService;

    //跳转在线咨询
    @RequestMapping("/lawmanage_ZXWD")
    public String lawmanage_ZXWD() {
        return "/wechat/home/lawmanage/zxzx/lawmanage_ZXWD";
    }


    //跳转我要咨询
    @RequestMapping("/lawmanage_ZXWD_FB")
    public String lawmanage_ZXWD_FB() {
        return "/wechat/home/lawmanage/zxzx/lawmanage_ZXWD_FB";
    }


    //我要咨询--存储
    @ResponseBody
    @RequestMapping("/saveWenda")
    public Object saveWenda(HttpServletRequest req, Onlineconsultation ot) {
        // System.out.println(aff);
        ot.setId(UuidUtil.get32UUID());
        ot.setOpenid((String)req.getSession().getAttribute("openid"));
        ot.setNickname((String)req.getSession().getAttribute("nickname"));
        String boole="提交成功";
        try {
            tblHomeLawmanageOnlineconsultationService.save(ot);
        }catch (Exception e){
            boole="提交失败";
        }
        return R.ok().put("data", boole);
    }


    //我的问题
    @ResponseBody
    @RequestMapping("/myWenda")
    public Object myWenda(HttpServletRequest req, Onlineconsultation ot,String pagination) {

        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        ot.setOpenid((String)req.getSession().getAttribute("openid"));
        ot.setYwtype("kefu");

        PageData pageData = getPageData( tblHomeLawmanageOnlineconsultationService.queryPage2(pageParameter,ot));
        return R.ok().put("data", pageData);
    }

    //所有问题
    @ResponseBody
    @RequestMapping("/allWenda")
    public Object allWenda(HttpServletRequest req, Onlineconsultation ot,String pagination) {

        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //aff.setOpenid((String)req.getSession().getAttribute("openid"));
        ot.setYwtype("kefu");

        PageData pageData = getPageData( tblHomeLawmanageOnlineconsultationService.queryPage3(pageParameter,ot));
        return R.ok().put("data", pageData);
    }








    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
