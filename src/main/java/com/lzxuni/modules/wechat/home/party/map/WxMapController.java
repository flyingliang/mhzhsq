package com.lzxuni.modules.wechat.home.party.map;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.party.map.entity.HomePartyMap;
import com.lzxuni.modules.pccontrol.home.party.map.service.HomePartyMapService;
import com.lzxuni.modules.pccontrol.home.party.style.entity.Style;
import com.lzxuni.modules.pccontrol.home.party.style.service.StyleService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 主页_党建_作风建设 前端控制器
 * Created by 潘云明
 * 2019/7/16
 */

@RestController
@RequestMapping("/wechat/home/party/map")
public class WxMapController extends WeChatController {
    @Autowired
    private HomePartyMapService homePartyMapService;

    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("map_index")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/wechat/home/party/map/map_index");
        mv.addObject("mapJson",JSON.toJSON(homePartyMapService.queryList()).toString());
        return mv;
    }


}