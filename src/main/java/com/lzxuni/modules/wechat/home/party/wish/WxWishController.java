package com.lzxuni.modules.wechat.home.party.wish;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.basics.community.entity.Community;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import com.lzxuni.modules.pccontrol.home.party.wish.entity.Wish;
import com.lzxuni.modules.pccontrol.home.party.wish.service.WishService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 *  功能：</b>党建-->心愿管理模块 <br>
 *  作者：</b>任东方<br>
 *  日期：</b>2019年8月15日
 */

@RestController
@RequestMapping("/wechat/home/party/wish")
public class WxWishController extends WeChatController {

    @Autowired
    private WishService wishService;
    @Autowired
    private CommunityService communityService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/wish_index")
    public ModelAndView wish_index() {
        ModelAndView mv = new ModelAndView("/wechat/home/party/wish/wish_index");
        return mv;
    }

    //已实现愿望列表页
    @RequestMapping("/wish_list_true")
    public ModelAndView wish_list_true() {
        ModelAndView mv = new ModelAndView("/wechat/home/party/wish/wish_list_true");
        return mv;
    }

    //已实现愿望分页
    @RequestMapping("/wish_list_true_fenye")
    public Object GetList_true(String pagination) {
        Wish wish =new Wish();
        String ywType="wish";
        wish.setIsdel("1");
        wish.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(wishService.sxqueryPageAndImg(pageParameter,wish,ywType));
        return R.ok().put("data", pageData);
    }

    //已实现愿望详细页
    @RequestMapping("/wish_query")
    public ModelAndView wish_query(String id) throws Exception {
        ModelAndView mv = new ModelAndView("/wechat/home/party/wish/wish_query");
        List<Wish> wishListsx = wishService.list(new QueryWrapper<Wish>().eq("id", id));
        if(wishListsx != null && wishListsx.size() >0){
            for (int i=0;i<wishListsx.size();i++){
                FileEntity fileEntity = new FileEntity();
                String ywid = wishListsx.get(i).getId();
                fileEntity.setYwId(ywid);
                fileEntity.setYwType("wish");
                String communityid = wishListsx.get(i).getVillageid();
                List<FileEntity> fileList = fileEntityService.queryListByFileEntity(fileEntity);
                List<Community> commList = communityService.list(new QueryWrapper<Community>().eq("id",communityid));
                wishListsx.get(i).setFileEntities(fileList);
                if(commList !=null && commList.size() != 0){
                    wishListsx.get(i).setCommunityname(commList.get(i).getCommunityname());
                }
            }
        }
        mv.addObject("wishListsx", wishListsx);
        return mv;
    }
    //待实现愿望列表页
    @RequestMapping("/wish_list_flase")
    public ModelAndView wish_list_flase() {
        ModelAndView mv = new ModelAndView("/wechat/home/party/wish/wish_list_flase");
        return mv;
    }
    //待实现愿望分页
    @RequestMapping("/wish_list_flase_fenye")
    public Object GetList(String pagination) {
        Wish wish =new Wish();
        String ywType="wish";
        wish.setIsdel("1");
        wish.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(wishService.queryPageAndImg(pageParameter,wish,ywType));
        return R.ok().put("data", pageData);
    }

    //待实现愿望帮助页
    @RequestMapping("/wish_help")
    public ModelAndView wish_help(String id,HttpServletRequest req,String dsx) {
        ModelAndView mv = new ModelAndView("/wechat/home/party/wish/wish_help");
        List<Wish> wishList = wishService.list(new QueryWrapper<Wish>().eq("id", id));
        if(wishList != null && wishList.size() >0) {
            for (int i = 0; i < wishList.size(); i++) {
                String sqId = wishList.get(i).getVillageid();
                List<Community> commList = communityService.list(new QueryWrapper<Community>().eq("id", sqId));
                if (commList != null && commList.size() != 0) {
                    wishList.get(i).setCommunityname(commList.get(i).getCommunityname());
                }
                mv.addObject("wishId", wishList.get(i).getId());
            }
        }
        mv.addObject("dsx", dsx);
        mv.addObject("wishList", wishList);
        String url = PortUrl + req.getRequestURI() + "?" + req.getQueryString();
        mv.addObject("jsApiConfig", getJsAPiConfig(url));
        return mv;
    }

    @RequestMapping("/update")
    public Object update(HttpServletRequest req,Wish wish,String uploadImgUrls)throws Exception {
        wish.setIsrealize("1");
        wish.setSxopenid(getOpenId(req));
        wish.setSxnickname(getNickName(req));
        String typeName = "实现愿望图片";
        String ywType = "wish";
        wish.getId();
        String ywId = wish.getId();
        wishService.updateById(wish);
        getTempFile(uploadImgUrls, typeName, ywType, ywId, "image");
//        return R.ok().put("data", "保存成功！");
        return R.ok("保存成功");
    }

    //添加心愿页
    @RequestMapping("/wish_add")
    public ModelAndView wish_add() {
        ModelAndView mv = new ModelAndView("/wechat/home/party/wish/wish_add");
        List<Community> commList = communityService.list(new QueryWrapper<Community>());
        mv.addObject("commList", commList);
        return mv;
    }

    @RequestMapping("/saveForm")
    public Object saveForm(HttpServletRequest req,Wish wish) throws Exception {
        String uuid = UuidUtil.get32UUID();
        wish.setId(uuid);
        wish.setDate(new Date());
        wish.setIsrealize("0");
        wish.setOpenid(getOpenId(req));
        wish.setNickname(getNickName(req));
        wishService.save(wish);
        return R.ok().put("data", "保存成功！");
    }



}
