package com.lzxuni.modules.wechat.home.residentsAnswer;

import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.onlineanswers.entity.OnlineAnswers;
import com.lzxuni.modules.onlineanswers.entity.RandomNum;
import com.lzxuni.modules.onlineanswers.service.OnlineAnswersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 首页--居民答题
 * Created by Lhl
 * 2019/8/02
 */
@Controller
@RequestMapping("/wechat/home")
public class ResidentsAnswerMVController extends BaseController {
    @Autowired
    private OnlineAnswersService onlineAnswersService;

    @RequestMapping("/residentsAnswer/zaiXianDaTi")
    public String zaiXianDaTi() {
        return "/wechat/home/residentsAnswer/zaiXianDaTi";
    }

    @ResponseBody
    @RequestMapping("/GetQuestionList")
    public Object GetQuestionList() {
        OnlineAnswers oa=new OnlineAnswers ();
        oa.setTypeid("67e10adfe0ee4a309ead632cdbc0dd56");
        this.randomAnswers(oa,5);
        //PageData pageData = getPageData((PageInfo<OnlineAnswers>) this.randomAnswers(oa,5));
        return R.ok().put("data", this.randomAnswers(oa,5));
    }

    public List randomAnswers(OnlineAnswers oa, int number){
        List<OnlineAnswers> list=onlineAnswersService.findlistbytypeid(oa);
        int[] arr= RandomNum.Rnumber(list.size(),number);
        List<OnlineAnswers> newlist =new ArrayList<>();
        for(int i=0;i<number;i++){
            newlist.add(list.get(arr[i]));
        }
        return newlist;
    }
}
