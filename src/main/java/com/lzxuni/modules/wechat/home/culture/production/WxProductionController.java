package com.lzxuni.modules.wechat.home.culture.production;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.basics.village.entity.Village;
import com.lzxuni.modules.pccontrol.basics.village.service.VillageService;
import com.lzxuni.modules.pccontrol.home.culture.production.entity.CultureProduction;
import com.lzxuni.modules.pccontrol.home.culture.production.service.CultureProductionService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * 主页_文化_文化作品 前端控制器
 * Created by fcd
 * 2019/7/16
 */

@RestController
@RequestMapping("/wechat/home/culture/production")
public class WxProductionController extends WeChatController {
    @Autowired
    private CultureProductionService cultureProductionService;
    @Autowired
    private VillageService villageService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/wechat/home/culture/production/production_list");
        return mv;
    }

    @RequestMapping("/list")
    public Object GetList(String pagination) {
        CultureProduction cultureProduction =new CultureProduction();
        String ywType="production";
        cultureProduction.setIsdel("1");
        cultureProduction.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(cultureProductionService.queryPageAndImg(pageParameter, cultureProduction,ywType));
       return R.ok().put("data", pageData);
    }

    @RequestMapping("/production_info")
    public Object style_info(String id) {
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        String ywType="production";
        CultureProduction cultureProduction=new CultureProduction();
        cultureProduction.setId(id);
        ModelAndView mv = new ModelAndView("/wechat/home/culture/production/production_info");
        mv.addObject("cultureProduction",getPageData(cultureProductionService.queryPageAndImg(pageParameter,cultureProduction,ywType)));
        return mv;
    }
    @RequestMapping("/add")
    public ModelAndView add(HttpServletRequest request) {
        ModelAndView mv = new ModelAndView("/wechat/home/culture/production/production_add");
        String url = PortUrl + request.getRequestURI() ;
        mv.addObject("jsApiConfig", getJsAPiConfig(url));
         List<Village> Villagelist=villageService.selectVillage();
        mv.addObject("Villagelist",Villagelist);
        return mv;
    }

    //发布信息
    @RequestMapping("/insert")
    public Object insert(HttpServletRequest request, CultureProduction cultureProduction, String uploadImgUrls) {
        cultureProduction.setOpenid(getOpenId(request));
        cultureProduction.setNickname(getNickName(request));
        String typeName = "文化作品图片";
        String ywType = "production";
        String ywId = UuidUtil.get32UUID();
        cultureProduction.setId(ywId);
        cultureProduction.setIsdel("1");
        cultureProduction.setIsshow("1");
        cultureProduction.setDate(new Date());
        cultureProductionService.save(cultureProduction);
        getTempFile(uploadImgUrls, typeName, ywType, ywId, "image");
        return R.ok("发布成功");
    }

}