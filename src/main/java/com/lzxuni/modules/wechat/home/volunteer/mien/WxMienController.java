package com.lzxuni.modules.wechat.home.volunteer.mien;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.volunteer.join.entity.VolunteerJoin;
import com.lzxuni.modules.pccontrol.home.volunteer.join.service.VolunteerJoinService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 主页_文化_文化作品 前端控制器
 * Created by fcd
 * 2019/7/16
 */

@RestController
@RequestMapping("/wechat/home/volunteer/mien")
public class WxMienController extends WeChatController {

    @Autowired
    private VolunteerJoinService volunteerJoinService;


    @RequestMapping("/Index")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/wechat/home/volunteer/mien/mien_list");
        return mv;
    }

    @RequestMapping("/list")
    public Object GetList(String pagination) {
        VolunteerJoin volunteerJoin =new VolunteerJoin();
        String ywType="personage";
        volunteerJoin.setIsdel("1");
        volunteerJoin.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(volunteerJoinService.queryPageAndImg(pageParameter, volunteerJoin,ywType));
       return R.ok().put("data", pageData);
    }

    @RequestMapping("/mien_info")
    public Object style_info(String id) {
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        String ywType="personage";
        VolunteerJoin volunteerJoin =new VolunteerJoin();
        volunteerJoin.setId(id);
        ModelAndView mv = new ModelAndView("/wechat/home/volunteer/mien/mien_info");
        mv.addObject("volunteerActivity",getPageData(volunteerJoinService.queryPageAndImg(pageParameter,volunteerJoin,ywType)));
        return mv;
    }
}