package com.lzxuni.modules.wechat.home.volunteer.statistics;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.volunteer.join.service.VolunteerJoinService;
import com.lzxuni.modules.pccontrol.home.volunteer.vractivity.entity.VolunteerActivity;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 主页_文化_文化作品 前端控制器
 * Created by fcd
 * 2019/7/16
 */

@RestController
@RequestMapping("/wechat/home/volunteer/statistics")
public class WxStatisticsController extends WeChatController {

    @Autowired
    private FileEntityService fileEntityService;
    @Autowired
    private VolunteerJoinService tblHomeVolunteerJoinService;



    @RequestMapping("/Index")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/wechat/home/volunteer/statistics/statistics_list");
        return mv;
    }

    @RequestMapping("/list")
    public Object GetList(String pagination) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(tblHomeVolunteerJoinService.queryList(pageParameter));
       return R.ok().put("data", pageData);
    }
}