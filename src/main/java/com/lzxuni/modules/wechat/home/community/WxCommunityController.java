package com.lzxuni.modules.wechat.home.community;

import com.alibaba.fastjson.JSON;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 首页--我的社区
 * Created by Lhl
 * 2019/8/02
 * */
@RestController
@RequestMapping("/wechat/home/community")
public class WxCommunityController extends BaseController {
    @Autowired
    private CommunityService communityService;
    @RequestMapping("/community")
    public ModelAndView index() {
        ModelAndView mv =new ModelAndView("/wechat/home/community/community_index");
        mv.addObject("community", JSON.toJSONString(communityService.queryLList()));
        return mv;
    }

    @RequestMapping("/info")
    public ModelAndView info(String id) {
        ModelAndView mv =new ModelAndView("/wechat/home/community/community_info");
        mv.addObject("community", communityService.queryOne(id).get(0));
        return mv;
    }

}
