package com.lzxuni.modules.wechat.home.party.study;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.onlineanswers.entity.OnlineAnswers;
import com.lzxuni.modules.onlineanswers.entity.RandomNum;
import com.lzxuni.modules.onlineanswers.service.OnlineAnswersService;
import com.lzxuni.modules.pccontrol.basics.community.entity.Community;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import com.lzxuni.modules.pccontrol.home.party.study.entity.PartyStudy;
import com.lzxuni.modules.pccontrol.home.party.study.service.PartyStudyService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 *  功能：</b>党建-->在线学习 <br>
 *  作者：</b>任东方<br>
 *  日期：</b>2019年8月22日
 */

@RestController
@RequestMapping("/wechat/home/party/stu")
public class WxStuController extends WeChatController {

    @Autowired
    private PartyStudyService partyStudyService;
    @Autowired
    private CommunityService communityService;
    @Autowired
    private FileEntityService fileEntityService;
    @Autowired
    private OnlineAnswersService onlineAnswersService;


    @RequestMapping("/stu_index")
    public ModelAndView stu_index(){
        ModelAndView mv = new ModelAndView("/wechat/home/party/stu/stu_index");
        return mv;
    }

    //党章党规列表页
    @RequestMapping("/rules/rules_list")
    public ModelAndView rules_list(){
        ModelAndView mv = new ModelAndView("/wechat/home/party/stu/rules/rules_list");
        return mv;
    }

    //党章党规分页
    @RequestMapping("/rules/rules_list_fenye")
    public Object GetList(String pagination) {
        PartyStudy partyStudy = new PartyStudy();
        String ywType="partyStudyDgdz";
        partyStudy.setIsdel("1");
        partyStudy.setIsshow("1");
//        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(partyStudyService.queryPageAndImg(pageParameter,partyStudy,ywType));
        return R.ok().put("data", pageData);
    }
    //党章党规详细页
    @RequestMapping("/rules/rules_info")
    public ModelAndView rules_info(String id){
        ModelAndView mv = new ModelAndView("/wechat/home/party/stu/rules/rules_info");
        List<PartyStudy> partyStuList = partyStudyService.list(new QueryWrapper<PartyStudy>().eq("id", id));
        if(partyStuList != null && partyStuList.size()>0){
            for(int i=0;i<partyStuList.size();i++){
                String communityid = partyStuList.get(i).getCommunityid();
                List<Community> commList = communityService.list(new QueryWrapper<Community>().eq("id", communityid));
                if(commList != null && commList.size()>0){
                    partyStuList.get(i).setCommunityname(commList.get(i).getCommunityname());
                }
            }
        }
        mv.addObject("partyStuList", partyStuList);
        return mv;
    }
    //时政词条列表页
    @RequestMapping("/entry/entry_list")
    public ModelAndView entry_list(){
        ModelAndView mv = new ModelAndView("/wechat/home/party/stu/entry/entry_list");
        return mv;
    }
    //时政词条分页
    @RequestMapping("/entry/entry_list_fenye")
    public Object rules_list_fenye(String pagination) {
        PartyStudy partyStudy = new PartyStudy();
        String ywType="partyStudySzct";
        partyStudy.setIsdel("1");
        partyStudy.setIsshow("1");
//        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(partyStudyService.queryPageSzxt(pageParameter,partyStudy,ywType));
        return R.ok().put("data", pageData);
    }
    //时政词条详细页
    @RequestMapping("/entry/entry_info")
    public ModelAndView entry_info(String id){
        ModelAndView mv = new ModelAndView("/wechat/home/party/stu/entry/entry_info");
        List<PartyStudy> szpartyStuList = partyStudyService.list(new QueryWrapper<PartyStudy>().eq("id", id));
        if(szpartyStuList != null && szpartyStuList.size()>0){
            for(int i=0;i<szpartyStuList.size();i++){
                String communityid = szpartyStuList.get(i).getCommunityid();
                List<Community> commList = communityService.list(new QueryWrapper<Community>().eq("id", communityid));
                if(commList != null && commList.size()>0){
                    szpartyStuList.get(i).setCommunityname(commList.get(i).getCommunityname());
                }
            }
        }
        mv.addObject("szpartyStuList", szpartyStuList);
        return mv;
    }
    //头条新闻列表页
    @RequestMapping("/news/news_list")
    public ModelAndView news_list(){
        ModelAndView mv = new ModelAndView("/wechat/home/party/stu/news/news_list");
        return mv;
    }

    //头条新闻分页
    @RequestMapping("/news/news_list_fenye")
    public Object news_list_fenye(String pagination) {
        PartyStudy partyStudy = new PartyStudy();
        String ywType="partyStudyTtxw";
        partyStudy.setIsdel("1");
        partyStudy.setIsshow("1");
//        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(partyStudyService.queryPageTtxw(pageParameter,partyStudy,ywType));
        return R.ok().put("data", pageData);
    }

    //头条新闻详细页
    @RequestMapping("/news/news_info")
    public ModelAndView news_info(String id) throws Exception {
        ModelAndView mv = new ModelAndView("/wechat/home/party/stu/news/news_info");
        List<PartyStudy> ttxwpartyStuList = partyStudyService.list(new QueryWrapper<PartyStudy>().eq("id", id));
        if(ttxwpartyStuList != null && ttxwpartyStuList.size()>0){
            for(int i=0;i<ttxwpartyStuList.size();i++){
                FileEntity fileEntity = new FileEntity();
                String ywid = ttxwpartyStuList.get(i).getId();
                fileEntity.setYwId(ywid);
                fileEntity.setYwType("partyStudyTtxw");
                List<FileEntity> fileList = fileEntityService.queryListByFileEntity(fileEntity);
                ttxwpartyStuList.get(i).setFileEntities(fileList);
                String communityid = ttxwpartyStuList.get(i).getCommunityid();
                List<Community> commList = communityService.list(new QueryWrapper<Community>().eq("id", communityid));
                if(commList != null && commList.size()>0){
                    ttxwpartyStuList.get(i).setCommunityname(commList.get(i).getCommunityname());
                }
            }
        }
        mv.addObject("szpartyStuList", ttxwpartyStuList);
        return mv;
    }
    @RequestMapping("/stutest")
    public ModelAndView stutest(){
        ModelAndView mv = new ModelAndView("/wechat/home/party/stu/stutest/stutest");
        return mv;
    }
    @ResponseBody
    @RequestMapping("/GetQuestionList")
    public Object GetQuestionList() {
        OnlineAnswers oa=new OnlineAnswers ();
        oa.setTypeid("c1284919506f4868a3d4cef9f5be7767");
        this.randomAnswers(oa,5);
        //PageData pageData = getPageData((PageInfo<OnlineAnswers>) this.randomAnswers(oa,5));
        return R.ok().put("data", this.randomAnswers(oa,5));
    }
    public List randomAnswers(OnlineAnswers oa, int number){
        List<OnlineAnswers> list=onlineAnswersService.findlistbytypeid(oa);
        int[] arr= RandomNum.Rnumber(list.size(),number);
        List<OnlineAnswers> newlist =new ArrayList<>();
        for(int i=0;i<number;i++){
            newlist.add(list.get(arr[i]));
        }
        return newlist;
    }




}
