package com.lzxuni.modules.wechat.home.lawmanage;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.lawmanage.comtraining.entity.Comtraining;
import com.lzxuni.modules.pccontrol.home.lawmanage.comtraining.service.ComtrainingService;
import com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.service.ManagementcenterService;
import com.lzxuni.modules.pccontrol.home.lawmanage.policeroom.entity.Policeroom;
import com.lzxuni.modules.pccontrol.home.lawmanage.policeroom.service.PoliceroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/19
 * 综治中心-
 */

@Controller
@RequestMapping("/wechat/home/lawmanage/sqjws")
public class LawSqjwsController {

    @Autowired
    private ManagementcenterService tblHomeLawmanageManagementcenterService;
    @Autowired
    private PoliceroomService tblHomeLawmanagePoliceroomService;
    @Autowired
    private ComtrainingService comtrainingService;
    //跳转综治中心
    @RequestMapping("/lawmanage_JWS")
    public String lawmanage_JWS() {
        return "/wechat/home/lawmanage/sqjws/lawmanage_JWS";
    }

    //跳转
    @RequestMapping("/lawmanage_JWS_xx")
    public ModelAndView lawmanage_JWS_xx(String id) {
        ModelAndView mv = new ModelAndView("wechat/home/lawmanage/sqjws/lawmanage_JWS_xx");

        Policeroom mt=new Policeroom();
        mt.setId(id);
        mt.setYwtype("sj");
        Policeroom pp=tblHomeLawmanagePoliceroomService.queryBymcId(mt);
        System.out.println(pp.toString());
        mv.addObject("pp", pp);

        return mv;
    }

   //跳转警讯
    @RequestMapping("/lawmanage_jws_jx_xx")
    public ModelAndView lawmanage_jws_jx_xx(String id) {
        ModelAndView mv = new ModelAndView("wechat/home/lawmanage/sqjws/lawmanage_jws_jx_xx");
        Comtraining cc = new  Comtraining ();
        cc.setId(id);
        Comtraining comtraining = comtrainingService.queryBymcId(cc );
        mv.addObject("comtraining", comtraining);
        return mv;
    }

    //所有
    @ResponseBody
    @RequestMapping("/allList")
    public Object allWenda(HttpServletRequest req, String pagination) {
        Policeroom mt=new Policeroom();
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //aff.setOpenid((String)req.getSession().getAttribute("openid"));
        mt.setYwtype("sj");

       List list =tblHomeLawmanagePoliceroomService.queryPage3(mt);
        return R.ok().put("data", list);
    }

    //所有
    @ResponseBody
    @RequestMapping("/selectListthree")
    public Object selectListthree(HttpServletRequest req, String communityid) {
        Comtraining mt=new Comtraining();
        mt.setCommunityid(communityid);

        List list =comtrainingService.selectListthree(mt);
        return R.ok().put("data", list);
    }






    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
