package com.lzxuni.modules.wechat.home.culture.activity;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.culture.activity.entity.CultureActivity;
import com.lzxuni.modules.pccontrol.home.culture.activity.service.CultureActivityService;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.service.CultureIntelligentService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * 主页_文化_文化作品 前端控制器
 * Created by fcd
 * 2019/7/16
 */

@RestController
@RequestMapping("/wechat/home/culture/activity")
public class WxCeActivityController extends WeChatController {
    @Autowired
    private CultureActivityService cultureActivityService;

    @Autowired
    private FileEntityService fileEntityService;


    @RequestMapping("/Index")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/wechat/home/culture/activity/activity_list");
        return mv;
    }

    @RequestMapping("/list")
    public Object GetList(String pagination) {
        CultureActivity cultureActivity =new CultureActivity();
        String ywType="activity";
        cultureActivity.setIsdel("1");
        cultureActivity.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(cultureActivityService.queryPageAndImg(pageParameter, cultureActivity,ywType));
       return R.ok().put("data", pageData);
    }

    @RequestMapping("/activity_info")
    public Object style_info(String id) {
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        String ywType="activity";
        CultureActivity cultureActivity=new CultureActivity();
        cultureActivity.setId(id);
        ModelAndView mv = new ModelAndView("/wechat/home/culture/activity/activity_info");
        mv.addObject("cultureActivity",getPageData(cultureActivityService.queryPageAndImg(pageParameter,cultureActivity,ywType)));
        return mv;
    }
}