package com.lzxuni.modules.wechat.home.lawmanage;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.service.LegislationService;
import com.lzxuni.modules.pccontrol.home.lawmanage.regulations.entity.Regulations;
import com.lzxuni.modules.pccontrol.home.lawmanage.regulations.service.RegulationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/19
 * 法律法规-
 */

@Controller
@RequestMapping("/wechat/home/lawmanage/flfg")
public class LawFlfgController {


    @Autowired
    private LegislationService tblHomeLawmanageLegislationService;

    @Autowired
    private CommunityService communityService;
    @Autowired
    private RegulationsService comtrainingService;

    //跳转法律法规
    @RequestMapping("/lawmanage_FLFG")
    public String lawmanage_FLFG() {
        return "/wechat/home/lawmanage/flfg/lawmanage_FLFG";
    }



    //所有
    @ResponseBody
    @RequestMapping("/allLaw")
    public Object allLaw(HttpServletRequest req, String pagination) {
        Regulations rt=new Regulations();
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //aff.setOpenid((String)req.getSession().getAttribute("openid"));


        PageData pageData = getPageData( comtrainingService.queryPage4(pageParameter,rt));
        return R.ok().put("data", pageData);
    }


    //跳转
    @RequestMapping("/lawmanage_FLFG_xx")
    public ModelAndView lawmanage_FLFG_xx(String id) {
        ModelAndView mv = new ModelAndView("wechat/home/lawmanage/flfg/lawmanage_FLFG_xx");
        Regulations rt=new Regulations();
        rt.setId(id);
        Regulations regulations=comtrainingService.queryBymcId(rt);
       // Community cc= communityService.getById(new Community().setId(legislation.getCommunityid()));
       // legislation.setCommunityname(cc.getCommunityname());
        mv.addObject("regulations", regulations);
        return mv;
    }






    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
