package com.lzxuni.modules.wechat.home.gridding;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.buildingnum.service.BuildingnumService;
import com.lzxuni.modules.pccontrol.basics.village.service.VillageService;
import com.lzxuni.modules.pccontrol.home.gridding.entity.Gridding;
import com.lzxuni.modules.pccontrol.home.gridding.service.GriddingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 首页--我的网格
 * Created by Lhl
 * 2019/8/02
 */
@Controller
@RequestMapping("/wechat/home/gridding")
public class GriMVController extends BaseController {
    @Autowired
    private VillageService villageService;

    @Autowired
    private BuildingnumService buildingnumService;

    @Autowired
    private GriddingService tblHomeGriddingService;

    @RequestMapping("/gridding")
    public ModelAndView community() {
        ModelAndView mv=new ModelAndView("/wechat/home/gridding/gridding");
        mv.addObject("village", JSON.toJSONString(buildingnumService.getTree(null)));
        mv.addObject("buildingnum",JSON.toJSONString(buildingnumService.getBuildingNumTree()));

        return mv;
    }
    //图片ID返回给实体类  Update页传回
    @ResponseBody
    @RequestMapping("/GetEntity")
    public Object  GetEntity(String buildingnumid){
        System.out.println("##"+buildingnumid);
        Gridding entity=new Gridding();
        entity.setIsdel("1");
        entity.setIsshow("1");
        entity.setBuildingnumid(buildingnumid);
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        System.out.println("###"+JSON.toJSONString(getPageData(tblHomeGriddingService.queryPage(pageParameter,entity))));
        return R.ok().put("data",JSON.toJSONString(getPageData(tblHomeGriddingService.queryPage(pageParameter,entity))));
    }

}
