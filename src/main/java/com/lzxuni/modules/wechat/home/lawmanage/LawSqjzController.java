package com.lzxuni.modules.wechat.home.lawmanage;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.community.entity.Community;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import com.lzxuni.modules.pccontrol.home.lawmanage.correct.entity.Correct;
import com.lzxuni.modules.pccontrol.home.lawmanage.correct.service.CorrectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author gyl
 * @description TODO
 * @date 2019/8/19
 * 社区矫正-
 */

@Controller
@RequestMapping("/wechat/home/lawmanage/sqjz")
public class LawSqjzController {

    @Autowired
    private CorrectService tblHomeLawmanageCorrectService;
    @Autowired
    private CommunityService communityService;

    //跳转社区矫正List
    @RequestMapping("/lawmanage_SQJZ_list")
    public String lawmanage_SQJZ_list() {
        return "/wechat/home/lawmanage/sqjz/lawmanage_SQJZ_list";
    }

    //跳转社区矫正
    @RequestMapping("/lawmanage_SQJZ")
    public String lawmanage_ZZZZ() {
        return "/wechat/home/lawmanage/sqjz/lawmanage_SQJZ";
    }

    //跳转
    @RequestMapping("/lawmanage_SQJZ_xx")
    public ModelAndView lawmanage_SQJZ_xx(String id) {
        ModelAndView mv = new ModelAndView("wechat/home/lawmanage/sqjz/lawmanage_SQJZ_xx");
        Correct cc=new Correct();
        cc.setId(id);
        //mt.setYwtype("sj");
        Correct mc=tblHomeLawmanageCorrectService.getById(cc);
        Community community= communityService.getById(new Community().setId(mc.getCommunityid()));
        mc.setCommunityname(community.getCommunityname());
        mv.addObject("correct", mc);
        return mv;
    }

    //所有
    @ResponseBody
    @RequestMapping("/selectListthree")
    public Object selectListthree(HttpServletRequest req, String pagination) {
        Correct cc=new Correct();
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);

        cc.setYwtype("sj");

       List list =tblHomeLawmanageCorrectService.selectListthree(cc);
        return R.ok().put("data", list);
    }

    //所有
    @ResponseBody
    @RequestMapping("/selectListall")
    public Object allWenda(HttpServletRequest req, String pagination) {
        Correct cc=new Correct();
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);

        cc.setYwtype("sj");
        PageData pageData = getPageData( tblHomeLawmanageCorrectService.selectListall(pageParameter,cc));
        return R.ok().put("data", pageData);
    }






    public PageData getPageData(PageInfo<?> pageInfo) {
        List<?> list = pageInfo.getList();
        PageData pageData = new PageData();
        pageData.setRows(list);
        pageData.setTotal(pageInfo.getPages());
        pageData.setPage(pageInfo.getPageNum());
        pageData.setRecords(pageInfo.getTotal());
        pageData.setCosttime(10);
        return pageData;
    }
}
