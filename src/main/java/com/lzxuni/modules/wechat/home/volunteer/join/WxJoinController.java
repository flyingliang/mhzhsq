package com.lzxuni.modules.wechat.home.volunteer.join;

import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.volunteer.join.entity.VolunteerJoin;
import com.lzxuni.modules.pccontrol.home.volunteer.join.service.VolunteerJoinService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * 主页_文化_文化作品 前端控制器
 * Created by fcd
 * 2019/7/16
 */

@RestController
@RequestMapping("/wechat/home/volunteer/join")
public class WxJoinController extends WeChatController {

    @Autowired
    private VolunteerJoinService volunteerJoinService;


     @RequestMapping("/add")
    public ModelAndView add(HttpServletRequest request) {
        ModelAndView mv = new ModelAndView("/wechat/home/volunteer/join/join_add");
        String url = PortUrl + request.getRequestURI() ;
        mv.addObject("jsApiConfig", getJsAPiConfig(url));
        List<Tree> joinlist=volunteerJoinService.getTree();
        mv.addObject("joinlist",joinlist);
        return mv;
    }

    //发布信息
    @RequestMapping("/insert")
    public Object insert(HttpServletRequest request, VolunteerJoin volunteerJoin,String uploadImgUrls1,String uploadImgUrls2) {
        volunteerJoin.setOpenid(getOpenId(request));
        volunteerJoin.setNickname(getNickName(request));
        String ywId = UuidUtil.get32UUID();
        volunteerJoin.setId(ywId);
        volunteerJoin.setIsdel("1");
        volunteerJoin.setIsshow("1");
        volunteerJoin.setDate(new Date());
        volunteerJoinService.save(volunteerJoin);
        String typeName = "志愿者组织图片";
        String ywType = "identity";
        String ywTypes = "personage";
        System.out.println("++++++++++++++++++++++++++++++++++++"+uploadImgUrls1);
        System.out.println("++++++++++++++++++++++++++++++++++++"+uploadImgUrls2);
        getTempFile(uploadImgUrls1, typeName, ywType, ywId, "image");
        getTempFile(uploadImgUrls2, typeName, ywTypes, ywId, "image");

        return R.ok("发布成功");
    }
}