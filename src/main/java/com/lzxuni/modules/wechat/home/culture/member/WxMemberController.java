package com.lzxuni.modules.wechat.home.culture.member;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.basics.group.entity.BasicsGroup;
import com.lzxuni.modules.pccontrol.basics.group.service.BasicsGroupService;
import com.lzxuni.modules.pccontrol.basics.village.entity.Village;
import com.lzxuni.modules.pccontrol.basics.village.service.VillageService;
import com.lzxuni.modules.pccontrol.forum.entity.Comment;
import com.lzxuni.modules.pccontrol.home.culture.member.entity.CultureMember;
import com.lzxuni.modules.pccontrol.home.culture.member.service.CultureMemberService;
import com.lzxuni.modules.pccontrol.home.culture.production.entity.CultureProduction;
import com.lzxuni.modules.pccontrol.home.culture.production.service.CultureProductionService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * 主页_文化_文艺团体 前端控制器
 * Created by fcd
 * 2019/7/16
 */

@RestController
@RequestMapping("/wechat/home/culture/member")
public class WxMemberController extends WeChatController {
    @Autowired
    private CultureMemberService cultureMemberService;
    @Autowired
    private BasicsGroupService basicsGroupService;
    @Autowired
    private VillageService villageService;


    @RequestMapping("/Index")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/wechat/home/culture/member/member_list");
        return mv;
    }

    @RequestMapping("/list")
    public Object GetList(String pagination) {
        BasicsGroup basicsGroup =new BasicsGroup();
        basicsGroup.setIsdel("1");
        String ywType="group";
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(basicsGroupService.queryPageAndImg(pageParameter, basicsGroup,ywType));
       return R.ok().put("data", pageData);
    }

    @RequestMapping("/member_info")
    public Object style_info(String id) {
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        BasicsGroup basicsGroup =new BasicsGroup();
        basicsGroup.setId(id);
        basicsGroup.setIsdel("1");
        String ywType="group";
        ModelAndView mv = new ModelAndView("/wechat/home/culture/member/member_info");
        mv.addObject("basicsGroup",getPageData(basicsGroupService.queryPageAndImgs(pageParameter,basicsGroup,ywType)));
        return mv;
    }

      @RequestMapping("/add")
    public ModelAndView add(HttpServletRequest request) {
        ModelAndView mv = new ModelAndView("/wechat/home/culture/member/member_add");
        String url = PortUrl + request.getRequestURI() + "?" + request.getQueryString();
        mv.addObject("jsApiConfig", getJsAPiConfig(url));
        List<BasicsGroup> grouplist=basicsGroupService.selectgroup();
        mv.addObject("grouplist",grouplist);
        List<Village> Villagelist=villageService.selectVillage();
        mv.addObject("Villagelist",Villagelist);
        return mv;
    }

    //发布信息
    @RequestMapping("/insert")
    public Object insert(HttpServletRequest request,CultureMember cultureMember, String uploadImgUrls) {
        cultureMember.setOpenid(getOpenId(request));
        cultureMember.setNickname(getNickName(request));
//        String typeName = "发帖图片";
//        String ywType = "addForumPic";
        String ywId = UuidUtil.get32UUID();
        cultureMember.setId(ywId);
        cultureMember.setIsdel("1");
        cultureMember.setIsshow("1");
        cultureMember.setDate(new Date());
        cultureMemberService.save(cultureMember);
//        getTempFile(uploadImgUrls, typeName, ywType, ywId, "image");
        return R.ok("加入成功");
    }
}