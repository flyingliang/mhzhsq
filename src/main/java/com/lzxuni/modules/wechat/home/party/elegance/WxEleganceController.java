package com.lzxuni.modules.wechat.home.party.elegance;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.party.elegance.entity.Elegance;
import com.lzxuni.modules.pccontrol.home.party.elegance.service.EleganceService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 *  功能：</b>党建-群团风采 <br>
 *  作者：</b>任东方<br>
 *  日期：</b>2019年8月12日
 */
@RestController
@RequestMapping("/wechat/home/party/elegance")
public class WxEleganceController extends WeChatController {

    @Autowired
    private EleganceService eleganceService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/elegance_list")
    public ModelAndView elegance_list() {
        ModelAndView mv = new ModelAndView("/wechat/home/party/elegance/elegance_list");
        return mv;
    }
    //分页
    @RequestMapping("/elegance_list_fenye")
    public Object GetList(String pagination) {
        Elegance elegance =new Elegance();
        String ywType="elegancepic";
        elegance.setIsdel("1");
        elegance.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(eleganceService.queryPageAndImg(pageParameter,elegance,ywType));
        return R.ok().put("data", pageData);
    }

    //详细页
    @RequestMapping("/elegance_info")
    public ModelAndView elegance_info(String id) throws Exception {
        ModelAndView mv = new ModelAndView("/wechat/home/party/elegance/elegance_info");
        List<Elegance> eleList = eleganceService.list(new QueryWrapper<Elegance>().eq("id", id));
        if(eleList != null && eleList.size() >0){
            for(int i=0;i<eleList.size();i++){
    //            eleList.get(i).setContent(StringUtils.removeHtml(eleList.get(i).getContent()));
                FileEntity fileEntity = new FileEntity();
                String id3 = eleList.get(i).getId();
                fileEntity.setYwId(id3);
                fileEntity.setYwType("elegancepic");
                List<FileEntity> list = fileEntityService.queryListByFileEntity(fileEntity);
                eleList.get(i).setFileEntities(list);
            }
        }
        mv.addObject("eleList", eleList);
        return mv;
    }

}
