package com.lzxuni.modules.wechat.home.party.style;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.party.style.entity.Style;
import com.lzxuni.modules.pccontrol.home.party.style.service.StyleService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

/**
 * 主页_党建_作风建设 前端控制器
 * Created by 潘云明
 * 2019/7/16
 */

@RestController
@RequestMapping("/wechat/home/party/style")
public class WxStyleController extends WeChatController {
    @Autowired
    private StyleService styleService;

    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/wechat/home/party/style/style_list");
        return mv;
    }

    @RequestMapping("/list")
    public Object GetList(String pagination) {
        Style style =new Style();
        String ywType="stylepic";
        style.setIsdel("1");
        style.setIsshow("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(styleService.queryPageAndImg(pageParameter, style,ywType));
       return R.ok().put("data", pageData);
    }

    @RequestMapping("/style_info")
    public Object style_info(String id) {
        PageParameter pageParameter = JSON.parseObject("{page:\"1\",rows:\"1\",sidx:\"\",sord:\"\"}", PageParameter.class);
        String ywType="stylepic";
        Style style=new Style();
        style.setId(id);

        ModelAndView mv = new ModelAndView("/wechat/home/party/style/style_info");
        mv.addObject("style",getPageData(styleService.queryPageAndImg(pageParameter, style,ywType)));

        return mv;
    }


}