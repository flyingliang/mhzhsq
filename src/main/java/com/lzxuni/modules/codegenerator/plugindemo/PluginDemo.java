package com.lzxuni.modules.codegenerator.plugindemo;

import com.lzxuni.common.utils.R;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author:szq
 * @create:2019-03-06 11:00
 * @Modified BY:
 **/

@RestController
public class PluginDemo {
	@RequestMapping("/LR_CodeGeneratorModule/PluginDemo/Index")
	public ModelAndView list(HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/PluginDemo/index");
		return mv;
	}
	@RequestMapping("/Utility/GirdSelectIndex")
	public ModelAndView GirdSelectIndex(HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/Utility/GirdSelectIndex");
		return mv;
	}
	@RequestMapping("/Utility/TreeSelectIndex")
	public ModelAndView TreeSelectIndex(HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/Utility/TreeSelectIndex");
		return mv;
	}



	@RequestMapping("/LR_SystemModule/DataSource/GetMap")
	public ModelAndView getMap(String ver, String code){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/DataItem/getMap");
		if(code.equals("dataitemc")){
			mv = new ModelAndView("/LR_SystemModule/datasource/GetMap1");
		}else if(code.equals("dataitem")){
			mv = new ModelAndView("/LR_SystemModule/datasource/GetMap2");
		}
		return mv;
	}

	@RequestMapping("/LR_CodeDemo/GridDemo/CommonIndex")
	public ModelAndView CommonIndex(){
		ModelAndView mv = new ModelAndView("/LR_CodeDemo/GridDemo/CommonIndex");
		return mv;
	}
	@RequestMapping("/LR_CodeDemo/GridDemo/EditIndex")
	public ModelAndView EditIndex(){
		ModelAndView mv = new ModelAndView("/LR_CodeDemo/GridDemo/EditIndex");
		return mv;
	}
	@RequestMapping("/LR_CodeDemo/GridDemo/EditIndexData")
	public Object EditIndexData(){
		List<Object> list = new ArrayList<>();

		Map<Object, Object> map = new HashMap<>();
		map.put("input", "input");
		map.put("select", "2");
		map.put("radio", "元/亩");
		map.put("checkbox", "1,2");
		map.put("datatime", "2019-02-03");
		map.put("layer", "layer");
		map.put("layer2", "layer2");

		list.add(map);
		return R.ok().put("data", list);
	}
	@RequestMapping("/LR_CodeDemo/GridDemo/ReportIndex")
	public ModelAndView ReportIndex(){
		ModelAndView mv = new ModelAndView("/LR_CodeDemo/GridDemo/ReportIndex");
		return mv;
	}
}
