package com.lzxuni.modules.organization.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Goods;
import com.lzxuni.modules.organization.entity.Pos;
import com.lzxuni.modules.organization.service.PosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 杨国营
 * @since 2019-02-12
 */
@RestController
@RequestMapping("/LR_OrganizationModule/Dealer/Pos")
public class DealerPosController extends BaseController{

	@Autowired
	private PosService posService;

	//Pos列表页
	@RequestMapping("/Index")
	public ModelAndView list(){
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Dealer/Pos/index");
		return mv;
	}

	// Pos列表
	@RequestMapping("/GetList")
	public Object GetList(String companyId,String keyword){
		return R.ok().put("data",posService.list(new QueryWrapper<Pos>().eq("dealer_id",companyId).like("pos_num", keyword==null?"":keyword).orderByDesc("pos_id")));
	}

	//Pos添加页面
	@RequestMapping("/Form")
	public ModelAndView form(){
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Dealer/Pos/Form");
		return mv;
	}
	//Pos参数添加
	@RequestMapping("/SaveForm")
	public Object SaveForm(String keyValue,String companyId,String companyName, Pos pos){
		if(StringUtils.isNotEmpty(keyValue)){
			pos.setPosId(keyValue);
			posService.updateById(pos);
			return R.ok("修改成功");
		}else{
			pos.setPosId(UuidUtil.get32UUID());
			pos.setDealerId(companyId);
			pos.setDealerName(companyName);
			posService.save(pos);
			return R.ok("保存成功");
		}
	}
	@RequestMapping("/DeleteForm")
	public Object deleteForm(String keyValue){
		posService.removeById(keyValue);
		return R.ok("删除成功");
	}

	/**
	 * 新增页面选择商品类型之后展示商品的下拉选
	 */
	@RequestMapping("/QueryGoodsName")
	public Object GetTree(String plantName,String goodsName){
		Map map = new HashMap();
		map.put("plantName", plantName);
		map.put("goodsName", goodsName);
		List<Tree> goodsNameList = posService.GetTree(map);
		return R.ok().put("data",goodsNameList);
	}

	/**
	 * 选中商品后回显的数据，生资厂商和种植类型名称和商品名称
	 * @param goodId
	 * @return
	 */
	@RequestMapping("/QueryGoodAndFactory")
	public Object QueryGoodAndFactory(String goodId){
		Goods GoodAndFactory = posService.QueryGoodAndFactory(goodId);
		return R.ok().put("data",GoodAndFactory);
	}

}

