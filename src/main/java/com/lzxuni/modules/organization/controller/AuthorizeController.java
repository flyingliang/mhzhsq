package com.lzxuni.modules.organization.controller;


import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.organization.entity.Authorize;
import com.lzxuni.modules.organization.service.AuthorizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * <p>
 * 角色信息表 前端控制器
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
@RestController
@RequestMapping("/LR_AuthorizeModule/Authorize")
public class AuthorizeController extends BaseController {

	@Autowired
	private AuthorizeService authorizeService;
	// 列表
	@RequestMapping("/Form")
	public ModelAndView Form() {
		ModelAndView mv = new ModelAndView("/LR_AuthorizeModule/Authorize/form");
		return mv;
	}
	// 列表
	@RequestMapping("/GetFormData")
	public Object getFormData(String objectId, Integer objectType) {
		return R.ok().put("data", authorizeService.getFormData(objectId,objectType));
	}
	// 保存
	@RequestMapping("/SaveForm")
	public Object saveForm(Authorize authorize, String[] strModuleId, String[] strModuleButtonId, String[] strModuleColumnId, String[] strModuleFormId) {
		return R.ok("操作成功").put("data",authorizeService.insertOrUpdate(getUser(),authorize, strModuleId, strModuleButtonId, strModuleColumnId, strModuleFormId));
	}
}

