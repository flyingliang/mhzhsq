package com.lzxuni.modules.organization.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Company;
import com.lzxuni.modules.organization.service.FactoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/LR_OrganizationModule/Factory")
public class FactoryController extends BaseController {
	@Autowired
	private FactoryService factoryService;
	// 列表
	@RequestMapping("/index")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Factory/index");
		return mv;
	}
	// 部门列表
	@RequestMapping("/GetList")
	public Object GetList() {
		return R.ok().put("data", factoryService.list(new QueryWrapper<Company>().eq("nature", "factory")));
	}
	// 根据parentId查找
	@RequestMapping("/GetTree")
	public Object GetTree(String parentId)  {
		List<Tree> companyList = factoryService.getTree(parentId);
		return R.ok().put("data",companyList);
	}

	// 添加展示页面
	@RequestMapping("/Form")
	public ModelAndView insert(String parentId){
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Factory/form");
		return mv;
	}
	@RequestMapping("/SaveForm")
	public Object insertDo(String keyValue, Company company) {
		company.setCompanyId(keyValue);
		if(StringUtils.isEmpty(company.getCompanyId())){
			factoryService.insert(getUser(), company);
			return R.ok("保存成功");
		}else{
			factoryService.update(getUser(), company);
			return R.ok("修改成功");
		}
	}

	//公司管理删除
	@RequestMapping("/DeleteForm")
	public Object delete(String keyValue) {
		factoryService.removeById(keyValue);
		return R.ok("删除成功");
	}

	@RequestMapping("/GetMap")
	public Object getMap(String ver){
		return R.ok("响应成功").put("data",factoryService.getMap(ver));
	}
}
