package com.lzxuni.modules.organization.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.exception.LzxException;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.organization.entity.Customer;
import com.lzxuni.modules.organization.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/LR_OrganizationModule/Customer")
public class CustomerController extends BaseController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping("/Index")
    public ModelAndView index() {
        ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Customer/index");
        return mv;
    }

    @RequestMapping("/GetPageList")
    public Object GetPageList(String pagination, String queryJson, Customer customer) throws LzxException {
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        if(StringUtils.isNotEmpty(queryJson)){
            String str = queryJson.replaceAll("&quot", "\"");
            com.alibaba.fastjson.JSONObject jsStr = com.alibaba.fastjson.JSONObject.parseObject(str);

            if (StringUtils.isNotEmpty(jsStr.getString("key"))) {
                customer.setCustomRealName(jsStr.getString("key"));
            }
            if (StringUtils.isNotEmpty(jsStr.getString("phone"))) {
                customer.setCustomPhone(jsStr.getString("phone"));
            }
            if (StringUtils.isNotEmpty(jsStr.getString("code"))) {
                customer.setCustomCode(jsStr.getString("code"));
            }

            if (StringUtils.isNotEmpty(jsStr.getString("p"))) {
                customer.setCustomProvinceId(jsStr.getString("p"));
            }
            if (StringUtils.isNotEmpty(jsStr.getString("c"))) {
                customer.setCustomCityId(jsStr.getString("c"));
            }
            if (StringUtils.isNotEmpty(jsStr.getString("co"))) {
                customer.setCustomCountyId(jsStr.getString("co"));
            }
            if (StringUtils.isNotEmpty(jsStr.getString("t"))) {
                customer.setCustomTownId(jsStr.getString("t"));
            }
            if (StringUtils.isNotEmpty(jsStr.getString("v"))) {
                customer.setCustomVillageId(jsStr.getString("v"));
            }
        }
        PageData pageData = getPageData(customerService.queryPage(pageParameter, customer));
        return R.ok().put("data", pageData);
    }

    // 添加展示页面
    @RequestMapping("/Form")
    public ModelAndView insert(String customId){
        ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Customer/form");
        return mv;
    }

    //添加或者修改操作
    @RequestMapping("/SaveForm")
    public Object insertDo(String keyValue, Customer customer) {
        customer.setCustomId(keyValue);
        if(StringUtils.isEmpty(customer.getCustomId())){
            customerService.insert(customer);
            return R.ok("保存成功");
        }else{
            customerService.update(customer);
            return R.ok("修改成功");
        }
    }

    //删除
    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        customerService.removeById(keyValue);
        return R.ok("删除成功");
    }
}
