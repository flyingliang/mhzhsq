package com.lzxuni.modules.organization.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Dept;
import com.lzxuni.modules.organization.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 * 单位部门表 前端控制器
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
@RestController
@RequestMapping("/LR_OrganizationModule/Department")
public class DeptController extends BaseController {
	@Autowired
	private DeptService deptService;
	// 列表
	@RequestMapping("/Index")
	public ModelAndView Index() {
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Department/index");
		return mv;
	}
	// 部门列表
	@RequestMapping("/GetList")
	public Object getList(String companyId) {
		return R.ok().put("data",deptService.list(new QueryWrapper<Dept>().eq("company_id",companyId).orderByAsc("sort_code")));
	}
	// 根据parentId查找
	@RequestMapping("/GetTree")
	public Object getTree(String companyId)  {
		List<Tree> deptList = deptService.getTree(companyId);
		return R.ok().put("data",deptList);
	}

	// 添加展示页面
	@RequestMapping("/Form")
	public ModelAndView insert(){
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Department/form");
		return mv;
	}
	@RequestMapping("/SaveForm")
	public Object insertDo(String keyValue, Dept dept) {
		if (dept.getParentId().equals("undefined")) {
			dept.setParentId("0");
		}
			if (StringUtils.isEmpty(keyValue)) {
				deptService.insert(getUser(), dept);
				return R.ok("保存成功");
			} else {
				dept.setDeptId(keyValue);
				deptService.update(getUser(), dept);
				return R.ok("修改成功");
			}
	}

	//公司管理删除
	@RequestMapping("/DeleteForm")
	public Object delete(String keyValue) {
		deptService.removeById(keyValue);
		return R.ok("删除成功");
	}

	@RequestMapping("/GetMap")
	public Object getMap(String ver){
		return R.ok("响应成功").put("data",deptService.getMap(ver));
	}
}

