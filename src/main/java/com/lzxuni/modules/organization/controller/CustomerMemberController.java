package com.lzxuni.modules.organization.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 王智贤
 * @since 2019-01-27
 */
@Controller
@RequestMapping("/customerMember")
public class CustomerMemberController {

}

