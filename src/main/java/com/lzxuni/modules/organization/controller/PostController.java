package com.lzxuni.modules.organization.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author 杨国营
 * @create 2019-01-09-9:46
 * 快捷键：
 * 1.psvm主方法
 * 2.sout+p+m+v、xxx.sout(变量输出)
 * 3.fori、iter、itar(赋值),循环
 * 4.list.for、list.fori、list.forr倒序遍历
 * 5.ifn 、inn、xxx.nn等于xxx.null
 * 6.prsf、psf、psfi、psfs 静态常量
 * 7.alt+enter--enter 可以快速生成左边的变量
 * 8.shift+enter 新建下一行
 * 9.alt+enter 导jar包
 * 10.shift+上/下 上下选中
 * 11.ctrl+p查看构造器参数ctrl+下（只能一次一次选择）自动补全
 * 12.ctrl+alt+t 选中代码添加try catch
 * 13.Ctrl+Shift+Alt+N 知道类名全局查找该类
 * 14.alt+7查看当前类的所有方法
 * 15.alt+F8查看变量的值或者鼠标悬停点击绿色+
 * 16.ctrl+alt+u/ctrl+alt+shift+u(可看方法) :上面的tree是类的结构，下面的是该类属于哪个jar包
 * 17.ctrl+H可以查看父类和子类
 * 18.ctrl+o重写父类方法
 * 19.alt+Fn+Insert:新建文件夹等
 * 20.ctrl+alt+b:选中控制类方法，直接进入实现类
 * 21.alt+enter:快速显示红线的意义
 * 22.选中两个文件，右键，compare Files:比较两个文件
 * 23.ctrl+alt+l 格式化html代码
 */
@RestController
@RequestMapping("/LR_OrganizationModule/Post")
public class PostController {
	@RequestMapping("/SelectForm")
	public ModelAndView PostForm() {
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Post/SelectForm");
		return mv;
	}
	@RequestMapping("/GetTree")
	public ModelAndView GetTree() {
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Post/GetTree");
		return mv;
	}



}
