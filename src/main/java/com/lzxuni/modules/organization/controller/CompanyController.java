package com.lzxuni.modules.organization.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Company;
import com.lzxuni.modules.organization.service.CompanyService;
import com.lzxuni.modules.organization.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/LR_OrganizationModule/Company")
public class CompanyController extends BaseController {
	@Autowired
	private CompanyService companyService;
    @Autowired
    private UserService userService;
	// 列表
	@RequestMapping("/Index")
	public ModelAndView Index() {
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Company/index");
		return mv;
	}
	// 部门列表
	@RequestMapping("/GetList")
	public Object GetList() {
		return R.ok().put("data",companyService.list(null));
	}

	// 查询合作社
	@RequestMapping("/GetListByNature")
	public Object GetListByNature(String nature) {
		Company company = new Company();
		company.setNature(nature);
		return R.ok().put("data",companyService.list(new QueryWrapper<>(company)));
	}
	// 根据parentId查找
	@RequestMapping("/GetTree")
	public Object GetTree(String parentId)  {
		List<Tree> companyList = companyService.getTree(parentId);
		return R.ok().put("data",companyList);
	}

	// 添加展示页面
	@RequestMapping("/Form")
	public ModelAndView insert(String parentId){
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Company/form");
		return mv;
	}
	@RequestMapping("/SaveForm")
	public Object insertDo(String keyValue, Company company) {
		company.setCompanyId(keyValue);
		if(StringUtils.isEmpty(company.getCompanyId())){
			companyService.insert(getUser(), company);
			return R.ok("保存成功");
		}else{
			companyService.update(getUser(), company);
			return R.ok("修改成功");
		}
	}

	//公司管理删除
	@RequestMapping("/DeleteForm")
	public Object delete(String keyValue) {
		companyService.removeById(keyValue);
		return R.ok("删除成功");
	}

	@RequestMapping("/GetMap")
	public Object getMap(String ver){

//		return R.ok("no update");
		return R.ok("响应成功").put("data",companyService.getMap(ver));
	}
}
