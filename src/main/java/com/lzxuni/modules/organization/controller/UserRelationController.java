package com.lzxuni.modules.organization.controller;


import com.lzxuni.common.exception.LzxException;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.organization.entity.UserRelation;
import com.lzxuni.modules.organization.service.UserRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * <p>
 * 角色信息表 前端控制器
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
@RestController
@RequestMapping("/LR_AuthorizeModule/UserRelation")
public class UserRelationController extends BaseController {

	@Autowired
	private UserRelationService userRelationService;
	// 角色选择用户编辑
	@RequestMapping("/SelectForm")
	public ModelAndView selectForm() {
		ModelAndView mv = new ModelAndView("/LR_AuthorizeModule/UserRelation/SelectForm");
		return mv;
	}
	// 角色选择用户查看
	@RequestMapping("/LookForm")
	public ModelAndView lookForm() {
		ModelAndView mv = new ModelAndView("/LR_AuthorizeModule/UserRelation/LookForm");
		return mv;
	}
	// 根据角色查询用户列表
	@RequestMapping("/GetUserIdList")
	public Object getUserIdList(String objectId) throws LzxException {
		return R.ok().put("data", userRelationService.getUserIdList(objectId));
	}
	// 角色选择用户保存
	@RequestMapping("/SaveForm")
	public Object saveForm(String[] userIds, UserRelation userRelation) throws LzxException {
		return R.ok("保存成功").put("data", userRelationService.insertOrUpdate(getUser(),userIds,userRelation));
	}
	// 用户选择角色编辑
	@RequestMapping("/SelectRoleForm")
	public ModelAndView SelectRoleForm() {
		ModelAndView mv = new ModelAndView("/LR_AuthorizeModule/UserRelation/SelectRoleForm");
		return mv;
	}
	// 用户选择角色查看
	@RequestMapping("/LookRoleForm")
	public ModelAndView LookRoleForm() {
		ModelAndView mv = new ModelAndView("/LR_AuthorizeModule/UserRelation/LookRoleForm");
		return mv;
	}

	// 根据角色查询用户列表
	@RequestMapping("/GetRoleIdList")
	public Object GetRoleIdList(String userId) throws LzxException {
		return R.ok().put("data", userRelationService.getRoleIdList(userId));
	}
	// 用户选择角色保存
	@RequestMapping("/SaveRoleForm")
	public Object SaveRoleForm(String[] roleIds, UserRelation userRelation) throws LzxException {
		return R.ok("保存成功").put("data", userRelationService.insertOrUpdateRole(getUser(),roleIds,userRelation));
	}



}

