package com.lzxuni.modules.organization.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.exception.LzxException;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.organization.entity.Role;
import com.lzxuni.modules.organization.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 * 角色信息表 前端控制器
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
@RestController
@RequestMapping("/LR_OrganizationModule/Role")
public class RoleController extends BaseController {

	@Autowired
	private RoleService roleService;
	// 列表
	@RequestMapping("/Index")
	public ModelAndView Index() {
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Role/index");
		return mv;
	}
	@RequestMapping("/GetPageList")
	public Object GetPageList(String pagination, Role role) throws LzxException {
		PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
		PageData pageData = getPageData(roleService.queryPage(pageParameter, role));
		return R.ok().put("data",pageData);
	}
	// 添加展示页面
	@RequestMapping("/Form")
	public ModelAndView insert(){
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Role/form");
		return mv;
	}
	@RequestMapping("/SaveForm")
	public Object insertDo(String keyValue, Role role) {
		role.setRoleId(keyValue);
		if(StringUtils.isEmpty(role.getRoleId())){
			roleService.insert(getUser(), role);
			return R.ok("保存成功");
		}else{
			roleService.update(getUser(), role);
			return R.ok("修改成功");
		}
	}

	//公司管理删除
	@RequestMapping("/DeleteForm")
	public Object delete(String keyValue) {
		roleService.delete(keyValue);
		return R.ok("删除成功");
	}
	//==================================================杨国营  =====UI 素材修改======================================
	@RequestMapping("/GetList")
	public Object GetList() {
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/Role/GetList");
		List<Role> list = roleService.list();
		return R.ok().put("data", list);
	}

}

