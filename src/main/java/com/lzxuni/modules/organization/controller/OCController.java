package com.lzxuni.modules.organization.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Company;
import com.lzxuni.modules.organization.service.OCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/LR_OrganizationModule/OC")
public class OCController extends BaseController {
	@Autowired
	private OCService ocService;
	// 列表
	@RequestMapping("/index")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/OC/index");
		return mv;
	}
	// 部门列表
	@RequestMapping("/GetList")
	public Object GetList(String areaId) {
		List<Company> list = ocService.list(new QueryWrapper<Company>()
				.eq("nature", "oc")
				.eq("county_id", areaId));

		if (list == null || list.size()==0) {
			list = ocService.list(new QueryWrapper<Company>()
					.like("full_name", "凌之迅"));
		}
		return R.ok().put("data", list);
	}
	// 根据parentId查找
	@RequestMapping("/GetTree")
	public Object GetTree(String parentId)  {
		List<Tree> companyList = ocService.getTree(parentId);
		return R.ok().put("data",companyList);
	}

	// 添加展示页面
	@RequestMapping("/Form")
	public ModelAndView insert(String parentId){
		ModelAndView mv = new ModelAndView("/LR_OrganizationModule/OC/form");
		return mv;
	}
	@RequestMapping("/SaveForm")
	public Object insertDo(String keyValue, Company company) {
		company.setCompanyId(keyValue);
		if(StringUtils.isEmpty(company.getCompanyId())){
			ocService.insert(getUser(), company);
			return R.ok("保存成功");
		}else{
			ocService.update(getUser(), company);
			return R.ok("修改成功");
		}
	}

	//公司管理删除
	@RequestMapping("/DeleteForm")
	public Object delete(String keyValue) {
		ocService.removeById(keyValue);
		return R.ok("删除成功");
	}

	@RequestMapping("/GetMap")
	public Object getMap(String ver){
		return R.ok("响应成功").put("data",ocService.getMap(ver));
	}
}
