package com.lzxuni.modules.organization.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lzxuni.modules.common.entity.Tree;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**

/**
 * 贷款模型
 * @author 韩蓄
 * @create 2018-12-29 9:46
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("base_dkmx")
public class Dkmx extends Tree implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private String productId;
	/**
	 * 银行ID
	 */
	private String companyId;
	/**
	 * 银行名称
	 */
	@TableField(exist = false)
	private String companyName;

	@TableField(exist = false)
	private String areaId;

	@TableField(exist = false)
	private String areaName;

	/**
	 * 省级编码
	 */
	private String provinceId;

	@TableField(exist = false)
	private String proName;
	/**
	 * 市级编码
	 */
	private String cityId;

	@TableField(exist = false)
	private String cityName;
	/**
	 *县级编码
	 */
	private String countyId;

	@TableField(exist = false)
	private String countyName;
	/**
	 *镇级编码
	 */
	private String townId;

	@TableField(exist = false)
	private String townName;
	/**
	 *村级编码
	 */
	private String villageId;

	@TableField(exist = false)
	private String villageName;
	/**
	 * 贷款模型名称
	 */
	private String dkmxName;

	/**
	 * 年信息
	 */
	private String year;
	/**
	 * 利息收取方式1，固定利息，2，按月，3按天
	 */
	private String type;
	/**
	 * 利率（可能是年化利率，也可能是日利率，月利率）
	 */
	private BigDecimal interestRate;
	/**
	 * 担保公司
	 */
	private String bonding;

	/**
	 * 担保公司名称
	 */
	@TableField(exist=false)
	private String BondingName;

	/**
	 * 开始时间
	 */
	private Date startDate;
	/**
	 * 结束时间
	 */
	private Date endDate;
	/**
	 * 记录生成时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;
	/**
	 * 创建用户主键
	 */
	private String createUserId;
	/**
	 * 创建用户
	 */
	private String createUsername;
	/**
	 * 需改日期
	 */
	@TableField(fill = FieldFill.UPDATE)
	private Date modifyDate;
	/**
	 * 修改用户主键
	 */
	private String modifyUserId;
	/**
	 * 修改用户
	 */
	private String modifyUsername;


}
