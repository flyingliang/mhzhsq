package com.lzxuni.modules.organization.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
@Data
@Accessors(chain = true)
public class UserOnLine implements  Comparable<UserOnLine> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户主键
     */
    private String userId;
    private String username;
    private String realName;
    private String mobile;
    private String companyId;
    //IP地址
    private String ipAddress;
    //IP所在地
    private String ipAddressName;
    //浏览器
    private String browser;
    //操作系统
    private String os;
    //更新时间
    private Date lastAccessTime;
    //登录时间
    private Date startTimestamp;


    @Override
    public int compareTo(UserOnLine o) {
        int flag = o.getLastAccessTime().compareTo(this.getLastAccessTime());
        return flag;
    }
}
