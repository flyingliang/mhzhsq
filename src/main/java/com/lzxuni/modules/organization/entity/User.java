package com.lzxuni.modules.organization.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lzxuni.common.validator.group.AddGroup;
import com.lzxuni.common.validator.group.UpdateGroup;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
@Data
@Accessors(chain = true)
@TableName("base_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户主键
     */
    @TableId
    private String userId;
    /**
     * 工号
     */
    private String enCode;
    /**
     * 登录账户
     */
    @NotBlank(message="用户名不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String username;
    /**
     * 登录密码
     */
    private String password;
    /**
     * 盐值，密码秘钥
     */
    private String secretkey;
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 呢称
     */
    private String nickName;
    /**
     * 头像
     */
    private String headIcon;
    /**
     * 快速查询
     */
    private String quickQuery;
    /**
     * 简拼
     */
    private String simpleSpelling;
    /**
     * 性别
     */
    private String gender;
    /**
     * 生日
     */
    private Date birthday;
    /**
     * 手机
     */
    private String mobile;
    /**
     * 电话
     */
    private String telephone;
    /**
     * 电子邮件
     */
    private String email;
    /**
     * QQ号
     */
    private String oicq;
    /**
     * 微信
     */
    private String weChat;
    /**
     * MSN
     */
    private String msn;
    /**
     * 机构主键
     */
    private String companyId;
    /**
     * 部门主键
     */
    private String deptId;
    /**
     * 安全级别
     */
    private Integer securityLevel;
    /**
     * 单点登录标识
     */
    private Integer openId;
    /**
     * 密码提示问题
     */
    private String question;
    /**
     * 密码提示答案
     */
    private String answerQuestion;
    /**
     * 允许多用户同时登录
     */
    @TableField("check_onLine")
    private Integer checkOnline;
    /**
     * 允许登录时间开始
     */
    private Date allowStartTime;
    /**
     * 允许登录时间结束
     */
    private Date allowEndTime;
    /**
     * 暂停用户开始日期
     */
    private Date lockStartDate;
    /**
     * 暂停用户结束日期
     */
    private Date lockEndDate;
    /**
     * 排序码
     */
    private Integer sortCode;
    /**
     * 删除标记
     */
	@TableField(value = "delete_mark",fill = FieldFill.INSERT_UPDATE)
    private Integer deleteMark;
    /**
     * 有效标志
     */
	@TableField(value = "enabled_mark",fill = FieldFill.INSERT_UPDATE)
    private Integer enabledMark;
    /**
     * 备注
     */
    private String description;
    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;
    /**
     * 创建用户主键
     */
    private String createUserId;
    /**
     * 创建用户
     */
    private String createUsername;
    /**
     * 修改日期
     */
    @TableField(fill = FieldFill.UPDATE)
    private Date modifyDate;
    /**
     * 修改用户主键
     */
    private String modifyUserId;
    /**
     * 修改用户
     */
    private String modifyUsername;
    /**
     * 权限类型Y：超级管理员，开发用，其他用户不可见N：普通用户
     */
    private String isSystem;

    @TableField(exist = false)
    private List<Role> roleList;
    /**
     * 最后登录时间
     */
    private Date lastLogin;
    /**
     * 本方法用来判断该用户是不是有这个角色
     * @param roleName
     * @return 有返回true 没有返回false
     */
	public boolean hashRole(String roleName) {
	    //定义返回值
        boolean flag = false;
        /**
         * 用户本身有这个方法
         * getRoleList 在userRealm文件中就给这个用户赋值了所以我们直接就可以取到
         */
        List<Role> roleList = this.getRoleList();
        if (roleList != null) {
            for (int i = 0; i < roleList.size(); i++) {
                if (roleList.get(i).getFullName().equals(roleName)) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
	}
}
