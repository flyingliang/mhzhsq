package com.lzxuni.modules.organization.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 过滤IP
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
@Data
@Accessors(chain = true)
@TableName("base_filter_time")
public class FilterTime implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 过滤时段主键
     */
    private String filterTimeId;
    /**
     * 对象类型
     */
    private String objectType;
    /**
     * 访问
     */
    private String visitType;
    /**
     * 星期一
     */
    private String weekDay1;
    /**
     * 星期二
     */
    private String weekDay2;
    /**
     * 星期三
     */
    private String weekDay3;
    /**
     * 星期四
     */
    private String weekDay4;
    /**
     * 星期五
     */
    private String weekDay5;
    /**
     * 星期六
     */
    private String weekDay6;
    /**
     * 星期日
     */
    private String weekDay7;
    /**
     * 排序码
     */
    private String sortCode;
    /**
     * 删除标记
     */
    private String deleteMark;
    /**
     * 有效标志
     */
    private String enabledMark;
    /**
     * 备注
     */
    private String description;
    /**
     * 创建日期
     */
    private Date createDate;
    /**
     * 创建用户主键
     */
    private String createUserId;
    /**
     * 创建用户
     */
    private String createUsername;
    /**
     * 修改日期
     */
    private Date modifyDate;
    /**
     * 修改用户主键
     */
    private String modifyUserId;
    /**
     * 修改用户
     */
    private String modifyUsername;


}
