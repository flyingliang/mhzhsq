package com.lzxuni.modules.organization.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 王智贤
 * @since 2019-01-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_customer_member")
public class CustomerMember implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户家庭成员表
     */
    @TableId
    private String customerMemberId;

    /**
     * 客户主键
     */
    private String loanId;

    /**
     * 家庭成员姓名
     */
    private String name;

    /**
     * 家庭成员性别
     */
    private String sex;

    /**
     * 家庭成员电话号
     */
    private String phone;

    /**
     * 家庭成员关系
     */
    private String relation;

    /**
     * 排序号
     */
    private String sortSode;


}
