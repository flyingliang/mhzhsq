package com.lzxuni.modules.organization.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 角色信息表
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_role")
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色主键
     */
    @TableId
    private String roleId;

    /**
     * 分类
     */
    private String category;

    /**
     * 角色编码
     */
    private String enCode;

    /**
     * 角色名称
     */
    private String fullName;

    /**
     * 排序码
     */
    private Integer sortCode;

    /**
     * 删除标记
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer deleteMark;

    /**
     * 有效标志
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer enabledMark;

    /**
     * 备注
     */
    private String description;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;

    /**
     * 创建用户主键
     */
    private String createUserId;

    /**
     * 创建用户
     */
    private String createUsername;

    /**
     * 修改日期
     */
    @TableField(fill = FieldFill.UPDATE)
    private Date modifyDate;

    /**
     * 修改用户主键
     */
    private String modifyUserId;

    /**
     * 修改用户
     */
    private String modifyUsername;


}
