package com.lzxuni.modules.organization.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 杨国营
 * @since 2019-02-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_pos")
public class Pos implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * pos机主键
     */
    @TableId
    private String posId;

    /**
     * 经销商主键
     */
    private String dealerId;

    /**
     * 经销商名字
     */
    private String dealerName;

    /**
     * pos机编号
     */
    private String posNum;

    /**
     * 种植物品类型id
     */
    private String plantName;
    /**
     * 种植物品类型name
     */
    private String plantNameName;

    /**
     * 商品类型
     */
    private String goodsType;

    /**
     * 商品名称id
     */
    private String goodsName;
    /**
     * 商品名称
     */
    private String goodsNameName;

    /**
     * 生资企业主键
     */
    private String factoryId;

    /**
     * 生资企业名称
     */
    private String factoryName;


}
