package com.lzxuni.modules.organization.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author liuzp
 * @since 2019-01-08
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("base_goods")
public class Goods extends Model<Goods> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String goodsId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品型号
     */
    private String goodsModel;

    /**
     * 进货价格
     */
    private BigDecimal price;

    /**
     * 销售价格
     */
    private BigDecimal salePrice;

    /**
     * 进售价单位
     */
    private String unit;

    /**
     * 种植类型
     */
    private String plantType;

    /**
     * 商品种类
     */
    private String goodsType;

    /**
     * 生资企业
     */
    private String factory;

    /**
     * 经销商
     */
    private String dealer;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 农机-适用范围
     */
    private String machineSyfw;

    /**
     * 农资-适用类型
     */
    private String agricultureSytype;

    /**
     * 公司地点省级编码
     */
    private String provinceId;

    /**
     * 市级编码
     */
    private String cityId;

    /**
     * 区县级编码
     */
    private String countyId;

    /**
     * 乡镇级编码
     */
    private String townId;

    /**
     * 村级编码
     */
    private String villageId;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 生资企业名字
     */
    @TableField(exist = false)
    private String factoryName;

    /**
     * 经销商名字
     */
    @TableField(exist = false)
    private String dealerName;

    /**
     * 种植类型名称
     */
    @TableField(exist = false)
    private String plantTypeName;

}
