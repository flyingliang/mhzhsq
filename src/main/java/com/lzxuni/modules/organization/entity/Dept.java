package com.lzxuni.modules.organization.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lzxuni.modules.common.entity.Tree;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 单位部门表
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_dept")
public class Dept extends Tree implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 部门主键
     */
    @TableId
    private String deptId;

    /**
     * 公司主键
     */
    private String companyId;

    /**
     * 部门编码
     */
    private String enCode;

    /**
     * 部门简称
     */
    private String shortName;

    /**
     * 部门名称
     */
    private String fullName;

    /**
     * 部门性质
     */
    private String nature;

    /**
     * 负责人
     */
    private String manager;

    /**
     * 外线
     */
    private String outerPhone;

    /**
     * 内线
     */
    private String innerPhone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 传真
     */
    private String fax;

    /**
     * 公司地点省级编码
     */
    private Integer sortCode;

    /**
     * 删除标记
     */
    private Integer deleteMark;

    /**
     * 有效标志
     */
    private Integer enabledMark;

    /**
     * 备注
     */
    private String description;

    /**
     * 记录生成时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;

    /**
     * 创建用户主键
     */
    private String createUserId;

    /**
     * 创建用户
     */
    private String createUsername;

    /**
     * 需改日期
     */
    @TableField(value = "modify_date",fill = FieldFill.UPDATE)
    private Date modifyDate;

    /**
     * 修改用户主键
     */
    private String modifyUserId;

    /**
     * 修改用户
     */
    private String modifyUsername;

//    @TableField(exist=false)
//    private List<Dept> childNodes ;
//
//    @Override
//    public String getId() {
//        return getDeptId();
//    }
//
//    @Override
//    public String getText() {
//        return getFullName();
//    }
//
//    @Override
//    public Boolean getShowcheck() {
//        return false;
//    }
//
//    @Override
//    public Boolean getHasChildren() {
//        if (getChildNodes() != null && getChildNodes().size() > 0) {
//            return true;
//        }else{
//            return false;
//        }
//    }
//    @Override
//    public Boolean getIsexpand() {
//        return true;
//    }
//
//    @Override
//    public Boolean getComplete() {
//        return true;
//    }


}
