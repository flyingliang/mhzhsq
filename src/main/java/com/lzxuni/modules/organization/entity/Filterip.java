package com.lzxuni.modules.organization.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 过滤IP
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
@Data
@Accessors(chain = true)
@TableName("base_filterip")
public class Filterip implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 过滤IP主键
     */
    @TableId("filterIP_id")
    private String filteripId;
    /**
     * 对象类型
     */
    private String objectType;
    /**
     * 对象Id
     */
    private String objectId;
    /**
     * 访问
     */
    private String visitType;
    /**
     * 类型
     */
    private String type;
    /**
     * IP访问
     */
    @TableField("IP_limit")
    private String ipLimit;
    /**
     * 排序码
     */
    private String sortCode;
    /**
     * 删除标记
     */
    private String deleteMark;
    /**
     * 有效标志
     */
    private String enabledMark;
    /**
     * 备注
     */
    private String description;
    /**
     * 创建日期
     */
    private Date createDate;
    /**
     * 创建用户主键
     */
    private String createUserId;
    /**
     * 创建用户
     */
    private String createUsername;
    /**
     * 修改日期
     */
    private Date modifyDate;
    /**
     * 修改用户主键
     */
    private String modifyUserId;
    /**
     * 修改用户
     */
    private String modifyUsername;


}
