package com.lzxuni.modules.organization.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户关系表
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_user_relation")
public class UserRelation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户关系主键
     */
    @TableId
    private String userRelationId;

    /**
     * 用户主键
     */
    private String userId;

    /**
     * 分类:1-角色2-岗位
     */
    private String category;

    /**
     * 对象主键(比如，角色对用户，1对多，存储角色主键)
     */
    private String objectId;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;

    /**
     * 创建用户主键
     */
    private String createUserId;

    /**
     * 创建用户
     */
    private String createUsername;


}
