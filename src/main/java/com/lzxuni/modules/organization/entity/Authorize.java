package com.lzxuni.modules.organization.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 授权功能表
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_authorize")
public class Authorize implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 授权功能主键
     */
    @TableId
    private String authorizeId;

    /**
     * 对象分类:1-角色2-用户
     */
    private Integer objectType;

    /**
     * 对象主键
     */
    private String objectId;

    /**
     * 项目类型:1-菜单2-按钮3-视图4-表单5-app功能
     */
    private Integer itemType;

    /**
     * 项目主键
     */
    private String itemId;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;

    /**
     * 创建用户主键
     */
    private String createUserId;

    /**
     * 创建用户
     */
    private String createUsername;


}
