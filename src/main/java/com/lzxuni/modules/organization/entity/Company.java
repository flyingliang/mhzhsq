package com.lzxuni.modules.organization.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lzxuni.modules.common.entity.Tree;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**

/**
 * 〈一句话功能简述〉<br>
 * 公司管理
 * @author szq
 * @create 2018-03-19 10:36
 * @Modified BY
 **/
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("base_company")
public class Company extends Tree implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private String companyId;
	/**
	 * 公司简称
	 */
	private String enCode;
	/**
	 * 公司简称
	 */
	private String shortName;
	/**
	 * 公司名称
	 */
	private String fullName;
	/**
	 * 成立时间
	 */
	private Date foundedTime;
	/**
	 * 公司性质
	 */
	private String nature;
	/**
	 * 负责人
	 */
	private String manager;
	/**
	 * 电话
	 */
	private String outerPhone;
	/**
	 * 联系人
	 */
	private String lxr;
	/**
	 * 联系人电话
	 */
	private String lxrPhone;
	/**
	 * 监管账户
	 */
	private String jgzh;
	/**
	 * 公司地点省级编码
	 */
	private String provinceId;
	/**
	 * 公司地点市级编码
	 */
	private String cityId;
	/**
	 * 公司地点区县级编码
	 */
	private String countyId;
	/**
	 * 公司地点乡镇级编码
	 */
	private String townId;
	/**
	 * 公司地点村级编码
	 */
	private String villageId;
	/**
	 * 地址
	 */
	private String address;
	/**
	 * 备注
	 */
	private String description;
	/**
	 * 记录生成时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;
	/**
	 * 创建用户主键
	 */
	private String createUserId;
	/**
	 * 创建用户
	 */
	private String createUsername;
	/**
	 * 需改日期
	 */
	@TableField(fill = FieldFill.UPDATE)
	private Date modifyDate;
	/**
	 * 修改用户主键
	 */
	private String modifyUserId;
	/**
	 * 修改用户
	 */
	private String modifyUsername;
	// 一个公司对应多个部门
	@TableField(exist=false)
	private List<Dept> deptList ;

	//公司人数
	@TableField(exist=false)
	private String count ;

	@TableField(exist=false)
	private String proName ;

	@TableField(exist=false)
	private String cityName ;

	@TableField(exist=false)
	private String countyName ;

	@TableField(exist=false)
	private String townName ;

	@TableField(exist=false)
	private String villageName ;
	// 一个公司包含的分公司或者子公司
//	@TableField(exist=false)
//	private List<Company> childNodes ;
//
//	@Override
//	public String getId() {
//		return getCompanyId();
//	}
//
//	@Override
//	public String getText() {
//		return getFullName();
//	}
//
//	@Override
//	public Boolean getShowcheck() {
//		return false;
//	}
//
//	@Override
//	public Boolean getHasChildren() {
//		if (getChildNodes() != null && getChildNodes().size() > 0) {
//			return true;
//		}else{
//			return false;
//		}
//
//	}
//
//	@Override
//	public Boolean getIsexpand() {
//		return true;
//	}
//
//	@Override
//	public Boolean getComplete() {
//		return true;
//	}
}
