package com.lzxuni.modules.organization.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author:孙志强
 * @create:2018-12-13 16:48
 * @Modified BY:
 **/
@Data
@Accessors(chain = true)
@TableName("base_user_img")
public class UserImg {
	@TableId
	private String userId;

	private byte[] img;
}
