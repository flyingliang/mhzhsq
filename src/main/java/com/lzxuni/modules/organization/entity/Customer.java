package com.lzxuni.modules.organization.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("base_customer")
public class Customer implements Serializable {


    //客户编号
    @TableId
    private String customId;
    //客户名称
    private String customRealName;
    //身份证号
    private String customCode;
    //出生日期
    @TableField(fill = FieldFill.INSERT)
    private Date customBirthday;
    //客户性别
    private String customSex;
    //客户婚姻状态
    private String customMaritalStatus;
    //客户手机号
    private String customPhone;
    //登录账号
    private String customUsername;
    //    登录密码
    private String customPassword;
    //        省Id
    private String customProvinceId;
    //    市Id
    private String customCityId;
    //        县Id
    private String customCountyId;
    //    镇Id
    private String customTownId;
    //        村Id
    private String customVillageId;
    //    详细地址
    private String customAddress;
    //        客户合作社Id
    private String customCooperativeId;

    @TableField(exist = false)
    private String address;

    private Integer isExcel;

    private Date createDate;

    private Integer sort;

    @TableField(exist = false)
    private String proName;

    @TableField(exist = false)
    private String cityName;

    @TableField(exist = false)
    private String countyName;

    @TableField(exist = false)
    private String townName;

    @TableField(exist = false)
    private String villageName;

    public String getCustomId() {
        return customId;
    }

    public void setCustomId(String customId) {
        this.customId = customId;
    }

    public String getCustomRealName() {
        return customRealName;
    }

    public void setCustomRealName(String customRealName) {
        this.customRealName = customRealName;
    }

    public String getCustomCode() {
        return customCode;
    }

    public void setCustomCode(String customCode) {
        this.customCode = customCode;
    }

    public Date getCustomBirthday() {
        return customBirthday;
    }

    public void setCustomBirthday(Date customBirthday) {
        this.customBirthday = customBirthday;
    }

    public String getCustomSex() {
        return customSex;
    }

    public void setCustomSex(String customSex) {
        this.customSex = customSex;
    }

    public String getCustomMaritalStatus() {
        return customMaritalStatus;
    }

    public void setCustomMaritalStatus(String customMaritalStatus) {
        this.customMaritalStatus = customMaritalStatus;
    }

    public String getCustomPhone() {
        return customPhone;
    }

    public void setCustomPhone(String customPhone) {
        this.customPhone = customPhone;
    }

    public String getCustomUsername() {
        return customUsername;
    }

    public void setCustomUsername(String customUsername) {
        this.customUsername = customUsername;
    }

    public String getCustomPassword() {
        return customPassword;
    }

    public void setCustomPassword(String customPassword) {
        this.customPassword = customPassword;
    }

    public String getCustomProvinceId() {
        return customProvinceId;
    }

    public void setCustomProvinceId(String customProvinceId) {
        this.customProvinceId = customProvinceId;
    }

    public String getCustomCityId() {
        return customCityId;
    }

    public void setCustomCityId(String customCityId) {
        this.customCityId = customCityId;
    }

    public String getCustomCountyId() {
        return customCountyId;
    }

    public void setCustomCountyId(String customCountyId) {
        this.customCountyId = customCountyId;
    }

    public String getCustomTownId() {
        return customTownId;
    }

    public void setCustomTownId(String customTownId) {
        this.customTownId = customTownId;
    }

    public String getCustomVillageId() {
        return customVillageId;
    }

    public void setCustomVillageId(String customVillageId) {
        this.customVillageId = customVillageId;
    }

    public String getCustomAddress() {
        return customAddress;
    }

    public void setCustomAddress(String customAddress) {
        this.customAddress = customAddress;
    }

    public String getCustomCooperativeId() {
        return customCooperativeId;
    }

    public void setCustomCooperativeId(String customCooperativeId) {
        this.customCooperativeId = customCooperativeId;
    }
}
