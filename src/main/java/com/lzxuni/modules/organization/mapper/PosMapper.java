package com.lzxuni.modules.organization.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Goods;
import com.lzxuni.modules.organization.entity.Pos;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 杨国营
 * @since 2019-02-28
 */
public interface PosMapper extends BaseMapper<Pos> {

	List<Tree> GetTree(Map map);

	Goods QueryGoodAndFactory(String goodId);
}
