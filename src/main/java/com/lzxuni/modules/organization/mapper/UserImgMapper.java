package com.lzxuni.modules.organization.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.organization.entity.UserImg;

/**
 * <p>
 * 用户图片表 Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-13
 */
public interface UserImgMapper extends BaseMapper<UserImg> {

}
