package com.lzxuni.modules.organization.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.organization.entity.Goods;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author liuzp
 * @since 2019-01-08
 */
public interface GoodsMapper extends BaseMapper<Goods> {

}
