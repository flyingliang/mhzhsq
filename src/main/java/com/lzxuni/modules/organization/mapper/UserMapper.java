package com.lzxuni.modules.organization.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    List<Tree> queryTreeList();
    void updateLastLoginTime(@Param("username") String username);
}
