package com.lzxuni.modules.organization.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.organization.entity.FilterTime;

/**
 * <p>
 * 过滤IP Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
public interface FilterTimeMapper extends BaseMapper<FilterTime> {

}
