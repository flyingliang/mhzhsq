package com.lzxuni.modules.organization.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.organization.entity.Authorize;

import java.util.List;

/**
 * <p>
 * 授权功能表 Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
public interface AuthorizeMapper extends BaseMapper<Authorize> {
	List<String> listItemIds(Authorize authorize);
}
