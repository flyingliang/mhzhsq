package com.lzxuni.modules.organization.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.organization.entity.Role;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.organization.entity.UserRelation;

import java.util.List;

/**
 * <p>
 * 用户关系表 Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
public interface UserRelationMapper extends BaseMapper<UserRelation> {
	List<User> getUserIdList(String objectId);
	List<Role> getRoleIdList(String userId);

}
