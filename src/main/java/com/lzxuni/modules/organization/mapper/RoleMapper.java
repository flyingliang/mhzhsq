package com.lzxuni.modules.organization.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.organization.entity.Role;

/**
 * <p>
 * 角色信息表 Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
public interface RoleMapper extends BaseMapper<Role> {

}
