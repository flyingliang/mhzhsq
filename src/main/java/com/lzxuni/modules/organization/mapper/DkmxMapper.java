package com.lzxuni.modules.organization.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Dkmx;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 银行管理-贷款模型 Mapper 接口
 * </p>
 *
 * @author 韩蓄
 * @since 2019-01-24
 */
@Mapper
public interface DkmxMapper extends BaseMapper<Dkmx> {

    List<Dkmx> queryListByDkmx(Dkmx dkmx);

    List<Dkmx> queryListByDkmx2(Dkmx dkmx);

    Dkmx editByproductId(@Param("productId") String productId);

    List<Tree> queryList(Map map);

}
