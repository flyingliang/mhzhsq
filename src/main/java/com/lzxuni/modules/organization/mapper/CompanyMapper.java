package com.lzxuni.modules.organization.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Company;
import com.lzxuni.modules.organization.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 *@Title CompanyMapper.java
 *@description 
 *@author 孙志强
 *@time 2016年11月4日 下午3:57:10
 *@version 1.0
 **/
@Mapper
public interface CompanyMapper extends BaseMapper<Company> {
	List<Tree> queryListByParentId(String parentId) ;
	List<Tree> queryListByNature(String nature) ;
	List<Tree> cooperativeAssociationTree(Map map);

	List<Company> queryList(@Param("company") Company company, @Param("user") User user);

}
