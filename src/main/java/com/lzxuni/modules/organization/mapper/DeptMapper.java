package com.lzxuni.modules.organization.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Dept;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 单位部门表 Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
public interface DeptMapper extends BaseMapper<Dept> {
	List<Tree> queryListByParentId(@Param("parentId") String parentId, @Param("companyId") String companyId) ;
}
