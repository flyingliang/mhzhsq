package com.lzxuni.modules.organization.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.organization.entity.CustomerMember;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 王智贤
 * @since 2019-01-27
 */
public interface CustomerMemberMapper extends BaseMapper<CustomerMember> {

}
