package com.lzxuni.modules.organization.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.organization.entity.Role;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.organization.entity.UserRelation;
import com.lzxuni.modules.organization.mapper.UserRelationMapper;
import com.lzxuni.modules.organization.service.UserRelationService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户关系表 服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
@Service
public class UserRelationServiceImpl extends ServiceImpl<UserRelationMapper, UserRelation> implements UserRelationService {

	@Override
	public Map<String, Object> getUserIdList(String objectId) {
		Map<String, Object> map = new HashMap<>();
		String userIds = "";


		List<User> userInfoList = baseMapper.getUserIdList(objectId);
		if (userInfoList != null && userInfoList.size() > 0) {
			for (User user : userInfoList) {
				userIds = userIds+user.getUserId() + ",";
			}
			userIds = userIds.substring(0, userIds.length() - 1);
		}


		map.put("userIds", userIds);
		map.put("userInfoList", userInfoList);

		return map;
	}

	@Override
	public Boolean insertOrUpdate(User sessionUser,String[] userIds, UserRelation userRelation) {
		this.remove(new QueryWrapper<UserRelation>().eq("object_id", userRelation.getObjectId()));
		if (userIds != null && userIds.length > 0) {
			for (String userId:userIds) {
				userRelation.setUserRelationId(UuidUtil.get32UUID());
				userRelation.setUserId(userId);
				if (sessionUser != null) {
					userRelation.setCreateUserId(sessionUser.getUserId());
					userRelation.setCreateUsername(sessionUser.getUserId());
				}
				this.save(userRelation);
			}
		}
		return true;
	}

	@Override
	public Map<String, Object> getRoleIdList(String userId) {
		Map<String, Object> map = new HashMap<>();
		String roleIds = "";


		List<Role> roleInfoList = baseMapper.getRoleIdList(userId);
		if (roleInfoList != null && roleInfoList.size() > 0) {
			for (Role role : roleInfoList) {
				roleIds = roleIds+role.getRoleId() + ",";
			}
			roleIds = roleIds.substring(0, roleIds.length() - 1);
		}


		map.put("roleIds", roleIds);
		map.put("roleInfoList", roleInfoList);

		return map;
	}

	@Override
	public Boolean insertOrUpdateRole(User sessionUser, String[] roleIds, UserRelation userRelation) {
		this.remove(new QueryWrapper<UserRelation>().eq("user_id", userRelation.getUserId()));
		if (roleIds != null && roleIds.length > 0) {
			for (String roleId:roleIds) {
				userRelation.setUserRelationId(UuidUtil.get32UUID());
				userRelation.setObjectId(roleId);
				if (sessionUser != null) {
					userRelation.setCreateUserId(sessionUser.getUserId());
					userRelation.setCreateUsername(sessionUser.getUserId());
				}
				this.save(userRelation);
			}
		}
		return true;
	}
}
