package com.lzxuni.modules.organization.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.FormatConvert;
import com.lzxuni.common.utils.poi.ExcelUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Company;
import com.lzxuni.modules.organization.entity.Customer;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.organization.mapper.CompanyMapper;
import com.lzxuni.modules.organization.service.CompanyService;
import com.lzxuni.modules.organization.service.CustomerService;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 机构单位表 服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-05
 */
@Service
public class CompanyServiceImpl extends ServiceImpl<CompanyMapper, Company> implements CompanyService {

	@Autowired
	private CompanyMapper companyMapper;

	@Autowired
	private CustomerService customerService;

	@Override
	public List<Tree> getTree(String parentId) {
		List<Tree> list = baseMapper.queryListByParentId(parentId);

		getTreeDg(list);
		return list;
	}

	@Override
	public Boolean insert(User user, Company company) {
		if (user != null) {
			company.setCreateUserId(user.getUserId());
			company.setCreateUsername(user.getUsername());
		}
		if(company.getParentId().equals("-1")){
			company.setParentId("0");
		}
		return this.save(company);
	}

	@Override
	public Boolean update(User user, Company company) {
		if (user != null) {
			company.setModifyUserId(user.getUserId());
			company.setModifyUsername(user.getUsername());
		}
		if(company.getParentId().equals("-1")){
			company.setParentId("0");
		}
		return this.updateById(company);
	}

	@Override
	public Object getMap(String ver) {
		List<Company> list = this.list();
		Map<String, Object> map = new HashMap<>();
		Map<String, Object> map1 = new HashMap<>();

		for (int i = 0; i < list.size(); i++) {
			HashMap<String, Object> map2 = new HashMap<>();
			map2.put("parentId", list.get(i).getParentId());
			map2.put("name", list.get(i).getFullName());

			map1.put(list.get(i).getCompanyId(), map2);
		}
		map.put("data", map1);
		map.put("ver", "123");
		return map;
	}

	@Override
	public PageInfo<Company> queryPage(PageParameter pageParameter, Company company, User user) {
		PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(pageParameter.getSidx() + " " + pageParameter.getSord());
		List<Company> companies = companyMapper.queryList(company, user);
		if (companies != null && companies.size() > 0) {
			for (Company company1 : companies) {
				Integer i = customerService.count(new QueryWrapper<Customer>().eq("custom_cooperative_id", company1.getCompanyId()));
				company1.setCount(i.toString());
				if(user.hashRole("演示")){
					company1.setFullName(FormatConvert.substring(company1.getFullName()));
					company1.setManager(FormatConvert.replaceNameX(company1.getManager()));
					company1.setOuterPhone(FormatConvert.replacePhoneNumber(company1.getOuterPhone()));
					company1.setAddress("****************");
					company1.setCount("****");
				}
			}
		}
		PageInfo<Company> pageInfo = new PageInfo<>(companies);
		return pageInfo;
	}

	@Override
	public Workbook exportExcel(Company company, User user) throws Exception {
		LinkedHashMap<String, List<List<String>>> dataMap = new LinkedHashMap<>();
		List<List<String>> listData1 = new ArrayList<>();
		Workbook wb = null;
		List<Company> companies = companyMapper.queryList(company, user);
		List<String> list0 = new ArrayList<>();
		list0.add("合作社名称");
		list0.add("法人姓名");
		list0.add("法人电话");
		list0.add("省");
		list0.add("市");
		list0.add("县");
		list0.add("镇");
		list0.add("村");
		list0.add("详细地址");
		list0.add("注册时间");
		list0.add("农户数量");
		listData1.add(list0);
		if (companies != null && companies.size() > 0) {
			for (int i = 0; i < companies.size(); i++) {
				Company company1 = companies.get(i);
				Integer o = customerService.count(new QueryWrapper<Customer>().eq("custom_cooperative_id", company1.getCompanyId()));
				company1.setCount(o.toString());
				if (user.hashRole("演示")) {
					company1.setFullName(FormatConvert.substring(company1.getFullName()));
					company1.setManager(FormatConvert.replaceNameX(company1.getManager()));
					company1.setOuterPhone(FormatConvert.replacePhoneNumber(company1.getOuterPhone()));
					company1.setAddress("****************");
					company1.setCount("****");
				}
				List<String> list = new ArrayList<>();
				list.add(company1.getFullName());
				list.add(company1.getManager());
				list.add(company1.getOuterPhone());
				list.add(company1.getProName());
				list.add(company1.getCityName());
				list.add(company1.getCountyName());
				list.add(company1.getTownName());
				list.add(company1.getVillageName());
				list.add(company1.getAddress());
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				list.add(sdf.format(company1.getCreateDate()));
				list.add(company1.getCount());
				listData1.add(list);
			}
		}
		dataMap.put("合作社统计", listData1);
		ExcelUtil excelUtil = new ExcelUtil();
		wb = excelUtil.writeExcel(dataMap, "合作社统计");
		return wb;
	}

	/*私有方法区域*/
	private void getTreeDg(List<Tree> ztreeList) {
		for (int i = 0; i < ztreeList.size(); i++) {
			Tree tree = ztreeList.get(i);
			List<Tree> treeList = baseMapper.queryListByParentId(tree.getId());
			if (treeList != null && treeList.size() > 0) {
				tree.setChildNodes(treeList);
				tree.setHasChildren(true);
				tree.setIsexpand(true);
			}
			getTreeDg(treeList);
		}
	}
}
