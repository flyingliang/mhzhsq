package com.lzxuni.modules.organization.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Dkmx;
import com.lzxuni.modules.organization.entity.User;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 银行管理-贷款模型 服务类
 * </p>
 *
 * @author 韩蓄
 * @since 2018-12-29
 */
public interface BankDkmxService extends IService<Dkmx> {
    List<Dkmx> getListByDkmx(Dkmx dkmx);

    List<Dkmx> getListByDkmx2(Dkmx dkmx);

    void insert(User user, Dkmx dkmx);

    void update(User user, Dkmx dkmx);

    Dkmx editByproductId(String productId);

    List<Tree> queryList(Map mp);

}
