package com.lzxuni.modules.organization.service;

//import com.baomidou.mybatisplus.extension.service.IService;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.User;

import java.util.List;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
public interface UserService extends IService<User> {
	PageInfo<User> queryPage(PageParameter pageParameter, User user);
	Boolean insert(User sessionUser, User user);
	Boolean update(User sessionUser, User user);
	void updateLastLoginTime(String username);
	Boolean delete(String userId);

	boolean validationOldPassword(String userId, String oldPassword);
	/**
	 * 修改密码
	 * @param userId       用户ID
	 * @param password     原密码
	 * @param newPassword  新密码
	 */
	boolean updatePassword(String userId, String password, String newPassword);


	/**
	 * 功能描述: 根据userId重置密码<br>
	 * @Author 孙志强
	 * @date 2018/6/2 22:59
	 * @param userId
	 * @return:boolean
	 */
	boolean resetPassword(String userId);

	User queryByUserName(String username);


	Boolean isHzsOrJxs(User user);
	Boolean isYhOrDbgsOrPt(User user);

    List<Tree> getTree();

}
