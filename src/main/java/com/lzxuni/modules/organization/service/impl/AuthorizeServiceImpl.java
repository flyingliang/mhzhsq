package com.lzxuni.modules.organization.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.organization.entity.Authorize;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.organization.mapper.AuthorizeMapper;
import com.lzxuni.modules.organization.service.AuthorizeService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 授权功能表 服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
@Service
public class AuthorizeServiceImpl extends ServiceImpl<AuthorizeMapper, Authorize> implements AuthorizeService {

	@Override
	public Map<String, Object> getFormData(String objectId, Integer objectType) {
		Map<String, Object> map = new HashMap<>();
		Authorize authorize = new Authorize();
		authorize.setObjectType(objectType);
		authorize.setObjectId(objectId);

		//功能
		authorize.setItemType(1);
		List<String> modules = baseMapper.listItemIds(authorize);
		//按钮
		authorize.setItemType(2);
		List<String> buttons = baseMapper.listItemIds(authorize);
		//表格
		authorize.setItemType(3);
		List<String> columns = baseMapper.listItemIds(authorize);
		//表单
		authorize.setItemType(4);
		List<String> forms = baseMapper.listItemIds(authorize);

		map.put("modules", modules);
		map.put("buttons", buttons);
		map.put("columns", columns);
		map.put("forms", forms);
		return map;
	}

	@Override
	public Boolean insertOrUpdate(User sessionUser, Authorize authorize, String[] strModuleId, String[] strModuleButtonId, String[] strModuleModuleColumnId, String[] strModuleFormId) {
		//1先删除关联表--条件 base_authorize 中 object_id=authorize.getObjectId()
		this.remove(new QueryWrapper<Authorize>().eq("object_id", authorize.getObjectId()));
		//2保存关联表 其中item_type表示项目类型:1-菜单2-按钮3-视图4-表单5-app功能

		//2.1保存系统功能表
		if (strModuleId != null && strModuleId.length > 0) {
			for (int i = 0; i < strModuleId.length; i++) {
				Authorize authorizeItem = new Authorize();
				BeanUtils.copyProperties(authorize,authorizeItem);
				authorizeItem.setItemId(strModuleId[i]);
				authorizeItem.setItemType(1);
				if (sessionUser != null) {
					authorizeItem.setCreateUserId(sessionUser.getUserId());
					authorizeItem.setCreateUsername(sessionUser.getUsername());
				}
				this.save(authorizeItem);
			}
		}
		//2.2保存功能按钮表
		if (strModuleButtonId != null && strModuleButtonId.length > 0) {
			for (int i = 0; i < strModuleButtonId.length; i++) {
				Authorize authorizeItem = new Authorize();
				BeanUtils.copyProperties(authorize,authorizeItem);
				authorizeItem.setItemId(strModuleButtonId[i]);
				authorizeItem.setItemType(2);
				if (sessionUser != null) {
					authorizeItem.setCreateUserId(sessionUser.getUserId());
					authorizeItem.setCreateUsername(sessionUser.getUsername());
				}
				this.save(authorizeItem);
			}
		}
		//2.3保存功能表格列表
		if (strModuleModuleColumnId != null && strModuleModuleColumnId.length > 0) {
			for (int i = 0; i < strModuleModuleColumnId.length; i++) {
				Authorize authorizeItem = new Authorize();
				BeanUtils.copyProperties(authorize,authorizeItem);
				authorizeItem.setItemId(strModuleModuleColumnId[i]);
				authorizeItem.setItemType(3);
				if (sessionUser != null) {
					authorizeItem.setCreateUserId(sessionUser.getUserId());
					authorizeItem.setCreateUsername(sessionUser.getUsername());
				}
				this.save(authorizeItem);
			}
		}
		//2.4保存功能表单字段
		if (strModuleFormId != null && strModuleFormId.length > 0) {
			for (int i = 0; i < strModuleFormId.length; i++) {
				Authorize authorizeItem = new Authorize();
				BeanUtils.copyProperties(authorize,authorizeItem);
				authorizeItem.setItemId(strModuleFormId[i]);
				authorizeItem.setItemType(4);
				if (sessionUser != null) {
					authorizeItem.setCreateUserId(sessionUser.getUserId());
					authorizeItem.setCreateUsername(sessionUser.getUsername());
				}
				this.save(authorizeItem);
			}
		}
		return true;
	}

}
