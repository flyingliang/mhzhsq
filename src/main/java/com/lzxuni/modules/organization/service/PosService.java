package com.lzxuni.modules.organization.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Goods;
import com.lzxuni.modules.organization.entity.Pos;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 杨国营
 * @since 2019-02-28
 */
public interface PosService extends IService<Pos> {

	List<Tree> GetTree(Map map);

	Goods QueryGoodAndFactory(String goodId);
}
