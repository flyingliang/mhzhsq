package com.lzxuni.modules.organization.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.organization.entity.Filterip;
import com.lzxuni.modules.organization.mapper.FilteripMapper;
import com.lzxuni.modules.organization.service.FilteripService;
import org.springframework.stereotype.Service;

//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * <p>
 * 过滤IP 服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
@Service
public class FilteripServiceImpl extends ServiceImpl<FilteripMapper, Filterip> implements FilteripService {

}
