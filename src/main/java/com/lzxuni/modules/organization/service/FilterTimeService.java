package com.lzxuni.modules.organization.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.organization.entity.FilterTime;

/**
 * <p>
 * 过滤IP 服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
public interface FilterTimeService extends IService<FilterTime> {

}
