package com.lzxuni.modules.organization.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Goods;
import com.lzxuni.modules.organization.entity.Pos;
import com.lzxuni.modules.organization.mapper.PosMapper;
import com.lzxuni.modules.organization.service.PosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 杨国营
 * @since 2019-02-28
 */
@Service
public class PosServiceImpl extends ServiceImpl<PosMapper, Pos> implements PosService {

	@Autowired
    PosMapper posMapper;
	@Override
	public List<Tree> GetTree(Map map) {
		return posMapper.GetTree(map);
	}

	@Override
	public Goods QueryGoodAndFactory(String goodId) {
		return posMapper.QueryGoodAndFactory(goodId);
	}
}
