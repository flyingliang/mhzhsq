package com.lzxuni.modules.organization.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Company;
import com.lzxuni.modules.organization.entity.User;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

/**
 * <p>
 * 机构单位表 服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-05
 */
public interface CompanyService extends IService<Company> {
	List<Tree> getTree(String parentId);

	Boolean insert(User user, Company company);
	Boolean update(User user, Company company);

	Object getMap(String ver);

	PageInfo<Company> queryPage(PageParameter pageParameter, Company company, User user);

	Workbook exportExcel(Company company, User user)throws Exception;
}
