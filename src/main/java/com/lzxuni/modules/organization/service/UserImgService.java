package com.lzxuni.modules.organization.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.organization.entity.UserImg;

/**
 * <p>
 * 用户图片表 服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-13
 */
public interface UserImgService extends IService<UserImg> {

}
