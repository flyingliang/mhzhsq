package com.lzxuni.modules.organization.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.common.utils.FormatConvert;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.common.utils.poi.ExcelUtil;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Company;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.organization.mapper.CompanyMapper;
import com.lzxuni.modules.organization.service.CooperativeService;
import com.lzxuni.modules.system.entity.BaseArea;
import com.lzxuni.modules.system.mapper.BaseAreaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 机构单位表 服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-05
 */
@Service
public class CooperativeServiceImpl extends ServiceImpl<CompanyMapper, Company> implements CooperativeService {

	@Autowired
	private BaseAreaMapper baseAreaMapper;
	@Override
	public List<Tree> getTree(String parentId) {
		List<Tree> list = baseMapper.queryListByParentId(parentId);

		getTreeDg(list);
		return list;
	}

	@Override
	public Boolean insert(User user, Company company) {
		if (user != null) {
			company.setCreateUserId(user.getUserId());
			company.setCreateUsername(user.getUsername());
		}
		if (StringUtils.isEmpty(company.getParentId())) {
			company.setParentId("0");
		}else{
			if(company.getParentId().equals("-1")){
				company.setParentId("0");
			}
		}

		return this.save(company);
	}

	@Override
	public Boolean update(User user, Company company) {
		if (user != null) {
			company.setModifyUserId(user.getUserId());
			company.setModifyUsername(user.getUsername());
		}
		if (StringUtils.isEmpty(company.getParentId())) {
			company.setParentId("0");
		}else{
			if(company.getParentId().equals("-1")){
				company.setParentId("0");
			}
		}
		return this.updateById(company);
	}

	@Override
	public Object getMap(String ver) {
		List<Company> list = this.list();
		Map<String, Object> map = new HashMap<>();
		Map<String, Object> map1 = new HashMap<>();

		for (int i = 0; i < list.size(); i++) {
			HashMap<String, Object> map2 = new HashMap<>();
			map2.put("parentId", list.get(i).getParentId());
			map2.put("name", list.get(i).getFullName());

			map1.put(list.get(i).getCompanyId(), map2);
		}
		map.put("data", map1);
		map.put("ver", "123");
		return map;
	}

	@Override
	public List<Tree> getTreeSelectTree(String nature, User user) {
		List<Tree> list = baseMapper.queryListByNature(nature);
		if (user != null && user.hashRole("演示")) {
			if (nature.equals("cooperative")) {
				for (Tree tree : list) {
					tree.setText(FormatConvert.substring(tree.getText()));
				}
			}
		}
		return list;
	}
	@Override
	public List<Tree> cooperativeAssociationTree(Map map) {
		List<Tree> list = baseMapper.cooperativeAssociationTree(map);
		return list;
	}

	@Override
	public Map<String, String> readExcelData(InputStream is, String originalFilename, String keyValue,User user) {
		ExcelUtil excelUtil = new ExcelUtil();
		int successmsg = 0;
		int errormsg = 0;
		int adderrormsg = 0;
		int readline = 3;
		StringBuffer errorNum = new StringBuffer();
		//工具类的作用就是将数据转换成map的形式
		Map<String, List<String[]>> data = excelUtil.readExcel(is, 2);
		for (Map.Entry<String, List<String[]>> entry : data.entrySet()) {
			//每个key就是一个sheet的名字，如果是多层可以进行判断，然后分别进行添加
			//如果是一个表的话就没关系了
			String key = entry.getKey();
			System.out.println("key:" + key);
			/**
			 * 每个值就是一个row的数据，一个row又是一个数组里面包含一行单元格的数据
			 * 1.解决思路每一行遍历然后取值，给公司的对象赋值，然后进行添加操作,一个Value就是一个对象
			 * 2.有两个计数的一个是记录成功的数
			 * 3.另一个是记录失败的数，将数据放在一个map中返回到页面进行利用，就可以了
			 */
			List<String[]> value = entry.getValue();
			//外层表示一行数据（一共多少行）
			for (int i = 0; i < value.size(); i++) {
				Company company = new Company();
				company.setCompanyId(UuidUtil.get32UUID());
				company.setParentId("0");
				//里层代表的是每列的单元格的数量我们不应该把每列都写出来，因为如果excel表格中String[]添加的不够对应的列数就会报数组越界的错误
				//并不是把空格转换成了""而就是没有 所以要先判断数组的长度是不是大于对应的下角标，如果大于才能进行里面的操作
				//所以我用了一个&& 短路的且操作，如果前面满足才进行里面的操作，否则不判断后面的
				if(value.get(i).length>0 && StringUtils.isNotEmpty(value.get(i)[0])){
					//在这个位置判断合作社的名字是不是重复如果重复的话不要将这行添加按着错误行进行处理然后手动去除
					//先根据名字进行查询合作社看看合作社是不是存在
					Company company1 = this.getOne(new QueryWrapper<Company>().eq("full_name", value.get(i).length>0?value.get(i)[0]:""));
					if(company1 ==null){
						//没有重复的进行赋值，重复了当做错误的进行添加
						company.setFullName(value.get(i)[0]);
					}else{
						errormsg++;
						errorNum.append((i+readline)+",");
						continue;
					}
				}else{
					errormsg++;
					errorNum.append((i+readline)+",");
					continue;
				}
				if(value.get(i).length>1 && StringUtils.isNotEmpty(value.get(i)[1])){
					company.setEnCode(value.get(i)[1]);
				}else{
					errormsg++;
					errorNum.append((i+readline)+",");
					continue;
				}
				if(value.get(i).length>2 && StringUtils.isNotEmpty(value.get(i)[2])){
					company.setShortName(value.get(i)[2]);
				}else{
					errormsg++;
					errorNum.append((i+readline)+",");
					continue;
				}
				company.setNature("cooperative");
				if(value.get(i).length>3){
					company.setManager(value.get(i)[3]);
				}
				if(value.get(i).length>4){
					company.setOuterPhone(value.get(i)[4]);
				}
				if(value.get(i).length>5){
					company.setLxr(value.get(i)[5]);
				}
				if(value.get(i).length>6){
					company.setLxrPhone(value.get(i)[6]);
				}
				//由于excel输入的是汉字所以我们要根据汉字来查询区域的id然后存入表中才行
				//这个也要进行判断的否则也要数组越界的，
				BaseArea proArea = baseAreaMapper.selectOne(new QueryWrapper<BaseArea>().eq("area_name", value.get(i).length>7?value.get(i)[7]:""));
				if(proArea !=null){
					company.setProvinceId(proArea.getAreaId());
				}else{
					errormsg++;
					errorNum.append((i+readline)+",");
					continue;
				}
				BaseArea cityArea = baseAreaMapper.selectOne(new QueryWrapper<BaseArea>().eq("area_name", value.get(i).length>8?value.get(i)[8]:"")
																						 .eq("parent_id", proArea.getAreaId()));
				if(cityArea !=null){
					company.setCityId(cityArea.getAreaId());
				}else{
					errormsg++;
					errorNum.append((i+readline)+",");
					continue;
				}
				BaseArea countryArea = baseAreaMapper.selectOne(new QueryWrapper<BaseArea>().eq("area_name", value.get(i).length>9?value.get(i)[9]:"")
																						    .eq("parent_id", cityArea.getAreaId()));
				if(countryArea !=null){
					company.setCountyId(countryArea.getAreaId());
				}else{
					errormsg++;
					errorNum.append((i+readline)+",");
					continue;
				}
				BaseArea townArea = baseAreaMapper.selectOne(new QueryWrapper<BaseArea>().eq("area_name", value.get(i).length>10?value.get(i)[10]:"")
						                                                                 .eq("parent_id", countryArea.getAreaId()));
				if(townArea != null){
					company.setTownId(townArea.getAreaId());
					//如果镇没有添加但是只添加了一个村子虽然excel表格中有但是实际是添加不上的
					BaseArea villageArea = baseAreaMapper.selectOne(new QueryWrapper<BaseArea>().eq("area_name", value.get(i).length>11?value.get(i)[11]:"")
							                                                                    .eq("parent_id", townArea.getAreaId()));
					if(villageArea != null){
						company.setVillageId(villageArea.getAreaId());
					}
				}
				if(value.get(i).length>12){
					company.setAddress(value.get(i)[12]);
				}
				if(value.get(i).length>13){
					company.setDescription(value.get(i)[13]);
				}
				company.setCreateDate(new Date());
				if (user != null) {
					company.setCreateUserId(user.getUserId());
					company.setCreateUsername(user.getUsername());
				}
				/**
				 * 前面都通过之后再进行添加的操作
				 */
				boolean flag = this.save(company);
				if(flag){
					//成功增加一条
					successmsg++;
				}else{
					//这个原因是Dao层的错误信息了，不是表单验证的信息了
					errorNum.append((i+readline)+",");
					adderrormsg++;
				}
			}
		}
		Map<String, String> map =new HashMap<>();
		map.put("successmsg", String.valueOf(successmsg));
		map.put("errormsg", String.valueOf(errormsg+adderrormsg));
		//如果没有错误行的话,这行就是空的字符串了
		String errorNummsg = "";
		//如果有一行的话就是例如3，字符串的长度就大于1了
		if(errorNum.toString().length()>1){
			 errorNummsg = errorNum.toString().substring(0, errorNum.toString().length()-1);
		}
		map.put("errorNum", errorNummsg);
		return map;
	}


	/*私有方法区域*/
	private void getTreeDg(List<Tree> ztreeList) {
		for (int i = 0; i < ztreeList.size(); i++) {
			Tree tree = ztreeList.get(i);
			List<Tree> treeList = baseMapper.queryListByParentId(tree.getId());
			if (treeList != null && treeList.size() > 0) {
				tree.setChildNodes(treeList);
				tree.setHasChildren(true);
				tree.setIsexpand(true);
			}
			getTreeDg(treeList);
		}
	}
}
