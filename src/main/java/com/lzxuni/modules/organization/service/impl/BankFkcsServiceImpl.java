package com.lzxuni.modules.organization.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.organization.entity.Fkcs;
import com.lzxuni.modules.organization.entity.FkcsCondition;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.organization.mapper.BankFkcsConditionMapper;
import com.lzxuni.modules.organization.mapper.BankFkcsMapper;
import com.lzxuni.modules.organization.service.BankFkcsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import static com.lzxuni.modules.shiro.ShiroUtils.getUser;

/**
 * @author 杨国营
 * @create 2019-01-04-15:46
 * 快捷键：
 * 1.psvm主方法
 * 2.sout+p+m+v、xxx.sout(变量输出)
 * 3.fori、iter、itar(赋值),循环
 * 4.list.for、list.fori、list.forr倒序遍历
 * 5.ifn 、inn、xxx.nn等于xxx.null
 * 6.prsf、psf、psfi、psfs 静态常量
 * 7.alt+enter--enter 可以快速生成左边的变量
 * 8.shift+enter 新建下一行
 * 9.alt+enter 导jar包
 * 10.shift+上/下 上下选中
 * 11.ctrl+p查看构造器参数ctrl+下（只能一次一次选择）自动补全
 * 12.ctrl+alt+t 选中代码添加try catch
 * 13.Ctrl+Shift+Alt+N 知道类名全局查找该类
 * 14.alt+7查看当前类的所有方法
 * 15.alt+F8查看变量的值或者鼠标悬停点击绿色+
 * 16.ctrl+alt+u/ctrl+alt+shift+u(可看方法) :上面的tree是类的结构，下面的是该类属于哪个jar包
 * 17.ctrl+H可以查看父类和子类
 * 18.ctrl+o重写父类方法
 * 19.alt+Fn+Insert:新建文件夹等
 * 20.ctrl+alt+b:选中控制类方法，直接进入实现类
 * 21.alt+enter:快速显示红线的意义
 * 22.选中两个文件，右键，compare Files:比较两个文件
 */
@Service
public class BankFkcsServiceImpl extends ServiceImpl<BankFkcsMapper,Fkcs> implements BankFkcsService {
	@Autowired
    BankFkcsMapper bankFkcsMapper;
	@Autowired
	BankFkcsConditionMapper bankFkcsConditionMapper;
	@Override
	public List<Fkcs> queryListByFkcs(Fkcs fkcs) {
		List<Fkcs> fkcses = bankFkcsMapper.queryListByFkcs(fkcs);
		return fkcses;
	}

	/**
	 * 需要根据内容保存到两个表中fkcs,fkcs-condition
	 * 这种情况一定要添加事物
	 * @param fkcs
	 */
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void SaveForm(Fkcs fkcs) {
		User user = getUser();
		if (user != null) {
			fkcs.setCreateUserId(user.getUserId());
			fkcs.setCreateUsername(user.getUsername());
		}
		//首先保存数据到fkcs 表中
		String uuid = UuidUtil.get32UUID();
		//设置parameterId
		fkcs.setParameterId(uuid);
		//设置formula
		fkcs.setFormula((String) JSON.parseObject(fkcs.getRelation()).get("F_Formula"));
		//添加年度信息
		fkcs.setYear((String) JSON.parseObject(fkcs.getRelation()).get("F_Name"));
		//添加到fkcs表中
		bankFkcsMapper.insert(fkcs);
		//添加另一个表中的数据
		List objList = JSONArray.parseArray(fkcs.getFkcsConditionList());
		for (Object o : objList) {
			Map map = (Map)o;
			FkcsCondition fkcsCondition = new FkcsCondition();
			if (user != null) {
				fkcsCondition.setCreateUserId(user.getUserId());
				fkcsCondition.setCreateUsername(user.getUsername());
			}
			fkcsCondition.setId(UuidUtil.get32UUID());
			fkcsCondition.setFieldName((String) map.get("F_FieldName"));
			fkcsCondition.setSymbol((String)map.get("F_SymbolName"));
			fkcsCondition.setFiledValue((String)map.get("F_FiledValue"));
			fkcsCondition.setFiledValueType((String)map.get("F_FieldId"));
			fkcsCondition.setParameterId(uuid);
			bankFkcsConditionMapper.insert(fkcsCondition);
		}

	}

	/**
	 * 进行更改操作，同样需要更改两个表中的参数
	 * 1.根据ParameterId 更改 fkcs表中的数据
	 * @param fkcs
	 */
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateForm(Fkcs fkcs) {
		User user = getUser();
		if (user != null) {
			fkcs.setModifyUserId(user.getUserId());
			fkcs.setModifyUsername(user.getUsername());
		}
		//设置formula
		fkcs.setFormula((String) JSON.parseObject(fkcs.getRelation()).get("F_Formula"));
		//更改年度信息
		fkcs.setYear((String) JSON.parseObject(fkcs.getRelation()).get("F_Name"));
		//根据id进行修改
		bankFkcsMapper.updateById(fkcs);
		//更改另一个表，先删除，后按着ParameterId进行添加就可以了
		//根据条件删除
		bankFkcsConditionMapper.delete(new QueryWrapper<FkcsCondition>().eq("parameter_id", fkcs.getParameterId()));
		//然后再进行添加操作
		//添加另一个表中的数据根据fkcs.getParameterId()
		//由于是重新添加的所以没有修改的modify_data 所以我们统一调整成创建的时间了，要查询修改时间查询主表即可
		List objList = JSONArray.parseArray(fkcs.getFkcsConditionList());
		for (Object o : objList) {
			Map map = (Map)o;
			FkcsCondition fkcsCondition = new FkcsCondition();
			if (user != null) {
				fkcsCondition.setCreateUserId(user.getUserId());
				fkcsCondition.setCreateUsername(user.getUsername());
			}
			fkcsCondition.setId(UuidUtil.get32UUID());
			fkcsCondition.setFieldName((String) map.get("F_FieldName"));
			fkcsCondition.setSymbol((String)map.get("F_SymbolName"));
			fkcsCondition.setFiledValue((String)map.get("F_FiledValue"));
			fkcsCondition.setFiledValueType((String)map.get("F_FieldId"));
			fkcsCondition.setParameterId(fkcs.getParameterId());
			bankFkcsConditionMapper.insert(fkcsCondition);
		}

	}
}
