package com.lzxuni.modules.organization.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.organization.entity.Customer;
import com.lzxuni.modules.organization.mapper.CustomerMapper;
import com.lzxuni.modules.organization.service.CustomerService;
import com.lzxuni.modules.shiro.ShiroUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class CustomerServiceImpl extends ServiceImpl<CustomerMapper, Customer> implements CustomerService {

    @Autowired
    private CustomerMapper customerMapper;

    @Override
    public PageInfo<Customer> queryPage(PageParameter pageParameter, Customer customer) {
        PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(pageParameter.getSidx()+" "+pageParameter.getSord());
        List<Customer> customerList = customerMapper.queryList(customer);
        PageInfo<Customer> pageInfo = new PageInfo<>(customerList);
        return pageInfo;
    }

    @Override
    public Boolean insert(Customer customer) {
        birthday(customer);
        // 初始密码
        String password = "111111";
        if(StringUtils.isNotEmpty(customer.getCustomPassword())){
            password = customer.getCustomPassword();
        }
        // 密码加密
        //sha256加密
        SimpleHash passwordMd5 = new SimpleHash(ShiroUtils.hashAlgorithmName, password,customer.getCustomPhone(), ShiroUtils.hashIterations);
        customer.setCustomPassword(passwordMd5.toString());
        return this.save(customer);
    }

    @Override
    public Boolean update(Customer customer) {
        birthday(customer);
        return this.updateById(customer);
    }

    @Override
    public Boolean delete(String customId) {
        return this.delete(customId);
    }

    public void birthday(Customer customer){
        String code = customer.getCustomCode();
        if(StringUtils.isNotEmpty(code) && (code.length() == 18 || code.length() == 15)){
            String year = code.substring(6, 10);
            String month =code.substring(10, 12);
            String day = code.substring(12, 14);
            String birthday = year + "-" + month + "-" + day;
            DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = fmt.parse(birthday);
                customer.setCustomBirthday(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
}
