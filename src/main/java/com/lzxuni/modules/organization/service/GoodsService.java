package com.lzxuni.modules.organization.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.organization.entity.Goods;

import java.util.List;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author liuzp
 * @since 2019-01-08
 */
public interface GoodsService extends IService<Goods> {
    PageInfo<Goods> queryPage(PageParameter pageParameter, String keyword);
    //给商品表中的合作社和生资企业名称赋值
    List<Goods> queryGoods(List<Goods> goodsList);
}
