package com.lzxuni.modules.organization.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.organization.entity.Customer;

public interface CustomerService extends IService<Customer> {

    PageInfo<Customer> queryPage(PageParameter pageParameter, Customer customer);
    Boolean insert(Customer customer);
    Boolean update(Customer customer);
    Boolean delete(String customId);

}
