package com.lzxuni.modules.organization.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.organization.entity.FilterTime;
import com.lzxuni.modules.organization.mapper.FilterTimeMapper;
import com.lzxuni.modules.organization.service.FilterTimeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 过滤IP 服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
@Service
public class FilterTimeServiceImpl extends ServiceImpl<FilterTimeMapper, FilterTime> implements FilterTimeService {

}
