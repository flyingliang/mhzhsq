package com.lzxuni.modules.organization.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.organization.entity.Role;
import com.lzxuni.modules.organization.entity.User;

/**
 * <p>
 * 角色信息表 服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
public interface RoleService extends IService<Role> {
	PageInfo<Role> queryPage(PageParameter pageParameter, Role role);
	Boolean insert(User sessionUser, Role role);
	Boolean update(User sessionUser, Role role);
	Boolean delete(String roleId);
}
