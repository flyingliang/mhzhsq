package com.lzxuni.modules.organization.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.organization.entity.CustomerMember;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 王智贤
 * @since 2019-01-27
 */
public interface CustomerMemberService extends IService<CustomerMember> {

    List<CustomerMember> queryList(String loanId);
}
