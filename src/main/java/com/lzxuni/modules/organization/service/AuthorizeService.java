package com.lzxuni.modules.organization.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.organization.entity.Authorize;
import com.lzxuni.modules.organization.entity.User;

import java.util.Map;

/**
 * <p>
 * 授权功能表 服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
public interface AuthorizeService extends IService<Authorize> {
	Map<String, Object> getFormData(String objectId, Integer objectType);

	Boolean insertOrUpdate(User sessionUser, Authorize authorize, String[] strModuleId, String[] strModuleButtonId,
                           String[] strModuleModuleColumnId, String[] strModuleFormId);

}
