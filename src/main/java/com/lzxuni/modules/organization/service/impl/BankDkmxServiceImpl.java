package com.lzxuni.modules.organization.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Company;
import com.lzxuni.modules.organization.entity.Dkmx;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.organization.mapper.DkmxMapper;
import com.lzxuni.modules.organization.service.BankDkmxService;
import com.lzxuni.modules.organization.service.CompanyService;
import com.lzxuni.modules.system.entity.BaseArea;
import com.lzxuni.modules.system.service.BaseAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 银行管理-贷款模型 服务实现类
 * </p>
 *
 * @author 韩蓄
 * @since 2018-12-29
 */
@Service
public class BankDkmxServiceImpl extends ServiceImpl<DkmxMapper, Dkmx> implements BankDkmxService {

    @Autowired
    DkmxMapper dkmxMapper;
    @Autowired
    CompanyService companyService;
    @Autowired
    private BaseAreaService areaService;

    @Override
    public List<Dkmx> getListByDkmx(Dkmx dkmx) {
        List<Dkmx> dkmxes = dkmxMapper.queryListByDkmx(dkmx);
        this.queryDkmx(dkmxes);
        return dkmxes;
    }

    public List<Dkmx> queryDkmx(List<Dkmx> dkmxList){
        for(int i=0; i<dkmxList.size(); i++){
            if(StringUtils.isNotEmpty(dkmxList.get(i).getBonding())){
                Company company = companyService.getById(dkmxList.get(i).getBonding());
                if(StringUtils.isNotBlank(company)){
                    dkmxList.get(i).setBondingName(company.getFullName());
                }
            }
        }
        return dkmxList;
    }

    @Override
    public List<Dkmx> getListByDkmx2(Dkmx dkmx) {
        if(StringUtils.isEmpty(dkmx.getAreaId())){
            dkmx.setProvinceId("4c2b24a871f146f3a77644dabb33ba49");
        }else {
            BaseArea area = areaService.getById(dkmx.getAreaId());
            setArea(dkmx,area);
        }
        List<Dkmx> dkmxes = dkmxMapper.queryListByDkmx2(dkmx);
        for (Dkmx dkmx1 : dkmxes) {
            if(StringUtils.isNotEmpty(dkmx1.getVillageName())){
                dkmx1.setAreaName(dkmx1.getVillageName());
            }else if(StringUtils.isNotEmpty(dkmx1.getTownName())){
                dkmx1.setAreaName(dkmx1.getTownName());
            }else if(StringUtils.isNotEmpty(dkmx1.getCountyName())){
                dkmx1.setAreaName(dkmx1.getCountyName());
            }else if(StringUtils.isNotEmpty(dkmx1.getCityName())){
                dkmx1.setAreaName(dkmx1.getCityName());
            }else if(StringUtils.isNotEmpty(dkmx1.getProName())){
                dkmx1.setAreaName(dkmx1.getProName());
            }else{
                dkmx1.setAreaName("获取区域异常");
            }
        }
        return dkmxes;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insert(User user, Dkmx dkmx) {
        if (user != null) {
            dkmx.setCreateUserId(user.getUserId());
            dkmx.setCreateUsername(user.getUsername());
        }
        BaseArea area = areaService.getById(dkmx.getAreaId());
        setArea(dkmx,area);
        dkmxMapper.insert(dkmx);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(User user,Dkmx dkmx) {
        if (user != null) {
            dkmx.setModifyUserId(user.getUserId());
            dkmx.setModifyUsername(user.getUsername());
        }
        BaseArea area = areaService.getById(dkmx.getAreaId());
        setArea(dkmx,area);
        dkmxMapper.updateById(dkmx);
    }

    private Dkmx setArea(Dkmx dkmx, BaseArea area) {
        if (area != null) {
            int i = area.getLayer();
            if (i == 1) {
                dkmx.setProvinceId(area.getAreaId());
            } else if (i == 2) {
                dkmx.setCityId(area.getAreaId());
                BaseArea proArea = areaService.getById(area.getParentId());
                if (proArea != null) {
                    dkmx.setProvinceId(proArea.getAreaId());
                }
            } else if (i == 3) {
                dkmx.setCountyId(area.getAreaId());
                BaseArea cityArea = areaService.getById(area.getParentId());
                if (cityArea != null) {
                    dkmx.setCityId(cityArea.getAreaId());
                    BaseArea proArea = areaService.getById(cityArea.getParentId());
                    if (proArea != null) {
                        dkmx.setProvinceId(proArea.getAreaId());
                    }
                }
            } else if (i == 4) {
                dkmx.setTownId(area.getAreaId());
                BaseArea countyArea = areaService.getById(area.getParentId());
                if (countyArea != null) {
                    dkmx.setCountyId(countyArea.getAreaId());
                    BaseArea cityArea = areaService.getById(countyArea.getParentId());
                    if (cityArea != null) {
                        dkmx.setCityId(cityArea.getAreaId());
                        BaseArea proArea = areaService.getById(cityArea.getParentId());
                        if (proArea != null) {
                            dkmx.setProvinceId(proArea.getAreaId());
                        }
                    }
                }
            } else if (i == 5) {
                dkmx.setVillageId(area.getAreaId());
                BaseArea townArea = areaService.getById(area.getParentId());
                if (townArea != null) {
                    dkmx.setTownId(townArea.getAreaId());
                    BaseArea countyArea = areaService.getById(townArea.getParentId());
                    if (countyArea != null) {
                        dkmx.setCountyId(countyArea.getAreaId());
                        BaseArea cityArea = areaService.getById(countyArea.getParentId());
                        if (cityArea != null) {
                            dkmx.setCityId(cityArea.getAreaId());
                            BaseArea proArea = areaService.getById(cityArea.getParentId());
                            if (proArea != null) {
                                dkmx.setProvinceId(proArea.getAreaId());
                            }
                        }
                    }
                }
            }
        }
        return dkmx;
    }

    @Override
    public Dkmx editByproductId(String productId) {
       return dkmxMapper.editByproductId(productId);
    }

    @Override
    public List<Tree> queryList(Map map) {
        return dkmxMapper.queryList(map);
    }



}
