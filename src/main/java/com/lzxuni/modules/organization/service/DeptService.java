package com.lzxuni.modules.organization.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Dept;
import com.lzxuni.modules.organization.entity.User;

import java.util.List;

/**
 * <p>
 * 单位部门表 服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
public interface DeptService extends IService<Dept> {
	List<Tree> getTree(String parentId);

	Boolean insert(User user, Dept dept);
	Boolean update(User user, Dept dept);

	Object getMap(String ver);
}
