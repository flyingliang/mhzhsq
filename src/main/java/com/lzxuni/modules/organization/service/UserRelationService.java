package com.lzxuni.modules.organization.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.organization.entity.UserRelation;

import java.util.Map;

/**
 * <p>
 * 用户关系表 服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-07
 */
public interface UserRelationService extends IService<UserRelation> {
	Map<String, Object> getUserIdList(String roleId);

	Boolean insertOrUpdate(User sessionUser, String[] userIds, UserRelation userRelation);

	Map<String, Object> getRoleIdList(String userId);

	Boolean insertOrUpdateRole(User sessionUser, String[] roleIds, UserRelation userRelation);

}
