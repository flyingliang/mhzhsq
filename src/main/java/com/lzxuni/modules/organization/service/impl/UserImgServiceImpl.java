package com.lzxuni.modules.organization.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.organization.entity.UserImg;
import com.lzxuni.modules.organization.mapper.UserImgMapper;
import com.lzxuni.modules.organization.service.UserImgService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户图片表 服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-13
 */
@Service
public class UserImgServiceImpl extends ServiceImpl<UserImgMapper, UserImg> implements UserImgService {

}
