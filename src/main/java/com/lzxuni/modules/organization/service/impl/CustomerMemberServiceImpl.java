package com.lzxuni.modules.organization.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.organization.entity.CustomerMember;
import com.lzxuni.modules.organization.mapper.CustomerMemberMapper;
import com.lzxuni.modules.organization.service.CustomerMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 王智贤
 * @since 2019-01-27
 */
@Service
public class CustomerMemberServiceImpl extends ServiceImpl<CustomerMemberMapper, CustomerMember> implements CustomerMemberService {
	@Autowired
	private CustomerMemberMapper customerMemberMapper;

	@Override
	public List<CustomerMember> queryList(String loanId) {
		return customerMemberMapper.selectList(
				new QueryWrapper<CustomerMember>()
						.eq("loan_id", loanId)
		);
	}
}
