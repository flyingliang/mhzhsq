package com.lzxuni.modules.usercenter;

import com.google.code.kaptcha.Constants;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.organization.entity.UserImg;
import com.lzxuni.modules.organization.service.UserImgService;
import com.lzxuni.modules.organization.service.UserService;
import com.lzxuni.modules.shiro.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author:孙志强
 * @create:2018-12-12 21:22
 * @Modified BY:
 **/
@Controller
@RequestMapping("/UserCenter")
public class UserCenterController extends BaseController {
	@Autowired
	private UserService userService;
	@Autowired
	private UserImgService userImgService;
	@RequestMapping("/Index")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("/UserCenter/index");
		return mv;
	}

	@RequestMapping("/ContactForm")
	public ModelAndView ContactForm() {
		ModelAndView mv = new ModelAndView("/UserCenter/ContactForm");
		return mv;
	}

	@RequestMapping("/HeadForm")
	public ModelAndView HeadForm() {
		ModelAndView mv = new ModelAndView("/UserCenter/HeadForm");
		return mv;
	}

	@RequestMapping("/PassWordForm")
	public ModelAndView PassWordForm() {
		ModelAndView mv = new ModelAndView("/UserCenter/PassWordForm");
		return mv;
	}

	@RequestMapping("/LogIndex")
	public ModelAndView LogIndex() {
		ModelAndView mv = new ModelAndView("/UserCenter/LogIndex");
		return mv;
	}

	@RequestMapping("/LanguageForm")
	public ModelAndView LanguageForm() {
		ModelAndView mv = new ModelAndView("/UserCenter/LanguageForm");
		return mv;
	}

	@ResponseBody
	@RequestMapping("/GetUserInfo")
	public Object GetUserInfo() {
		HashMap<String, Object> data = new HashMap<>();
//		data.put("baseinfo", userService.queryByUserName(getUser().getUsername()));
		data.put("baseinfo", getUser());
		data.put("post", new ArrayList<String>());
		data.put("role", new ArrayList<String>());
		return R.ok().put("data", data);
	}
	@ResponseBody
	@RequestMapping("/ValidationOldPassword")
	public Object ValidationOldPassword(String oldPassword) {
		boolean b = userService.validationOldPassword(getUserId(), oldPassword);
		if (b) {
			return R.ok("原密码正确");
		}
		return R.error("原密码错误，请重新输入");
	}
	@ResponseBody
	@RequestMapping("/SubmitResetPassword")
	public Object SubmitResetPassword(String oldPassword, String password, String verifycode) {
		String sessionVerifycode = (String) ShiroUtils.getSessionAttribute(Constants.KAPTCHA_SESSION_KEY);
		if (sessionVerifycode.equalsIgnoreCase(verifycode)) {
			boolean b = userService.updatePassword(getUserId(),oldPassword , password);
			return R.ok("修改成功");
		} else {
			return R.error("修改失败");
		}
	}
	@RequestMapping("/UploadFile")
	public Object UploadFile(MultipartFile uploadFile) throws Exception{
		UserImg userImg = new UserImg();
		userImg.setUserId(getUserId());

		ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
		byte[] buff = new byte[1024 * 4];
		int rc = 0;
		InputStream inputStream = uploadFile.getInputStream();
		while ((rc = inputStream.read(buff)) != -1) {
			swapStream.write(buff, 0, rc);
		}
		byte[] in2b = swapStream.toByteArray();
		userImg.setImg(in2b);
		userImgService.removeById(getUserId());
		userImgService.save(userImg);
//		R put = R.ok("上传成功").put("data", null);
//		return JSON.toJSONString(put);
		ModelAndView mv = new ModelAndView("/UserCenter/UploadFile");
		return mv;
	}

}
