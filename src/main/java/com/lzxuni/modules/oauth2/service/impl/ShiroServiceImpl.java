package com.lzxuni.modules.oauth2.service.impl;

import com.lzxuni.modules.oauth2.entity.UserToken;
import com.lzxuni.modules.oauth2.mapper.UserTokenMapper;
import com.lzxuni.modules.oauth2.service.ShiroService;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.organization.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class ShiroServiceImpl implements ShiroService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserTokenMapper userTokenMapper;

    @Override
    public Set<String> getUserPermissions(String userId) {
//        List<String> permsList;
//
//        //系统管理员，拥有最高权限
//        if(userId == Constant.SUPER_ADMIN){
//            List<SysMenuEntity> menuList = sysMenuDao.selectList(null);
//            permsList = new ArrayList<>(menuList.size());
//            for(SysMenuEntity menu : menuList){
//                permsList.add(menu.getPerms());
//            }
//        }else{
//            permsList = sysUserDao.queryAllPerms(userId);
//        }
//        //用户权限列表
//        Set<String> permsSet = new HashSet<>();
//        for(String perms : permsList){
//            if(StringUtils.isBlank(perms)){
//                continue;
//            }
//            permsSet.addAll(Arrays.asList(perms.trim().split(",")));
//        }
//        return permsSet;
        return null;
    }

    @Override
    public UserToken queryByToken(String token) {
        return userTokenMapper.queryByToken(token);
    }

    @Override
    public User queryUser(String userId) {
        return userMapper.selectById(userId);
    }
}
