package com.lzxuni.modules.oauth2.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.oauth2.entity.UserToken;

/**
 * 用户Token
 * 
 * @author liuzp

 * @date 2017-03-23 15:22:07
 */
public interface UserTokenService extends IService<UserToken> {

	/**
	 * 生成token
	 * @param userId  用户ID
	 */
	R createOrUpdateToken(String userId);
	void updateToken(String userId);

	/**
	 * 退出，修改token值
	 * @param userId  用户ID
	 */
	void logout(String userId);

}
