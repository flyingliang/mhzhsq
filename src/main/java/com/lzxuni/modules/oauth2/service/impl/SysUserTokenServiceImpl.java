package com.lzxuni.modules.oauth2.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.oauth2.TokenGenerator;
import com.lzxuni.modules.oauth2.entity.UserToken;
import com.lzxuni.modules.oauth2.mapper.UserTokenMapper;
import com.lzxuni.modules.oauth2.service.UserTokenService;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service("sysUserTokenService")
public class SysUserTokenServiceImpl extends ServiceImpl<UserTokenMapper, UserToken> implements UserTokenService {
	//12小时后过期
	private final static int EXPIRE = 60 * 10;


	@Override
	public R createOrUpdateToken(String userId) {
		//生成一个token
		String token = TokenGenerator.generateValue();

		//当前时间
		Date now = new Date();
		//过期时间
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);

		//判断是否生成过token
		UserToken tokenEntity = this.getOne(new QueryWrapper<UserToken>().eq("user_id", userId));
		if(tokenEntity == null){
			tokenEntity = new UserToken();
			tokenEntity.setUserId(userId);
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);

			//保存token
			this.save(tokenEntity);
		}else{
			token = tokenEntity.getToken();
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);

			//更新token
			this.updateById(tokenEntity);
		}

		R r = R.ok().put("token", token).put("expire", EXPIRE);

		return r;
	}
	@Override
	public void updateToken(String userId) {
		//当前时间
		Date now = new Date();
		//过期时间
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);

		//判断是否生成过token
		UserToken tokenEntity = this.getOne(new QueryWrapper<UserToken>().eq("user_id", userId));
		tokenEntity.setUpdateTime(now);
		tokenEntity.setExpireTime(expireTime);
		//更新token
		this.updateById(tokenEntity);
	}

	@Override
	public void logout(String userId) {
		//生成一个token
		String token = TokenGenerator.generateValue();

		//修改token
		UserToken tokenEntity = new UserToken();
		tokenEntity.setUserId(userId);
		tokenEntity.setToken(token);
		this.updateById(tokenEntity);
	}
}
