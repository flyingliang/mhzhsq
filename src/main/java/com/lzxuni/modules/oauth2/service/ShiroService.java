package com.lzxuni.modules.oauth2.service;

import com.lzxuni.modules.oauth2.entity.UserToken;
import com.lzxuni.modules.organization.entity.User;

import java.util.Set;

/**
 * shiro相关接口
 * @author liuzp
 * @email liuzp6@163.com
 * @date 2017-06-06 8:49
 */
public interface ShiroService {
    /**
     * 获取用户权限列表
     */
    Set<String> getUserPermissions(String userId);

    UserToken queryByToken(String token);

    /**
     * 根据用户ID，查询用户
     * @param userId
     */
    User queryUser(String userId);
}
