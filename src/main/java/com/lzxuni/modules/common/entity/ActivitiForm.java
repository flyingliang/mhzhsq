package com.lzxuni.modules.common.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.lzxuni.modules.organization.entity.User;
import lombok.Data;

import java.util.Date;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author:孙志强
 * @create:2019-01-22 15:09
 * @Modified BY:
 **/
@Data
public class ActivitiForm {
	/**
	 * 流程实例ID，流程启动成功之后存储
	 */
	@TableField(strategy = FieldStrategy.IGNORED)
	private String processInstanceId ;
	/**
	 * 流程进程有效标志 1正常2草稿3作废
	 */
	private Integer enabledMark ;

	/**
	 * 流程进程是否结束1是0不是
	 */
	private Integer isFinished ;

	/**
	 * 流程进程是否催办1是0不是
	 */
	private Integer isUrge ;

	/**
	 * 请假人
	 */
	@TableField(exist = false)
	private User user;

	@TableField(exist = false)
	/**
	 * 请假人username
	 */
	private String username;
	/**
	 * 请假人姓名
	 */
	@TableField(exist = false)
	private String realName;


	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 流程key <br>
	 */
	private String schemeCode;

	/**
	 * 流程模板名称 <br>
	 */
	@TableField(exist = false)
	private String schemeName;

	/**
	 * 删除标记1,删除0，未删除
	 */
//    @TableLogic //删除标识，加上此注解，假删除
	private Integer deleteMark ;

	@TableField(exist = false)
	private String  taskName ;
}
