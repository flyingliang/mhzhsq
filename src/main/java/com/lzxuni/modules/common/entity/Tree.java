package com.lzxuni.modules.common.entity;

//import com.baomidou.mybatisplus.annotation.TableField;

//import com.baomidou.mybatisplus.annotation.TableField;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author:孙志强
 * @create:2018-05-31 15:11
 * @Modified BY:
 **/
@Data
public class Tree implements Serializable {
	@TableField(exist=false)
	private String id ;
	@TableField(exist=false)
	private String text ;
	@TableField(exist = false)
	private String title;
	@TableField(exist=false)
	private String value ;
	@TableField(exist=false)
	private String icon ;

	private String parentId ;
    @TableField(exist=false)
	private Boolean showcheck = false;
    @TableField(exist=false)
	private Integer checkstate;
    @TableField(exist=false)
	private Boolean hasChildren = false;
	@TableField(exist = false)
	private Boolean isexpand = false;
    @TableField(exist=false)
	private Boolean complete =false;
	@TableField(exist=false)
	private List childNodes;

	@Override
	protected Object clone() {
		Tree clone = null;
		try{
			clone = (Tree) super.clone();

		}catch(CloneNotSupportedException e){
			throw new RuntimeException(e); // won't happen
		}

		return clone;
	}
}
