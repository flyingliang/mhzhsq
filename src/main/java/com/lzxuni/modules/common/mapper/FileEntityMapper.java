package com.lzxuni.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.FileEntity;
import org.apache.ibatis.annotations.Mapper;


/**
 *@Title FileBeanMapper.java
 *@description 
 *@author 孙志强
 *@time 2016年11月4日 下午3:57:10
 *@version 1.0
 **/
@Mapper
public interface FileEntityMapper extends BaseMapper<FileEntity> {
}
