package com.lzxuni.modules.onlineanswers.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 在线答题
 * Created by gyl
 * 2019/7/30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_online_answers")
public class OnlineAnswers implements Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1L;
    @TableId
    /** 主键，uuid */
    private String id;

    /** 问题类型id */
    private String typeid;

    /** 问题 */
    private String question;
    /** A */
    private String optiona;
    /** B */
    private String optionb;
    /** C */
    private String optionc;
    /** D */
    private String optiond;
    /**答案*/
    private String answer;

    /** 建立时间 */
    private Date date;


    /** 问题类型 */
    @TableField(exist=false)
    private String typename;

}
