package com.lzxuni.modules.onlineanswers.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.constant.LogConstant;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.onlineanswers.entity.OnlineType;
import com.lzxuni.modules.onlineanswers.service.OnlineTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 在线答题 -类型 前端控制器
 * </p>
 *
 * @author gyl
 * @since 2019-07-31
 */

@RestController
@RequestMapping("/OnlineAnswers/questionType")
public class questionTypeController extends BaseController {
	@Autowired
	private OnlineTypeService onlineTypeService;
	//跳转首页
	@SysLog(categoryId = LogConstant.VIEWID,module = "在线答题-类型",operateType = LogConstant.VIEW)
	@RequestMapping("/Index")
	public ModelAndView Index() {
		ModelAndView mv = new ModelAndView("/OnlineAnswers/type/index");
		return mv;
	}
	@RequestMapping("/GetList")
	public Object GetList(String pagination) {

		//1.Json转换成实体类
		PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
		//调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
		PageData pageData = getPageData(onlineTypeService.queryPage(pageParameter));
		return R.ok().put("data", pageData);
	}
	@RequestMapping("/Form")
	public ModelAndView Form() {
		ModelAndView mv = new ModelAndView("/OnlineAnswers/type/form");
		return mv;
	}

	@SysLog(categoryId = LogConstant.OPERATEID,module = "在线答题-类型-增改",operateType = LogConstant.INSERT)
	@RequestMapping("/SaveForm")
	public Object SaveForm(OnlineType ot, String keyValue){
		if(StringUtils.isNotEmpty(keyValue)){
			ot.setId(keyValue);
			onlineTypeService.updateById(ot);
			return R.ok("修改成功");
		}else{
			String uuid = UuidUtil.get32UUID();
			ot.setId(uuid);
			ot.setDate(new Date());
			onlineTypeService.save(ot);
			return R.ok("保存成功");
		}

	}
	@SysLog(categoryId = LogConstant.OPERATEID,module = "在线答题-类型-删",operateType = LogConstant.DELETE)
	@RequestMapping("/DeleteForm")
	public Object DeleteForm(String keyValue){
		onlineTypeService.removeById(keyValue);
		return R.ok("删除成功");
	}
	@RequestMapping("/GetTypeList")
	public Object GetDkmxList() throws Exception {
		Map map = new HashMap();

		return R.ok().put("data",onlineTypeService.queryList(map));
	}


}

