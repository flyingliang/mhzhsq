package com.lzxuni.modules.onlineanswers.controller;


import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.constant.LogConstant;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.onlineanswers.entity.OnlineAnswers;
import com.lzxuni.modules.onlineanswers.entity.RandomNum;
import com.lzxuni.modules.onlineanswers.service.OnlineAnswersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 在线答题 前端控制器
 * </p>
 *
 * @author gyl
 * @since 2019-07-30
 */

@RestController
@RequestMapping("/onlineanswers")
public class onlineAnswersController extends BaseController {
	@Autowired
	private OnlineAnswersService onlineAnswersService;
	//跳转首页
	@SysLog(categoryId = LogConstant.VIEWID,module = "在线答题",operateType = LogConstant.VIEW)
	@RequestMapping("/Index")
	public ModelAndView Index() {
		ModelAndView mv = new ModelAndView("/OnlineAnswers/answers/index");
		return mv;
	}

	@RequestMapping("/GetList")
	public Object GetList(String pagination,String keyword, OnlineAnswers oa) {
		if(StringUtils.isNotEmpty(keyword)) {
			oa.setTypename(keyword);
		}
		//1.Json转换成实体类
		PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
		//调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
		PageData pageData = getPageData(onlineAnswersService.queryPage(pageParameter, oa));
		return R.ok().put("data", pageData);
	}
	@RequestMapping("/Form")
	public ModelAndView Form() {

		ModelAndView mv = new ModelAndView("/OnlineAnswers/answers/form");
		return mv;
	}
	@SysLog(categoryId = LogConstant.OPERATEID,module = "在线答题-增改",operateType = LogConstant.INSERT)
	@RequestMapping("/SaveForm")
	public Object SaveForm(OnlineAnswers oa, String keyValue){


		if(StringUtils.isNotEmpty(keyValue)){
			oa.setId(keyValue);
			onlineAnswersService.updateById(oa);
			return R.ok("修改成功");
		}else{
			String uuid = UuidUtil.get32UUID();
			oa.setId(uuid);
			oa.setDate(new Date());
			onlineAnswersService.save(oa);
			return R.ok("保存成功");
		}

	}
	@SysLog(categoryId = LogConstant.OPERATEID,module = "在线答题-删",operateType = LogConstant.DELETE)
	@RequestMapping("/DeleteForm")
	public Object DeleteForm(String keyValue){
		onlineAnswersService.removeById(keyValue);
		return R.ok("删除成功");
	}




}

