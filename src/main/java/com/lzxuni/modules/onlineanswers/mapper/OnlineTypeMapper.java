package com.lzxuni.modules.onlineanswers.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.onlineanswers.entity.OnlineType;

import java.util.List;
import java.util.Map;

/**
 * 在线答题 Mapper 接口
 * Created by gyl
 * 2019/7/
 */
public interface OnlineTypeMapper extends BaseMapper<OnlineType> {

    public List<Tree> queryList(Map map);
}
