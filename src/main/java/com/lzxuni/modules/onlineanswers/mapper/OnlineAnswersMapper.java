package com.lzxuni.modules.onlineanswers.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.onlineanswers.entity.OnlineAnswers;
import com.lzxuni.modules.pccontrol.home.party.style.entity.Style;

import java.util.List;
import java.util.Map;

/**
 * 在线答题 Mapper 接口
 * Created by gyl
 * 2019/7/
 */
public interface OnlineAnswersMapper extends BaseMapper<OnlineAnswers> {
    List<OnlineAnswers> findlist(OnlineAnswers demo) ;
    List<OnlineAnswers> findlistbytypeid(OnlineAnswers demo) ;

}
