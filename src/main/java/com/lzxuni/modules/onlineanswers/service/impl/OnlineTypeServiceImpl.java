package com.lzxuni.modules.onlineanswers.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.onlineanswers.entity.OnlineAnswers;
import com.lzxuni.modules.onlineanswers.entity.OnlineType;
import com.lzxuni.modules.onlineanswers.mapper.OnlineAnswersMapper;
import com.lzxuni.modules.onlineanswers.mapper.OnlineTypeMapper;
import com.lzxuni.modules.onlineanswers.service.OnlineAnswersService;
import com.lzxuni.modules.onlineanswers.service.OnlineTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 在线答题 服务实现类
 * Created by GYL
 * 2019/7/16
 */
@Service
public class OnlineTypeServiceImpl extends ServiceImpl<OnlineTypeMapper, OnlineType> implements OnlineTypeService {
    @Autowired
    private OnlineTypeMapper onlineTypeMapper;
    @Override
    public PageInfo<OnlineType> queryPage(PageParameter pageParameter) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<OnlineType> styleList = onlineTypeMapper.selectList( null);

        PageInfo<OnlineType> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }

    @Override
    public List<Tree> queryList(Map map) {
        return onlineTypeMapper.queryList(map);
    }


}
