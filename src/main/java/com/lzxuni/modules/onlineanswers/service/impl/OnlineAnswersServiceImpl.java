package com.lzxuni.modules.onlineanswers.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.onlineanswers.entity.OnlineAnswers;
import com.lzxuni.modules.onlineanswers.mapper.OnlineAnswersMapper;
import com.lzxuni.modules.onlineanswers.service.OnlineAnswersService;
import com.lzxuni.modules.pccontrol.home.party.style.entity.Style;
import com.lzxuni.modules.pccontrol.home.party.style.mapper.StyleMapper;
import com.lzxuni.modules.pccontrol.home.party.style.service.StyleService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 在线答题 服务实现类
 * Created by GYL
 * 2019/7/16
 */
@Service
public class OnlineAnswersServiceImpl extends ServiceImpl<OnlineAnswersMapper, OnlineAnswers> implements OnlineAnswersService {
    @Autowired
    private OnlineAnswersMapper onlineAnswersMapper;
    @Override
    public PageInfo<OnlineAnswers> queryPage(PageParameter pageParameter,OnlineAnswers demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<OnlineAnswers>  styleList = onlineAnswersMapper.findlist(demo);

        PageInfo<OnlineAnswers> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }
    @Override
    public List<OnlineAnswers> findlistbytypeid(OnlineAnswers demo) {
        List<OnlineAnswers>  styleList = onlineAnswersMapper.findlistbytypeid(demo);
        return styleList;
    }


}
