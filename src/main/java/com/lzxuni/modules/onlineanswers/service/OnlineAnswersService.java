package com.lzxuni.modules.onlineanswers.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.onlineanswers.entity.OnlineAnswers;
import com.lzxuni.modules.pccontrol.home.party.style.entity.Style;

import java.util.List;
import java.util.Map;

/**
 * 主页_党建_作风建设 服务类
 * Created by 潘云明
 * 2019/7/16
 */
public interface OnlineAnswersService extends IService<OnlineAnswers> {
    //查询
    PageInfo<OnlineAnswers> queryPage(PageParameter pageParameter, OnlineAnswers demo);

    List<OnlineAnswers> findlistbytypeid(OnlineAnswers demo) ;
}
