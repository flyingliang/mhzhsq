package com.lzxuni.modules.onlineanswers.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.onlineanswers.entity.OnlineType;

import java.util.List;
import java.util.Map;

/**
 * 主页_党建_作风建设 服务类
 * Created by 潘云明
 * 2019/7/16
 */
public interface OnlineTypeService extends IService<OnlineType> {
    //查询
    PageInfo<OnlineType> queryPage(PageParameter pageParameter);
    List<Tree> queryList(Map map);
}
