package com.lzxuni.modules.serverMonitor.controller;

import com.lzxuni.modules.common.controller.BaseController;
import org.springframework.web.bind.annotation.RestController;

/**
 * SnmpController
 *
 * @author liuzp
 * @version 1.0
 * @createTime 2019-05-30 11:37
 * @description 
 **/
@RestController
public class SnmpController extends BaseController {

}
