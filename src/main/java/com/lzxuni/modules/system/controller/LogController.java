package com.lzxuni.modules.system.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.exception.LzxException;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.system.entity.Log;
import com.lzxuni.modules.system.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 孙志强
 * @since 2018-06-11
 */
@RestController
@RequestMapping("/LR_SystemModule/Log")
public class LogController extends BaseController {
	@Autowired
	private LogService logService;

	@RequestMapping("/Index")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("/LR_SystemModule/Log/index");
		return mv;
	}
	@RequestMapping("/GetPageList")
	public Object GetPageList(String pagination, String queryJson) throws LzxException {
		PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
		Log log = JSON.parseObject(queryJson, Log.class);
		PageData pageData = getPageData(logService.queryPage(pageParameter, log));
		return R.ok().put("data",pageData);
	}
	@RequestMapping("/GetPageListByMy")
	public Object GetPageListByMy(String pagination, String queryJson) throws LzxException {
		PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
		Log log = JSON.parseObject(queryJson, Log.class);
		log.setOperateUserId(getUserId());
		PageData pageData = getPageData(logService.queryPage(pageParameter, log));
		return R.ok().put("data",pageData);
	}

	// 删除日志
	@RequestMapping("/Form")
	public Object form() throws LzxException {
		ModelAndView mv = new ModelAndView("/LR_SystemModule/Log/form");
		return mv;
	}
	// 删除日志
	@RequestMapping("/SaveRemoveLog")
	public Object SaveRemoveLog(Integer categoryId,String keepTime) throws LzxException {
		logService.delete(categoryId,keepTime);
		return R.ok("删除成功");
	}

}

