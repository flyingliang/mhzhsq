package com.lzxuni.modules.system.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.constant.ConstantEnum;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.VersionUtil;
import com.lzxuni.common.utils.web.HttpContextUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.system.entity.BaseDataItem;
import com.lzxuni.modules.system.entity.BaseDataItemDetail;
import com.lzxuni.modules.system.service.BaseDataItemDetailService;
import com.lzxuni.modules.system.service.BaseDataItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 孙志强
 * @since 2018-06-11
 */
@RestController
@RequestMapping("/LR_SystemModule/DataItem")
public class BaseDataItemController extends BaseController {
	@Autowired
	private BaseDataItemService baseDataItemService;
	@Autowired
	private BaseDataItemDetailService baseDataItemDetailService;
	@RequestMapping("/Index")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("/LR_SystemModule/DataItem/index");
		return mv;
	}
	// 树
	@RequestMapping("/GetClassifyTree")
	public Object getClassifyTree(String parentId) {
		if (StringUtils.isEmpty(parentId)) {
			parentId = "0";
		}
		List<Tree> baseDataItemList = baseDataItemService.getTree(parentId);
		return R.ok().put("data", baseDataItemList);
	}
	// 列表
	@RequestMapping("/GetClassifyList")
	public Object getClassifyList(){
		// 分页
		List<BaseDataItem> baseDataItemList = baseDataItemService.list();
		return R.ok().put("data", baseDataItemList);
	}


	@RequestMapping("/ClassifyIndex")
	public ModelAndView classifyIndex() {
		ModelAndView mv = new ModelAndView("/LR_SystemModule/DataItem/classifyIndex");
		return mv;
	}
	@RequestMapping("/ClassifyForm")
	public ModelAndView classifyForm() {
		ModelAndView mv = new ModelAndView("/LR_SystemModule/DataItem/ClassifyForm");
		return mv;
	}
	@RequestMapping("/ExistItemName")
	public Object existItemName(String itemName) {
		int count = baseDataItemService.count(new QueryWrapper<BaseDataItem>()
				.eq("item_name", itemName));
		if(count>0){
			return R.ok("响应成功").put("data",false);
		}else{
			return R.ok("响应成功").put("data",true);
		}
	}
	@RequestMapping("/ExistItemCode")
	public Object existItemCode(String itemCode) {
//		int count = baseDataItemService.selectCount(new QueryWrapper<BaseDataItem>().eq("item_code", itemCode));
		int count = baseDataItemService.count(new QueryWrapper<BaseDataItem>().eq("item_code", itemCode));
		if(count>0){
			return R.ok("响应成功").put("data",false);
		}else{
			return R.ok("响应成功").put("data",true);
		}
	}

	@RequestMapping("/SaveClassifyForm")
	public Object saveClassifyForm(String keyValue,BaseDataItem baseDataitem) {
		baseDataitem.setItemId(keyValue);
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		VersionUtil.updateVersion(request, ConstantEnum.VersionEnum.ITEMDATA.getModule());
		if(StringUtils.isEmpty(baseDataitem.getItemId())){
			baseDataitem.setCreateDate(new Date());
			baseDataItemService.save(baseDataitem);
			return R.ok("保存成功");
		}else{
			baseDataitem.setModifyDate(new Date());
			baseDataItemService.update(baseDataitem,
					new QueryWrapper<BaseDataItem>().eq("item_id", baseDataitem.getItemId()));
			return R.ok("修改成功");
		}
	}
	//字典分类删除删除
	@RequestMapping("/DeleteClassifyForm")
	public Object delete(@RequestParam("keyValue")String itemId) {
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		VersionUtil.updateVersion(request, ConstantEnum.VersionEnum.ITEMDATA.getModule());
		baseDataItemService.removeById(itemId);
		return R.ok("删除成功");
	}

	@RequestMapping("/Form")
	public ModelAndView form(){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/DataItem/form");
		return mv;
	}
	@RequestMapping("/ExistDetailItemName")
	public Object existDetailItemName(String itemName,String itemCode) {
		List<BaseDataItemDetail> list = baseDataItemDetailService.list(new QueryWrapper<BaseDataItemDetail>()
				.eq("item_code", itemCode)
				.eq("item_name", itemName));
		if(list!=null && list.size()>0){
			return R.ok("响应成功").put("data",false);
		}
		return R.ok();
	}
	@RequestMapping("/ExistDetailItemValue")
	public Object existDetailItemValue(String itemCode,String itemValue) {
		List<BaseDataItemDetail> list = baseDataItemDetailService.list(new QueryWrapper<BaseDataItemDetail>()
				.eq("item_code", itemCode)
				.eq("item_value", itemValue));
		if(list!=null && list.size()>0){
			return R.ok("响应成功").put("data",false);
		}
		return R.ok();

	}
	@RequestMapping("/SaveDetailForm")
	public Object saveDetailForm(@RequestParam("keyValue")String itemDetailId, String itemCode, BaseDataItemDetail baseDataitemDetail) {
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		VersionUtil.updateVersion(request, ConstantEnum.VersionEnum.ITEMDATA.getModule());

		baseDataitemDetail.setItemDetailId(itemDetailId);
		User user = getUser();
		if(StringUtils.isEmpty(itemDetailId)){
			baseDataItemDetailService.insert(itemCode,user,baseDataitemDetail);
			return R.ok("保存成功");
		}else{
			baseDataItemDetailService.update(user,baseDataitemDetail);
			return R.ok("修改成功");
		}
	}
	// 列表
	@RequestMapping("/GetSelfDetailList")
	public Object GetSelfDetailList(BaseDataItemDetail baseDataitemDetail){
		// 分页
		List<BaseDataItemDetail> baseDataItemDetailList = baseDataItemDetailService.list(new QueryWrapper<BaseDataItemDetail>().eq("item_code",baseDataitemDetail.getItemCode()));
		return R.ok().put("data", baseDataItemDetailList);
	}
	@RequestMapping("/GetDetailList")
	public Object GetDetailList(String itemCode,String parentId){
		// 分页
		List<Tree> baseDataItemDetailList = baseDataItemDetailService.queryListByItemCode(itemCode);
		return R.ok().put("data", baseDataItemDetailList);
	}
	@RequestMapping("/GetDetaillist")
	public Object GetDetailist(BaseDataItemDetail baseDataItemDetail){
		List<BaseDataItemDetail> baseDataItemDetailList = baseDataItemDetailService.list(new QueryWrapper<>(baseDataItemDetail));
		return R.ok().put("data", baseDataItemDetailList);
	}
	//字典删除
	@RequestMapping("/DeleteDetailForm")
	public Object deleteDetailForm(@RequestParam("keyValue")String itemDetailId) {
		baseDataItemDetailService.removeById(itemDetailId);
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		VersionUtil.updateVersion(request, ConstantEnum.VersionEnum.ITEMDATA.getModule());
		return R.ok("删除成功");
	}
	@RequestMapping("/GetMap")
	public Object getMap(String ver){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/DataItem/getMap");
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		//1 判断版本号，如果为空，则初始化数据返回到浏览器，并存储到localStorage中
		if (StringUtils.isEmpty(ver)) {
			String version = VersionUtil.updateVersion(request, ConstantEnum.VersionEnum.ITEMDATA.getModule());
			return R.ok("响应成功").put("data",baseDataItemDetailService.getMap(version));
		}else{
			//2 如果不为空，判断版本号，是否更改过，如果没有，返回空，如果有，重新查询返回
			String verContext = (String) request.getServletContext().getAttribute(ConstantEnum.VersionEnum.ITEMDATA.getModule());
			if(ver.equals(verContext)){
				return R.ok("no updata").put("data","{}");
			}else{
				String version = VersionUtil.getVersion(request, ConstantEnum.VersionEnum.ITEMDATA.getModule());
				return R.ok("响应成功").put("data",baseDataItemDetailService.getMap(version));
			}
		}
	}

	//========================杨国营20190109================对于数据在哪个目录下就转向对应的页面目录利于目录的管理=========================UI素材========================================================
	@RequestMapping("/GetDetailTree")
	public Object GetInfoPageList(String itemCode) {
//		ModelAndView mv = new ModelAndView("/LR_SystemModule/DataItem/GetDetailTree");
		List<Tree> trees = baseDataItemDetailService.queryListByItemCode(itemCode);
		return R.ok().put("data", trees);
	}
	@RequestMapping("/DetailIndex")
	public ModelAndView DetailIndex() {
		ModelAndView mv = new ModelAndView("/LR_SystemModule/DataItem/DetailIndex");
		return mv;
	}

}

