package com.lzxuni.modules.system.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.system.service.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;
import java.util.List;


@RestController
@RequestMapping("/LR_SystemModule/OSSUpload")
public class FileController extends BaseController {
	protected Logger log = LoggerFactory.getLogger(FileController.class);

	@Autowired
	private FileService fileService;

	@Autowired
	private FileEntityService fileEntityService;

	@RequestMapping("/Index")
	public ModelAndView list(){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/OSSUpload/index");
		return mv;
	}

	@RequestMapping("/GetList")
	@SysLog(categoryId = 2,module = "上传云服务",operateType = "访问")
	public Object GetList(String pagination) throws SQLException {
		PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
		PageData pageData = getPageData(fileService.queryPage(pageParameter));
		return R.ok().put("data",pageData);
	}

	@RequestMapping("/Insert")
	public ModelAndView insert(){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/OSSUpload/Insert");
		return mv;
	}

	@RequestMapping("/SaveForm")
	@SysLog(categoryId = 3,module = "上传云服务",operateType = "操作")
	public Object insertDo(String keyValue, String demopics, String demofiles) throws Exception{

		if(StringUtils.isEmpty(keyValue)){
			String ywId = UuidUtil.get32UUID();
			if(!StringUtils.isEmpty(demopics) && !"&amp;nbsp;".equals(demopics)) {
				fileEntityService.insert(demopics.replace("&quot;", "\""), ywId, "tp", "demopic", null);
			}
			if(!StringUtils.isEmpty(demofiles) && !"&amp;nbsp;".equals(demofiles)) {
				fileEntityService.insert(demofiles.replace("&quot;", "\""), ywId, "ntp", "demofile", null);
			}
			return R.ok("保存成功");
		}else{
			fileEntityService.deleteByYwId(keyValue);
			if(!StringUtils.isEmpty(demopics) && !"&amp;nbsp;".equals(demopics)) {
				fileEntityService.insert(demopics.replace("&quot;", "\""), keyValue, "tp", "demopic", null);
			}
			if(!StringUtils.isEmpty(demofiles) && !"&amp;nbsp;".equals(demofiles)) {
				fileEntityService.insert(demofiles.replace("&quot;", "\""), keyValue, "ntp", "demofile", null);
			}
			return R.ok("修改成功");
		}

	}

	@RequestMapping("/Update")
	public ModelAndView update(String ywId) throws Exception{
		ModelAndView mv = new ModelAndView("/LR_SystemModule/OSSUpload/Update");
		FileEntity fileBeanCustom = new FileEntity();
		fileBeanCustom.setYwId(ywId);
		fileBeanCustom.setYwType("demopic");
		mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
		fileBeanCustom.setYwType("demofile");
		mv.addObject("fileNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
		List<FileEntity> fileEntities = fileEntityService.queryListByFileEntity(fileBeanCustom);
		System.out.println("=========:"+StringUtils.isBlank(fileEntities));
		if(!fileEntities.isEmpty()){
			for(FileEntity fileEntity : fileEntities){
				mv.addObject("videoFile", fileEntity);
			}
		}else{
			mv.addObject("videoFile", "");
		}

		return mv;
	}

}
