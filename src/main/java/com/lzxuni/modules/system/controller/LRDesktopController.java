package com.lzxuni.modules.system.controller;

import com.lzxuni.modules.common.controller.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author:孙志强
 * @create:2018-05-02 21:32
 * @Modified BY:
 **/
@RestController
public class LRDesktopController extends BaseController {

	@RequestMapping("/LR_Desktop/DTTarget/GetMap")
	public ModelAndView list(){
		ModelAndView mv = new ModelAndView("/LR_Desktop/DTTarget/GetMap");
		return mv;
	}
	@RequestMapping("/LR_Desktop/DTTarget/GetSqlData")
	public ModelAndView GetSqlData(){
		ModelAndView mv = new ModelAndView("/LR_Desktop/DTTarget/GetSqlData");
		return mv;
	}
	@RequestMapping("/LR_Desktop/DTList/GetSqlData")
	public ModelAndView GetSqlData1(){
		ModelAndView mv = new ModelAndView("/LR_Desktop/DTList/GetSqlData");
		return mv;
	}
	@RequestMapping("/LR_Desktop/DTChart/GetSqlData")
	public ModelAndView GetSqlData2(){
		ModelAndView mv = new ModelAndView("/LR_Desktop/DTChart/GetSqlData");
		return mv;
	}
	@RequestMapping("/LR_SystemModule/DatabaseLink/GetMap")
	public Object GetMap1(){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/DatabaseLink/getMap");
		return mv;
	}
	@RequestMapping("/LR_SystemModule/ExcelImport/GetList")
	public ModelAndView ExcelImport(){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/ExcelImport/GetList");
		return mv;
	}
	@RequestMapping("/LR_SystemModule/ExcelExport/GetList")
	public ModelAndView ExcelExport(){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/ExcelExport/GetList");
		return mv;
	}
	//========================================杨国营================工作流UI素材========================================
	@RequestMapping("/LR_SystemModule/DatabaseLink/GetTreeList")
	public ModelAndView DatabaseLink(){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/DatabaseLink/GetTreeList");
		return mv;
	}
}
