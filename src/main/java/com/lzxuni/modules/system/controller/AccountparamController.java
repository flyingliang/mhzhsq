package com.lzxuni.modules.system.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.system.entity.Accountparam;
import com.lzxuni.modules.system.service.AccountparamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 杨国营
 * @since 2019-02-12
 */
@RestController
@RequestMapping("/LR_SystemModule/Area/accountparam")
public class AccountparamController extends BaseController{

	@Autowired
	private AccountparamService accountparamService;

	//分账参数列表页
	@RequestMapping("/Index")
	public ModelAndView list(){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/Area/accountparam/index");
		return mv;
	}

	// 分账参数列表
	@RequestMapping("/GetList")
	public Object GetList(String areaId){
		ModelAndView mv = new ModelAndView("/LR_Systeule/Area/GetList");
		return R.ok().put("data",accountparamService.list(new QueryWrapper<Accountparam>().eq("area_id",areaId).orderByDesc("create_date")));
	}

	//分账参数添加页面
	@RequestMapping("/Form")
	public ModelAndView form(){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/Area/accountparam/Form");
		return mv;
	}
	//分账参数添加
	@RequestMapping("/SaveForm")
	public Object SaveForm(String keyValue,String areaId, Accountparam accountparam){
		if(StringUtils.isNotEmpty(keyValue)){
			accountparam.setAccountparamId(keyValue);
			accountparam.setModifyDate(LocalDateTime.now());
			accountparamService.update(getUser(),accountparam);
			return R.ok("修改成功");
		}else{
			accountparam.setAccountparamId(UuidUtil.get32UUID());
			accountparam.setAreaId(areaId);
			accountparam.setCreateDate(LocalDateTime.now());
			accountparamService.insert(getUser(),accountparam);
			return R.ok("保存成功");
		}
	}
	@RequestMapping("/DeleteForm")
	public Object deleteForm(String keyValue){
		accountparamService.removeById(keyValue);
		return R.ok("删除成功");
	}

}

