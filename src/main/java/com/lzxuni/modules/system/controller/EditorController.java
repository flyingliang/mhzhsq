package com.lzxuni.modules.system.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.exception.LzxException;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.system.entity.Editor;
import com.lzxuni.modules.system.service.EditorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 杨国营
 * @since 2019-06-27
 */
@RestController
@RequestMapping("/LR_SystemModule/Ueditor")
public class EditorController extends BaseController {
	@Autowired
	private EditorService editorService;
	@Autowired
	private FileEntityService fileEntityService;

	// 列表页
	@RequestMapping("/Index")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("/LR_SystemModule/Ueditor/index");
		return mv;
	}

	// 列表
	@RequestMapping("/GetPageList")
	public Object GetPageList(String pagination, Editor editor) throws LzxException {
		PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
		PageData pageData = getPageData(editorService.queryPage(pageParameter, editor));

		return R.ok().put("data",pageData);
	}

	// 添加修改展示页面
	@RequestMapping("/Form")
	public ModelAndView insert(){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/Ueditor/Insert");
		return mv;
	}
	@RequestMapping("GetEntity")
	public Object  GetEntity(String keyValue) {
		Editor editor = editorService.getById(keyValue);
		return R.ok().put("data", editor);
	}
	//添加或者修改操作
	@RequestMapping("/SaveForm")
	public Object insertDo(String keyValue, String demopics,Editor editor)throws Exception {
		editor.setId(keyValue);
		if(StringUtils.isEmpty(editor.getId())){
			editorService.save(editor);
			return R.ok("保存成功");
		}else{
			editorService.updateById(editor);
			return R.ok("修改成功");
		}

//		editor.setId(keyValue);
//		if(StringUtils.isEmpty(keyValue)){
//			String ywId = UuidUtil.get32UUID();
//			editor.setId(ywId);// editor表里ID  与sys_file表里YD_ID对应
//			editorService.save(editor); //添加
//			//添加图片必要判断 如果不添加图片就不在file表里添加数据
//			if(!StringUtils.isEmpty(demopics)&& !"&amp;nbsp;".equals(demopics)) {
//
//				fileEntityService.insert(demopics.replace("&quot;", "\""), ywId, "tp", "information-news", null);
//			}
//			return R.ok("保存成功");
//		}else{
//			fileEntityService.deleteByYwId(keyValue);
//			if(!StringUtils.isEmpty(demopics) && !"&amp;nbsp;".equals(demopics)) {
//				fileEntityService.insert(demopics.replace("&quot;", "\""), keyValue, "tp", "information-news", null);
//				editor.setId(keyValue);
//				editorService.updateById(editor);
//			}
//			return R.ok("修改成功");
//		}
	}

	//删除
	@RequestMapping("/DeleteForm")
	public Object delete(String keyValue) {
		editorService.removeById(keyValue);
		return R.ok("删除成功");
	}

	//图片回显
	@RequestMapping("/Update")
	public ModelAndView update(String ywId,String keyValue) throws Exception{
		ModelAndView mv = new ModelAndView("/LR_SystemModule/Ueditor/Update");
		FileEntity fileBeanCustom = new FileEntity();
		fileBeanCustom.setYwId(ywId);
		fileBeanCustom.setYwType("information-news");
		mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
		return mv;
	}

}

