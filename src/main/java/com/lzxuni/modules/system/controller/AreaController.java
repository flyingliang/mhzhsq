package com.lzxuni.modules.system.controller;

import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.system.entity.BaseArea;
import com.lzxuni.modules.system.service.BaseAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author:孙志强
 * @create:2018-05-02 21:32
 * @Modified BY:
 **/
@RestController
@RequestMapping("/LR_SystemModule/Area")
public class AreaController extends BaseController {

	@Autowired
	private BaseAreaService baseAreaService;

	@RequestMapping("/Index")
	public ModelAndView list(){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/Area/index");
		return mv;
	}
	// 根据parentId查找
	@RequestMapping("/GetTree")
	public Object GetTree(String parentId) throws Exception{
		parentId = StringUtils.isNotEmpty(parentId) ? parentId : "0";
		return R.ok().put("data",baseAreaService.getTreeByParentId(parentId));
	}
	// 部门列表
	@RequestMapping("/GetList")
	public Object GetList(String parentId){
		ModelAndView mv = new ModelAndView("/LR_Systeule/Area/GetList");
		return R.ok().put("data",baseAreaService.getListByParentId(parentId));

	}
	// 部门列表
	@RequestMapping("/Form")
	public ModelAndView form(String parentId){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/Area/Form");
		return mv;
	}
	// 部门列表
	@RequestMapping("/SaveForm")
	public Object SaveForm(String keyValue, BaseArea baseArea){
		ModelAndView mv = new ModelAndView("/LR_SystemModule/Area/SaveForm");
		if(StringUtils.isNotEmpty(keyValue)){
			baseArea.setAreaId(keyValue);
			baseAreaService.update(getUser(),baseArea);
			return R.ok("修改成功");
		}else{
			baseAreaService.insert(getUser(),baseArea);
			return R.ok("保存成功");
		}

	}
	@RequestMapping("/DeleteForm")
	public Object deleteForm(String keyValue){
		baseAreaService.delete(keyValue);
		return R.ok("删除成功");
	}
}
