package com.lzxuni.modules.system.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.system.entity.Module;
import com.lzxuni.modules.system.entity.ModuleButton;
import com.lzxuni.modules.system.entity.ModuleColumn;
import com.lzxuni.modules.system.entity.ModuleForm;
import com.lzxuni.modules.system.service.ModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 孙志强
 * @since 2018-06-11
 */
@RestController
@RequestMapping("/LR_SystemModule/Module")
public class ModuleController extends BaseController {
	@Autowired
	private ModuleService moduleService;
	// 列表
	@RequestMapping("/GetAuthorizeButtonColumnList")
	//LR_SystemModule/Module/GetAuthorizeButtonColumnList?url=/LR_SystemModule/Area/Index&_=1543890234906
	public Object getAuthorizeButtonColumnList(String url){
//		ModelAndView mv = new ModelAndView("/LR_SystemModule/Module/GetAuthorizeButtonColumnList");
//		return mv;
//		return moduleService.GetAuthorizeButtonColumnList(getUserId(), url);
		return R.ok().put("data", moduleService.GetAuthorizeButtonColumnList(getUser(), url));
	}

	@RequestMapping("/Index")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("/LR_SystemModule/Module/Index");
		return mv;
	}
	/**
	 * 功能描述: 用户登陆后，系统菜单，级管理员可全部看到，其他用户根据角色查找菜单，返回值类型为一个个module，靠parentId实现级联
	 * 说明：本系统可单独对用户进行授权，也可以根据用户组授权。
	 * 规则：先查看用户是否单独授权过，如果有，以单独用户授权为准，如果没有，以该用户角色为准。
	 *
	 * <br>
	 * @Author 孙志强
	 * @date 2018/12/11 20:59
	 * @param 
	 * @return:org.springframework.web.servlet.ModelAndView
	 */
	@RequestMapping("/GetModuleList")
	public Object GetModuleList() {
		return R.ok().put("data", moduleService.getModuleList(getUser()));
//		ModelAndView mv = new ModelAndView("/LR_SystemModule/Module/GetModuleList");
//		return mv;
	}

	// 功能树行菜单
	@RequestMapping("/GetModuleTree")
	public Object getModuleTree() {
		String parentId = "0";
		List<Tree> moduleList = moduleService.GetModuleTree(parentId,false);
		return R.ok().put("data", moduleList);
	}
	// 功能列表
	@RequestMapping("/GetModuleListByParentId")
	public Object getModuleListByParentId(String parentId){
		// 分页
		List<Module> moduleList = moduleService.list(new QueryWrapper<Module>().eq("parent_id",parentId).orderByAsc("sort_code"));
		return R.ok().put("data", moduleList);
	}
	// 功能添加页面
	@RequestMapping("/Form")
	public Object Form() {
		ModelAndView mv = new ModelAndView("/LR_SystemModule/Module/Form");
		return mv;
	}
	// 功能添加页面选择上级菜单
	@RequestMapping("/GetExpendModuleTree")
	public Object getExpendModuleTree() {
		return R.ok().put("data",moduleService.getExpendModuleTree("0",false));
	}

	// 保存
	@RequestMapping("/SaveForm")
	public Object saveForm(String keyValue,String moduleButtonListJson,String moduleColumnListJson,String moduleFormListJson, String moduleEntityJson) {
		Module module = JSON.parseObject(moduleEntityJson, Module.class);
		List<ModuleButton> moduleButtonList = JSON.parseArray(moduleButtonListJson, ModuleButton.class);
		List<ModuleColumn> moduleColumnList = JSON.parseArray(moduleColumnListJson, ModuleColumn.class);
		List<ModuleForm> moduleFormList = JSON.parseArray(moduleFormListJson, ModuleForm.class);
		module.setModuleButtonList(moduleButtonList);
		module.setModuleColumnList(moduleColumnList);
		module.setModuleFormList(moduleFormList);

		if(StringUtils.isEmpty(keyValue)){
			moduleService.insert(getUser(),module);
			return R.ok("保存成功");
		}else{
			module.setModuleId(keyValue);
			moduleService.update(getUser(),module);
			return R.ok("修改成功");
		}
	}
	//删除
	@RequestMapping("/DeleteForm")
	public Object delete(String keyValue) {
		moduleService.deleteByParentId(keyValue);
		return R.ok("删除成功");
	}
	// 功能修改页面需要的数据
	@RequestMapping("/GetFormData")
	public Object getFormData(String keyValue) {
		return R.ok().put("data", moduleService.getFormData(keyValue));
	}

	// 角色模块选功能
	@RequestMapping("/GetCheckTree")
	public Object getCheckTree() {
		return R.ok().put("data", moduleService.getCheckTree());
	}

}

