package com.lzxuni.modules.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lzxuni.modules.common.entity.Tree;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 孙志强
 * @since 2018-06-11
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("base_dataitem_detail")
public class BaseDataItemDetail extends Tree implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 明细主键
     */
	@TableId
    private String itemDetailId;

    /**
     * 父类主键
     */
    private String itemId;
    /**
     * 编码
     */
    private String itemCode;
    /**
     * 名称
     */
    private String itemName;
    /**
     * 值
     */
    private String itemValue;
    /**
     * 快速查询
     */
    private String quickQuery;
    /**
     * 简拼
     */
    private String simpleSpelling;
    /**
     * 是否默认
     */
    private Integer isDefault;
    /**
     * 排序码
     */
    private Integer sortCode;
    /**
     * 删除标记
     */
    private Integer deleteMark;
    /**
     * 有效标志
     */
    private Integer enabledMark;
    /**
     * 备注
     */
    private String description;
    /**
     * 创建日期
     */
    private Date createDate;
    /**
     * 创建用户主键
     */
    private String createUserid;
    /**
     * 创建用户
     */
    private String createUsername;
    /**
     * 修改日期
     */
    private Date modifyDate;
    /**
     * 修改用户主键
     */
    private String modifyUserid;
    /**
     * 修改用户
     */
    private String modifyUsername;
//	@TableField(exist=false)
//    private List<BaseDataItemDetail> childNodes ;
//
//	@Override
//	public String getText() {
//		return getItemName();
//	}
//
//	@Override
//	public Boolean getComplete() {
//		return false;
//	}
//
//	@Override
//	public String getId() {
//		return getItemDetailId();
//	}
//
//	@Override
//	public Boolean getShowcheck() {
//		return false;
//	}
//	@Override
//	public Boolean getHasChildren() {
//		if(getChildNodes()!=null && getChildNodes().size()>0){
//			return true;
//		}else{
//			return false;
//		}
//	}
}
