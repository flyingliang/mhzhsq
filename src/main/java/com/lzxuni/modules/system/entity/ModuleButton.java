package com.lzxuni.modules.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lzxuni.modules.common.entity.Tree;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 功能按钮表
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("base_module_button")
public class ModuleButton extends Tree implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 按钮主键
     */
    @TableId
    private String moduleButtonId;
    /**
     * 功能主键
     */
    private String moduleId;
    /**
     * 图标
     */

    private String icon;
    /**
     * 编码
     */
    private String enCode;
    /**
     * 名称
     */
    private String fullName;
    /**
     * Action地址
     */
    private String actionAddress;
    /**
     * 排序码
     */
    private Integer sortCode;

    /**
     * 儿子
     */
//    @TableField(exist=false)
//    private List<ModuleButton> childNodes;


    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
