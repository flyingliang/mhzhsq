package com.lzxuni.modules.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.lzxuni.modules.common.entity.Tree;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 行政区域表
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-04
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class BaseArea extends Tree implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 区域主键
     */
    @TableId
    private String areaId;
    /**
     * 区域编码
     */
    private String areaCode;
    /**
     * 区域名称
     */
    private String areaName;
    /**
     * 快速查询
     */
    private String quickQuery;
    /**
     * 简拼
     */
    private String simpleSpelling;
    /**
     * 层次
     */
    private Integer layer;
    /**
     * 排序码
     */
    private Long sortCode;
    /**
     * 删除标记1,删除0，未删除
     */
//	@TableLogic //删除标识，加上此注解，假删除
	private Integer deleteMark;
    /**
     * 有效标志1，有效0.无效
     */
    private Integer enabledMark;
    /**
     * 备注
     */
    private String description;
    /**
     * 创建日期
     */
    private Date createDate;
    /**
     * 创建用户主键
     */
    private String createUserId;
    /**
     * 创建用户
     */
    private String createUsername;
    /**
     * 修改日期
     */
    private Date modifyDate;

    /**
     * 修改用户主键
     */
    @TableField("modify_user_Id")
    private String modifyUserId;
    /**
     * 修改用户
     */
    private String modifyUsername;
//    @TableField(exist=false)
//    private List<BaseArea> ChildNodes;


//	@Override
//	public String getText() {
//		return getAreaName();
//	}
//
//	@Override
//	public Boolean getComplete() {
//		return false;
//	}
//
//	@Override
//	public String getId() {
//		return getAreaId();
//	}
//
//	@Override
//	public Boolean getShowcheck() {
//		return false;
//	}
}
