package com.lzxuni.modules.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 系统日志表
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_log")
public class Log implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日志主键
     */
    @TableId
    private String logId;

    /**
     * 分类Id 1-登陆2-访问3-操作4-异常
     */
    private Integer categoryId;

    /**
     * 来源对象主键
     */
    @TableField("source_objectId")
    private String sourceObjectid;

    /**
     * 来源日志内容
     */
    @TableField("source_contentJson")
    private String sourceContentjson;

    /**
     * 操作时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date operateTime;
    @TableField(exist=false)
    private Date startTime;
    @TableField(exist=false)
    private Date endTime;

    /**
     * 操作用户Id
     */
    private String operateUserId;

    /**
     * 操作用户
     */
    private String operateUsername;

    /**
     * 操作类型Id
     */
    private String operateTypeId;

    /**
     * 操作类型
     */
    private String operateType;

    /**
     * 系统功能
     */
    private String module;

    /**
     * IP地址
     */
    @TableField("IP_address")
    private String ipAddress;

    /**
     * IP地址所在城市
     */
    @TableField("IP_address_name")
    private String ipAddressName;

    /**
     * 主机
     */
    private String host;

    /**
     * 浏览器
     */
    private String browser;

    /**
     * 执行结果状态
     */
    private Integer executeResult;

    /**
     * 执行结果信息
     */
    private String executeResultJson;

    /**
     * 备注
     */
    private String description;

    /**
     * 删除标记
     */
    private Integer deleteMark;

    /**
     * 有效标志
     */
    private Integer enabledMark;

    /**
     * 访问地址
     */
    private String url;

    /**
     * 操作方法
     */
    private String method;

    /**
     * 请求参数
     */
    private String params;

    /**
     * 请求参数
     */
    private Long time;

    /**
     * 如果异常堆栈信息
     */
    private String operationContentExt;

}
