package com.lzxuni.modules.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author 杨国营
 * @since 2019-02-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_accountparam")
public class Accountparam implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId
    private String accountparamId;

    private String areaId;

    private String year;
    /**
     * 运营中心收益利率
     */
    private BigDecimal operationIncomerate;
    /**
     * 合作社收益利率
     */
    private BigDecimal cooperativeIncomerate;
    /**
     * 经销商收益利率
     */
    private BigDecimal dealerIncomerate;
    /**
     * 农资厂商收益利率
     */
    private BigDecimal agriculturalIncomerate;
    /**
     * 运营中心奖惩机制
     */
    private BigDecimal operationReward;
    /**
     * 合作社奖惩机制
     */
    private BigDecimal cooperativeReward;
    /**
     * 经销商奖惩机制
     */
    private BigDecimal dealerReward;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 创建用户主键
     */
    private String createUserId;

    /**
     * 创建用户
     */
    private String createUsername;

    /**
     * 修改日期
     */
    private LocalDateTime modifyDate;

    /**
     * 修改用户主键
     */
    @TableField("modify_user_Id")
    private String modifyUserId;

    /**
     * 修改用户
     */
    private String modifyUsername;


}
