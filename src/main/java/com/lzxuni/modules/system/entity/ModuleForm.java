package com.lzxuni.modules.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 功能表单字段
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("base_module_form")
public class ModuleForm implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 列主键
     */
    @TableId
    private String moduleFormId;
    /**
     * 功能主键
     */
    private String moduleId;
    /**
     * 编码
     */
    private String enCode;
    /**
     * 名称
     */
    private String fullName;
    /**
     * 排序码
     */
    private Integer sortCode;


}
