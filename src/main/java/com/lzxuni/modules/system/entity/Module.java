package com.lzxuni.modules.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lzxuni.modules.common.entity.Tree;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 系统功能表
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
@Data
@Accessors(chain = true)
@TableName("base_module")
@EqualsAndHashCode(callSuper = false)
public class Module extends Tree implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 功能主键
     */
    @TableId
    private String moduleId;
    /**
     * 编码
     */
    private String enCode;
    /**
     * 名称
     */
    private String fullName;
    /**
     * 图标
     */
    private String icon;
    /**
     * 导航地址
     */
    private String urlAddress;
    /**
     * 导航目标
     */
    private String target;
    /**
     * 菜单选项
     */
    private Integer isMenu;
    /**
     * 允许展开
     */
    private Integer allowExpand;
    /**
     * 是否公开
     */
    private Integer isPublic;
    /**
     * 允许编辑
     */
    private Integer allowEdit;
    /**
     * 允许删除
     */
    private Integer allowDelete;
    /**
     * 排序码
     */
    private Integer sortCode;
    /**
     * 删除标记
     */
    private Integer deleteMark;
    /**
     * 有效标志
     */
    private Integer enabledMark;
    /**
     * 备注
     */
    private String description;
    /**
     * 创建日期
     */
    private Date createDate;
    /**
     * 创建用户主键
     */
    private String createUserId;
    /**
     * 创建用户
     */
    private String createUsername;
    /**
     * 修改日期
     */
    private Date modifyDate;
    /**
     * 修改用户主键
     */
    private String modifyUserId;
    /**
     * 修改用户
     */
    private String modifyUsername;

    //
    @TableField(exist=false)
    private List<ModuleButton> moduleButtonList ;
    @TableField(exist=false)
    private List<ModuleColumn> moduleColumnList ;
    @TableField(exist=false)
    private List<ModuleForm> moduleFormList ;
    // 父子关系1-对
//    @TableField(exist=false)
//    private List<Module> childNodes ;


//    @Override
//    public String getId() {
//        return getModuleId();
//    }
//
//    @Override
//    public String getText() {
//        return getFullName();
//    }
//
//    @Override
//    public Boolean getHasChildren() {
//        if (getChildNodes() != null && getChildNodes().size() > 0) {
//            return true;
//        }else{
//            return false;
//        }
//
//    }
//    @Override
//    public Boolean getComplete() {
//        return true;
//    }


    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
