package com.lzxuni.modules.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 系统logo设置
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_logo")
public class Logo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编码
     */
    @TableId
    private String code;

    /**
     * 文件名字
     */
    private byte[] fileName;


}
