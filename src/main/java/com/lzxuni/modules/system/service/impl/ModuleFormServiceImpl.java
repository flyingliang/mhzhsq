package com.lzxuni.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.system.entity.ModuleForm;
import com.lzxuni.modules.system.mapper.ModuleFormMapper;
import com.lzxuni.modules.system.service.ModuleFormService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 功能表单字段 服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
@Service
public class ModuleFormServiceImpl extends ServiceImpl<ModuleFormMapper, ModuleForm> implements ModuleFormService {
	@Override
	public List<Tree> queryListByModuleId(String moduleId) {
		return baseMapper.queryListByModuleId(moduleId);
	}
}
