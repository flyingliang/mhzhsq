package com.lzxuni.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.system.entity.Log;

/**
 * <p>
 * 系统日志表 服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-12
 */
public interface LogService extends IService<Log> {
	Log queryLoginObjectByUserId(String userId);
	PageInfo<Log> queryPage(PageParameter pageParameter, Log log);
	void delete(Integer categoryId, String keepTime);
}
