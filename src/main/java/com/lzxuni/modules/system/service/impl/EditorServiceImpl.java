package com.lzxuni.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.EmptyWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.system.entity.Editor;
import com.lzxuni.modules.system.mapper.EditorMapper;
import com.lzxuni.modules.system.service.EditorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 杨国营
 * @since 2019-06-27
 */
@Service
public class EditorServiceImpl extends ServiceImpl<EditorMapper, Editor> implements EditorService {

	@Autowired
	EditorMapper editorMapper;

	@Override
	public PageInfo<Editor> queryPage(PageParameter pageParameter, Editor editor) {

		PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
		List<Editor> plantproList = editorMapper.selectList(new EmptyWrapper<>());
		PageInfo<Editor> pageInfo = new PageInfo<>(plantproList);
		return pageInfo;

	}
}
