package com.lzxuni.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageParameter;

import java.sql.SQLException;

public interface FileService extends IService<FileEntity> {

	PageInfo<FileEntity> queryPage(PageParameter pageParameter) throws SQLException;

}
