package com.lzxuni.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.date.DateUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.system.entity.Log;
import com.lzxuni.modules.system.mapper.LogMapper;
import com.lzxuni.modules.system.service.LogService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 系统日志表 服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-12
 */
@Service
public class LogServiceImpl extends ServiceImpl<LogMapper, Log> implements LogService {

	@Override
	public Log queryLoginObjectByUserId(String userId) {
		return baseMapper.queryLoginObjectByUserId(userId);
	}

	@Override
	public PageInfo<Log> queryPage(PageParameter pageParameter, Log log) {
		PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(pageParameter.getSidx() + " " + pageParameter.getSord());
		List<Log> userList = baseMapper.queryList(log);
		PageInfo<Log> pageInfo = new PageInfo<>(userList);
		return pageInfo;
	}

	@Override
	public void delete(Integer categoryId, String keepTime) {
		Date date = new Date();
		Integer day = 0;
		switch (keepTime){
			case "7"://7天
				day = 7;
				break;
			case "1"://一个月
				day = 30;

				break;
			case "3"://三个月
				day = 90;
				break;
			case "0":
				day = 0;
				break;
			default:
				break;
		}
		Date operateTime = DateUtil.getDateBefore(date, day);
		Log log = new Log();
		log.setCategoryId(categoryId);
		log.setOperateTime(operateTime);
		baseMapper.delete(log);


	}

}
