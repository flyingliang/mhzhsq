package com.lzxuni.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.mapper.FileEntityMapper;
import com.lzxuni.modules.system.mapper.FileMapper;
import com.lzxuni.modules.system.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class FileServiceImpl extends ServiceImpl<FileMapper, FileEntity> implements FileService {

	@Autowired
	private FileMapper fileMapper;

	@Autowired
	private FileEntityMapper fileEntityMapper;

	@Override
	public PageInfo<FileEntity> queryPage(PageParameter pageParameter) throws SQLException {
		PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
		List<FileEntity> list = fileMapper.queryList();
		/*for(FileEntity fileEntity : list){
			Integer count = fileEntityMapper.selectCount(new QueryWrapper<FileEntity>().eq("yw_id",fileEntity.getYwId()).eq("yw_type",fileEntity.getYwType()));

		}*/
		PageInfo<FileEntity> pageInfo = new PageInfo<>(list);
		return pageInfo;
	}
}
