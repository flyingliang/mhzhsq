package com.lzxuni.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.system.entity.Module;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统功能表 服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
public interface ModuleService extends IService<Module> {
	List<Module> getModuleList(User user);
	List<Tree> GetModuleTree(String parentId, Boolean showcheck);
	List<Tree> getExpendModuleTree(String parentId, Boolean showcheck);

	Map<String, Object> getFormData(String moduleId);
	Boolean insert(User user, Module module);
	Boolean update(User user, Module module);
	void deleteByParentId(String parentId);
	Map<String, Object> getCheckTree();
	Map<String, Object> GetAuthorizeButtonColumnList(User user, String url);

}
