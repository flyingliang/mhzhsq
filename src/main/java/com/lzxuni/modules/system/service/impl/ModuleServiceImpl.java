package com.lzxuni.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.Authorize;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.organization.service.AuthorizeService;
import com.lzxuni.modules.system.entity.Module;
import com.lzxuni.modules.system.entity.ModuleButton;
import com.lzxuni.modules.system.entity.ModuleColumn;
import com.lzxuni.modules.system.entity.ModuleForm;
import com.lzxuni.modules.system.mapper.ModuleMapper;
import com.lzxuni.modules.system.service.ModuleButtonService;
import com.lzxuni.modules.system.service.ModuleColumnService;
import com.lzxuni.modules.system.service.ModuleFormService;
import com.lzxuni.modules.system.service.ModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.*;

/**
 * <p>
 * 系统功能表 服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
@Service
public class ModuleServiceImpl extends ServiceImpl<ModuleMapper, Module> implements ModuleService {
	@Autowired
	private ModuleButtonService moduleButtonService;
	@Autowired
	private ModuleColumnService moduleColumnService;
	@Autowired
	private ModuleFormService moduleFormService;
	@Autowired
	private AuthorizeService authorizeService;


	@Override
	public List<Module> getModuleList(User user) {
		List<Module> moduleList;
		if(user.getIsSystem().equals("Y")){
			moduleList = this.list(new QueryWrapper<Module>().orderByAsc("sort_code"));
			return moduleList;
		}else{
			//根据用户查找权限
			moduleList = baseMapper.queryAuthorizeByUser(user.getUserId());
			if (moduleList != null && moduleList.size() > 0) {
				return moduleList;
			}else{
				//根据角色查找权限
				moduleList = baseMapper.queryAuthorizeByRole(user.getUserId());
				return moduleList;
			}
		}
	}

	@Override
	public List<Tree> GetModuleTree(String parentId, Boolean showcheck) {
		List<Tree> list = baseMapper.queryListByParentId(parentId);
		GetModuleTreeDg(list,showcheck);
		return list;
	}

	@Override
	public List<Tree> getExpendModuleTree(String parentId, Boolean showcheck) {
		List<Tree> list = baseMapper.queryExpendListByParentId(parentId);
		getExpendModuleTreeDg(list,showcheck);
		return list;
	}
	@Override
	public Map<String, Object> getFormData(String moduleId) {

		Module module = baseMapper.selectById(moduleId);
		List<ModuleButton> moduleButtonList = moduleButtonService.list(new QueryWrapper<ModuleButton>().eq("module_id", moduleId));
		List<ModuleColumn> moduleColumnList = moduleColumnService.list(new QueryWrapper<ModuleColumn>().eq("module_id", moduleId));
		List<ModuleForm> moduleFormList = moduleFormService.list(new QueryWrapper<ModuleForm>().eq("module_id", moduleId));

		Map<String, Object> data = new HashMap<>();
		data.put("moduleEntity", module);
		data.put("moduleButtons", moduleButtonList);
		data.put("moduleColumns", moduleColumnList);
		data.put("moduleFields", moduleFormList);
		return data;
	}

	@Transactional(rollbackFor = Exception.class,readOnly=false)
	@Override
	public Boolean insert(User user, Module module) {
		//1 保存主表
		if(user !=null){
			module.setCreateUserId(user.getUserId());
			module.setCreateUsername(user.getUsername());
			module.setCreateDate(new Date());
		}

		boolean save = this.save(module);

		List<ModuleButton> moduleButtonList = module.getModuleButtonList();
		if (moduleButtonList != null && moduleButtonList.size() > 0) {
			for (int i = 0; i < moduleButtonList.size(); i++) {
				ModuleButton moduleButton = moduleButtonList.get(i);
				moduleButton.setModuleId(module.getModuleId());
				moduleButton.setSortCode(i);
				moduleButtonService.save(moduleButton);
			}
		}
		//3 保存功能表格列表
		List<ModuleColumn> moduleColumnList = module.getModuleColumnList();
		if (moduleColumnList != null && moduleColumnList.size() > 0) {
			for (int i = 0; i < moduleColumnList.size(); i++) {
				ModuleColumn moduleColumn = moduleColumnList.get(i);
				moduleColumn.setModuleId(module.getModuleId());
				moduleColumn.setSortCode(i);
				moduleColumnService.save(moduleColumn);
			}
		}
		//4 保存功能表单字段
		List<ModuleForm> moduleFormList = module.getModuleFormList();
		if (moduleFormList != null && moduleFormList.size() > 0) {
			for (int i = 0; i < moduleFormList.size(); i++) {
				ModuleForm moduleForm = moduleFormList.get(i);
				moduleForm.setModuleId(module.getModuleId());
				moduleForm.setSortCode(i);
				moduleFormService.save(moduleForm);
			}
		}
		return save;
	}

	@Override
	public Boolean update(User user, Module module) {
		//1 修改自己
		if(user !=null){
			module.setModifyUserId(user.getUserId());
			module.setModifyUsername(user.getUsername());
			module.setModifyDate(new Date());
		}
		//2 保存其他
		saveButton(module);
		saveColumn(module);
		saveForm(module);
		return this.updateById(module);
	}

	@Override
	public void deleteByParentId(String parentId) {
		//1先删除自己和权限表关联
		authorizeService.remove(new QueryWrapper<Authorize>().eq("item_id", parentId));
		this.removeById(parentId);
		deleteOther(parentId);
		List<Module> list = this.list(new QueryWrapper<Module>().eq("parent_id", parentId));
		//2先删除子孙
		deleteByDg(list);
	}

	@Override
	public Map<String, Object> getCheckTree() {
		HashMap<String, Object> map = new HashMap<>();
		List<Tree> moduleList = GetModuleTree4Role("0",true);
		List<Tree> buttonList = deepCopyList(moduleList);
		List<Tree> columnList = deepCopyList(moduleList);
		List<Tree> formList = deepCopyList(moduleList);

		getButtonList(buttonList);
		getColumnList(columnList);
		getFormList(formList);

		map.put("moduleList", moduleList);
		map.put("buttonList", buttonList);
		map.put("columnList", columnList);
		map.put("formList", formList);
		return map;
	}

	@Override
	public Map<String, Object> GetAuthorizeButtonColumnList(User user, String url) {
		Map<String, Object> map = new HashMap<>();
		Module module = this.getOne(new QueryWrapper<Module>().eq("url_address", url));
		Map<String, Object> buttonMap = new HashMap<>();
		Map<String, Object> columnMap = new HashMap<>();


		List<ModuleColumn> moduleColumnList ;
		List<ModuleButton> moduleButtonList ;
		if(module != null){

			if(user.getIsSystem().equals("Y")){
				moduleButtonList = moduleButtonService.list(new QueryWrapper<ModuleButton>().eq("module_id", module.getModuleId()));
			}else{
				moduleButtonList = baseMapper.queryAuthorizeBtnsByUser(user.getUserId(), module.getModuleId());
				if (moduleButtonList == null || moduleButtonList.size() == 0) {
					moduleButtonList = baseMapper.queryAuthorizeBtnsByRole(user.getUserId(), module.getModuleId());
				}
			}


			if(user.getIsSystem().equals("Y")){
				moduleColumnList = moduleColumnService.list(new QueryWrapper<ModuleColumn>().eq("module_id", module.getModuleId()));
			}else{
				moduleColumnList = baseMapper.queryAuthorizeColsByUser(user.getUserId(), module.getModuleId());
				if (moduleColumnList == null || moduleColumnList.size() == 0) {
					moduleColumnList = baseMapper.queryAuthorizeColsByRole(user.getUserId(), module.getModuleId());
				}
			}

			if (moduleButtonList != null && moduleButtonList.size() > 0) {
				for (ModuleButton moduleButton:moduleButtonList) {
					buttonMap.put(moduleButton.getEnCode(), moduleButton.getFullName());
				}
			}

			if (moduleButtonList != null && moduleButtonList.size() > 0) {
				for (ModuleColumn moduleColumn:moduleColumnList) {
					columnMap.put(moduleColumn.getEnCode(), moduleColumn.getFullName());
				}
			}

		}

		map.put("module", module);
		map.put("btns", buttonMap);
		map.put("cols", columnMap);
		return map;
	}

	public  List deepCopyList(List source)  {
		List target = new ArrayList<>();
		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			objectOutputStream.writeObject(source);

			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
			ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
			target = (List) objectInputStream.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return  target;
	}

	/*私有方法区域*/
	private void getButtonList(List<Tree> buttonList){
		if(buttonList!=null && buttonList.size()>0){

			Iterator<Tree> iterator = buttonList.iterator();
			while(iterator.hasNext()){
				Tree tree = iterator.next();
				String moduleId = tree.getId();
				tree.setId(tree.getId()+"_learun_moduleId");
				tree.setParentId(tree.getParentId()+"_learun_moduleId");
				if (tree.getHasChildren()) {
					getButtonList(tree.getChildNodes());
				}else{
					//判断是否是否是frame，如果不是，需要remove
					Module module = this.getById(moduleId);
					if(module.getTarget().equals("iframe")){
						List<Tree> moduleButtons = moduleButtonService.getTree(moduleId, true);
						if (moduleButtons != null && moduleButtons.size() > 0) {
							for (Tree tree1:moduleButtons) {
								tree1.setParentId(tree.getId());
							}
							tree.setHasChildren(true);
							tree.setIsexpand(true);
							tree.setChildNodes(moduleButtons);
						}
					}else{
						iterator.remove();
					}
				}
			}
		}
	}
	private void getColumnList(List<Tree> columnList){
		if(columnList!=null && columnList.size()>0){
			Iterator<Tree> iterator = columnList.iterator();
			while(iterator.hasNext()){
				Tree tree = iterator.next();
				String moduleId = tree.getId();
				tree.setId(tree.getId()+"_learun_moduleId");
				tree.setParentId(tree.getParentId()+"_learun_moduleId");

				if (tree.getHasChildren()) {
					getColumnList(tree.getChildNodes());
				}else{
					//判断是否是否是frame，如果不是，需要remove
					Module module = this.getById(moduleId);
					if(module.getTarget().equals("iframe")){
						List<Tree> moduleColumnList = moduleColumnService.queryListByModuleId(moduleId);
						if (moduleColumnList != null && moduleColumnList.size() > 0) {
							for (Tree tree1:moduleColumnList) {
								tree1.setParentId(tree.getId());
								tree1.setShowcheck(true);
							}
							tree.setHasChildren(true);
							tree.setIsexpand(true);
							tree.setChildNodes(moduleColumnList);
						}
					}else{
						iterator.remove();
					}
				}


			}
		}
	}
	private void getFormList(List<Tree> formList){
		if(formList!=null && formList.size()>0){
			Iterator<Tree> iterator = formList.iterator();
			while(iterator.hasNext()){
				Tree tree = iterator.next();
				String moduleId = tree.getId();
				tree.setId(tree.getId()+"_learun_moduleId");
				tree.setParentId(tree.getParentId()+"_learun_moduleId");

				if (tree.getHasChildren()) {
					getFormList(tree.getChildNodes());
				}else{
					//判断是否是否是frame，如果不是，需要remove
					Module module = this.getById(moduleId);
					if(module.getTarget().equals("iframe")){
						List<Tree> moduleFormList = moduleFormService.queryListByModuleId(moduleId);
						if (moduleFormList != null && moduleFormList.size() > 0) {
							for (Tree tree1:moduleFormList) {
								tree1.setParentId(tree.getId());
								tree1.setShowcheck(true);
							}
							tree.setHasChildren(true);
							tree.setIsexpand(true);
							tree.setChildNodes(moduleFormList);
						}
					}else{
						iterator.remove();
					}
				}
			}
		}
	}

	private void saveButton(Module module) {
		//2 保存功能按钮表
		List<ModuleButton> moduleButtonList = module.getModuleButtonList();
		//2.1 先得到老的 moduleButtonIdList 的id集合
		List<ModuleButton> moduleButtons = moduleButtonService.list(new QueryWrapper<ModuleButton>().eq("module_id", module.getModuleId()));
		List<String> olsModuleButtonIdList = new ArrayList<>();
		if (moduleButtons != null && moduleButtons.size() > 0) {
			for(int i=0;i<moduleButtons.size();i++){
				olsModuleButtonIdList.add(moduleButtons.get(i).getModuleButtonId());
			}
		}
		//2.2 得到新的 moduleButtonIdList 的 id集合
		List<String> moduleButtonIdList = new ArrayList<>();
		if (moduleButtonList != null && moduleButtonList.size() > 0) {
			for (int i = 0; i < moduleButtonList.size(); i++) {
				moduleButtonIdList.add(moduleButtonList.get(i).getModuleButtonId());
			}
		}
		//2.3 计算两个集合的交集,交集修改,新的没有的,删除,新的新增的,插入

		//先复制三份新的集合,防止丢失原始集合
		List olsModuleButtonIdList1 = deepCopyList(olsModuleButtonIdList);
		List olsModuleButtonIdList2 = deepCopyList(olsModuleButtonIdList);
		List olsModuleButtonIdList3 = deepCopyList(olsModuleButtonIdList);

		List moduleButtonIdList1 = deepCopyList(moduleButtonIdList);
		List moduleButtonIdList2 = deepCopyList(moduleButtonIdList);
		List moduleButtonIdList3 = deepCopyList(moduleButtonIdList);
		//2.3.1 ,计算交集,交集修改
		moduleButtonIdList1.retainAll(olsModuleButtonIdList1);

		//2.3.2 ,计算新的没有的,删除
		olsModuleButtonIdList2.removeAll(moduleButtonIdList2);
		if (olsModuleButtonIdList2 != null && olsModuleButtonIdList2.size() > 0) {
			moduleButtonService.removeByIds(olsModuleButtonIdList2);
		}
		//2.3.2 ,计算新的新增的,插入
		moduleButtonIdList3.removeAll(olsModuleButtonIdList3);

		if (moduleButtonList != null && moduleButtonList.size() > 0) {
			for (int i = 0; i < moduleButtonList.size(); i++) {
				ModuleButton moduleButton = moduleButtonList.get(i);
				moduleButton.setSortCode(i);
				moduleButton.setModuleId(module.getModuleId());

				if (moduleButtonIdList1.contains(moduleButton.getModuleButtonId())) {
					moduleButtonService.updateById(moduleButton);
				}
				if(moduleButtonIdList3.contains(moduleButton.getModuleButtonId())){
					moduleButtonService.save(moduleButton);
				}
			}
		}
	}
	private void saveColumn(Module module) {
		//2 保存功能按钮表
		List<ModuleColumn> moduleColumnList = module.getModuleColumnList();
		//2.1 先得到老的 moduleButtonIdList 的id集合
		List<ModuleColumn> moduleColumns = moduleColumnService.list(new QueryWrapper<ModuleColumn>().eq("module_id", module.getModuleId()));
		List<String> olsModuleColumnIdList = new ArrayList<>();
		if (moduleColumns != null && moduleColumns.size() > 0) {
			for(int i=0;i<moduleColumns.size();i++){
				olsModuleColumnIdList.add(moduleColumns.get(i).getModuleColumnId());
			}
		}
		//2.2 得到新的 moduleButtonIdList 的 id集合
		List<String> moduleColumnIdList = new ArrayList<>();
		if (moduleColumnList != null && moduleColumnList.size() > 0) {
			for (int i = 0; i < moduleColumnList.size(); i++) {
				moduleColumnIdList.add(moduleColumnList.get(i).getModuleColumnId());
			}
		}
		//2.3 计算两个集合的交集,交集修改,新的没有的,删除,新的新增的,插入

		//先复制三份新的集合,防止丢失原始集合
		List olsModuleColumnIdList1 = deepCopyList(olsModuleColumnIdList);
		List olsModuleColumnIdList2 = deepCopyList(olsModuleColumnIdList);
		List olsModuleColumnIdList3 = deepCopyList(olsModuleColumnIdList);

		List moduleColumnIdList1 = deepCopyList(moduleColumnIdList);
		List moduleColumnIdList2 = deepCopyList(moduleColumnIdList);
		List moduleColumnIdList3 = deepCopyList(moduleColumnIdList);
		//2.3.1 ,计算交集,交集修改
		moduleColumnIdList1.retainAll(olsModuleColumnIdList1);

		//2.3.2 ,计算新的没有的,删除
		olsModuleColumnIdList2.removeAll(moduleColumnIdList2);
		if (olsModuleColumnIdList2 != null && olsModuleColumnIdList2.size() > 0) {
			moduleButtonService.removeByIds(olsModuleColumnIdList2);
		}
		//2.3.2 ,计算新的新增的,插入
		moduleColumnIdList3.removeAll(olsModuleColumnIdList3);

		if (moduleColumnList != null && moduleColumnList.size() > 0) {
			for (int i = 0; i < moduleColumnList.size(); i++) {
				ModuleColumn moduleColumn = moduleColumnList.get(i);
				moduleColumn.setSortCode(i);
				moduleColumn.setModuleId(module.getModuleId());

				if (moduleColumnIdList1.contains(moduleColumn.getModuleColumnId())) {
					moduleColumnService.updateById(moduleColumn);
				}
				if(moduleColumnIdList3.contains(moduleColumn.getModuleColumnId())){
					moduleColumnService.save(moduleColumn);
				}
			}
		}
	}
	private void saveForm(Module module) {
		//2 保存功能按钮表
		List<ModuleForm> moduleFormList = module.getModuleFormList();
		//2.1 先得到老的 moduleButtonIdList 的id集合
		List<ModuleForm> moduleForms = moduleFormService.list(new QueryWrapper<ModuleForm>().eq("module_id", module.getModuleId()));
		List<String> olsModuleFormIdList = new ArrayList<>();
		if (moduleForms != null && moduleForms.size() > 0) {
			for(int i=0;i<moduleForms.size();i++){
				olsModuleFormIdList.add(moduleForms.get(i).getModuleFormId());
			}
		}
		//2.2 得到新的 moduleFormIdList 的 id集合
		List<String> moduleFormIdList = new ArrayList<>();
		if (moduleFormList != null && moduleFormList.size() > 0) {
			for (int i = 0; i < moduleFormList.size(); i++) {
				moduleFormIdList.add(moduleFormList.get(i).getModuleFormId());
			}
		}
		//2.3 计算两个集合的交集,交集修改,新的没有的,删除,新的新增的,插入

		//先复制三份新的集合,防止丢失原始集合
		List olsModuleFormIdList1 = deepCopyList(olsModuleFormIdList);
		List olsModuleFormIdList2 = deepCopyList(olsModuleFormIdList);
		List olsModuleFormIdList3 = deepCopyList(olsModuleFormIdList);

		List moduleFormIdList1 = deepCopyList(moduleFormIdList);
		List moduleFormIdList2 = deepCopyList(moduleFormIdList);
		List moduleFormIdList3 = deepCopyList(moduleFormIdList);
		//2.3.1 ,计算交集,交集修改
		moduleFormIdList1.retainAll(olsModuleFormIdList1);

		//2.3.2 ,计算新的没有的,删除
		olsModuleFormIdList2.removeAll(moduleFormIdList2);
		if (olsModuleFormIdList2 != null && olsModuleFormIdList2.size() > 0) {
			moduleFormService.removeByIds(olsModuleFormIdList2);
		}
		//2.3.2 ,计算新的新增的,插入
		moduleFormIdList3.removeAll(olsModuleFormIdList3);

		if (moduleFormList != null && moduleFormList.size() > 0) {
			for (int i = 0; i < moduleFormList.size(); i++) {
				ModuleForm moduleForm = moduleFormList.get(i);
				moduleForm.setSortCode(i);
				moduleForm.setModuleId(module.getModuleId());

				if (moduleFormIdList1.contains(moduleForm.getModuleFormId())) {
					moduleFormService.updateById(moduleForm);
				}
				if(moduleFormIdList3.contains(moduleForm.getModuleFormId())){
					moduleFormService.save(moduleForm);
				}
			}
		}
	}

	private void deleteOther(String moduleId) {
		//删除按钮
		moduleButtonService.remove(new QueryWrapper<ModuleButton>().eq("module_id", moduleId));
		//删除按钮权限
		List<ModuleButton> moduleButtonList = moduleButtonService.list(new QueryWrapper<ModuleButton>().eq("module_id", moduleId));
		if (moduleButtonList != null && moduleButtonList.size() > 0) {
			for (int i = 0; i < moduleButtonList.size(); i++) {
				authorizeService.remove(new QueryWrapper<Authorize>().eq("item_id", moduleButtonList.get(i).getModuleButtonId()));
			}
		}
		moduleColumnService.remove(new QueryWrapper<ModuleColumn>().eq("module_id", moduleId));
		//删除按钮权限
		List<ModuleColumn> moduleColumnList = moduleColumnService.list(new QueryWrapper<ModuleColumn>().eq("module_id", moduleId));
		if (moduleButtonList != null && moduleButtonList.size() > 0) {
			for (int i = 0; i < moduleColumnList.size(); i++) {
				authorizeService.remove(new QueryWrapper<Authorize>().eq("item_id", moduleColumnList.get(i).getModuleColumnId()));
			}
		}
		moduleFormService.remove(new QueryWrapper<ModuleForm>().eq("module_id", moduleId));
		//删除按钮权限
		List<ModuleForm> moduleFormList = moduleFormService.list(new QueryWrapper<ModuleForm>().eq("module_id", moduleId));
		if (moduleButtonList != null && moduleButtonList.size() > 0) {
			for (int i = 0; i < moduleFormList.size(); i++) {
				authorizeService.remove(new QueryWrapper<Authorize>().eq("item_id", moduleFormList.get(i).getModuleFormId()));
			}
		}
	}

	private void deleteByDg(List<Module> ztreeList) {
		for (int i = 0; i < ztreeList.size(); i++) {
			Module module = ztreeList.get(i);
			authorizeService.remove(new QueryWrapper<Authorize>().eq("item_id", module.getModuleId()));
			this.removeById(module.getModuleId());
			deleteOther(module.getModuleId());
			List<Module> tree = this.list(new QueryWrapper<Module>().eq("parent_id", module.getModuleId()));
			deleteByDg(tree);
		}
	}

	private void GetModuleTreeDg(List<Tree> ztreeList, Boolean showcheck) {
		for (int i = 0; i < ztreeList.size(); i++) {
			Tree tree = ztreeList.get(i);
			tree.setShowcheck(showcheck);
			List<Tree> treeList = baseMapper.queryListByParentId(tree.getId());
			if (treeList != null && treeList.size() > 0) {
				tree.setChildNodes(treeList);
				tree.setHasChildren(true);
			}
			GetModuleTreeDg(treeList,showcheck);
		}
	}
	private List<Tree> GetModuleTree4Role(String parentId, Boolean showcheck) {
		List<Tree> list = baseMapper.queryListByParentId(parentId);
		GetModuleTreeDg4Role(list,showcheck);
		return list;
	}
	private void GetModuleTreeDg4Role(List<Tree> ztreeList, Boolean showcheck) {
		for (int i = 0; i < ztreeList.size(); i++) {
			Tree tree = ztreeList.get(i);
			tree.setShowcheck(showcheck);
			List<Tree> treeList = baseMapper.queryListByParentId(tree.getId());
			if (treeList != null && treeList.size() > 0) {
				tree.setChildNodes(treeList);
				tree.setHasChildren(true);
				tree.setIsexpand(true);
			}
			GetModuleTreeDg4Role(treeList,showcheck);
		}
	}

	private void getExpendModuleTreeDg(List<Tree> ztreeList, Boolean showcheck) {
		for (int i = 0; i < ztreeList.size(); i++) {
			Tree tree = ztreeList.get(i);
			List<Tree> treeList = baseMapper.queryExpendListByParentId(tree.getId());
			ztreeList.get(i).setShowcheck(showcheck);
			if (treeList != null && treeList.size() > 0) {
				tree.setHasChildren(true);
				tree.setIsexpand(true);
				tree.setChildNodes(treeList);
			}
			getExpendModuleTreeDg(treeList,showcheck);
		}
	}
}
