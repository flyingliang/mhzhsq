package com.lzxuni.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.system.entity.BaseArea;

import java.util.List;

/**
 * <p>
 * 行政区域表 服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-04
 */
public interface BaseAreaService extends IService<BaseArea> {

	List<BaseArea> getListByParentId(String parentId);

	List<Tree> getTreeByParentId(String parentId) ;
	Boolean insert(User sessionUser, BaseArea baseArea);
	Boolean update(User sessionUser, BaseArea baseArea);

	void delete(String parentId);

}
