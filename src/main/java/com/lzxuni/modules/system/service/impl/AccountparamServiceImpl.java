package com.lzxuni.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.system.entity.Accountparam;
import com.lzxuni.modules.system.mapper.AccountparamMapper;
import com.lzxuni.modules.system.service.AccountparamService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 杨国营
 * @since 2019-02-12
 */
@Service
public class AccountparamServiceImpl extends ServiceImpl<AccountparamMapper, Accountparam> implements AccountparamService {

	@Override
	public Boolean insert(User user, Accountparam accountparam) {
		if (user != null) {
			accountparam.setCreateUserId(user.getUserId());
			accountparam.setCreateUsername(user.getUsername());
		}
		return this.save(accountparam);
	}

	@Override
	public Boolean update(User user, Accountparam accountparam) {
		if (user != null) {
			accountparam.setModifyUserId(user.getUserId());
			accountparam.setModifyUsername(user.getUsername());
		}
		return this.updateById(accountparam);
	}
}
