package com.lzxuni.modules.system.service;

import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.system.entity.Editor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 杨国营
 * @since 2019-06-27
 */
public interface EditorService extends IService<Editor> {
	PageInfo<Editor> queryPage(PageParameter pageParameter, Editor editor);
}
