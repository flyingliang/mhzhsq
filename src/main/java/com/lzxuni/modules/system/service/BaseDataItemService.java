package com.lzxuni.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.system.entity.BaseDataItem;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-06-11
 */
public interface BaseDataItemService extends IService<BaseDataItem> {
	//列表
	List<BaseDataItem> getList(List<BaseDataItem> ztreeList, String parentId) ;
	List<Tree> getTree(String parentId) ;

}
