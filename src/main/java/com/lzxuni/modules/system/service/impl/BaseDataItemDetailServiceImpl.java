package com.lzxuni.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.common.utils.GetPinyin;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.system.entity.BaseDataItem;
import com.lzxuni.modules.system.entity.BaseDataItemDetail;
import com.lzxuni.modules.system.mapper.BaseDataitemDetailMapper;
import com.lzxuni.modules.system.service.BaseDataItemDetailService;
import com.lzxuni.modules.system.service.BaseDataItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-06-11
 */
@Service
public class BaseDataItemDetailServiceImpl extends ServiceImpl<BaseDataitemDetailMapper, BaseDataItemDetail> implements BaseDataItemDetailService {

	@Autowired
	private BaseDataItemService baseDataItemService;

	@Override
	public List<Tree> queryListByItemCode(String itemCode) {
		List<Tree> treeList = baseMapper.queryListByItemCode(itemCode);
		return treeList;
	}

	@Override
	public Boolean insert(String itemCode, User user, BaseDataItemDetail baseDataItemDetail) {
		BaseDataItem baseDataItem = baseDataItemService.getOne(new QueryWrapper<BaseDataItem>()
				.eq("item_code", itemCode));
		baseDataItemDetail.setCreateDate(new Date());
		baseDataItemDetail.setQuickQuery(GetPinyin.getPingYin(baseDataItemDetail.getItemName()));
		baseDataItemDetail.setSimpleSpelling(GetPinyin.getPinYinHeadChar(baseDataItemDetail.getItemName()));
		if(user!=null){
			baseDataItemDetail.setCreateUserid(user.getUserId());
			baseDataItemDetail.setCreateUsername(user.getUsername());
			baseDataItemDetail.setItemId(baseDataItem.getItemId());
		}

		//不是树形菜单
		if(baseDataItem.getIsTree()==0){
			baseDataItemDetail.setParentId("0");
		}
		return this.save(baseDataItemDetail);
	}

	@Override
	public Boolean update(User user, BaseDataItemDetail baseDataItemDetail) {
		if(user!=null){
			baseDataItemDetail.setModifyDate(new Date());
			baseDataItemDetail.setModifyUserid(user.getUserId());
			baseDataItemDetail.setModifyUsername(user.getUsername());
		}

		baseDataItemDetail.setSimpleSpelling(GetPinyin.getPingYin(baseDataItemDetail.getItemName()));
		return this.updateById(baseDataItemDetail);
	}

	@Override
	public Object getMap(String ver) {
	    //查询出数据字典分类表中的所有类别
		List<BaseDataItem> list = baseDataItemService.list(new QueryWrapper<BaseDataItem>().eq("enabled_mark","1"));
		//创建存储返回数据的map
		Map<String, Object> map = new HashMap<>();
		//创建存储返回数据中data值的map
		Map<String, Object> map1 = new HashMap<>();
        //循环查询出数据字典分类表中的所有类别
		for (int i = 0; i < list.size(); i++) {
		    //创建存储数据字典分类的map
			HashMap<String, Object> map2 = new HashMap<>();
			//根据数据字典分类表中的item_code查询出数据字典明细的第一级数据集合
			List<BaseDataItemDetail> baseDataItemDetails = this.list(new QueryWrapper<BaseDataItemDetail>()
                    .eq("item_code", list.get(i).getItemCode())
                    .eq("enabled_mark","1"));
//                    .eq("parent_id","0"));
			//循环数据字典明细第一级集合的数据
			for (BaseDataItemDetail baseDataItemDetail: baseDataItemDetails) {
			    //创建存储数据字典明细第一级的map
				HashMap<String, Object> map3 = new HashMap<>();
				//存储数据字典明细第一级
				map3.put("parentId", baseDataItemDetail.getParentId());
				map3.put("text", baseDataItemDetail.getItemName());
				map3.put("value", baseDataItemDetail.getItemName());
				//把数据字典明细第一级数据放入数据字典分类中
				map2.put(baseDataItemDetail.getItemDetailId(), map3);

//				//创建存储数据字典明细第二级的map
//                HashMap<String, Object> map4 = new HashMap<>();
//				//根据数据字典分类的表中item_code询出数据字典明细的第二级数据集合
//                List<BaseDataItemDetail> baseDataItemDetails2 = this.list(new QueryWrapper<BaseDataItemDetail>()
//                        .eq("item_code", baseDataItemDetail.getItemCode())
//                        .eq("enabled_mark","1")
//                        .eq("parentId",baseDataItemDetail.getParentId()));
//                //循环数据字典明细第二级集合的数据
//                for(BaseDataItemDetail baseDataItemDetail2: baseDataItemDetails2){
//                    //存储数据字典明细第二级
//                    map4.put("parentId", baseDataItemDetail2.getParentId());
//                    map4.put("text", baseDataItemDetail2.getItemName());
//                    map4.put("value", baseDataItemDetail2.getItemName());
//                    //把数据字典明细第二级数据放入数据字典分类中
//                    map2.put(baseDataItemDetail.getItemDetailId(), map4);
//                }

            }
			//把数据字典分类和明细放入返回数据中data值的map
			map1.put(list.get(i).getItemCode(), map2);
		}
		map.put("data", map1);
		map.put("ver", ver);
		return map;
	}
}
