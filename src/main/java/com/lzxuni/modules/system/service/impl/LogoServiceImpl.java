package com.lzxuni.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.system.entity.Logo;
import com.lzxuni.modules.system.mapper.LogoMapper;
import com.lzxuni.modules.system.service.LogoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统logo设置 服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-19
 */
@Service
public class LogoServiceImpl extends ServiceImpl<LogoMapper, Logo> implements LogoService {

}
