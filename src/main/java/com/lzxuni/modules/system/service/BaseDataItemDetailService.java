package com.lzxuni.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.system.entity.BaseDataItemDetail;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-06-11
 */
public interface BaseDataItemDetailService extends IService<BaseDataItemDetail> {
	List<Tree> queryListByItemCode(String itemCode);

	Boolean insert(String itemCode, User user, BaseDataItemDetail baseDataItemDetail);
	Boolean update(User user, BaseDataItemDetail baseDataItemDetail);

	Object getMap(String ver);
}
