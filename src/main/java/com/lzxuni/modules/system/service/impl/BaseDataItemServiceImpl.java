package com.lzxuni.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.system.entity.BaseDataItem;
import com.lzxuni.modules.system.mapper.BaseDataitemMapper;
import com.lzxuni.modules.system.service.BaseDataItemService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-06-11
 */
@Service
public class BaseDataItemServiceImpl extends ServiceImpl<BaseDataitemMapper, BaseDataItem> implements BaseDataItemService {
	@Override
	public List<BaseDataItem> getList(List<BaseDataItem> ztreeList, String parentId) {
		List<Tree> list = baseMapper.queryListByParentId(parentId);
		for (int i = 0; i < list.size(); i++) {
			Tree tree = list.get(i);
			getList(ztreeList, tree.getId());
		}
		return ztreeList;
	}

	@Override
	public List<Tree> getTree(String parentId) {
		parentId = StringUtils.isNotEmpty(parentId) ? parentId : "0";
		//定义返回对象
		List<Tree> list = baseMapper.queryListByParentId(parentId);
		getTreeDg(list);
		return list;
	}

	private void getTreeDg(List<Tree> treeList){
		for (Tree tree : treeList) {
			List<Tree> treeList1 = baseMapper.queryListByParentId(tree.getId());
			if(treeList1!=null && treeList1.size()>0){
				tree.setChildNodes(treeList1);
				tree.setHasChildren(true);
				tree.setIsexpand(true);
			}
			getTreeDg(treeList1);
		}
	}
}
