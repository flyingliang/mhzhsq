package com.lzxuni.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.system.entity.ModuleColumn;
import com.lzxuni.modules.system.mapper.ModuleColumnMapper;
import com.lzxuni.modules.system.service.ModuleColumnService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 功能按钮表 服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
@Service
public class ModuleColumnServiceImpl extends ServiceImpl<ModuleColumnMapper, ModuleColumn> implements ModuleColumnService {
	@Override
	public List<Tree> queryListByModuleId(String moduleId) {
		return baseMapper.queryListByModuleId(moduleId);
	}
}
