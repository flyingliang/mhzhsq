package com.lzxuni.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.system.entity.ModuleButton;
import com.lzxuni.modules.system.mapper.ModuleButtonMapper;
import com.lzxuni.modules.system.service.ModuleButtonService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 功能按钮表 服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
@Service
public class ModuleButtonServiceImpl extends ServiceImpl<ModuleButtonMapper, ModuleButton> implements ModuleButtonService {

	@Override
	public List<Tree> getTree(String moduleId, Boolean showcheck) {
		List<Tree> moduleButtonList = baseMapper.queryListByParentId(moduleId,"0");
		getModuleButtonTreeDg(moduleId,moduleButtonList,showcheck);
		return moduleButtonList;
	}


	private void getModuleButtonTreeDg(String moduleId, List<Tree> ztreeList, Boolean showcheck) {
		for (int i = 0; i < ztreeList.size(); i++) {
			Tree tree =  ztreeList.get(i);
			List<Tree> moduleButtonList = baseMapper.queryListByParentId(moduleId,tree.getId());
			tree.setId(tree.getId());
			tree.setParentId(tree.getParentId());
			tree.setShowcheck(showcheck);
			if (moduleButtonList != null && moduleButtonList.size() > 0) {
				tree.setChildNodes(moduleButtonList);
				tree.setIsexpand(true);
				tree.setHasChildren(true);
			} else {
				tree.setIsexpand(false);
				tree.setHasChildren(false);
			}
			getModuleButtonTreeDg(moduleId,moduleButtonList,showcheck);
		}
	}
}
