package com.lzxuni.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.system.entity.Accountparam;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 杨国营
 * @since 2019-02-12
 */
public interface AccountparamService extends IService<Accountparam> {

	Boolean insert(User user, Accountparam accountparam);

	Boolean update(User user, Accountparam accountparam);
}
