package com.lzxuni.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.common.utils.GetPinyin;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.system.entity.BaseArea;
import com.lzxuni.modules.system.mapper.BaseAreaMapper;
import com.lzxuni.modules.system.service.BaseAreaService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 行政区域表 服务实现类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-04
 */
@Service
public class BaseAreaServiceImpl extends ServiceImpl<BaseAreaMapper, BaseArea> implements BaseAreaService {

	@Override
	public List<BaseArea> getListByParentId(String parentId) {
		parentId = StringUtils.isNotEmpty(parentId) ? parentId : "0";
		return this.list(new QueryWrapper<BaseArea>().eq("parent_id", parentId));
	}

	@Override
	public List<Tree> getTreeByParentId(String parentId) {
		parentId = StringUtils.isNotEmpty(parentId) ? parentId : "0";
		//定义返回对象
		List<Tree> treeList = baseMapper.queryListByParentId(parentId);
		for (Tree tree : treeList) {
			List<Tree> childNodes = baseMapper.queryListByParentId(tree.getId());
			if (childNodes != null && childNodes.size() > 0) {
				tree.setHasChildren(true);
			} else {
				tree.setHasChildren(false);
			}
		}
		return treeList;
	}

	@Override
	public Boolean insert(User sessionUser, BaseArea baseArea) {
		BaseArea baseAreaParent = baseMapper.selectById(baseArea.getParentId());
		List<BaseArea> childNodes = getListByParentId(baseArea.getParentId());
		int layer=1;
		Long sortCode = 1L;
		if (ObjectUtils.allNotNull(baseAreaParent)) {
			layer = baseAreaParent.getLayer() + 1;
			sortCode = childNodes.size() + 1L;
		}
		if(StringUtils.isEmpty(baseArea.getParentId())){

		}
		baseArea.setQuickQuery(GetPinyin.getPingYin(baseArea.getAreaName()));
		baseArea.setSimpleSpelling(GetPinyin.getPinYinHeadChar(baseArea.getAreaName()));
		baseArea.setCreateDate(new Date());
		baseArea.setCreateUserId(sessionUser.getUserId());
		baseArea.setCreateUsername(sessionUser.getUsername());
		baseArea.setLayer(layer);
		baseArea.setDeleteMark(0);
		baseArea.setEnabledMark(1);
		if(baseArea.getSortCode()==null){
			baseArea.setSortCode(sortCode);
		}
		return this.save(baseArea);
	}
	@Override
	public Boolean update(User sessionUser, BaseArea baseArea) {
		BaseArea baseAreaOld = baseMapper.selectById(baseArea.getAreaId());
		Long sortCode = baseAreaOld.getSortCode();
		baseArea.setQuickQuery(GetPinyin.getPingYin(baseArea.getAreaName()));
		baseArea.setSimpleSpelling(GetPinyin.getPinYinHeadChar(baseArea.getAreaName()));
		baseArea.setModifyDate(new Date());
		baseArea.setModifyUserId(sessionUser.getUserId());
		baseArea.setModifyUsername(sessionUser.getUsername());
		if(baseArea.getSortCode()==null){
			baseArea.setSortCode(sortCode);
		}
		return this.updateById(baseArea);
	}

	@Override
	public void delete(String parentId) {
		List<Tree> list = baseMapper.queryListByParentId(parentId);
		deleteTreeDg(list);
		baseMapper.deleteById(parentId);
	}
	/*私有方法区域*/
	private void deleteTreeDg(List<Tree> ztreeList) {
		for (int i = 0; i < ztreeList.size(); i++) {
			Tree tree = ztreeList.get(i);
			baseMapper.deleteById(tree.getId());
			List<Tree> treeList = baseMapper.queryListByParentId(tree.getId());
			deleteTreeDg(treeList);
		}
	}
}
