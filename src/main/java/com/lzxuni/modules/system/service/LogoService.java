package com.lzxuni.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.system.entity.Logo;

/**
 * <p>
 * 系统logo设置 服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-19
 */
public interface LogoService extends IService<Logo> {

}
