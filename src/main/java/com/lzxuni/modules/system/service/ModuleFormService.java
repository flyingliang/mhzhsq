package com.lzxuni.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.system.entity.ModuleForm;

import java.util.List;

/**
 * <p>
 * 功能表单字段 服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
public interface ModuleFormService extends IService<ModuleForm> {
	List<Tree> queryListByModuleId(String moduleId) ;
}
