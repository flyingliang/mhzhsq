package com.lzxuni.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.system.entity.ModuleButton;

import java.util.List;

/**
 * <p>
 * 功能按钮表 服务类
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
public interface ModuleButtonService extends IService<ModuleButton> {

	List<Tree> getTree(String moduleId, Boolean showcheck) ;

}
