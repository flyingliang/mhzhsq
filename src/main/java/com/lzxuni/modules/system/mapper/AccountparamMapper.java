package com.lzxuni.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.system.entity.Accountparam;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 杨国营
 * @since 2019-02-12
 */
public interface AccountparamMapper extends BaseMapper<Accountparam> {

}
