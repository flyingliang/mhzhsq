package com.lzxuni.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.system.entity.Logo;

/**
 * <p>
 * 系统logo设置 Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-19
 */
public interface LogoMapper extends BaseMapper<Logo> {

}
