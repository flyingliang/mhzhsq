package com.lzxuni.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.system.entity.ModuleButton;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 功能按钮表 Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
public interface ModuleButtonMapper extends BaseMapper<ModuleButton> {
	List<Tree> queryListByParentId(@Param("moduleId") String moduleId, @Param("parentId") String parentId) ;
}
