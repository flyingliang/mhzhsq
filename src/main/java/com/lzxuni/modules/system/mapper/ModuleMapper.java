package com.lzxuni.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.system.entity.Module;
import com.lzxuni.modules.system.entity.ModuleButton;
import com.lzxuni.modules.system.entity.ModuleColumn;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统功能表 Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
public interface ModuleMapper extends BaseMapper<Module> {
	List<Tree> queryListByParentId(String parentId) ;
	List<Tree> queryExpendListByParentId(String parentId) ;

	List<Module> queryAuthorizeByRole(String userId);
	List<Module> queryAuthorizeByUser(String userId);

	List<ModuleButton> queryAuthorizeBtnsByRole(@Param("userId") String userId, @Param("moduleId") String moduleId);
	List<ModuleButton> queryAuthorizeBtnsByUser(@Param("userId") String userId, @Param("moduleId") String moduleId);

	List<ModuleColumn> queryAuthorizeColsByRole(@Param("userId") String userId, @Param("moduleId") String moduleId);
	List<ModuleColumn> queryAuthorizeColsByUser(@Param("userId") String userId, @Param("moduleId") String moduleId);
}
