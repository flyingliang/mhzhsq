package com.lzxuni.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.system.entity.BaseDataItemDetail;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-06-11
 */
public interface BaseDataitemDetailMapper extends BaseMapper<BaseDataItemDetail> {
	List<Tree> queryListByItemCode(String itemCode) ;
//	List<BaseDataItemDetail> queryListByItemId(String itemId) ;
}
