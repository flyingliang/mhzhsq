package com.lzxuni.modules.system.mapper;

import com.lzxuni.modules.system.entity.Editor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 杨国营
 * @since 2019-06-27
 */
public interface EditorMapper extends BaseMapper<Editor> {

}
