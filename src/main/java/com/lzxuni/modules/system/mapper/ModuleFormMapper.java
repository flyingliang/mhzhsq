package com.lzxuni.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.system.entity.ModuleForm;

import java.util.List;

/**
 * <p>
 * 功能表单字段 Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-06
 */
public interface ModuleFormMapper extends BaseMapper<ModuleForm> {
	List<Tree> queryListByModuleId(String moduleId) ;
}
