package com.lzxuni.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.system.entity.Log;

import java.util.List;

/**
 * <p>
 * 系统日志表 Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-12
 */
public interface LogMapper extends BaseMapper<Log> {
	Log queryLoginObjectByUserId(String userId);

	List<Log> queryList(Log log);
	Integer delete(Log log);

}
