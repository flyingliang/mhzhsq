package com.lzxuni.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.FileEntity;

import java.sql.SQLException;
import java.util.List;


public interface FileMapper extends BaseMapper<FileEntity> {

    List<FileEntity> queryList() throws SQLException;
}
