package com.lzxuni.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.system.entity.BaseArea;

import java.util.List;

/**
 * <p>
 * 行政区域表 Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-12-04
 */
public interface BaseAreaMapper extends BaseMapper<BaseArea> {
	List<Tree> queryListByParentId(String parentId) ;
}
