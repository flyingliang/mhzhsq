package com.lzxuni.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.system.entity.BaseDataItem;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 孙志强
 * @since 2018-06-11
 */
public interface BaseDataitemMapper extends BaseMapper<BaseDataItem> {
	List<Tree> queryListByParentId(String parentId) ;
}
