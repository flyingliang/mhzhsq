package com.lzxuni.modules.pccontrol.home.lawmanage.correct.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.correct.entity.Correct;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区矫正 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-25
 */
public interface CorrectService extends IService<Correct> {

    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Correct tblHomeLawmanageCorrect);
    //保存
    void SaveForm(Correct tblHomeLawmanageCorrect);

    List<Tree> getTree(String communityid);

    //查询
    PageInfo<Correct> selectListall(PageParameter pageParameter, Correct demo);
    //查询
    List<Correct> selectListthree(Correct demo);

}
