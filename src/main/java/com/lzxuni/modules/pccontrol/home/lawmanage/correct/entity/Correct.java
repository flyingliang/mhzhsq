package com.lzxuni.modules.pccontrol.home.lawmanage.correct.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 首页--综治--社区矫正
 * </p>
 *
 * @author Lhl
 * @since 2019-07-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_lawmanage_correct")
public class Correct extends Model<Correct> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId
    private String id;

    /**
     * 所属社区
     */
    private String communityid;

    /**
     * 新闻标题
     */
    private String headline;

    /**
     * 新闻内容
     */
    private String content;

    /**
     * 新闻创建时间
     */
    private Date comdate;

    /**
     * 新闻作者
     */
    private String author;

    /**
     * 建立时间
     */
    private Date date;

    /**
     * 1未删除，0删除
     */
    private String isdel;


    /** 图片 */
    @TableField(exist=false)
    private String imgurl;
    /** 业务类型 */
    @TableField(exist=false)
    private String ywtype;
    /** 所属社区 */
    @TableField(exist=false)
    private String communityname;



}
