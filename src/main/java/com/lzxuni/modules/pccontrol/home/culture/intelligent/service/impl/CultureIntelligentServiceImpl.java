package com.lzxuni.modules.pccontrol.home.culture.intelligent.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.mapper.CultureIntelligentMapper;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.service.CultureIntelligentService;
import com.lzxuni.modules.pccontrol.home.culture.production.entity.CultureProduction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理-社区达人表 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-07-15
 */
@Service
public class CultureIntelligentServiceImpl extends ServiceImpl<CultureIntelligentMapper, CultureIntelligent> implements CultureIntelligentService {

    @Autowired
    private CultureIntelligentMapper tblHomeCultureIntelligentMapper;



//    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, TblHomeCultureIntelligent tblHomeCultureIntelligent){
//        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
//            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
//                    pageParameter.getSidx() + " " + pageParameter.getSord());
//        } else {
//            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
//        }
//        if (StringUtils.isNotEmpty(tblHomeCultureIntelligent.getIntelligentName())) {
//            List<Map<String, Object>> villageList =tblHomeCultureIntelligentMapper.selectListByCondition(tblHomeCultureIntelligent);
//        }else {
//            List<Map<String, Object>> villageList =tblHomeCultureIntelligentMapper.selectListByCondition(tblHomeCultureIntelligent);
//            PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
//            return pageInfo;
//        }
//        return null;
//    }
    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, CultureIntelligent tblHomeCultureIntelligent) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeCultureIntelligentMapper.selectListByCondition(tblHomeCultureIntelligent);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;

    }
    @Override
    public List<Tree> getTree() {
        List<Tree> list = tblHomeCultureIntelligentMapper.getTree();
//        getTreeDg(list);
        return list;
    }

    @Override
    public PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, CultureIntelligent cultureIntelligent, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeCultureIntelligentMapper.selectListByConditionAndImg(cultureIntelligent,ywType);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }
}
