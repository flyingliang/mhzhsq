package com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 首页--综治--综治中心
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_lawmanage_managementcenter")
public class Managementcenter implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId
    private String id;

    /**
     * 所属社区
     */
    private String communityid;

    /**
     * 综治工作站简介
     */
    private String synopsis;


    /**
     * 建立时间
     */
    private LocalDateTime date;

    /**
     * 1未删除，0删除
     */
    private String isdel;

    /** 回复图片 */
    @TableField(exist=false)
    private String imgurl;
    /** 业务类型 */
    @TableField(exist=false)
    private String ywtype;
    /** 业务类型 */
    @TableField(exist=false)
    private String communityname;
}
