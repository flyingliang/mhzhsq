package com.lzxuni.modules.pccontrol.home.life.medicine.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.affair.entity.Affair;
import com.lzxuni.modules.pccontrol.home.life.medicine.entity.Medicine;
import com.lzxuni.modules.pccontrol.home.life.medicine.mapper.MedicineMapper;
import com.lzxuni.modules.pccontrol.home.life.medicine.service.MedicineService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@Service
public class MedicineServiceImpl extends ServiceImpl<MedicineMapper, Medicine> implements MedicineService {

    @Autowired
    private MedicineMapper homeLifeMedicineMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Medicine homeLifeMedicine) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = homeLifeMedicineMapper.selectListByCondition(homeLifeMedicine);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public PageInfo<Medicine> queryPage2(PageParameter pageParameter, Medicine demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Medicine> homeLifeAffairs = homeLifeMedicineMapper.selectListByopenid(demo);
        PageInfo<Medicine> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }

    @Override
    public PageInfo<Medicine> queryPage3(PageParameter pageParameter, Medicine demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Medicine> homeLifeAffairs = homeLifeMedicineMapper.selectListAll(demo);
        PageInfo<Medicine> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }


    @Override
    public void SaveForm(Medicine tblHomeLifeMedicine) {
        String newsZzyhDomesticId = UuidUtil.get32UUID();
        tblHomeLifeMedicine.setId(newsZzyhDomesticId);
        this.save(tblHomeLifeMedicine);
    }
}
