package com.lzxuni.modules.pccontrol.home.publicity.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.pccontrol.home.publicity.entity.Notice;
import com.lzxuni.modules.pccontrol.home.publicity.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * <p>
 * 基础管理-->通知公告管理 前端控制器
 * </p>
 *
 * @author gyl
 * @since 2019-10-21
 */
@RestController
@RequestMapping("/tblHomeNotice")
public class NoticeController extends BaseController {
    //    @Autowired
//    private FileEntityService fileEntityService;
    @Autowired
    private NoticeService noticeService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "基础管理-通知公告管理", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/notice/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Notice demo) {
        demo.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(noticeService.queryPage(pageParameter, demo));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/notice/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3, module = "基础管理-通知公告管理", operateType = "操作")
    public Object insertDo(Notice demo, String keyValue, String isshow, String stylepics) throws Exception {
//        System.out.println("###参数列表：keyValue："+keyValue+",isshow:"+isshow);
//        String typeName="社区养老图片";
//        String ywType="organization";
        System.out.println(isshow);
        if (StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)) {//展示
            demo.setId(keyValue);
            demo.setIsshow(isshow);
            noticeService.updateById(demo);
            return R.ok("发布成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)) {//取消展示
            demo.setId(keyValue);
            demo.setIsshow(isshow);
            noticeService.updateById(demo);
            return R.ok("取消发布成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)) {//删除
            demo.setId(keyValue);
            demo.setIsdel("0");
            noticeService.updateById(demo);
            return R.ok("删除成功");
        }

        else if (StringUtils.isNotEmpty(keyValue)) {

            System.out.println(demo.getIsshow()+"------------------");
            demo.setId(keyValue);
            noticeService.updateById(demo);
            return R.ok("修改成功");
        } else {
            demo.setId( UuidUtil.get32UUID());
            demo.setIsdel("1");
            demo.setIsshow("0");
            demo.setDate(new Date());
            User user= getUser();
            demo.setAuthor(user.getRealName());
            demo.setAccount(user.getUsername());
            noticeService.save(demo);
            return R.ok("保存成功");
        }
    }
}

