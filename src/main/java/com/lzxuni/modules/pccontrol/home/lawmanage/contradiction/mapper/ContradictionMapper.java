package com.lzxuni.modules.pccontrol.home.lawmanage.contradiction.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.contradiction.entity.Contradiction;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区矛盾调解条例 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
public interface ContradictionMapper  extends BaseMapper<Contradiction> {
    //社区联动
    List<Map<String,Object>> selectListByCondition(Contradiction tblHomeLawmanageLegislation);
    List<Tree> getTree(@Param("communityid") String communityid);
    List<Contradiction> selectListAll(Contradiction demo);
}
