package com.lzxuni.modules.pccontrol.home.party.map.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 主页_党建_地图
 * Created by 潘云明
 * 2019/7/8 14:46
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_party_map")
public class HomePartyMap implements Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1L;
    @TableId
    /** 主键，uuid */
    private String id;

    /** 组织名称 */
    private String partyname;

    /** 负责人 */
    private String people;

    /** 联系电话 */
    private String phone;

    /** 地址 */
    private String address;

    /** 经度 */
    private String lon;

    /** 维度 */
    private String lat;

    /**1表示展示，1隐藏 */
    private String isshow;

    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除*/
    private String isdel;

}
