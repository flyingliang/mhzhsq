package com.lzxuni.modules.pccontrol.home.lawmanage.legislation.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.entity.Legislation;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
public interface LegislationMapper extends BaseMapper<Legislation> {

    //社区联动
    List<Map<String,Object>> selectListByCondition(Legislation tblHomeLawmanageLegislation);
    List<Tree> getTree(@Param("communityid") String communityid);
    List<Legislation> selectListAll(Legislation demo);
}
