package com.lzxuni.modules.pccontrol.home.party.study.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.party.study.entity.PartyStudy;
import com.lzxuni.modules.pccontrol.home.party.study.service.PartyStudyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * <p>
 * 党建管理-->在线学习管理 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-08-07
 */
@RestController
@RequestMapping("/tblHomePartyStudy")
public class PartyStudyController extends BaseController {


    @Autowired
    private PartyStudyService tblHomePartyStudyService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "党建管理-在线学习管理", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/party/study/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, PartyStudy tblHomePartyStudy) {
        tblHomePartyStudy.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblHomePartyStudyService.queryPage(pageParameter, tblHomePartyStudy));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/party/study/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3, module = "党建管理-在线学习管理", operateType = "操作")
    public Object insertDo(PartyStudy tblHomePartyStudy, String keyValue, String isshow) throws Exception {
        if (StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)) {//展示
            tblHomePartyStudy.setId(keyValue);
            tblHomePartyStudy.setIsshow(isshow);
            tblHomePartyStudyService.updateById(tblHomePartyStudy);
            return R.ok("展示成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)) {//取消展示
            tblHomePartyStudy.setId(keyValue);
            tblHomePartyStudy.setIsshow(isshow);
            tblHomePartyStudyService.updateById(tblHomePartyStudy);
            return R.ok("取消展示成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)) {//删除
            tblHomePartyStudy.setId(keyValue);
            tblHomePartyStudy.setIsdel("0");
            tblHomePartyStudyService.updateById(tblHomePartyStudy);
            return R.ok("删除成功");
        } else if (StringUtils.isNotEmpty(keyValue)) {
            tblHomePartyStudy.setId(keyValue);
            tblHomePartyStudyService.updateById(tblHomePartyStudy);
            return R.ok("修改成功");
        } else {
            String ywId = UuidUtil.get32UUID();
            tblHomePartyStudy.setId(ywId);
            tblHomePartyStudy.setIsdel("1");
            tblHomePartyStudy.setDate(new Date());
            tblHomePartyStudyService.save(tblHomePartyStudy);
            return R.ok("保存成功");
        }
    }
}

