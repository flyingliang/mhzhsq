package com.lzxuni.modules.pccontrol.home.card.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 办证
 * Created by 潘云明
 * 2019/7/16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_card")
public class Card implements Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1L;

    /** 主键，uuid */
    @TableId
    private String id;

    /** 证件类型 */
    private String type;

    /** 姓名 */
    private String name;

    /** 身份证号 */
    private String idnum;

    /** 电话 */
    private String phone;

    /** 1通过，0未通过，-1未审核 */
    private String ispass;

    /** 备注 */
    private String content;

    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除 */
    private String isdel;

}
