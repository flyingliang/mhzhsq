package com.lzxuni.modules.pccontrol.home.life.companytype.service;

import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.companytype.entity.Companytype;
import com.baomidou.mybatisplus.extension.service.IService;

import java.sql.SQLException;

/**
 * <p>
 * 首页--生活服务--单位类别 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
public interface CompanytypeService extends IService<Companytype> {

    PageInfo<Companytype> queryPage(PageParameter pageParameter, Companytype tblHomeLifeCompanytype) throws SQLException;

    void SaveForm(Companytype tblHomeLifeCompanytype);
}
