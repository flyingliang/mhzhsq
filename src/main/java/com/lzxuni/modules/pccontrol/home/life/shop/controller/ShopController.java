package com.lzxuni.modules.pccontrol.home.life.shop.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.life.shop.entity.Shop;
import com.lzxuni.modules.pccontrol.home.life.shop.service.ShopService;
import com.lzxuni.modules.pccontrol.home.publicity.entity.Publicity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-07-19
 */
@RestController
@RequestMapping("/pccontrol/home/life/shop")
public class ShopController extends BaseController {

    @Autowired
    private ShopService tblHomeLifeShopService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看小区楼号", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/shop/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Shop tblHomeLifeShop, String Keyword) {
        tblHomeLifeShop.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        tblHomeLifeShop.setSname(Keyword);
        PageData pageData = getPageData(tblHomeLifeShopService.queryPage(pageParameter, tblHomeLifeShop));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/GetTree")
    public Object GetTree(String communityid) {
        List<Tree> villageList = tblHomeLifeShopService.getTree(communityid);
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/GetTree1")
    public Object GetTree1(String shoptypeid) {
        List<Tree> shoptype = tblHomeLifeShopService.getTree1(shoptypeid);
        return R.ok().put("data",shoptype);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/shop/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    public Object SaveForm(Shop tblHomeLifeShop, String keyValue, String isshow, String sjs) throws Exception{
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            tblHomeLifeShop.setId(ywId);
            tblHomeLifeShopService.save(tblHomeLifeShop);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), ywId, "商家图片", "sj", null);
            }
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            tblHomeLifeShop.setUpdatetime(new Date());
            tblHomeLifeShopService.updateById(tblHomeLifeShop);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), keyValue, "商家图片", "sj", null);
                tblHomeLifeShop.setId(keyValue);
            }
            return R.ok("修改成功");
        }

    }

    @RequestMapping("/IsShow")
    public Object IsShow(Shop tblHomeLifeShop, String keyValue, String isshow) throws Exception{

        if(StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)){
            tblHomeLifeShop.setId(keyValue);
            tblHomeLifeShop.setIsshow(isshow);
            tblHomeLifeShopService.updateById(tblHomeLifeShop);
            return R.ok("展示成功");
        }else {
            tblHomeLifeShop.setId(keyValue);
            tblHomeLifeShop.setIsshow(isshow);
            tblHomeLifeShopService.updateById(tblHomeLifeShop);
            return R.ok("取消展示成功");
        }
    }
    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue) throws Exception{
        Shop tblHomeLifeShop = tblHomeLifeShopService.getById(keyValue);
        return R.ok().put("data",tblHomeLifeShop);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/shop/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("sj");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
        return mv;
    }

    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        tblHomeLifeShopService.removeById(keyValue);
        return R.ok("删除成功");
    }

    @RequestMapping("/SaveForm111")
    public Object insertDo111(Publicity tblHomePublicity, String keyValue, String isshow, String stylepics) throws Exception {
        //        System.out.println("###参数列表：keyValue："+keyValue+",isshow:"+isshow);
        String typeName="社区养老图片";
        String ywType="organization";
         if (StringUtils.isNotEmpty(keyValue) && "4".equals(isshow)) {//返回图片数量
            FileEntity fileBeanCustom = new FileEntity();
            fileBeanCustom.setYwId(keyValue);
            fileBeanCustom.setYwType(ywType);
            return R.ok().put("data",fileEntityService.queryNumByFileEntity(fileBeanCustom));
        }

            return R.ok("保存成功");
        }

}

