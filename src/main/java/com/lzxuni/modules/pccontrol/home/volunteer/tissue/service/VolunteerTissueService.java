package com.lzxuni.modules.pccontrol.home.volunteer.tissue.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.volunteer.join.entity.VolunteerJoin;
import com.lzxuni.modules.pccontrol.home.volunteer.tissue.entity.VolunteerTissue;

import java.util.Map;

/**
 * <p>
 * 志愿服务--志愿者组织表 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-07-24
 */
public interface VolunteerTissueService extends IService<VolunteerTissue> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter);


    //查询
    PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, VolunteerTissue volunteerTissue, String ywType);

}
