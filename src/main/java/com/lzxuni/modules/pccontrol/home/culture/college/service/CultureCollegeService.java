package com.lzxuni.modules.pccontrol.home.culture.college.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.culture.college.entity.CultureCollege;
import com.lzxuni.modules.pccontrol.home.party.style.entity.Style;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理-市民学院表 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-07-17
 */
public interface CultureCollegeService extends IService<CultureCollege> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, CultureCollege tblHomeCultureCollege);
    //查询
    PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter,CultureCollege cultureCollege,String ywType);

}
