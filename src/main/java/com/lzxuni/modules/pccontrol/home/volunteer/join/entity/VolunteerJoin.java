package com.lzxuni.modules.pccontrol.home.volunteer.join.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 志愿服务--在线加入表
 * </p>
 *
 * @author fcd
 * @since 2019-07-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_volunteer_join")
public class VolunteerJoin implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 部门
     */
    private String tissueid;

    /**
     * 姓名
     */
    private String joinname;

    /**
     * 手机
     */
    private String joinphone;

    /**
     * 身份证号
     */
    private String idnumber;

    /**
     * 1表示展示，0隐藏
     */
    private String isshow;

    /**
     * 添加时间
     */
    private Date date;

    /**
     * 1未删除，0删除
     */
    private String isdel;

    /** 加入人微信id */
    private String openid;

    /** 加入人微信名 */
    private String nickname;

//    /**志愿币 */
//    private String volunteercoin;

    /**志愿币 */
    private Integer volunteercoin;
}
