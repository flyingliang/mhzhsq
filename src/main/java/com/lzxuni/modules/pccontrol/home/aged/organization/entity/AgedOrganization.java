package com.lzxuni.modules.pccontrol.home.aged.organization.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import com.lzxuni.modules.common.entity.FileEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 养老管理--养老机构表
 * </p>
 *
 * @author fcd
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_aged_organization")
public class AgedOrganization implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 标题
     */
    private String title;

    /**
     * 所属社区
     */
    private String communityid;

    /**
     * 公布时间
     */
    private Date organizationdate;

    /**
     * 作者
     */
    private String author;

    /**
     * 内容
     */
    private String content;

    /**
     * 浏览量
     */
    private String pageview;

    /**
     * 1表示展示，0隐藏
     */
    private String isshow;

    /**
     * 添加时间
     */
    private Date date;

    /**
     * 1未删除，0删除
     */
    private String isdel;

    @TableField(exist = false)
    private String communityname;

    @TableField(exist = false)
    private List<FileEntity> fileEntities;

    @TableField(exist = false)
    private String urlPath;//web访问路径

    @TableField(exist = false)
    private String urlsPath;//缩略图访问路径


}
