package com.lzxuni.modules.pccontrol.home.life.shoptype.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.shoptype.entity.Shoptype;
import com.lzxuni.modules.pccontrol.home.life.shoptype.mapper.ShoptypeMapper;
import com.lzxuni.modules.pccontrol.home.life.shoptype.service.ShoptypeService;
import com.lzxuni.modules.system.mapper.BaseAreaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 * 首页--生活管理--商家类别 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-17
 */
@Service
public class ShoptypeServiceImpl extends ServiceImpl<ShoptypeMapper, Shoptype> implements ShoptypeService {

    @Autowired
    private ShoptypeMapper tblHomeLifeShoptypeMapper;
    @Autowired
    private BaseAreaMapper areaMapper;

    @Override
    public PageInfo<Shoptype> queryPage(PageParameter pageParameter, Shoptype tblHomeLifeShoptype) throws SQLException {
        /**
         * 之前的分页
         * 1.传入页号和每页几个
         * PageHelper.startPage(pageNumber, 5);
         * 2.查询
         * List<Employee> emps = employeeService.getAll();
         * 3.包装结果，每页5个
         * PageInfo<Employee> pageInfo = new PageInfo<Employee>(emps,5);
         * 4.pageParameter:{"rows":100,"page":1,"sidx":"","sord":"ASC","records":0,"total":0}
         */
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Shoptype> TblHomeLifeShoptypeList = tblHomeLifeShoptypeMapper.selectList(null);
        PageInfo<Shoptype> pageInfo = new PageInfo<>(TblHomeLifeShoptypeList);
        return pageInfo;


    }
    @Override
    public List<Shoptype> queryShopTypeList(Shoptype demo) throws SQLException {


        List<Shoptype> TblHomeLifeShoptypeList = tblHomeLifeShoptypeMapper.queryShopTypeList(demo);

        return TblHomeLifeShoptypeList;


    }
    @Override
    public void SaveForm(Shoptype tblHomeLifeShoptype) {

        String newsZzyhDomesticId = UuidUtil.get32UUID();
        tblHomeLifeShoptype.setId(newsZzyhDomesticId);
        this.save(tblHomeLifeShoptype);
    }
}
