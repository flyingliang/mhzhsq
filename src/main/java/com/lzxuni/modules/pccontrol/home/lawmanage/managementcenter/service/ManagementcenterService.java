package com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.entity.Managementcenter;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
public interface ManagementcenterService extends IService<Managementcenter> {

    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Managementcenter tblHomeLawmanageManagementcenter);
    //保存
    void SaveForm(Managementcenter tblHomeLawmanageManagementcenter);

    List<Tree> getTree();
    //查询
    List<Managementcenter> queryPage3( Managementcenter demo);

    Managementcenter queryBymcId(Managementcenter demmo);
}
