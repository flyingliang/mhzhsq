package com.lzxuni.modules.pccontrol.home.party.elegance.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.party.elegance.entity.Elegance;

/**
 * 主页_党建_群团风采 服务类
 * Created by 潘云明
 * 2019/7/16
 */
public interface EleganceService extends IService<Elegance> {
    //查询
    PageInfo<Elegance> queryPage(PageParameter pageParameter, Elegance entity);

    PageInfo<Elegance> queryPageAndImg(PageParameter pageParameter, Elegance elegance, String ywType);
}
