package com.lzxuni.modules.pccontrol.home.lawmanage.safety.mapper;

import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.safety.entity.Safety;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--平安创建条例 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
public interface SafetyMapper extends BaseMapper<Safety>{
    //社区联动
    List<Map<String,Object>> selectListByCondition(Safety tblHomeLawmanageLegislation);
    List<Tree> getTree(@Param("communityid") String communityid);
    List<Safety> selectListAll(Safety demo);
}
