package com.lzxuni.modules.pccontrol.home.volunteer.join.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import com.lzxuni.modules.pccontrol.home.volunteer.join.entity.VolunteerJoin;
import com.lzxuni.modules.pccontrol.home.volunteer.join.mapper.VolunteerJoinMapper;
import com.lzxuni.modules.pccontrol.home.volunteer.join.service.VolunteerJoinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 志愿服务--在线加入表 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-07-24
 */
@Service
class VolunteerJoinServciceImpl extends ServiceImpl<VolunteerJoinMapper, VolunteerJoin> implements VolunteerJoinService {
@Autowired
private VolunteerJoinMapper tblHomeVolunteerJoinMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, VolunteerJoin tblHomeVolunteerJoin) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeVolunteerJoinMapper.selectListByCondition(tblHomeVolunteerJoin);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;

    }
    @Override
    public List<Tree> getTree() {
        List<Tree> list = tblHomeVolunteerJoinMapper.getTree();
//        getTreeDg(list);
        return list;
    }

    @Override
    public PageInfo<Map<String, Object>> queryList(PageParameter pageParameter) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeVolunteerJoinMapper.selectList();
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;

    }

    @Override
    public PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, VolunteerJoin volunteerJoin, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeVolunteerJoinMapper.selectListByConditionAndImg(volunteerJoin,ywType);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }

    @Override
    public String selectJoinId(String openid) {
        return tblHomeVolunteerJoinMapper.selectJoinId(openid);
    }
}
