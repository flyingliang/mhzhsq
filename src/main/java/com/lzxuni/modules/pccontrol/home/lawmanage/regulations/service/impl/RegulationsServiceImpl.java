package com.lzxuni.modules.pccontrol.home.lawmanage.regulations.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.regulations.entity.Regulations;
import com.lzxuni.modules.pccontrol.home.lawmanage.regulations.mapper.RegulationsMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.regulations.service.RegulationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--法律法规管理 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-22
 */
@Service
public class RegulationsServiceImpl extends ServiceImpl<RegulationsMapper, Regulations> implements RegulationsService {
    @Autowired
    private RegulationsMapper ComtrainingMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Regulations communityper) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = ComtrainingMapper.selectListByCondition(communityper);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Regulations communityper) {
        String uuid = UuidUtil.get32UUID();
        communityper.setId(uuid);
        this.save(communityper);
    }

    @Override
    public List<Tree> getTree() {
        List<Tree> list = ComtrainingMapper.getTree();
        return list;
    }

    @Override
    public List<Regulations> queryPage3( Regulations demo) {

        List<Regulations> homeLifeAffairs = ComtrainingMapper.selectListAll(demo);

        return homeLifeAffairs;
    }

    public  Regulations queryBymcId(Regulations demmo){
        return ComtrainingMapper.queryBymcId(demmo);
    }


    @Override
    public List<Regulations> selectListthree( Regulations demo) {

        List<Regulations> homeLifeAffairs = ComtrainingMapper.selectListthree(demo);

        return homeLifeAffairs;
    }

    @Override
    public PageInfo<Regulations> queryPage4(PageParameter pageParameter, Regulations demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Regulations> homeLifeAffairs = ComtrainingMapper.selectListAll(demo);
        PageInfo<Regulations> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }
}
