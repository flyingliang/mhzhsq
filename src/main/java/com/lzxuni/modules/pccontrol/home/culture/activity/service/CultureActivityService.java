package com.lzxuni.modules.pccontrol.home.culture.activity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.culture.activity.entity.CultureActivity;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;

import java.util.Map;

/**
 * <p>
 * 文化管理--社区活动表 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-07-17
 */
public interface CultureActivityService extends IService<CultureActivity> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, CultureActivity tblHomeCultureActivity);

    //查询
    PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter,CultureActivity cultureActivity, String ywType);

}
