package com.lzxuni.modules.pccontrol.home.lawmanage.aidcase.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.aidcase.entity.Aidcase;
import com.lzxuni.modules.pccontrol.home.lawmanage.aidcase.mapper.AidcaseMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.aidcase.service.AidcaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--援助案例 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
@Service
public class AidcaseServiceImpl extends ServiceImpl<AidcaseMapper, Aidcase> implements AidcaseService {

    @Autowired
    private AidcaseMapper aidcaseMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Aidcase communityper) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = aidcaseMapper.selectListByCondition(communityper);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Aidcase communityper) {
        String uuid = UuidUtil.get32UUID();
        communityper.setId(uuid);
        this.save(communityper);
    }

    @Override
    public List<Tree> getTree() {
        List<Tree> list = aidcaseMapper.getTree();
        return list;
    }

    @Override
    public List<Aidcase> querylist6( Aidcase demo) {

        List<Aidcase> homeLifeAffairs = aidcaseMapper.selectListAll(demo);

        return homeLifeAffairs;
    }

    public  Aidcase queryBymcId(Aidcase demmo){
        return aidcaseMapper.queryBymcId(demmo);
    }

}
