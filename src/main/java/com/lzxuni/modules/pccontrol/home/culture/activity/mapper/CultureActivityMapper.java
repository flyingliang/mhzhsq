package com.lzxuni.modules.pccontrol.home.culture.activity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.culture.activity.entity.CultureActivity;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理--社区活动表 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-07-17
 */
public interface CultureActivityMapper extends BaseMapper<CultureActivity> {
    List<Map<String,Object>> selectListByCondition(CultureActivity tblHomeCultureActivity);

    List<Map<String, Object>> selectListByConditionAndImg(@Param("cultureActivity") CultureActivity cultureActivity, @Param("ywType") String ywType);

}
