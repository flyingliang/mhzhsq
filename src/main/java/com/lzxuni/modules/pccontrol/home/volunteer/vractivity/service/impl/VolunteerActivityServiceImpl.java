package com.lzxuni.modules.pccontrol.home.volunteer.vractivity.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import com.lzxuni.modules.pccontrol.home.volunteer.vractivity.entity.VolunteerActivity;
import com.lzxuni.modules.pccontrol.home.volunteer.vractivity.mapper.VolunteerActivityMapper;
import com.lzxuni.modules.pccontrol.home.volunteer.vractivity.service.VolunteerActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 志愿管理-->志愿者活动表 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-07-29
 */
@Service
public class VolunteerActivityServiceImpl extends ServiceImpl<VolunteerActivityMapper, VolunteerActivity> implements VolunteerActivityService {
@Autowired
 private VolunteerActivityMapper tblHomeVolunteerActivityMapper;


    @Override
    public PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, VolunteerActivity tblHomeVolunteerActivity) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeVolunteerActivityMapper.selectListByCondition(tblHomeVolunteerActivity);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }
    @Override
    public PageInfo<Map<String,Object>> queryList(PageParameter pageParameter, VolunteerActivity tblHomeVolunteerActivity) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeVolunteerActivityMapper.selectList(tblHomeVolunteerActivity);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }

    @Override
    public List<Tree> getTree() {
        List<Tree> list = tblHomeVolunteerActivityMapper.getTree();
        return list;
    }

    @Override
    public void updateAward(String award,String openid) {
        tblHomeVolunteerActivityMapper.updateAward(award,openid);
    }
    @Override
    public void updateAwards(String award,String joinid) {
        tblHomeVolunteerActivityMapper.updateAwards(award,joinid);
    }

    @Override
    public PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, VolunteerActivity volunteerActivity, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeVolunteerActivityMapper.selectListByConditionAndImg(volunteerActivity,ywType);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }

    @Override
    public PageInfo<Map<String,Object>> selectTask(PageParameter pageParameter, VolunteerActivity volunteerActivity) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeVolunteerActivityMapper.selectTask(volunteerActivity);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }
    @Override
    public PageInfo<Map<String,Object>> selectTasks(PageParameter pageParameter,String openid) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeVolunteerActivityMapper.selectTasks(openid);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }
}
