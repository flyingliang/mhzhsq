package com.lzxuni.modules.pccontrol.home.publicity.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.publicity.entity.Notice;
import com.lzxuni.modules.pccontrol.home.publicity.mapper.NoticeMapper;
import com.lzxuni.modules.pccontrol.home.publicity.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理-->政务公开管理 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-08-08
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

    @Autowired
    private NoticeMapper noticeMapper;

    @Override
    public PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Notice demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = noticeMapper.selectListByCondition(demo);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }
    @Override
    public List<Notice> selectListfour(Notice demo) {

        List<Notice> homeLifeAffairs = noticeMapper.selectListfour(demo);

        return homeLifeAffairs;
    }
    @Override
    public PageInfo<Notice> selectListAll(PageParameter pageParameter, Notice demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Notice> homeLifeAffairs = noticeMapper.selectListAll(demo);
        PageInfo<Notice> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }

    public void pageviewPlusOne(Notice demo){
        noticeMapper.pageviewPlusOne(demo);
    }


    
}
