package com.lzxuni.modules.pccontrol.home.life.affair.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.life.affair.entity.Affair;
import com.lzxuni.modules.pccontrol.home.life.affair.service.AffairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/*
 * form 查看
 * index 首页
 * Insert 添加
 * Update 修改
 * */
@RestController
@RequestMapping("/pccontrol/home/life/affair")
public class AffairController extends BaseController {
    @Autowired
    private AffairService homeLifeAffairService;
    @Autowired
    private FileEntityService fileEntityService;
    @RequestMapping("/Index")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/affair/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, AffairController homeLifeAffairController) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(homeLifeAffairService.queryPage(pageParameter, null));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/affair/Insert");
        return mv;
    }

    //查看跳转页
    @RequestMapping("/FormLook")
    public ModelAndView FormLook(String keyValue) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/affair/form");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(keyValue);
        fileBeanCustom.setYwType("kefus");
        mv.addObject("kefuNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
        mv.addObject("id",keyValue);
        return mv;
    }

    //新增修改
    @RequestMapping("/SaveForm")
    public Object insertDo(Affair homeLifeAffair, String keyValue,String isshow, String kefus) throws Exception{

        if(StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)){
            homeLifeAffair.setId(keyValue);
            homeLifeAffair.setIsshow(isshow);
            homeLifeAffairService.updateById(homeLifeAffair);
            return R.ok("展示成功");
        }else if(StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)){
            homeLifeAffair.setId(keyValue);
            homeLifeAffair.setIsshow(isshow);
            homeLifeAffairService.updateById(homeLifeAffair);
            return R.ok("取消展示成功");
        }else if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            homeLifeAffair.setId(ywId);
            homeLifeAffairService.save(homeLifeAffair);
            if(!StringUtils.isEmpty(kefus) && !"&amp;nbsp;".equals(kefus)) {
                fileEntityService.insert(kefus.replace("&quot;", "\""), ywId, "客服照片", "kefu", null);
            }
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            homeLifeAffairService.updateById(homeLifeAffair);
            if(!StringUtils.isEmpty(kefus) && !"&amp;nbsp;".equals(kefus)) {
                fileEntityService.insert(kefus.replace("&quot;", "\""), keyValue, "客服照片", "kefu", null);
                homeLifeAffair.setId(keyValue);
            }
            return R.ok("修改成功");
        }

    }

    //图片ID返回给实体类  Update页传回
    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue){
        Affair homeLifeAffair = homeLifeAffairService.getById(keyValue);
        return R.ok().put("data",homeLifeAffair);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/affair/Update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("kefus");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
        return mv;
    }


//    //真删假删
//    @RequestMapping("/DeleteForm")
//    public Object delete(Affair homeLifeAffair, String keyValue, String isshow, String sjlbs) {
//        if(StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)){
//            homeLifeAffair.setId(keyValue);
//            homeLifeAffair.setIsdel("0");
//            homeLifeAffairService.updateById(homeLifeAffair);
//            fileEntityService.deleteByYwId(keyValue);
////            return R.ok("删除成功");
//        }
//        return R.ok("删除成功");
//    }

    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        homeLifeAffairService.removeById(keyValue);
        return R.ok("删除成功");
    }
}
