package com.lzxuni.modules.pccontrol.home.lawmanage.conflictmediation.mapper;

import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.conflictmediation.entity.Conflictmediation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区矛盾调解 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-07-29
 */
public interface ConflictmediationMapper extends BaseMapper<Conflictmediation> {

    //社区联动
    List<Map<String,Object>> selectListByCondition(Conflictmediation tblHomeLawmanageConflictmediation);
    List<Tree> getTree(@Param("communityid") String communityid);
}
