package com.lzxuni.modules.pccontrol.home.lawmanage.mediate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.mediate.entity.Mediates;
import com.lzxuni.modules.pccontrol.home.lawmanage.mediate.mapper.MediatesMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.mediate.service.MediatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--矛盾调解案例 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
@Service
public class MediatesServiceImpl extends ServiceImpl<MediatesMapper, Mediates> implements MediatesService {

    @Autowired
    private MediatesMapper mediateMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Mediates communityper) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = mediateMapper.selectListByCondition(communityper);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Mediates communityper) {
        String uuid = UuidUtil.get32UUID();
        communityper.setId(uuid);
        this.save(communityper);
    }

    @Override
    public List<Tree> getTree() {
        List<Tree> list = mediateMapper.getTree();
        return list;
    }

    @Override
    public List<Mediates> querylist6(Mediates demo) {

        List<Mediates> homeLifeAffairs = mediateMapper.selectListAll(demo);

        return homeLifeAffairs;
    }

    public Mediates queryBymcId(Mediates demmo){
        return mediateMapper.queryBymcId(demmo);
    }

}
