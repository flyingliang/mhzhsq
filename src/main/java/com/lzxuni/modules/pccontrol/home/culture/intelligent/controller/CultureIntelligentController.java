package com.lzxuni.modules.pccontrol.home.culture.intelligent.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.service.CultureIntelligentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 文化管理-社区达人表 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-07-15
 */
@RestController
@RequestMapping("/tblHomeCultureIntelligent")
public class CultureIntelligentController extends BaseController {
    @Autowired
    private CultureIntelligentService tblHomeCultureIntelligentService;

    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "文化管理-社区达人管理", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/culture/intelligent/index");
        return mv;
    }
    //文化管理-社区达人管理的查询
    @RequestMapping("/GetList")
    public Object GetList(String pagination, CultureIntelligent tblHomeCultureIntelligent) throws SQLException {
        tblHomeCultureIntelligent.setIsdel("1");
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(tblHomeCultureIntelligentService.queryPage(pageParameter, tblHomeCultureIntelligent));
        return R.ok().put("data", pageData);
    }

//    @RequestMapping("/Form")
//    public ModelAndView insert() throws Exception{
//        ModelAndView mv = new ModelAndView("/pccontrol/home/culture/intelligent/Insert");
//        return mv;
//    }
    //添加、修改
    @RequestMapping("/SaveForm")
    public Object insertDo(String keyValue, String demopics, CultureIntelligent tblHomeCultureIntelligent, String isshow) throws Exception{

        if(StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)){
            tblHomeCultureIntelligent.setId(keyValue);
            tblHomeCultureIntelligent.setIsshow(isshow);
            tblHomeCultureIntelligentService.updateById(tblHomeCultureIntelligent);
            return R.ok("审核成功");
        }else if(StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)){
            tblHomeCultureIntelligent.setId(keyValue);
            tblHomeCultureIntelligent.setIsshow(isshow);
            tblHomeCultureIntelligentService.updateById(tblHomeCultureIntelligent);
            return R.ok("取消审核成功");
        }else
            if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            tblHomeCultureIntelligent.setId(ywId);
            tblHomeCultureIntelligent.setDate(new Date());
            tblHomeCultureIntelligentService.save(tblHomeCultureIntelligent);
            if(!StringUtils.isEmpty(demopics)&& !"&amp;nbsp;".equals(demopics)) {
                fileEntityService.insert(demopics.replace("&quot;", "\""), ywId, "社区文化照片", "intelligent", null);
            }
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            tblHomeCultureIntelligentService.updateById(tblHomeCultureIntelligent);
            if(!StringUtils.isEmpty(demopics) && !"&amp;nbsp;".equals(demopics)) {
                fileEntityService.insert(demopics.replace("&quot;", "\""), keyValue, "社区文化照片", "intelligent", null);
                tblHomeCultureIntelligent.setId(keyValue);
            }
            return R.ok("修改成功");
        }
    }
    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId, String keyValue) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/culture/intelligent/Update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("intelligent");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
        return mv;
    }
    //真删假删
    @RequestMapping("/DeleteForm")
    public Object delete(CultureIntelligent tblHomeCultureIntelligent, String keyValue, String isshow) {
        if(StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)){
            tblHomeCultureIntelligent.setId(keyValue);
            tblHomeCultureIntelligent.setIsdel("0");
            tblHomeCultureIntelligentService.updateById(tblHomeCultureIntelligent);
//            fileEntityService.deleteByYwId(keyValue);
//            return R.ok("删除成功");
        }
        return R.ok("删除成功");
    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue){
        CultureIntelligent tblHomeCultureIntelligent = tblHomeCultureIntelligentService.getById(keyValue);
        return R.ok().put("data",tblHomeCultureIntelligent);
    }

    @RequestMapping("/GetTree")
    public Object GetTree(String pagination, CultureIntelligent tblHomeCultureIntelligent) {
        List<Tree> companyList = tblHomeCultureIntelligentService.getTree();
        return R.ok().put("data",companyList);
    }
}






