package com.lzxuni.modules.pccontrol.home.publicity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.publicity.entity.Publicity;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理-->政务公开管理 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-08-08
 */
public interface PublicityService extends IService<Publicity> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Publicity tblHomePublicity);
    List<Publicity> selectListfour(Publicity demo);
    //查询
    PageInfo<Publicity> selectListAll(PageParameter pageParameter, Publicity demo);
    void pageviewPlusOne(Publicity tblHomePublicity);
}