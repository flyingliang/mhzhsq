package com.lzxuni.modules.pccontrol.home.life.company.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.life.company.entity.LifeCompany;
import com.lzxuni.modules.pccontrol.home.life.company.service.LifeCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 * 首页--生活服务--单位管理 前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@RestController
@RequestMapping("/pccontrol/home/life/company")
public class LifeCompanyController extends BaseController {

    @Autowired
    private LifeCompanyService tblHomeLifeCompanyService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/company/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, LifeCompany tblHomeLifeCompany, String Keyword) {
        tblHomeLifeCompany.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        tblHomeLifeCompany.setCompanyname(Keyword);
        PageData pageData = getPageData(tblHomeLifeCompanyService.queryPage(pageParameter, tblHomeLifeCompany));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/GetTree")
    public Object GetTree(String communityid) {
        List<Tree> villageList = tblHomeLifeCompanyService.getTree(communityid);
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/company/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    public Object SaveForm(LifeCompany tblHomeLifeCompany, String keyValue, String isshow, String sjs) throws Exception{
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            tblHomeLifeCompany.setId(ywId);
            tblHomeLifeCompanyService.save(tblHomeLifeCompany);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), ywId, "单位图片", "company", null);
            }
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            tblHomeLifeCompanyService.updateById(tblHomeLifeCompany);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), keyValue, "单位图片", "company", null);
                tblHomeLifeCompany.setId(keyValue);
            }
            return R.ok("修改成功");
        }

    }
    @RequestMapping("/IsShow")
    public Object IsShow(LifeCompany tblHomeLifeCompany, String keyValue, String isshow) throws Exception{

        if(StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)){
            tblHomeLifeCompany.setId(keyValue);
            tblHomeLifeCompany.setIsshow(isshow);
            tblHomeLifeCompanyService.updateById(tblHomeLifeCompany);
            return R.ok("展示成功");
        }else {
            tblHomeLifeCompany.setId(keyValue);
            tblHomeLifeCompany.setIsshow(isshow);
            tblHomeLifeCompanyService.updateById(tblHomeLifeCompany);
            return R.ok("取消展示成功");
        }
    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue) throws Exception{
        LifeCompany tblHomeLifeCompany = tblHomeLifeCompanyService.getById(keyValue);
        return R.ok().put("data",tblHomeLifeCompany);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/company/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("sjs");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
        return mv;
    }
    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        tblHomeLifeCompanyService.removeById(keyValue);
        return R.ok("删除成功");
    }



}

