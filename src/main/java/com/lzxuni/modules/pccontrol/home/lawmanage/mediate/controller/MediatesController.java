package com.lzxuni.modules.pccontrol.home.lawmanage.mediate.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.lawmanage.mediate.entity.Mediates;
import com.lzxuni.modules.pccontrol.home.lawmanage.mediate.service.MediatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 * 首页--综治--矛盾调解案例 前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
@RestController
@RequestMapping("/tblHomeLawmanageMediate")
public class MediatesController extends BaseController {
    @Autowired
    private MediatesService mediateService;
    @Autowired
    private FileEntityService fileEntityService;
    @RequestMapping("/Index")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/mediate/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(mediateService.queryPage(pageParameter, null));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/mediate/form");
        return mv;
    }

    //新增修改
    @RequestMapping("/SaveForm")
    public Object insertDo(Mediates comtraining, String keyValue, String sjs) throws Exception{

        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), ywId, "援助案例照片", "sj", null);
            }
            comtraining.setId(ywId);
            mediateService.save(comtraining);
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            mediateService.updateById(comtraining);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), keyValue, "援助案例照片", "sj", null);
                comtraining.setId(keyValue);
            }
            return R.ok("修改成功");
        }

    }

    //图片ID返回给实体类  Update页传回
    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue){
        Mediates comtraining = mediateService.getById(keyValue);
        return R.ok().put("data",comtraining);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/mediate/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("sjs");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
        return mv;
    }

    @RequestMapping("/GetTree")
    public Object GetTree() {
        List<Tree> villageList = mediateService.getTree();
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        mediateService.removeById(keyValue);
        return R.ok("删除成功");
    }
}

