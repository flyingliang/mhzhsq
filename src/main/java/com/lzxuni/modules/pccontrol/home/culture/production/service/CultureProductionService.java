package com.lzxuni.modules.pccontrol.home.culture.production.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.college.entity.CultureCollege;
import com.lzxuni.modules.pccontrol.home.culture.production.entity.CultureProduction;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理-文化作品表 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-07-16
 */
public interface CultureProductionService extends IService<CultureProduction> {
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, CultureProduction tblHomeCultureProduction);

    List<Tree> getTree(String communityid);

    //查询
    PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, CultureProduction cultureProduction, String ywType);
}
