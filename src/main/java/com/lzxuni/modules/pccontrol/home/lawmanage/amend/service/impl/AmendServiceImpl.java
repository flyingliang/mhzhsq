package com.lzxuni.modules.pccontrol.home.lawmanage.amend.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.amend.entity.Amend;
import com.lzxuni.modules.pccontrol.home.lawmanage.amend.mapper.AmendMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.amend.service.AmendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区矫正条例 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
@Service
public class AmendServiceImpl extends ServiceImpl<AmendMapper, Amend> implements AmendService {

    @Autowired
    private AmendMapper tblHomeLawmanageLegislationMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Amend tblHomeLawmanageLegislation) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeLawmanageLegislationMapper.selectListByCondition(tblHomeLawmanageLegislation);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Amend tblHomeLawmanageLegislation) {
        String uuid = UuidUtil.get32UUID();
        tblHomeLawmanageLegislation.setId(uuid);
        this.save(tblHomeLawmanageLegislation);
    }

    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = tblHomeLawmanageLegislationMapper.getTree(communityid);
        return list;
    }
    @Override
    public PageInfo<Amend> queryPage3(PageParameter pageParameter, Amend demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Amend> homeLifeAffairs = tblHomeLawmanageLegislationMapper.selectListAll(demo);
        PageInfo<Amend> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }
}
