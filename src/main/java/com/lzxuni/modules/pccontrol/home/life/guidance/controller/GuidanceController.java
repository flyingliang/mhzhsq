package com.lzxuni.modules.pccontrol.home.life.guidance.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.guidance.entity.Guidance;
import com.lzxuni.modules.pccontrol.home.life.guidance.service.GuidanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


/**
 * <p>
 * 生活服务-政务大厅-办事指南 前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-07-11
 */
@RestController
@RequestMapping("/pccontrol/home/life/guidance")
public class GuidanceController extends BaseController {

    @Autowired
    private GuidanceService tblHomeLifeGuidanceService;
    //跳转首页
    @RequestMapping("/Index")
    public ModelAndView Index() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/guidance/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Guidance tblHomeLifeGuidance) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblHomeLifeGuidanceService.queryPage(pageParameter, null));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/guidance/form");
        return mv;
    }

//    @RequestMapping("/SaveForm")
//    public Object SaveForm(TblHomeLifeGuidance tblHomeLifeGuidance, String keyValue){
//        if(StringUtils.isNotEmpty(keyValue)){
//            tblHomeLifeGuidance.setId(keyValue);
//            tblHomeLifeGuidanceService.updateById(tblHomeLifeGuidance);
//            return R.ok("修改成功");
//        }else{
//            tblHomeLifeGuidanceService.SaveForm(tblHomeLifeGuidance);
//            return R.ok("保存成功");
//        }
//
//    }

    @RequestMapping("/SaveForm")
    public Object SaveForm(Guidance tblHomeLifeGuidance, String keyValue, String isshow){
        System.out.println(keyValue+"#############"+isshow);
        if(StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)){
            tblHomeLifeGuidance.setId(keyValue);
            tblHomeLifeGuidance.setIsshow(isshow);
            tblHomeLifeGuidanceService.updateById(tblHomeLifeGuidance);
            return R.ok("展示成功");
        }else if(StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)){
            tblHomeLifeGuidance.setId(keyValue);
            tblHomeLifeGuidance.setIsshow(isshow);
            tblHomeLifeGuidanceService.updateById(tblHomeLifeGuidance);
            return R.ok("取消展示成功");
        }else if(StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)){

            tblHomeLifeGuidance.setId(keyValue);
            tblHomeLifeGuidance.setIsdel("0");
            tblHomeLifeGuidanceService.updateById(tblHomeLifeGuidance);
            return R.ok("删除成功");
        }else if(StringUtils.isNotEmpty(keyValue)){
            tblHomeLifeGuidance.setId(keyValue);
            tblHomeLifeGuidanceService.updateById(tblHomeLifeGuidance);
            return R.ok("修改成功");
        }else{
            tblHomeLifeGuidanceService.SaveForm(tblHomeLifeGuidance);
            return R.ok("保存成功");
        }

    }
}

