package com.lzxuni.modules.pccontrol.home.culture.college.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.culture.college.entity.CultureCollege;
import com.lzxuni.modules.pccontrol.home.culture.college.mapper.CultureCollegeMapper;
import com.lzxuni.modules.pccontrol.home.culture.college.service.CultureCollegeService;
import com.lzxuni.modules.pccontrol.home.party.style.entity.Style;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理-市民学院表 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-07-17
 */
@Service
public class CultureCollegeServiceImpl extends ServiceImpl<CultureCollegeMapper, CultureCollege> implements CultureCollegeService {
@Autowired
    private CultureCollegeMapper tblHomeCultureCollegeMapper;
    @Override
    public PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, CultureCollege tblHomeCultureCollege) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeCultureCollegeMapper.selectListByCondition(tblHomeCultureCollege);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }


    @Override
    public PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter,CultureCollege cultureCollege, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeCultureCollegeMapper.selectListByConditionAndImg(cultureCollege,ywType);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }

}
