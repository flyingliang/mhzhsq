package com.lzxuni.modules.pccontrol.home.aged.organization.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.aged.organization.entity.AgedOrganization;
import com.lzxuni.modules.pccontrol.home.aged.organization.mapper.AgedOrganizationMapper;
import com.lzxuni.modules.pccontrol.home.aged.organization.service.AgedOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 养老管理--养老机构表 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-07-22
 */
@Service
public class AgedOrganizationServiceImpl extends ServiceImpl<AgedOrganizationMapper, AgedOrganization> implements AgedOrganizationService {

    @Autowired
    private AgedOrganizationMapper tblHomeAgedOrganizationMapper;

    @Override
    public PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, AgedOrganization tblHomeAgedOrganization) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeAgedOrganizationMapper.selectListByCondition(tblHomeAgedOrganization);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }
    //养老机构分页
    @Override
    public PageInfo<Map<String, Object>> queryPageAndImg(PageParameter pageParameter, AgedOrganization aged, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeAgedOrganizationMapper.selectListByConditionAndImg(aged,ywType);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }

}
