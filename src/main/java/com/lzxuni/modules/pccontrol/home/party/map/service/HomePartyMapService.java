package com.lzxuni.modules.pccontrol.home.party.map.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.party.map.entity.HomePartyMap;

import java.util.List;

/**
 * 主页_党建_地图 服务类
 * Created by 潘云明
 * 2019/7/5 16:08
 */
public interface HomePartyMapService extends IService<HomePartyMap> {
    //查询
	PageInfo<HomePartyMap> queryPage(PageParameter pageParameter, HomePartyMap homePartyMap);
	//保存
    void SaveForm(HomePartyMap homePartyMap);

    //微醺查询list
    List<HomePartyMap> queryList();


}
