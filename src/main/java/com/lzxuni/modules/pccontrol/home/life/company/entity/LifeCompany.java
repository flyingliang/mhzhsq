package com.lzxuni.modules.pccontrol.home.life.company.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 首页--生活服务--单位管理
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_life_company")
public class LifeCompany implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId
    private String id;

    /**
     * 小区编号
     */
    private String villageid;

    /**
     * 楼号
     */
    private String buildingnumname;

    /**
     * 单位名称
     */
    private String companyname;

    /**
     * 单位电话
     */
    private String companyphone;

    /**
     * 单位类别ID
     */
    private String companytypeid;

    /**
     * 介绍
     */
    private String introduction;

    /**
     * 1未删除，0删除
     */
    private String isdel;

    /** 1表示展示，0隐藏*/
    private String isshow;
    /**
     * 建立时间
     */
    private LocalDateTime date;

    /** 修改时间 */
    private Date updatetime;


    /** 简介*/
    private String content;
    /** 经度*/
    private String lon;
    /** 维度*/
    private String lat;

    /** 图片 */
    @TableField(exist=false)
    private String imgurl;
    /** 业务类型 */
    @TableField(exist=false)
    private String ywtype;
    /** 小区 */
    @TableField(exist=false)
    private String villagename;
    /** 社区 */
    @TableField(exist=false)
    private String communityname;

}
