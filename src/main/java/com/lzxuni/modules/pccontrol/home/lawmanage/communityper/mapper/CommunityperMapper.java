package com.lzxuni.modules.pccontrol.home.lawmanage.communityper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.communityper.entity.Communityper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区服务人员表 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-08-20
 */
public interface CommunityperMapper extends BaseMapper<Communityper> {
    //社区联动
    List<Map<String,Object>> selectListByCondition(Communityper communityper);
    List<Tree> getTree();
    List<Communityper> selectListAll(Communityper demo);
    List<Communityper> selectpeopleAll(Communityper demo);
    List<Communityper> selectpeopleAll2(Communityper demo);

    Communityper queryBymcId(Communityper demo);
}
