package com.lzxuni.modules.pccontrol.home.life.company.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.life.company.entity.LifeCompany;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--生活服务--单位管理 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
public interface LifeCompanyMapper extends BaseMapper<LifeCompany> {

    //社区联动
    List<Map<String,Object>> selectListByCondition(LifeCompany tblHomeLifeCompany);
    List<LifeCompany> selectdemoList(LifeCompany tblHomeLifeCompany);
    List<Tree> getTree(@Param("communityid") String communityid);

    LifeCompany selectcompanybyid(LifeCompany demo);
}
