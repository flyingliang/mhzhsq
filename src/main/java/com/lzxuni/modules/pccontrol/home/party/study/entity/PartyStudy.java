package com.lzxuni.modules.pccontrol.home.party.study.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lzxuni.modules.common.entity.FileEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 党建管理-->在线学习管理
 * </p>
 *
 * @author fcd
 * @since 2019-08-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_party_study")
public class PartyStudy implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 类型
     */
    private String studytype;

    /**
     * 标题
     */
    private String studytitle;

    /**
     * 社区
     */
    private String communityid;

    /**
     * 内容
     */
    private String content;

    /**
     * 展示与不展示
     */
    private String isshow;

    /**
     * 创建时间
     */
    private Date date;

    /**
     * 0为删除
     */
    private String isdel;
    /** 社区名称 */
    @TableField(exist = false)
    private String communityname;

    @TableField(exist = false)
    private List<FileEntity> fileEntities;

    @TableField(exist = false)
    private String urlPath;//web访问路径

    @TableField(exist = false)
    private String urlsPath;//缩略图访问路径

}
