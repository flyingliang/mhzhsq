package com.lzxuni.modules.pccontrol.home.life.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.life.shop.entity.Shop;
import com.lzxuni.modules.pccontrol.home.life.shop.mapper.ShopMapper;
import com.lzxuni.modules.pccontrol.home.life.shop.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-19
 */
@Service
public class ShopServiceImpl extends ServiceImpl<ShopMapper, Shop> implements ShopService {

    @Autowired
    private ShopMapper tblHomeLifeShopMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Shop tblHomeLifeShop) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeLifeShopMapper.selectListByCondition(tblHomeLifeShop);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public  PageInfo<Shop> selectshopList(PageParameter pageParameter,Shop demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Shop> villageList = tblHomeLifeShopMapper.selectshopList(demo);
        PageInfo<Shop> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Shop tblHomeLifeShop) {
        String uuid = UuidUtil.get32UUID();
        tblHomeLifeShop.setId(uuid);
        this.save(tblHomeLifeShop);
    }

     @Override
    public Shop selectshopbyid(Shop tblHomeLifeShop) {
         return tblHomeLifeShopMapper.selectshopbyid(tblHomeLifeShop);
    }

    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = tblHomeLifeShopMapper.getTree(communityid);
        return list;
    }

    @Override
    public List<Tree> getTree1(String shoptypeid) {
        List<Tree> list = tblHomeLifeShopMapper.getTree1(shoptypeid);
        return list;
    }
}
