package com.lzxuni.modules.pccontrol.home.lawmanage.onlineconsultation.mapper;

import com.lzxuni.modules.pccontrol.home.lawmanage.onlineconsultation.entity.Onlineconsultation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.life.medicine.entity.Medicine;

import java.util.List;

/**
 * <p>
 * 首页--综治管理--在线咨询管理 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
public interface OnlineconsultationMapper extends BaseMapper<Onlineconsultation> {
    List<Onlineconsultation> selectListByopenid(Onlineconsultation demo);
    List<Onlineconsultation> selectListAll(Onlineconsultation demo);
}
