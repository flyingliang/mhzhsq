package com.lzxuni.modules.pccontrol.home.lawmanage.conflictmediation.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.lawmanage.conflictmediation.entity.Conflictmediation;
import com.lzxuni.modules.pccontrol.home.lawmanage.conflictmediation.service.ConflictmediationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 * 首页--综治--社区矛盾调解 前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-07-29
 */
@RestController
@RequestMapping("/tblHomeLawmanageConflictmediation")
public class ConflictmediationController extends BaseController {

    @Autowired
    private ConflictmediationService tblHomeLawmanageConflictmediationService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看小区楼号", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/conflictmediation/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Conflictmediation tblHomeLawmanageConflictmediation, String Keyword) {
        tblHomeLawmanageConflictmediation.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
//        tblHomeLawmanageConflictmediation.setSname(Keyword);
        PageData pageData = getPageData(tblHomeLawmanageConflictmediationService.queryPage(pageParameter, tblHomeLawmanageConflictmediation));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/GetTree")
    public Object GetTree(String communityid) {
        List<Tree> villageList = tblHomeLawmanageConflictmediationService.getTree(communityid);
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/conflictmediation/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    public Object SaveForm(Conflictmediation tblHomeLawmanageConflictmediation, String keyValue, String isshow, String sjs) throws Exception{
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            tblHomeLawmanageConflictmediation.setId(ywId);
            tblHomeLawmanageConflictmediationService.save(tblHomeLawmanageConflictmediation);
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            tblHomeLawmanageConflictmediationService.updateById(tblHomeLawmanageConflictmediation);
            return R.ok("修改成功");
        }

    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue) throws Exception{
        Conflictmediation tblHomeLawmanageConflictmediation = tblHomeLawmanageConflictmediationService.getById(keyValue);
        return R.ok().put("data",tblHomeLawmanageConflictmediation);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/conflictmediation/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("sjs");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
        return mv;
    }

    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        tblHomeLawmanageConflictmediationService.removeById(keyValue);
        return R.ok("删除成功");
    }

}

