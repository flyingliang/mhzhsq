package com.lzxuni.modules.pccontrol.home.lawmanage.communityper.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.communityper.entity.Communityper;
import com.lzxuni.modules.pccontrol.home.lawmanage.communityper.mapper.CommunityperMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.communityper.service.CommunityperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区服务人员表 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-20
 */
@Service
public class CommunityperServiceImpl extends ServiceImpl<CommunityperMapper, Communityper> implements CommunityperService {

    @Autowired
    private CommunityperMapper communityperMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Communityper communityper) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = communityperMapper.selectListByCondition(communityper);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Communityper communityper) {
        String uuid = UuidUtil.get32UUID();
        communityper.setId(uuid);
        this.save(communityper);
    }

    @Override
    public List<Tree> getTree() {
        List<Tree> list = communityperMapper.getTree();
        return list;
    }

    @Override
    public List<Communityper> queryPage3( Communityper demo) {

        List<Communityper> homeLifeAffairs = communityperMapper.selectListAll(demo);

        return homeLifeAffairs;
    }

    @Override
    public List<Communityper> selectpeopleAll( Communityper demo) {

        List<Communityper> homeLifeAffairs = communityperMapper.selectpeopleAll(demo);

        return homeLifeAffairs;
    }

    @Override
    public List<Communityper> selectpeopleAll2( Communityper demo) {

        List<Communityper> homeLifeAffairs = communityperMapper.selectpeopleAll2(demo);

        return homeLifeAffairs;
    }

    public  Communityper queryBymcId(Communityper demmo){
        return communityperMapper.queryBymcId(demmo);
    }

}
