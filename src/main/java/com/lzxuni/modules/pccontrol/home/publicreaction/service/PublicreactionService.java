package com.lzxuni.modules.pccontrol.home.publicreaction.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.publicreaction.entity.Publicreaction;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理-->社情反应管理 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-08-07
 */
public interface PublicreactionService extends IService<Publicreaction> {
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Publicreaction tblHomePublicreaction);

    List<Tree> getTree();
}
