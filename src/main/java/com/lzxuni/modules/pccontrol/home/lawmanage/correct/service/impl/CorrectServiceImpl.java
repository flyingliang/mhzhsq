package com.lzxuni.modules.pccontrol.home.lawmanage.correct.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.correct.entity.Correct;
import com.lzxuni.modules.pccontrol.home.lawmanage.correct.mapper.CorrectMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.correct.service.CorrectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区矫正 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-25
 */
@Service
public class CorrectServiceImpl extends ServiceImpl<CorrectMapper, Correct> implements CorrectService {

    @Autowired
    private CorrectMapper tblHomeLawmanageCorrectMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Correct tblHomeLawmanageCorrect) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeLawmanageCorrectMapper.selectListByCondition(tblHomeLawmanageCorrect);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Correct tblHomeLawmanageCorrect) {
        String uuid = UuidUtil.get32UUID();
        tblHomeLawmanageCorrect.setId(uuid);
        this.save(tblHomeLawmanageCorrect);
    }

    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = tblHomeLawmanageCorrectMapper.getTree(communityid);
        return list;
    }

    @Override
    public PageInfo<Correct> selectListall(PageParameter pageParameter, Correct demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Correct> homeLifeAffairs = tblHomeLawmanageCorrectMapper.selectListall(demo);
        PageInfo<Correct> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }

    @Override
    public List<Correct> selectListthree( Correct demo) {

        List<Correct> homeLifeAffairs = tblHomeLawmanageCorrectMapper.selectListthree(demo);

        return homeLifeAffairs;
    }





}
