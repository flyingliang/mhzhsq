package com.lzxuni.modules.pccontrol.home.lawmanage.communityper.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 首页--综治--社区服务人员表
 * </p>
 *
 * @author Lhl
 * @since 2019-08-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_lawmanage_communityper")
public class Communityper implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId
    private String id;

    /**
     * 所属社区
     */
    private String communityid;

    /**
     * 社区工作人员编号
     */
    private String numberid;

    /**
     * 社区工作人员姓名
     */
    private String name;

    /**
     * 电话
     */
    private String phone;

    /**
     * 建立时间
     */
    private LocalDateTime date;

    /**
     * 1未删除，0删除
     */
    private String isdel;

    /** 回复图片 */
    @TableField(exist=false)
    private String imgurl;
    /** 业务类型 */
    @TableField(exist=false)
    private String ywtype;
    /** 业务类型 */
    @TableField(exist=false)
    private String communityname;
    /** 连表名称 */
    @TableField(exist=false)
    private String tablename;
}
