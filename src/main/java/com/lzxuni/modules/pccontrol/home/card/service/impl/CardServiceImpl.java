package com.lzxuni.modules.pccontrol.home.card.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.real.entity.RealCar;
import com.lzxuni.modules.pccontrol.home.card.entity.Card;
import com.lzxuni.modules.pccontrol.home.card.mapper.CardMapper;
import com.lzxuni.modules.pccontrol.home.card.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 办证 服务实现类
 * Created by 潘云明
 * 2019/7/16
 */
@Service
public class CardServiceImpl extends ServiceImpl<CardMapper, Card> implements CardService {

    @Autowired
    private CardMapper cardMapper;
    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Card entity) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = cardMapper.selectListByCondition(entity);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

}
