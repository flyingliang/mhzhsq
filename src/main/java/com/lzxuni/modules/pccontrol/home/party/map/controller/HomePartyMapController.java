package com.lzxuni.modules.pccontrol.home.party.map.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.constant.LogConstant;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.party.map.entity.HomePartyMap;
import com.lzxuni.modules.pccontrol.home.party.map.service.HomePartyMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 主页_党建_地图 前端控制器
 * Created by 潘云明
 * 2019/7/5 16:08
 */

@RestController
@RequestMapping("/pccontrol/home/party/map")
public class HomePartyMapController extends BaseController {
    @Autowired
    private HomePartyMapService homePartyMapService;
    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看党建地图", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/party/map/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, HomePartyMapController homePartyMap) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(homePartyMapService.queryPage(pageParameter, null));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/party/map/form");
        return mv;
    }
    @SysLog(categoryId = LogConstant.OPERATEID,module = "党建地图",operateType = LogConstant.INSERT)
    @RequestMapping("/SaveForm")
    public Object SaveForm(HomePartyMap homePartyMap, String keyValue, String isshow){
        System.out.println(keyValue+"#############"+isshow);
        if(StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)){
            homePartyMap.setId(keyValue);
            homePartyMap.setIsshow(isshow);
            homePartyMapService.updateById(homePartyMap);
            return R.ok("展示成功");
        }else if(StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)){
            homePartyMap.setId(keyValue);
            homePartyMap.setIsshow(isshow);
            homePartyMapService.updateById(homePartyMap);
            return R.ok("取消展示成功");
        }else if(StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)){

            homePartyMap.setId(keyValue);
            homePartyMap.setIsdel("0");
            homePartyMapService.updateById(homePartyMap);
            return R.ok("删除成功");
        }else if(StringUtils.isNotEmpty(keyValue)){
            homePartyMap.setId(keyValue);
            homePartyMapService.updateById(homePartyMap);
            return R.ok("修改成功");
        }else{
            homePartyMapService.SaveForm(homePartyMap);
            return R.ok("保存成功");
        }

    }
//    @SysLog(categoryId = LogConstant.OPERATEID,module = "新闻媒体-ZZYH-境内",operateType = LogConstant.DELETE)
//    @RequestMapping("/DeleteForm")
//    public Object DeleteForm(String keyValue){
//        newsZzyhDomesticService.removeById(keyValue);
//        return R.ok("删除成功");
//    }
}
