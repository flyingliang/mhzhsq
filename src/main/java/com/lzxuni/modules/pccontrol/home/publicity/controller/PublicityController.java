package com.lzxuni.modules.pccontrol.home.publicity.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.organization.entity.User;
import com.lzxuni.modules.pccontrol.home.publicity.entity.Publicity;
import com.lzxuni.modules.pccontrol.home.publicity.service.PublicityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * <p>
 * 基础管理-->政务公开管理 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-08-08
 */
@RestController
@RequestMapping("/tblHomePublicity")
public class PublicityController extends BaseController {
    //    @Autowired
//    private FileEntityService fileEntityService;
    @Autowired
    private PublicityService tblHomePublicityService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "基础管理-政务公开管理", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/publicity/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Publicity tblHomePublicity) {
        tblHomePublicity.setIsdel("1");
        tblHomePublicity.setThistype("0");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblHomePublicityService.queryPage(pageParameter, tblHomePublicity));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/publicity/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3, module = "基础管理-政务公开管理", operateType = "操作")
    public Object insertDo(Publicity tblHomePublicity, String keyValue, String isshow, String stylepics) throws Exception {
//        System.out.println("###参数列表：keyValue："+keyValue+",isshow:"+isshow);
//        String typeName="社区养老图片";
//        String ywType="organization";

        if (StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)) {//展示
            tblHomePublicity.setId(keyValue);
            tblHomePublicity.setIsshow(isshow);
            tblHomePublicityService.updateById(tblHomePublicity);
            return R.ok("发布成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)) {//取消展示
            tblHomePublicity.setId(keyValue);
            tblHomePublicity.setIsshow(isshow);
            tblHomePublicityService.updateById(tblHomePublicity);
            return R.ok("取消发布成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)) {//删除
            tblHomePublicity.setId(keyValue);
            tblHomePublicity.setIsdel("0");
            tblHomePublicityService.updateById(tblHomePublicity);
            return R.ok("删除成功");
        }
//        else if (StringUtils.isNotEmpty(keyValue) && "4".equals(isshow)) {//返回图片数量
//            FileEntity fileBeanCustom = new FileEntity();
//            fileBeanCustom.setYwId(keyValue);
//            fileBeanCustom.setYwType(ywType);
//            return R.ok().put("data",fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        }
        else if (StringUtils.isNotEmpty(keyValue)) {
//            fileEntityService.deleteByYwId(keyValue);
//            if(!StringUtils.isEmpty(stylepics) && !"&amp;nbsp;".equals(stylepics)) {
//                fileEntityService.insert(stylepics.replace("&quot;", "\""), keyValue, typeName, ywType, null);
//            }

            tblHomePublicity.setId(keyValue);
            tblHomePublicityService.updateById(tblHomePublicity);
            return R.ok("修改成功");
        } else {
            String ywId = UuidUtil.get32UUID();
//            if (!StringUtils.isEmpty(stylepics) && !"&amp;nbsp;".equals(stylepics)) {
//              fileEntityService.insert(stylepics.replace("&quot;", "\""), ywId, typeName, ywType, null);
//           }

            User user= getUser();
            System.out.println(user.getUsername());
            tblHomePublicity.setUsername(user.getRealName());
            tblHomePublicity.setAccount(user.getUsername());
            tblHomePublicity.setId(ywId);
            tblHomePublicity.setIsdel("1");
            tblHomePublicity.setIsshow("0");
            tblHomePublicity.setThistype("0");
            tblHomePublicity.setDate(new Date());
            tblHomePublicityService.save(tblHomePublicity);
            return R.ok("保存成功");
        }
    }
}

