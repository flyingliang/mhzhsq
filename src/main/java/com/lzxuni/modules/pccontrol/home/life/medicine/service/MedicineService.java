package com.lzxuni.modules.pccontrol.home.life.medicine.service;

import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.lawmanage.amend.entity.Amend;
import com.lzxuni.modules.pccontrol.home.life.affair.entity.Affair;
import com.lzxuni.modules.pccontrol.home.life.medicine.entity.Medicine;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
public interface MedicineService extends IService<Medicine> {

    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Medicine tblHomeLawmanageLegislation);

    //查询
//    PageInfo<Medicine> queryPage(PageParameter pageParameter, Medicine tblHomeLifeMedicine);
    //查询
    PageInfo<Medicine> queryPage2(PageParameter pageParameter, Medicine homeLifeAffair);
    //查询
    PageInfo<Medicine> queryPage3(PageParameter pageParameter, Medicine homeLifeAffair);
    //保存
    void SaveForm(Medicine tblHomeLifeMedicine);

}
