package com.lzxuni.modules.pccontrol.home.volunteer.tissue.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 志愿服务--志愿者组织表
 * </p>
 *
 * @author fcd
 * @since 2019-07-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_volunteer_tissue")
public class VolunteerTissue implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 组织名称
     */
    private String tissuename;

    /**
     * 来源
     */
    private String source;

    /**
     * 介绍
     */
    private String introduce;

    /**
     * 所属社区
     */
    private String communityid;

    /**
     * 1表示展示，0隐藏
     */
    private String isshow;

    /**
     * 添加时间
     */
    private Date date;

    /**
     * 1未删除，0删除
     */
    private String isdel;


}
