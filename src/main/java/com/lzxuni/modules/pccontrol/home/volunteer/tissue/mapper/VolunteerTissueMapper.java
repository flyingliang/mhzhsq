package com.lzxuni.modules.pccontrol.home.volunteer.tissue.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.volunteer.join.entity.VolunteerJoin;
import com.lzxuni.modules.pccontrol.home.volunteer.tissue.entity.VolunteerTissue;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 志愿服务--志愿者组织表 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-07-24
 */
public interface VolunteerTissueMapper extends BaseMapper<VolunteerTissue> {
    List<Map<String,Object>> selectListByCondition();
    List<Map<String, Object>> selectListByConditionAndImg(@Param("volunteerTissue") VolunteerTissue volunteerTissue, @Param("ywType") String ywType);

}
