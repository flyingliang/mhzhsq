package com.lzxuni.modules.pccontrol.home.lawmanage.mediate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.mediate.entity.Mediates;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--矛盾调解案例 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
public interface MediatesMapper extends BaseMapper<Mediates>  {
    //社区联动
    List<Map<String,Object>> selectListByCondition(Mediates communityper);
    List<Tree> getTree();
    List<Mediates> selectListAll(Mediates demo);

    Mediates queryBymcId(Mediates demo);
}
