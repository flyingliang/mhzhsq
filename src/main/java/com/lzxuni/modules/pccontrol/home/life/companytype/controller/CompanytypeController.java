package com.lzxuni.modules.pccontrol.home.life.companytype.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.life.companytype.entity.Companytype;
import com.lzxuni.modules.pccontrol.home.life.companytype.service.CompanytypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;

/**
 * <p>
 * 首页--生活服务--单位类别 前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@RestController
@RequestMapping("/pccontrol/home/life/companytype")
public class CompanytypeController extends BaseController {

    @Autowired
    private CompanytypeService tblHomeLifeCompanytypeService;

    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/companytype/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Companytype tblHomeLifeCompanytype, String Keyword) throws SQLException {
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        tblHomeLifeCompanytype.setCompanytypename(Keyword);
        PageData pageData = getPageData(tblHomeLifeCompanytypeService.queryPage(pageParameter, tblHomeLifeCompanytype));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView insert() throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/companytype/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    public Object insertDo(String keyValue, Companytype tblHomeLifeCompanytype, String isShow, String sjs) throws Exception{
        if(StringUtils.isNotEmpty(keyValue) && "1".equals(isShow)){
            tblHomeLifeCompanytype.setId(keyValue);
            tblHomeLifeCompanytype.setIsshow(isShow);
            tblHomeLifeCompanytypeService.updateById(tblHomeLifeCompanytype);
            return R.ok("展示成功");
        }else if(StringUtils.isNotEmpty(keyValue) && "0".equals(isShow)){
            tblHomeLifeCompanytype.setId(keyValue);
            tblHomeLifeCompanytype.setIsshow(isShow);
            tblHomeLifeCompanytypeService.updateById(tblHomeLifeCompanytype);
            return R.ok("取消展示成功");
        }else if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            System.out.println("++++"+sjs);
            tblHomeLifeCompanytype.setId(ywId);
            tblHomeLifeCompanytypeService.save(tblHomeLifeCompanytype);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), ywId, "单位类别图片", "companytype", null);
            }
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            tblHomeLifeCompanytypeService.updateById(tblHomeLifeCompanytype);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), keyValue, "单位类别图片", "companytype", null);
                tblHomeLifeCompanytype.setId(keyValue);
            }
            return R.ok("修改成功");
        }

    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue) throws Exception{
        Companytype tblHomeLifeCompanytype = tblHomeLifeCompanytypeService.getById(keyValue);
        return R.ok().put("data",tblHomeLifeCompanytype);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/companytype/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("sjs");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
        return mv;
    }


    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        tblHomeLifeCompanytypeService.removeById(keyValue);
        return R.ok("删除成功");
    }

}

