package com.lzxuni.modules.pccontrol.home.lawmanage.safety.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 首页--综治--平安创建条例
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_lawmanage_safety")
public class Safety implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId
    private String id;

    /**
     * 所属社区
     */
    private String communityid;

    /**
     * 工作内容
     */
    private String content;

    /**
     * 创建目标
     */
    private String scope;

    /**
     * 1未删除，0删除
     */
    private String isdel;

    /**
     * 建立时间
     */
    private LocalDateTime date;

}
