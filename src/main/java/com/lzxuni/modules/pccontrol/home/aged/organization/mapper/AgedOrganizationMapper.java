package com.lzxuni.modules.pccontrol.home.aged.organization.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.aged.organization.entity.AgedOrganization;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 养老管理--养老机构表 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-07-22
 */
public interface AgedOrganizationMapper extends BaseMapper<AgedOrganization> {
    List<Map<String,Object>> selectListByCondition(AgedOrganization tblHomeAgedOrganization);
    List<Map<String,Object>> selectListByConditionAndImg(AgedOrganization aged, String ywType);
}
