package com.lzxuni.modules.pccontrol.home.gridding.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 基础管理-->我的网格管理
 * </p>
 *
 * @author fcd
 * @since 2019-08-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_gridding")
public class Gridding implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 网格名
     */
    private String gridding;

    /**
     * 网格长姓名
     */
    private String griddingname;

    /**
     * 网格长电话
     */
    private String griddingphone;

    /**
     * 楼号id
     */
    private String buildingnumid;

    /**
     * 审核通过与取消审核
     */
    private String isshow;

    /**
     * 创建时间
     */
    private Date date;

    /**
     * 1未删除0已删除
     */
    private String isdel;


}
