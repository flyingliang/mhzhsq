package com.lzxuni.modules.pccontrol.home.publicreaction.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.publicreaction.entity.Publicreaction;
import com.lzxuni.modules.pccontrol.home.publicreaction.mapper.PublicreactionMapper;
import com.lzxuni.modules.pccontrol.home.publicreaction.service.PublicreactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理-->社情反应管理 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-08-07
 */
@Service
public class PublicreactionServiceImpl extends ServiceImpl<PublicreactionMapper, Publicreaction> implements PublicreactionService {
    @Autowired
    private PublicreactionMapper tblHomePublicreactionMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Publicreaction tblHomePublicreaction) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomePublicreactionMapper.selectListByCondition(tblHomePublicreaction);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;

    }
    @Override
    public List<Tree> getTree() {
        List<Tree> list = tblHomePublicreactionMapper.getTree();
        return list;
    }
}
