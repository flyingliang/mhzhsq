package com.lzxuni.modules.pccontrol.home.volunteer.vractivity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import com.lzxuni.modules.pccontrol.home.volunteer.vractivity.entity.VolunteerActivity;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 志愿管理-->志愿者活动表 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-07-29
 */
public interface VolunteerActivityService extends IService<VolunteerActivity> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, VolunteerActivity tblHomeVolunteerActivity);
    //查询
    PageInfo<Map<String,Object>> queryList(PageParameter pageParameter, VolunteerActivity tblHomeVolunteerActivity);

    List<Tree> getTree();

    void updateAward(String award,String openid);
    void updateAwards(String award,String joinid);


    //查询
    PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, VolunteerActivity volunteerActivity, String ywType);

    //查询
    PageInfo<Map<String,Object>> selectTask(PageParameter pageParameter, VolunteerActivity volunteerActivity);

    //查询
    PageInfo<Map<String,Object>> selectTasks(PageParameter pageParameter,String openid);


}
