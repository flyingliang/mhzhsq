package com.lzxuni.modules.pccontrol.home.gridding.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.gridding.entity.Gridding;
import com.lzxuni.modules.pccontrol.home.gridding.mapper.GriddingMapper;
import com.lzxuni.modules.pccontrol.home.gridding.service.GriddingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理-->我的网格管理 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-08-08
 */
@Service
public class GriddingServiceImpl extends ServiceImpl<GriddingMapper, Gridding> implements GriddingService {
    @Autowired
    private GriddingMapper tblHomeGriddingMapper;
    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Gridding tblHomeGridding) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeGriddingMapper.selectListByCondition(tblHomeGridding);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }
    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = tblHomeGriddingMapper.getTree(communityid);
        return list;
    }
    @Override
    public List<Tree> getTreev(String villageid) {
        List<Tree> list = tblHomeGriddingMapper.getTreev(villageid);
        return list;
    }

    @Override
    public List<Tree> getTreevillage(){
        List<Tree> list = tblHomeGriddingMapper.getTreevillage();
        return list;
    }


}
