package com.lzxuni.modules.pccontrol.home.card.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.pccontrol.home.card.entity.CardZXBZ;
import com.lzxuni.modules.pccontrol.home.card.mapper.CardZXBZMapper;
import com.lzxuni.modules.pccontrol.home.card.service.CardZXBZService;
import org.springframework.stereotype.Service;

/**
 * 办证 服务实现类
 * Created by 潘云明
 * 2019/7/16
 */
@Service
public class CardZXBZServiceImpl extends ServiceImpl<CardZXBZMapper, CardZXBZ> implements CardZXBZService {
//    @Autowired
//    private CardMapper styleMapper;
//    @Override
//    public PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Card style) {
//        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
//            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
//                    pageParameter.getSidx() + " " + pageParameter.getSord());
//        }else{
//            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
//        }
//        List<Map<String,Object>> styleList = styleMapper.selectListByCondition(style);
//        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
//        return pageInfo;
//    }
//
//
//    @Override
//    public PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, Card style, String ywType) {
//        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
//            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
//                    pageParameter.getSidx() + " " + pageParameter.getSord());
//        }else{
//            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
//        }
//        List<Map<String,Object>> styleList = styleMapper.selectListByConditionAndImg(style,ywType);
//        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
//        return pageInfo;
//    }



}
