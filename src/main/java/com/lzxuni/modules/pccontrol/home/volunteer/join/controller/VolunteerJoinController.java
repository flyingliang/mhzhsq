package com.lzxuni.modules.pccontrol.home.volunteer.join.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;

import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.volunteer.join.entity.VolunteerJoin;
import com.lzxuni.modules.pccontrol.home.volunteer.join.service.VolunteerJoinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 志愿服务--在线加入表 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-07-24
 */
@RestController
@RequestMapping("/tblHomeVolunteerJoin")
public class VolunteerJoinController extends BaseController {
    @Autowired
    private FileEntityService fileEntityService;
    @Autowired
    private VolunteerJoinService tblHomeVolunteerJoinService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "志愿服务管理-在线加入管理", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/volunteer/join/index");
        return mv;
    }
    //志愿服务管理-在线加入管理的查询
    @RequestMapping("/GetList")
    public Object GetList(String pagination, VolunteerJoin tblHomeVolunteerJoin) throws SQLException {
        tblHomeVolunteerJoin.setIsdel("1");
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(tblHomeVolunteerJoinService.queryPage(pageParameter, tblHomeVolunteerJoin));
        return R.ok().put("data", pageData);
    }

    //添加、修改
    @RequestMapping("/SaveForm")
    public Object insertDo(String keyValue, String identitypics, String personagepics, VolunteerJoin tblHomeVolunteerJoin, String isshow) throws Exception{

        if(StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)){
            tblHomeVolunteerJoin.setId(keyValue);
            tblHomeVolunteerJoin.setIsshow(isshow);
            tblHomeVolunteerJoinService.updateById(tblHomeVolunteerJoin);
            return R.ok("加入成功");
        }else if(StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)){
            tblHomeVolunteerJoin.setId(keyValue);
            tblHomeVolunteerJoin.setIsshow(isshow);
            tblHomeVolunteerJoinService.updateById(tblHomeVolunteerJoin);
            return R.ok("取消加入成功");
        }else
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            tblHomeVolunteerJoin.setId(ywId);
            tblHomeVolunteerJoin.setDate(new Date());
            tblHomeVolunteerJoin.setIsdel("1");
            tblHomeVolunteerJoin.setIsshow("1");
            tblHomeVolunteerJoin.setVolunteercoin(0);
            tblHomeVolunteerJoinService.save(tblHomeVolunteerJoin);
            if(!StringUtils.isEmpty(identitypics)&& !"&amp;nbsp;".equals(identitypics)) {
                fileEntityService.insert(identitypics.replace("&quot;", "\""), ywId, "社区志愿照片", "identity", null);
            }
            if(!StringUtils.isEmpty(personagepics)&& !"&amp;nbsp;".equals(personagepics)) {
                fileEntityService.insert(personagepics.replace("&quot;", "\""), ywId, "社区志愿照片", "personage", null);
            }
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            tblHomeVolunteerJoinService.updateById(tblHomeVolunteerJoin);
            if(!StringUtils.isEmpty(identitypics) && !"&amp;nbsp;".equals(identitypics)) {
                fileEntityService.insert(identitypics.replace("&quot;", "\""), keyValue, "社区志愿照片", "identity", null);
                tblHomeVolunteerJoin.setId(keyValue);
            }
            if(!StringUtils.isEmpty(personagepics) && !"&amp;nbsp;".equals(personagepics)) {
                fileEntityService.insert(personagepics.replace("&quot;", "\""), keyValue, "社区志愿照片", "personage", null);
                tblHomeVolunteerJoin.setId(keyValue);
            }
            return R.ok("修改成功");
        }
    }
    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId, String keyValue) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/volunteer/join/Update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("identity");
        mv.addObject("identitypicNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
        FileEntity fileBeanCustoms = new FileEntity();
        fileBeanCustoms.setYwId(ywId);
        fileBeanCustoms.setYwType("personage");
        mv.addObject("personagepicNum", fileEntityService.queryNumByFileEntity(fileBeanCustoms));
        return mv;
    }
    //真删假删
    @RequestMapping("/DeleteForm")
    public Object delete(VolunteerJoin tblHomeVolunteerJoin, String keyValue, String isshow) {
        if(StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)){
            tblHomeVolunteerJoin.setId(keyValue);
            tblHomeVolunteerJoin.setIsdel("0");
            tblHomeVolunteerJoinService.updateById(tblHomeVolunteerJoin);
//            fileEntityService.deleteByYwId(keyValue);
//            return R.ok("删除成功");
        }
        return R.ok("删除成功");
    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue){
        VolunteerJoin tblHomeVolunteerJoin = tblHomeVolunteerJoinService.getById(keyValue);
        return R.ok().put("data",tblHomeVolunteerJoin);
    }

    @RequestMapping("/GetTree")
    public Object GetTree() {
        List<Tree> companyList = tblHomeVolunteerJoinService.getTree();
        return R.ok().put("data",companyList);
    }

        @RequestMapping("/Form")
    public ModelAndView insert() throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/volunteer/join/form");
        return mv;
    }
}

