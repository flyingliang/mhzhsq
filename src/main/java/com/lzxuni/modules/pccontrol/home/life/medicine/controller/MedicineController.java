package com.lzxuni.modules.pccontrol.home.life.medicine.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.life.medicine.entity.Medicine;
import com.lzxuni.modules.pccontrol.home.life.medicine.service.MedicineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@RestController
@RequestMapping("/pccontrol/home/life/medicine")
public class MedicineController extends BaseController {

    @Autowired
    private MedicineService tblHomeLifeMedicineService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/medicine/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, MedicineController tblHomeLifeMedicineController) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblHomeLifeMedicineService.queryPage(pageParameter, null));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/medicine/Insert");
        return mv;
    }

    //查看跳转页
    @RequestMapping("/FormLook")
    public ModelAndView FormLook(String keyValue) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/medicine/form");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(keyValue);
        fileBeanCustom.setYwType("kefus");
        mv.addObject("kefuNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
        mv.addObject("id",keyValue);
        return mv;
    }

    //新增修改
    @RequestMapping("/SaveForm")
    public Object insertDo(Medicine tblHomeLifeMedicine, String keyValue,String isshow, String kefus) throws Exception{

        if(StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)){
            tblHomeLifeMedicine.setId(keyValue);
            tblHomeLifeMedicine.setIsshow(isshow);
            tblHomeLifeMedicineService.updateById(tblHomeLifeMedicine);
            return R.ok("展示成功");
        }else if(StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)){
            tblHomeLifeMedicine.setId(keyValue);
            tblHomeLifeMedicine.setIsshow(isshow);
            tblHomeLifeMedicineService.updateById(tblHomeLifeMedicine);
            return R.ok("取消展示成功");
        }else if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            tblHomeLifeMedicine.setId(ywId);
            tblHomeLifeMedicineService.save(tblHomeLifeMedicine);
            if(!StringUtils.isEmpty(kefus) && !"&amp;nbsp;".equals(kefus)) {
                fileEntityService.insert(kefus.replace("&quot;", "\""), ywId, "医疗客服照片", "kefu", null);
            }
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            tblHomeLifeMedicineService.updateById(tblHomeLifeMedicine);
            if(!StringUtils.isEmpty(kefus) && !"&amp;nbsp;".equals(kefus)) {
                fileEntityService.insert(kefus.replace("&quot;", "\""), keyValue, "医疗客服照片", "kefu", null);
                tblHomeLifeMedicine.setId(keyValue);
            }
            return R.ok("修改成功");
        }

    }

    //图片ID返回给实体类  Update页传回
    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue){
        Medicine tblHomeLifeMedicine = tblHomeLifeMedicineService.getById(keyValue);
        return R.ok().put("data",tblHomeLifeMedicine);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/medicine/Update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("kefus");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
        return mv;
    }


//    //真删假删
//    @RequestMapping("/DeleteForm")
//    public Object delete(Medicine tblHomeLifeMedicine, String keyValue, String isshow, String sjlbs) {
//        if(StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)){
//            tblHomeLifeMedicine.setId(keyValue);
//            tblHomeLifeMedicine.setIsdel("0");
//            tblHomeLifeMedicineService.updateById(tblHomeLifeMedicine);
//            fileEntityService.deleteByYwId(keyValue);
////            return R.ok("删除成功");
//        }
//        return R.ok("删除成功");
//    }

    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        tblHomeLifeMedicineService.removeById(keyValue);
        return R.ok("删除成功");
    }


}

