package com.lzxuni.modules.pccontrol.home.life.guidance.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 生活服务-政务大厅-办事指南
 * </p>
 *
 * @author Lhl
 * @since 2019-07-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_life_guidance")
public class Guidance implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键uuid
     */
    @TableId
    private String id;

    /**
     * 办事指南
     */
    private String guide;

    /**
     * 机构
     */
    private String organization;

    /**
     * 所需材料
     */
    private String material;

    /**
     * 办理依据
     */
    private String according;

    /**
     * 办理程序
     */
    private String tprocedure;

    /**
     * 办理期限
     */
    private String time;

    /**
     * 收费标准
     */
    private String charge;

    /** 1表示展示，0隐藏 */
    private String isshow;

    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除 */
    private String isdel;

}
