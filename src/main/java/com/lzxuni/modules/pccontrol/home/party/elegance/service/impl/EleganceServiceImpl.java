package com.lzxuni.modules.pccontrol.home.party.elegance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.mapper.FileEntityMapper;
import com.lzxuni.modules.pccontrol.home.aged.lookafter.entity.AgedLookafter;
import com.lzxuni.modules.pccontrol.home.party.elegance.entity.Elegance;
import com.lzxuni.modules.pccontrol.home.party.elegance.mapper.EleganceMapper;
import com.lzxuni.modules.pccontrol.home.party.elegance.service.EleganceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 主页_党建_群团风采 服务实现类
 * Created by 潘云明
 * 2019/7/16
 */
@Service
public class EleganceServiceImpl extends ServiceImpl<EleganceMapper, Elegance> implements EleganceService {
    @Autowired
    private EleganceMapper mapper;

    @Autowired
    private FileEntityMapper fileEntityMapper;

    @Override
    public PageInfo<Elegance> queryPage(PageParameter pageParameter, Elegance entity) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Elegance> list = mapper.selectList(new QueryWrapper<Elegance>().eq("isdel", "1"));
        PageInfo<Elegance> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    //分页
    @Override
    public PageInfo<Elegance> queryPageAndImg(PageParameter pageParameter, Elegance elegance, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Elegance> eleganceList = baseMapper.selectList(new QueryWrapper<Elegance>().eq("isshow","1"));
        if(eleganceList != null && eleganceList.size() >0){
            for (int i=0;i<eleganceList.size();i++ ){
                FileEntity fileEntity = new FileEntity();
                String id = eleganceList.get(i).getId();
                fileEntity.setYwId(id);
                fileEntity.setYwType(ywType);
                List<FileEntity> filelist = fileEntityMapper.selectList(new QueryWrapper<>(fileEntity));
                if(filelist != null && filelist.size() != 0){
                    eleganceList.get(i).setUrlsPath(filelist.get(0).getUrlsPath());
                    eleganceList.get(i).setUrlPath(filelist.get(0).getUrlPath());
                }
            }
        }
        PageInfo<Elegance> pageInfo = new PageInfo<>(eleganceList);
        return pageInfo;
    }


}
