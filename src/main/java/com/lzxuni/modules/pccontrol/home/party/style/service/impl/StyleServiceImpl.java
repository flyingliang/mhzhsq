package com.lzxuni.modules.pccontrol.home.party.style.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.party.style.entity.Style;
import com.lzxuni.modules.pccontrol.home.party.style.mapper.StyleMapper;
import com.lzxuni.modules.pccontrol.home.party.style.service.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 主页_党建_作风建设 服务实现类
 * Created by 潘云明
 * 2019/7/16
 */
@Service
public class StyleServiceImpl extends ServiceImpl<StyleMapper, Style> implements StyleService {
    @Autowired
    private StyleMapper styleMapper;
    @Override
    public PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Style style) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = styleMapper.selectListByCondition(style);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }


    @Override
    public PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, Style style,String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = styleMapper.selectListByConditionAndImg(style,ywType);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }



}
