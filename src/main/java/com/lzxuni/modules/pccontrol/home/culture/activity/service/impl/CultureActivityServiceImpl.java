package com.lzxuni.modules.pccontrol.home.culture.activity.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.culture.activity.entity.CultureActivity;
import com.lzxuni.modules.pccontrol.home.culture.activity.mapper.CultureActivityMapper;
import com.lzxuni.modules.pccontrol.home.culture.activity.service.CultureActivityService;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理--社区活动表 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-07-17
 */
@Service
public class CultureActivityServiceImpl extends ServiceImpl<CultureActivityMapper, CultureActivity> implements CultureActivityService {

    @Autowired
    private CultureActivityMapper tblHomeCultureActivityMapper;

    @Override
    public PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, CultureActivity tblHomeCultureActivity) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeCultureActivityMapper.selectListByCondition(tblHomeCultureActivity);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }


    @Override
    public PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, CultureActivity cultureActivity, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeCultureActivityMapper.selectListByConditionAndImg(cultureActivity,ywType);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }
}
