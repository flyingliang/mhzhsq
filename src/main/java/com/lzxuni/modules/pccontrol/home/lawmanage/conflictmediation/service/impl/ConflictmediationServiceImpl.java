package com.lzxuni.modules.pccontrol.home.lawmanage.conflictmediation.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.conflictmediation.entity.Conflictmediation;
import com.lzxuni.modules.pccontrol.home.lawmanage.conflictmediation.mapper.ConflictmediationMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.conflictmediation.service.ConflictmediationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区矛盾调解 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-29
 */
@Service
public class ConflictmediationServiceImpl extends ServiceImpl<ConflictmediationMapper, Conflictmediation> implements ConflictmediationService {


    @Autowired
    private ConflictmediationMapper tblHomeLawmanageConflictmediationMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Conflictmediation tblHomeLawmanageConflictmediation) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeLawmanageConflictmediationMapper.selectListByCondition(tblHomeLawmanageConflictmediation);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Conflictmediation tblHomeLawmanageConflictmediation) {
        String uuid = UuidUtil.get32UUID();
        tblHomeLawmanageConflictmediation.setId(uuid);
        this.save(tblHomeLawmanageConflictmediation);
    }

    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = tblHomeLawmanageConflictmediationMapper.getTree(communityid);
        return list;
    }

}
