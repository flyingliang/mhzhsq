package com.lzxuni.modules.pccontrol.home.culture.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.member.entity.CultureMember;
import com.lzxuni.modules.pccontrol.home.culture.member.mapper.CultureMemberMapper;
import com.lzxuni.modules.pccontrol.home.culture.member.service.CultureMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理-文艺团体表 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-07-18
 */
@Service
public class CultureMemberServiceImpl extends ServiceImpl<CultureMemberMapper, CultureMember> implements CultureMemberService {

    @Autowired
    private CultureMemberMapper tblHomeCultureMemberMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, CultureMember tblHomeCultureMember) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeCultureMemberMapper.selectListByCondition(tblHomeCultureMember);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(CultureMember tblHomeCultureMember) {
        String uuid = UuidUtil.get32UUID();
        tblHomeCultureMember.setId(uuid);
        this.save(tblHomeCultureMember);
    }
    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = tblHomeCultureMemberMapper.getTree(communityid);
        return list;
    }


    @Override
    public List<Tree> getGroup() {
        List<Tree> list = tblHomeCultureMemberMapper.getGroup();
        return list;
    }
}
