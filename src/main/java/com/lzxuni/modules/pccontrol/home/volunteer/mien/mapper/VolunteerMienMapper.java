package com.lzxuni.modules.pccontrol.home.volunteer.mien.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.volunteer.mien.entity.VolunteerMien;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 志愿服务--志愿者风采表 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-07-24
 */
public interface VolunteerMienMapper extends BaseMapper<VolunteerMien> {
    List<Map<String,Object>> selectMien();
    List<Map<String,Object>> selectMienTwo();
    List<Map<String,Object>> selectMienList();
}
