package com.lzxuni.modules.pccontrol.home.lawmanage.safety.service;

import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.safety.entity.Safety;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--平安创建条例 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
public interface SafetyService extends IService<Safety> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Safety tblHomeLawmanageLegislation);
    //查询
    PageInfo<Safety> queryPage3(PageParameter pageParameter, Safety demo);
    //保存
    void SaveForm(Safety tblHomeLawmanageLegislation);

    List<Tree> getTree(String communityid);
}
