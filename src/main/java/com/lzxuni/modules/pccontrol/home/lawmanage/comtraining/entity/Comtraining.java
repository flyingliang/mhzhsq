package com.lzxuni.modules.pccontrol.home.lawmanage.comtraining.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 首页--综治--警务室警训
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_lawmanage_comtraining")
public class Comtraining implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId
    private String id;

    /**
     * 警训标题
     */
    private String title;

    /**
     * 所属社区
     */
    private String communityid;

    /**
     * 辖区警训
     */
    private String content;

    /**
     * 1未删除，0删除
     */
    private String isdel;

    /**
     * 建立时间
     */
    private LocalDateTime date;
    /** 所属社区 */
    @TableField(exist=false)
    private String communityname;

}
