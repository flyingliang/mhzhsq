package com.lzxuni.modules.pccontrol.home.party.ceactivity.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.party.ceactivity.entity.PartyCeactivity;
import com.lzxuni.modules.pccontrol.home.party.ceactivity.service.PartyCeactivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * <p>
 * 党建管理-->共驻共建管理-->创建活动管理 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
@RestController
@RequestMapping("/tblHomePartyCeactivity")
public class PartyCeactivityController extends BaseController {
    @Autowired
    private FileEntityService fileEntityService;
    @Autowired
    private PartyCeactivityService tblHomePartyCeactivityService;


    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "党建管理-共驻共建管理-创建活动管理", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/party/ceactivity/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, PartyCeactivity tblHomePartyCeactivity) {
        tblHomePartyCeactivity.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblHomePartyCeactivityService.queryPage(pageParameter, tblHomePartyCeactivity));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/party/ceactivity/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3, module = "党建管理-共驻共建管理-创建活动管理", operateType = "操作")
    public Object insertDo(PartyCeactivity tblHomePartyCeactivity, String keyValue, String isshow, String stylepics) throws Exception {
        System.out.println("###参数列表：keyValue："+keyValue+",isshow:"+isshow);
        String typeName="社区党建图片";
        String ywType="ceactivity";
        if (StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)) {//展示
            tblHomePartyCeactivity.setId(keyValue);
            tblHomePartyCeactivity.setIsshow(isshow);
            tblHomePartyCeactivityService.updateById(tblHomePartyCeactivity);
            return R.ok("发布成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)) {//取消展示
            tblHomePartyCeactivity.setId(keyValue);
            tblHomePartyCeactivity.setIsshow(isshow);
            tblHomePartyCeactivityService.updateById(tblHomePartyCeactivity);
            return R.ok("取消发布成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)) {//删除
            tblHomePartyCeactivity.setId(keyValue);
            tblHomePartyCeactivity.setIsdel("0");
            tblHomePartyCeactivityService.updateById(tblHomePartyCeactivity);
            return R.ok("删除成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "4".equals(isshow)) {//返回图片数量
            FileEntity fileBeanCustom = new FileEntity();
            fileBeanCustom.setYwId(keyValue);
            fileBeanCustom.setYwType(ywType);
            return R.ok().put("data",fileEntityService.queryNumByFileEntity(fileBeanCustom));
        } else if (StringUtils.isNotEmpty(keyValue)) {
            fileEntityService.deleteByYwId(keyValue);
            if(!StringUtils.isEmpty(stylepics) && !"&amp;nbsp;".equals(stylepics)) {
                fileEntityService.insert(stylepics.replace("&quot;", "\""), keyValue, typeName, ywType, null);
            }
            tblHomePartyCeactivity.setId(keyValue);
            tblHomePartyCeactivityService.updateById(tblHomePartyCeactivity);
            return R.ok("修改成功");
        }else{
            String ywId = UuidUtil.get32UUID();
            if (!StringUtils.isEmpty(stylepics) && !"&amp;nbsp;".equals(stylepics)) {
                fileEntityService.insert(stylepics.replace("&quot;", "\""), ywId, typeName, ywType, null);
            }
            tblHomePartyCeactivity.setId(ywId);
            tblHomePartyCeactivity.setIsdel("1");
            tblHomePartyCeactivity.setDate(new Date());
            tblHomePartyCeactivityService.save(tblHomePartyCeactivity);
            return R.ok("保存成功");
        }


    }
    @RequestMapping("GetEntity")
    public Object GetEntity(String keyValue){
        PartyCeactivity tblHomePartyCeactivity = tblHomePartyCeactivityService.getById(keyValue);
        return R.ok().put("data",tblHomePartyCeactivity);
    }
}

