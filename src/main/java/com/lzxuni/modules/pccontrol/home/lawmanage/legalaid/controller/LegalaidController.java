package com.lzxuni.modules.pccontrol.home.lawmanage.legalaid.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.lawmanage.legalaid.entity.Legalaid;
import com.lzxuni.modules.pccontrol.home.lawmanage.legalaid.service.LegalaidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-07-29
 */
@RestController
@RequestMapping("/tblHomeLawmanageLegalaid")
public class LegalaidController extends BaseController {


    @Autowired
    private LegalaidService tblHomeLawmanageLegalaidService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/legalaid/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Legalaid tblHomeLawmanageLegalaid, String Keyword) {
        tblHomeLawmanageLegalaid.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
//        tblHomeLawmanageConflictmediation.setSname(Keyword);
        PageData pageData = getPageData(tblHomeLawmanageLegalaidService.queryPage(pageParameter, tblHomeLawmanageLegalaid));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/GetTree")
    public Object GetTree(String communityid) {
        List<Tree> villageList = tblHomeLawmanageLegalaidService.getTree(communityid);
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/legalaid/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    public Object SaveForm(Legalaid tblHomeLawmanageLegalaid, String keyValue, String isshow, String sjs) throws Exception{
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            tblHomeLawmanageLegalaid.setId(ywId);
            tblHomeLawmanageLegalaidService.save(tblHomeLawmanageLegalaid);
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            tblHomeLawmanageLegalaidService.updateById(tblHomeLawmanageLegalaid);
            return R.ok("修改成功");
        }

    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue) throws Exception{
        Legalaid tblHomeLawmanageLegalaid = tblHomeLawmanageLegalaidService.getById(keyValue);
        return R.ok().put("data",tblHomeLawmanageLegalaid);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/legalaid/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("sjs");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
        return mv;
    }

    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        tblHomeLawmanageLegalaidService.removeById(keyValue);
        return R.ok("删除成功");
    }
}

