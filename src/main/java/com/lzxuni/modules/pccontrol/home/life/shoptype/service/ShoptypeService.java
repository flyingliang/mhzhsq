package com.lzxuni.modules.pccontrol.home.life.shoptype.service;

import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.medicine.entity.Medicine;
import com.lzxuni.modules.pccontrol.home.life.shoptype.entity.Shoptype;
import com.baomidou.mybatisplus.extension.service.IService;

import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 * 首页--生活管理--商家类别 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-17
 */
public interface ShoptypeService extends IService<Shoptype> {

    PageInfo<Shoptype> queryPage(PageParameter pageParameter, Shoptype tblHomeLifeShoptype) throws SQLException;
    List<Shoptype> queryShopTypeList(Shoptype demo) throws SQLException;

    void SaveForm(Shoptype tblHomeLifeShoptype);
}
