package com.lzxuni.modules.pccontrol.home.life.shop.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * <p>
 *商家管理
 * </p>
 *
 * @author Lhl
 * @since 2019-07-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_life_shop")
public class Shop implements Serializable {

    /** 版本号 */
    private static final long serialVersionUID = 1L;
    @TableId
    /** 主键，uuid */
    private String id;

    /**
     * 商家名称
     */
    private String sname;

    /**
     * 商家电话
     */
    private String sphone;

    /**
     * 商家类别ID
     */
    private String shoptypeid;

    /**
     * 是否签约
     */
    private String contract;


    /** 小区编号 */
    private String villageid;

    /** 楼号名 */
    private String buildingnumname;


    /** 建立时间 */
    private Date date;

     /** 修改时间 */
    private Date updatetime;

    /** 1未删除，0删除*/
    private String isdel;

    /** 1表示展示，0隐藏*/
    private String isshow;

    /** 简介*/
    private String content;
    /** 经度*/
    private String lon;
    /** 维度*/
    private String lat;




    /** 图片 */
    @TableField(exist=false)
    private String imgurl;
    /** 业务类型 */
    @TableField(exist=false)
    private String ywtype;
    /** 小区 */
    @TableField(exist=false)
    private String villagename;
    /** 社区 */
    @TableField(exist=false)
    private String communityname;


}
