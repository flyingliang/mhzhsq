package com.lzxuni.modules.pccontrol.home.party.map.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.party.map.entity.HomePartyMap;

/**
 * 主页_党建_地图 Mapper 接口
 * Created by 潘云明
 * 2019/7/8 15:01
 */
public interface HomePartyMapMapper extends BaseMapper<HomePartyMap> {
}
