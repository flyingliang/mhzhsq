package com.lzxuni.modules.pccontrol.home.culture.production.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.college.entity.CultureCollege;
import com.lzxuni.modules.pccontrol.home.culture.production.entity.CultureProduction;
import com.lzxuni.modules.pccontrol.home.culture.production.mapper.CultureProductionMapper;
import com.lzxuni.modules.pccontrol.home.culture.production.service.CultureProductionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理-文化作品表 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-07-16
 */
@Service
public class CultureProductionServiceImpl extends ServiceImpl<CultureProductionMapper, CultureProduction> implements CultureProductionService {

    @Autowired
    private CultureProductionMapper tblHomeCultureProductionMapper;
    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, CultureProduction tblHomeCultureProduction) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeCultureProductionMapper.selectListByCondition(tblHomeCultureProduction);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }
    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = tblHomeCultureProductionMapper.getTree(communityid);
        return list;
    }

    @Override
    public PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, CultureProduction cultureProduction, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeCultureProductionMapper.selectListByConditionAndImg(cultureProduction,ywType);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }
}
