package com.lzxuni.modules.pccontrol.home.party.wish.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.party.wish.entity.Wish;

import java.util.Map;

/**
 * 主页_党建_心愿 服务类
 * Created by 潘云明
 * 2019/7/17
 */
public interface WishService extends IService<Wish> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Wish wish);

    PageInfo<Wish> queryPageAndImg(PageParameter pageParameter, Wish wish, String ywType);

    PageInfo<Wish> sxqueryPageAndImg(PageParameter pageParameter, Wish wish, String ywType);

    PageInfo<Wish> sxqueryPageYinyWish(PageParameter pageParameter, Wish wish, String ywType);
}
