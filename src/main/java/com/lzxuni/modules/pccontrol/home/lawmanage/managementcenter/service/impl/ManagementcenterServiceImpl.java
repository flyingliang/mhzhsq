package com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.entity.Managementcenter;
import com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.mapper.ManagementcenterMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.service.ManagementcenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@Service
public class ManagementcenterServiceImpl extends ServiceImpl<ManagementcenterMapper, Managementcenter> implements ManagementcenterService {

    @Autowired
    private ManagementcenterMapper tblHomeLawmanageManagementcenterMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Managementcenter tblHomeLawmanageManagementcenter) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeLawmanageManagementcenterMapper.selectListByCondition(tblHomeLawmanageManagementcenter);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Managementcenter tblHomeLawmanageManagementcenter) {
        String uuid = UuidUtil.get32UUID();
        tblHomeLawmanageManagementcenter.setId(uuid);
        this.save(tblHomeLawmanageManagementcenter);
    }

    @Override
    public List<Tree> getTree() {
        List<Tree> list = tblHomeLawmanageManagementcenterMapper.getTree();
        return list;
    }

    @Override
    public List<Managementcenter> queryPage3( Managementcenter demo) {

        List<Managementcenter> homeLifeAffairs = tblHomeLawmanageManagementcenterMapper.selectListAll(demo);

        return homeLifeAffairs;
    }

    public  Managementcenter queryBymcId(Managementcenter demmo){
        return tblHomeLawmanageManagementcenterMapper.queryBymcId(demmo);
    }


}
