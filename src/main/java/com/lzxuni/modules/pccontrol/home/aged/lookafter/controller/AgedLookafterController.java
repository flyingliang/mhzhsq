package com.lzxuni.modules.pccontrol.home.aged.lookafter.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.aged.lookafter.entity.AgedLookafter;
import com.lzxuni.modules.pccontrol.home.aged.lookafter.service.AgedLookafterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;
import java.util.Date;

/**
 * <p>
 * 养老管理--日间照料表 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-07-22
 */
@RestController
@RequestMapping("/tblHomeAgedLookafter")
public class AgedLookafterController extends BaseController {
    @Autowired
    private AgedLookafterService tblHomeAgedLookafterService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "养老管理-日间照料管理", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/aged/lookafter/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, AgedLookafter tblHomeAgedLookafter) throws SQLException {
        tblHomeAgedLookafter.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblHomeAgedLookafterService.queryPage(pageParameter, tblHomeAgedLookafter));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/aged/lookafter/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3, module = "养老管理-日间照料管理", operateType = "操作")
    public Object insertDo(AgedLookafter tblHomeAgedLookafter, String keyValue, String isshow, String stylepics) throws Exception {
        System.out.println("###参数列表：keyValue："+keyValue+",isshow:"+isshow);
        String typeName="社区养老图片";
        String ywType="lookafter";
        if (StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)) {//展示
            tblHomeAgedLookafter.setId(keyValue);
            tblHomeAgedLookafter.setIsshow(isshow);
            tblHomeAgedLookafterService.updateById(tblHomeAgedLookafter);
            return R.ok("展示成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)) {//取消展示
            tblHomeAgedLookafter.setId(keyValue);
            tblHomeAgedLookafter.setIsshow(isshow);
            tblHomeAgedLookafterService.updateById(tblHomeAgedLookafter);
            return R.ok("取消展示成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)) {//删除
            tblHomeAgedLookafter.setId(keyValue);
            tblHomeAgedLookafter.setIsdel("0");
            tblHomeAgedLookafterService.updateById(tblHomeAgedLookafter);
            return R.ok("删除成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "4".equals(isshow)) {//返回图片数量
            FileEntity fileBeanCustom = new FileEntity();
            fileBeanCustom.setYwId(keyValue);
            fileBeanCustom.setYwType(ywType);
            return R.ok().put("data",fileEntityService.queryNumByFileEntity(fileBeanCustom));
        } else if (StringUtils.isNotEmpty(keyValue)) {
            fileEntityService.deleteByYwId(keyValue);
            if(!StringUtils.isEmpty(stylepics) && !"&amp;nbsp;".equals(stylepics)) {
                fileEntityService.insert(stylepics.replace("&quot;", "\""), keyValue, typeName, ywType, null);
            }
            tblHomeAgedLookafter.setId(keyValue);
            tblHomeAgedLookafterService.updateById(tblHomeAgedLookafter);
            return R.ok("修改成功");
        } else {
            String ywId = UuidUtil.get32UUID();
            if (!StringUtils.isEmpty(stylepics) && !"&amp;nbsp;".equals(stylepics)) {
                fileEntityService.insert(stylepics.replace("&quot;", "\""), ywId, typeName, ywType, null);
            }
            tblHomeAgedLookafter.setId(ywId);
            tblHomeAgedLookafter.setIsdel("1");
            tblHomeAgedLookafter.setDate(new Date());
            tblHomeAgedLookafterService.save(tblHomeAgedLookafter);
            return R.ok("保存成功");
        }


    }
}

