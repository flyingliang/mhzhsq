package com.lzxuni.modules.pccontrol.home.party.style.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.party.style.entity.Style;

import java.util.Map;

/**
 * 主页_党建_作风建设 服务类
 * Created by 潘云明
 * 2019/7/16
 */
public interface StyleService extends IService<Style> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Style style);
    //查询
    PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, Style style,String ywType);


}
