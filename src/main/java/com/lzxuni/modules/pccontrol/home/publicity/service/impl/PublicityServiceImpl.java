package com.lzxuni.modules.pccontrol.home.publicity.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.publicity.entity.Publicity;
import com.lzxuni.modules.pccontrol.home.publicity.mapper.PublicityMapper;
import com.lzxuni.modules.pccontrol.home.publicity.service.PublicityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理-->政务公开管理 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-08-08
 */
@Service
public class PublicityServiceImpl extends ServiceImpl<PublicityMapper, Publicity> implements PublicityService {

    @Autowired
    private PublicityMapper tblHomePublicityMapper;

    @Override
    public PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Publicity tblHomePublicity) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomePublicityMapper.selectListByCondition(tblHomePublicity);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }
    @Override
    public List<Publicity> selectListfour(Publicity demo) {

        List<Publicity> homeLifeAffairs = tblHomePublicityMapper.selectListfour(demo);

        return homeLifeAffairs;
    }
    @Override
    public PageInfo<Publicity> selectListAll(PageParameter pageParameter, Publicity demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Publicity> homeLifeAffairs = tblHomePublicityMapper.selectListAll(demo);
        PageInfo<Publicity> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }

    public void pageviewPlusOne(Publicity demo){
        tblHomePublicityMapper.pageviewPlusOne(demo);
    }


    
}
