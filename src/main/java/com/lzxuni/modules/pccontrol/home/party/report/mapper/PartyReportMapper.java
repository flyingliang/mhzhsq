package com.lzxuni.modules.pccontrol.home.party.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.party.report.entity.PartyReport;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 党建管理-->共驻共建管理-->社区报到管理 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
public interface PartyReportMapper extends BaseMapper<PartyReport> {
    List<Map<String,Object>> selectListByCondition(PartyReport tblHomePartyReport);
    List<Tree> getTree(@Param("communityid") String communityid);

    List<Tree> getPost();

    List<Tree> getCompany();

}
