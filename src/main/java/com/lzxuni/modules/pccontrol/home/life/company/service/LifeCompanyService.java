package com.lzxuni.modules.pccontrol.home.life.company.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.life.company.entity.LifeCompany;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--生活服务--单位管理 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
public interface LifeCompanyService extends IService<LifeCompany> {

    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, LifeCompany tblHomeLifeCompany);
     //查询
    PageInfo<LifeCompany> queryPage2(PageParameter pageParameter, LifeCompany tblHomeLifeCompany);
    //保存
    void SaveForm(LifeCompany tblHomeLifeCompany);

    List<Tree> getTree(String communityid);

    //单位详情
    LifeCompany selectcompanybyid (LifeCompany demo);
}
