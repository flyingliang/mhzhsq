package com.lzxuni.modules.pccontrol.home.lawmanage.comtraining.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.comtraining.entity.Comtraining;
import com.lzxuni.modules.pccontrol.home.lawmanage.comtraining.mapper.ComtrainingMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.comtraining.service.ComtrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--警务室警训 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
@Service
public class ComtrainingServiceImpl extends ServiceImpl<ComtrainingMapper, Comtraining> implements ComtrainingService {
    @Autowired
    private ComtrainingMapper ComtrainingMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Comtraining communityper) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = ComtrainingMapper.selectListByCondition(communityper);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Comtraining communityper) {
        String uuid = UuidUtil.get32UUID();
        communityper.setId(uuid);
        this.save(communityper);
    }

    @Override
    public List<Tree> getTree() {
        List<Tree> list = ComtrainingMapper.getTree();
        return list;
    }

    @Override
    public List<Comtraining> queryPage3( Comtraining demo) {

        List<Comtraining> homeLifeAffairs = ComtrainingMapper.selectListAll(demo);

        return homeLifeAffairs;
    }

    public  Comtraining queryBymcId(Comtraining demmo){
        return ComtrainingMapper.queryBymcId(demmo);
    }


    @Override
    public List<Comtraining> selectListthree( Comtraining demo) {

        List<Comtraining> homeLifeAffairs = ComtrainingMapper.selectListthree(demo);

        return homeLifeAffairs;
    }
}
