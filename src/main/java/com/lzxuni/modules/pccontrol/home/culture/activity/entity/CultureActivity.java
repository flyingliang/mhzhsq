package com.lzxuni.modules.pccontrol.home.culture.activity.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 文化管理--社区活动表
 * </p>
 *
 * @author fcd
 * @since 2019-07-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_culture_activity")
public class CultureActivity implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 活动标题
     */
    private String title;

    /**
     * 发布社区
     */
    private String communityid;

    /**
     * 开始时间
     */
    private Date starttime;

    /**
     * 结束时间
     */
    private Date endtime;

    /**
     * 活动状态
     */
    private String state;

    /**
     * 活动内容
     */
    private String content;

    /**
     * 1表示展示，0隐藏
     */
    private String isshow;

    /**
     * 创建时间
     */
    private Date date;

    /**
     * 1未删除，0删除
     */
    private String isdel;


}
