package com.lzxuni.modules.pccontrol.home.volunteer.vractivity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 志愿管理-->志愿者活动表
 * </p>
 *
 * @author fcd
 * @since 2019-07-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_volunteer_activity")
public class VolunteerActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 活动标题
     */
    private String title;

    /**
     * 发布社区
     */
    private String communityid;

    /**
     * 开始时间
     */
    private Date starttime;

    /**
     * 结束时间
     */
    private Date endtime;

    /**
     * 活动内容
     */
    private String content;

    /**
     * 1表示展示，0隐藏
     */
    private String isshow;

    /**
     * 创建时间
     */
    private Date date;

    /**
     * 1未删除，0删除
     */
    private String isdel;

    /**
     * 组织名称
     */
    private String tissueid;

    /**
     * 奖励志愿币数量
     */
    private Integer award;

    /**
     * 任务状态:0为待领取,1为进行中,2为以完成
     */
    private String state;

    /**人员姓名 */
    private String joinid;
}
