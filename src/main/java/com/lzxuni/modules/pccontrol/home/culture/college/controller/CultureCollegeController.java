package com.lzxuni.modules.pccontrol.home.culture.college.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.culture.college.entity.CultureCollege;
import com.lzxuni.modules.pccontrol.home.culture.college.service.CultureCollegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * <p>
 * 文化管理-市民学院表 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-07-17
 */
@RestController
@RequestMapping("/tblHomeCultureCollege")
public class CultureCollegeController extends BaseController {
    @Autowired
    private FileEntityService fileEntityService;
    @Autowired
    private CultureCollegeService tblHomeCultureCollegeService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "文化管理-市民学院管理", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/culture/college/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, CultureCollege tblHomeCultureCollege) {
        tblHomeCultureCollege.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblHomeCultureCollegeService.queryPage(pageParameter, tblHomeCultureCollege));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/culture/college/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3, module = "文化管理-市民学院管理", operateType = "操作")
    public Object insertDo(CultureCollege tblHomeCultureCollege, String keyValue, String isshow, String stylepics) throws Exception {
        System.out.println("###参数列表：keyValue："+keyValue+",isshow:"+isshow);
        String typeName="社区文化图片";
        String ywType="college";
        if (StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)) {//展示
            tblHomeCultureCollege.setId(keyValue);
            tblHomeCultureCollege.setIsshow(isshow);
            tblHomeCultureCollegeService.updateById(tblHomeCultureCollege);
            return R.ok("展示成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)) {//取消展示
            tblHomeCultureCollege.setId(keyValue);
            tblHomeCultureCollege.setIsshow(isshow);
            tblHomeCultureCollegeService.updateById(tblHomeCultureCollege);
            return R.ok("取消展示成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "4".equals(isshow)) {//返回图片数量
            FileEntity fileBeanCustom = new FileEntity();
            fileBeanCustom.setYwId(keyValue);
            fileBeanCustom.setYwType(ywType);
            return R.ok().put("data",fileEntityService.queryNumByFileEntity(fileBeanCustom));
        } else if (StringUtils.isNotEmpty(keyValue)) {
            fileEntityService.deleteByYwId(keyValue);
            if(!StringUtils.isEmpty(stylepics) && !"&amp;nbsp;".equals(stylepics)) {
                fileEntityService.insert(stylepics.replace("&quot;", "\""), keyValue, typeName, ywType, null);
            }
            tblHomeCultureCollege.setId(keyValue);
            tblHomeCultureCollegeService.updateById(tblHomeCultureCollege);
            return R.ok("修改成功");
        } else {
            String ywId = UuidUtil.get32UUID();
            if (!StringUtils.isEmpty(stylepics) && !"&amp;nbsp;".equals(stylepics)) {
                fileEntityService.insert(stylepics.replace("&quot;", "\""), ywId, typeName, ywType, null);
            }
            tblHomeCultureCollege.setId(ywId);
            tblHomeCultureCollege.setIsdel("1");
            tblHomeCultureCollege.setIsshow("1");
            tblHomeCultureCollege.setDate(new Date());
            tblHomeCultureCollegeService.save(tblHomeCultureCollege);
            return R.ok("保存成功");
        }


    }

    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        tblHomeCultureCollegeService.removeById(keyValue);
        return R.ok("删除成功");
    }
}

