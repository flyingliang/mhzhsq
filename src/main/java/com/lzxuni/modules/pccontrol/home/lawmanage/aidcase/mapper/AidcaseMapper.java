package com.lzxuni.modules.pccontrol.home.lawmanage.aidcase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.aidcase.entity.Aidcase;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--援助案例 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
public interface AidcaseMapper extends BaseMapper<Aidcase> {

    //社区联动
    List<Map<String,Object>> selectListByCondition(Aidcase communityper);
    List<Tree> getTree();
    List<Aidcase> selectListAll(Aidcase demo);

    Aidcase queryBymcId(Aidcase demo);
}
