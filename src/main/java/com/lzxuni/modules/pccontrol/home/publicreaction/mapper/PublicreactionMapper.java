package com.lzxuni.modules.pccontrol.home.publicreaction.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.publicreaction.entity.Publicreaction;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理-->社情反应管理 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-08-07
 */
public interface PublicreactionMapper extends BaseMapper<Publicreaction> {
    List<Map<String,Object>> selectListByCondition(Publicreaction tblHomePublicreaction);
    List<Tree> getTree();
}
