package com.lzxuni.modules.pccontrol.home.gridding.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.gridding.entity.Gridding;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理-->我的网格管理 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-08-08
 */
public interface GriddingService extends IService<Gridding> {
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Gridding tblHomeGridding);

    List<Tree> getTree(String communityid);
    List<Tree> getTreev(String villageid);
    List<Tree> getTreevillage();
}
