package com.lzxuni.modules.pccontrol.home.life.companytype.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.companytype.entity.Companytype;
import com.lzxuni.modules.pccontrol.home.life.companytype.mapper.CompanytypeMapper;
import com.lzxuni.modules.pccontrol.home.life.companytype.service.CompanytypeService;
import com.lzxuni.modules.system.mapper.BaseAreaMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

/**
 * <p>
 * 首页--生活服务--单位类别 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@Service
public class CompanytypeServiceImpl extends ServiceImpl<CompanytypeMapper, Companytype> implements CompanytypeService {

    @Autowired
    private CompanytypeMapper tblHomeLifeCompanytypeMapper;
    @Autowired
    private BaseAreaMapper areaMapper;

    @Override
    public PageInfo<Companytype> queryPage(PageParameter pageParameter, Companytype tblHomeLifeCompanytype) throws SQLException {
        /**
         * 之前的分页
         * 1.传入页号和每页几个
         * PageHelper.startPage(pageNumber, 5);
         * 2.查询
         * List<Employee> emps = employeeService.getAll();
         * 3.包装结果，每页5个
         * PageInfo<Employee> pageInfo = new PageInfo<Employee>(emps,5);
         * 4.pageParameter:{"rows":100,"page":1,"sidx":"","sord":"ASC","records":0,"total":0}
         */
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Companytype> tblHomeLifeCompanytypeList = tblHomeLifeCompanytypeMapper.selectList(null);
        PageInfo<Companytype> pageInfo = new PageInfo<>(tblHomeLifeCompanytypeList);
        return pageInfo;


    }

    @Override
    public void SaveForm(Companytype tblHomeLifeCompanytype) {

        String newsZzyhDomesticId = UuidUtil.get32UUID();
        tblHomeLifeCompanytype.setId(newsZzyhDomesticId);
        this.save(tblHomeLifeCompanytype);
    }
}
