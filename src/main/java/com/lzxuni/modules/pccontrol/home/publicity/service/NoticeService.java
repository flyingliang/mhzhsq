package com.lzxuni.modules.pccontrol.home.publicity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.publicity.entity.Notice;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理-->政务公开管理 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-08-08
 */
public interface NoticeService extends IService<Notice> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Notice demo);
    List<Notice> selectListfour(Notice demo);
    //查询
    PageInfo<Notice> selectListAll(PageParameter pageParameter, Notice demo);
    void pageviewPlusOne(Notice tblHomePublicity);
}