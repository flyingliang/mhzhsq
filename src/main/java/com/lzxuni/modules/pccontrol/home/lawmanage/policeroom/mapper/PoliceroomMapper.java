package com.lzxuni.modules.pccontrol.home.lawmanage.policeroom.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.policeroom.entity.Policeroom;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-07-31
 */
public interface PoliceroomMapper extends BaseMapper<Policeroom> {

    //社区联动
    List<Map<String,Object>> selectListByCondition(Policeroom tblHomeLawmanagePoliceroom);
    List<Tree> getTree();

    List<Policeroom> selectListAll(Policeroom demo);

    Policeroom queryBymcId(Policeroom demo);
}
