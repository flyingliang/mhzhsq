package com.lzxuni.modules.pccontrol.home.lawmanage.comtraining.service;

import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.comtraining.entity.Comtraining;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--警务室警训 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
public interface ComtrainingService extends IService<Comtraining>  {

    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Comtraining communityper);
    //保存
    void SaveForm(Comtraining communityper);

    List<Tree> getTree();
    //查询
    List<Comtraining> queryPage3( Comtraining demo);
    List<Comtraining> selectListthree( Comtraining demo);

    Comtraining queryBymcId(Comtraining demmo);
}
