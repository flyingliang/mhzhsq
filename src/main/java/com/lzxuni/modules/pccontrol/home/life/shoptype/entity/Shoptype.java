package com.lzxuni.modules.pccontrol.home.life.shoptype.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 首页--生活管理--商家类别
 * </p>
 *
 * @author Lhl
 * @since 2019-07-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_life_shoptype")
public class Shoptype extends Model<Shoptype> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键UUID
     */
    @TableId
    private String id;

    /**
     * 商家名称
     */
    private String shopname;

    /**1表示展示，1隐藏 */
    private String isshow;

    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除*/
    private String isdel;

    /** 图片 */
    @TableField(exist=false)
    private String imgurl;
    /** 业务类型 */
    @TableField(exist=false)
    private String ywtype;

}
