package com.lzxuni.modules.pccontrol.home.card.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.card.entity.CardZXBZ;

/**
 * 办证 Mapper 接口
 * Created by 潘云明
 * 2019/7/16
 */
public interface CardZXBZMapper extends BaseMapper<CardZXBZ> {
//    List<Map<String,Object>> selectListByCondition(Card style);
//
//    List<Map<String,Object>> selectListByConditionAndImg(@Param("style") Card style, @Param("ywType") String ywType);

}
