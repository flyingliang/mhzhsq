package com.lzxuni.modules.pccontrol.home.volunteer.mien.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.volunteer.mien.entity.VolunteerMien;
import com.lzxuni.modules.pccontrol.home.volunteer.mien.mapper.VolunteerMienMapper;
import com.lzxuni.modules.pccontrol.home.volunteer.mien.service.VolunteerMienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 志愿服务--志愿者风采表 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-07-24
 */
@Service
public class VolunteerMienServiceImpl extends ServiceImpl<VolunteerMienMapper, VolunteerMien> implements VolunteerMienService {
@Autowired
private VolunteerMienMapper volunteerMienMapper;
    @Override
    public PageInfo<VolunteerMien> queryPage(PageParameter pageParameter, VolunteerMien tblHomeVolunteerMien) throws SQLException {
        PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                pageParameter.getSidx() + " " + pageParameter.getSord());
        List<VolunteerMien> list;

        if (StringUtils.isNotEmpty(tblHomeVolunteerMien.getTitle())) {
            list = baseMapper.selectList(new QueryWrapper<VolunteerMien>().like("title", tblHomeVolunteerMien.getTitle()));
        }else {
            list = baseMapper.selectList(new QueryWrapper<VolunteerMien>().eq("isdel","1"));
        }
        PageInfo<VolunteerMien> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public PageInfo<Map<String, Object>> queryList(PageParameter pageParameter) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = volunteerMienMapper.selectMien();
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;

    }
    @Override
    public PageInfo<Map<String, Object>> queryListTwo(PageParameter pageParameter) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = volunteerMienMapper.selectMienTwo();
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;

    }
    @Override
    public PageInfo<Map<String, Object>> selectMienList(PageParameter pageParameter) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = volunteerMienMapper.selectMienList();
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;

    }
}
