package com.lzxuni.modules.pccontrol.home.lawmanage.legalaid.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 法律援助管理
 * </p>
 *
 * @author Lhl
 * @since 2019-07-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_lawmanage_legalaid")
public class Legalaid implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId
    private String id;

    /**
     * 所属社区
     */
    private String communityid;

    /**
     * 建立时间
     */
    private LocalDateTime date;

    /**
     * 1未删除，0删除
     */
    private String isdel;

    /**
     * 姓名
     */
    private String name;

    /**
     * 电话
     */
    private String phone;

    /**
     * 单位
     */
    private String unit;

    /**
     * 身份证
     */
    private String numberid;

    /**
     * 详细信息
     */
    private String content;



}
