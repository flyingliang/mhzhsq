package com.lzxuni.modules.pccontrol.home.aged.lookafter.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lzxuni.modules.common.entity.FileEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 养老管理--日间照料表
 * </p>
 *
 * @author fcd
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_aged_lookafter")
public class AgedLookafter implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 标题
     */
    private String title;

    /**
     * 发布时间
     */
//    private Date releaseTime;

    /**
     * 来源
     */
    private String source;

    /**
     * 联系人
     */
    private String linkman;

    /**
     * 联系电话
     */
    private String contactNumber;

    /**
     * 开放时间
     */
    private String openingHours;

    /**
     * 地址
     */
    private String site;

    /**
     * 内容
     */
    private String content;

    /**
     * 1表示展示，0隐藏
     */
    private String isshow;

    /**
     * 添加时间
     */
    private Date date;

    /**
     * 1未删除，0删除
     */
    private String isdel;

    @TableField(exist = false)
    private List<FileEntity> fileEntities;

    @TableField(exist = false)
    private String urlPath;//web访问路径

    @TableField(exist = false)
    private String urlsPath;//缩略图访问路径

}
