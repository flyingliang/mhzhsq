package com.lzxuni.modules.pccontrol.home.life.shoptype.mapper;

import com.lzxuni.modules.pccontrol.home.life.medicine.entity.Medicine;
import com.lzxuni.modules.pccontrol.home.life.shoptype.entity.Shoptype;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 首页--生活管理--商家类别 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-07-17
 */
public interface ShoptypeMapper extends BaseMapper<Shoptype> {
    List<Shoptype> queryShopTypeList(Shoptype demo);
}
