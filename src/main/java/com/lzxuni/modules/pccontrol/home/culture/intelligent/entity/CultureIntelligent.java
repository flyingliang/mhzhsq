package com.lzxuni.modules.pccontrol.home.culture.intelligent.entity;

import com.baomidou.mybatisplus.annotation.*;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 文化管理-社区达人表
 * </p>
 *
 * @author fcd
 * @since 2019-07-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_culture_intelligent")
public class CultureIntelligent implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 姓名
     */
    private String intelligentname;

    /**
     * 电话
     */
    private String intelligentphone;

    /**
     * 所属社区
     */
    private String communityid;

    /**
     * 类别
     */
//    private String intelligentcategory;

    /**
     * 自荐理由
     */
    private String intelligentreason;

    /**
     * 时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date date;

    /**
     * 是否展示
     */
    private String isshow;

    /** 1未删除，0删除*/
    private String isdel;


    /** 加入人微信id */
    private String openid;

    /** 加入人微信名 */
    private String nickname;
}
