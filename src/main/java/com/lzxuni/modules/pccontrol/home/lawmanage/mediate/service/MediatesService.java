package com.lzxuni.modules.pccontrol.home.lawmanage.mediate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.mediate.entity.Mediates;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--矛盾调解案例 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
public interface MediatesService extends IService<Mediates>  {


    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Mediates communityper);
    //保存
    void SaveForm(Mediates communityper);

    List<Tree> getTree();
    //查询
    List<Mediates> querylist6(Mediates demo);

    Mediates queryBymcId(Mediates demmo);
}
