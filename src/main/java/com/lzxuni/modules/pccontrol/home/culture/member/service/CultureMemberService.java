package com.lzxuni.modules.pccontrol.home.culture.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.member.entity.CultureMember;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理-文艺团体表 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-07-18
 */
public interface CultureMemberService extends IService<CultureMember> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, CultureMember tblHomeCultureMember);
    //保存
    void SaveForm(CultureMember tblHomeCultureMember);

    List<Tree> getTree(String communityid);

    List<Tree> getGroup();
}
