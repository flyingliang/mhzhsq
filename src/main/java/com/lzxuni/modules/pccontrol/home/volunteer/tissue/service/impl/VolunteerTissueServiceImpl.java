package com.lzxuni.modules.pccontrol.home.volunteer.tissue.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.volunteer.join.entity.VolunteerJoin;
import com.lzxuni.modules.pccontrol.home.volunteer.tissue.entity.VolunteerTissue;
import com.lzxuni.modules.pccontrol.home.volunteer.tissue.mapper.VolunteerTissueMapper;
import com.lzxuni.modules.pccontrol.home.volunteer.tissue.service.VolunteerTissueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 志愿服务--志愿者组织表 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-07-24
 */
@Service
public class VolunteerTissueServiceImpl extends ServiceImpl<VolunteerTissueMapper, VolunteerTissue> implements VolunteerTissueService {
    @Autowired
    private VolunteerTissueMapper tblHomeVolunteerTissueMapper;
    @Override
    public PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeVolunteerTissueMapper.selectListByCondition();
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }

    @Override
    public PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, VolunteerTissue volunteerTissue, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomeVolunteerTissueMapper.selectListByConditionAndImg(volunteerTissue,ywType);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }
}
