package com.lzxuni.modules.pccontrol.home.publicreaction.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.publicreaction.entity.Publicreaction;
import com.lzxuni.modules.pccontrol.home.publicreaction.service.PublicreactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 基础管理-->社情反应管理 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-08-07
 */
@RestController
@RequestMapping("/tblHomePublicreaction")
public class PublicreactionController extends BaseController {
    @Autowired
    private FileEntityService fileEntityService;
    @Autowired
    private PublicreactionService tblHomePublicreactionService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "基础服务管理-社情反应管理", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/publicReaction/index");
        System.out.println("###基础服务管理-社情反应管理");
        return mv;
    }

    //志愿服务管理-在线加入管理的查询
    @RequestMapping("/GetList")
    public Object GetList(String pagination, Publicreaction tblHomePublicreaction) throws SQLException {
        tblHomePublicreaction.setIsdel("1");
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(tblHomePublicreactionService.queryPage(pageParameter, tblHomePublicreaction));
        return R.ok().put("data", pageData);
    }

    //添加、修改
    @RequestMapping("/SaveForm")
    public Object insertDo(String keyValue, String reactiontypics, Publicreaction tblHomePublicreaction, String isshow, String content1) throws Exception {

        if (StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)) {
            tblHomePublicreaction.setId(keyValue);
            tblHomePublicreaction.setIsshow(isshow);
            tblHomePublicreactionService.updateById(tblHomePublicreaction);
            return R.ok("审核成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)) {
            tblHomePublicreaction.setId(keyValue);
            tblHomePublicreaction.setIsshow(isshow);
            tblHomePublicreactionService.updateById(tblHomePublicreaction);
            return R.ok("取消审核成功");
        } else if (StringUtils.isEmpty(keyValue)) {
            String ywId = UuidUtil.get32UUID();
            tblHomePublicreaction.setId(ywId);
            tblHomePublicreaction.setDate(new Date());
            tblHomePublicreaction.setIsdel("1");
            tblHomePublicreaction.setIsshow("1");
            tblHomePublicreactionService.save(tblHomePublicreaction);
            if (!StringUtils.isEmpty(reactiontypics) && !"&amp;nbsp;".equals(reactiontypics)) {
                fileEntityService.insert(reactiontypics.replace("&quot;", "\""), ywId, "社情反映图片", "publicreactionPic", null);
            }
            return R.ok("保存成功");
        } else {
            Publicreaction entity = new Publicreaction();
            System.out.println("content1:" + content1);
            entity.setId(keyValue);
            entity.setContent(content1);
            tblHomePublicreactionService.updateById(entity);
//            fileEntityService.deleteByYwId(keyValue);
//            tblHomePublicreactionService.updateById(tblHomePublicreaction);
//            if(!StringUtils.isEmpty(reactiontypics) && !"&amp;nbsp;".equals(reactiontypics)) {
//                fileEntityService.insert(reactiontypics.replace("&quot;", "\""), keyValue, "主页社情反应照片", "reaction", null);
//                tblHomePublicreaction.setId(keyValue);
//            }
            return R.ok("修改成功");
        }
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId, String keyValue) throws Exception {
        ModelAndView mv = new ModelAndView("/pccontrol/home/publicReaction/Update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("reaction");
        mv.addObject("reactiontypicNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
        return mv;
    }

    //真删假删
    @RequestMapping("/DeleteForm")
    public Object delete(Publicreaction tblHomePublicreaction, String keyValue, String isshow) {
        if (StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)) {
            tblHomePublicreaction.setId(keyValue);
            tblHomePublicreaction.setIsdel("0");
            tblHomePublicreactionService.updateById(tblHomePublicreaction);
        }
        return R.ok("删除成功");
    }

    @RequestMapping("GetEntity")
    public Object GetEntity(String keyValue) {
        Publicreaction tblHomePublicreaction = tblHomePublicreactionService.getById(keyValue);
        return R.ok().put("data", tblHomePublicreaction);
    }

    @RequestMapping("/GetTree")
    public Object GetTree() {
        List<Tree> companyList = tblHomePublicreactionService.getTree();
        return R.ok().put("data", companyList);
    }

    @RequestMapping("/Form")
    public ModelAndView insert() throws Exception {
        ModelAndView mv = new ModelAndView("/pccontrol/home/publicreaction/form");
        return mv;
    }
}

