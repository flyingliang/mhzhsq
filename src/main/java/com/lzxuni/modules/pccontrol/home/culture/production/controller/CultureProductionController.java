package com.lzxuni.modules.pccontrol.home.culture.production.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.culture.production.entity.CultureProduction;
import com.lzxuni.modules.pccontrol.home.culture.production.service.CultureProductionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 文化管理-文化作品表 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-07-16
 */
@RestController
@RequestMapping("/tblHomeCultureProduction")
public class CultureProductionController extends BaseController {

    @Autowired
    private CultureProductionService tblHomeCultureProductionService;

    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "文化管理-文化作品管理", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/culture/production/index");
        return mv;
    }
    //文化管理-文化作品的查询
    @RequestMapping("/GetList")
    public Object GetList(String pagination, CultureProduction tblHomeCultureProduction) throws SQLException {
        tblHomeCultureProduction.setIsdel("1");
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(tblHomeCultureProductionService.queryPage(pageParameter, tblHomeCultureProduction));
        return R.ok().put("data", pageData);
    }

//    @RequestMapping("/Form")
//    public ModelAndView insert() throws Exception{
//        ModelAndView mv = new ModelAndView("/pccontrol/home/culture/production/Insert");
//        return mv;
//    }
    //添加、修改
    @RequestMapping("/SaveForm")
    public Object insertDo(String keyValue, String demopics, CultureProduction tblHomeCultureProduction, String isshow) throws Exception{

        if(StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)){
            tblHomeCultureProduction.setId(keyValue);
            tblHomeCultureProduction.setIsshow(isshow);
            tblHomeCultureProductionService.updateById(tblHomeCultureProduction);
            return R.ok("展示成功");
        }else if(StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)){
            tblHomeCultureProduction.setId(keyValue);
            tblHomeCultureProduction.setIsshow(isshow);
            tblHomeCultureProductionService.updateById(tblHomeCultureProduction);
            return R.ok("取消展示成功");
        }else
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            tblHomeCultureProduction.setId(ywId);
            tblHomeCultureProduction.setDate(new Date());
            tblHomeCultureProductionService.save(tblHomeCultureProduction);
            if(!StringUtils.isEmpty(demopics)&& !"&amp;nbsp;".equals(demopics)) {
                fileEntityService.insert(demopics.replace("&quot;", "\""), ywId, "社区文化照片", "production", null);
            }
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            tblHomeCultureProductionService.updateById(tblHomeCultureProduction);
            if(!StringUtils.isEmpty(demopics) && !"&amp;nbsp;".equals(demopics)) {
                fileEntityService.insert(demopics.replace("&quot;", "\""), keyValue, "社区文化照片", "production", null);
                tblHomeCultureProduction.setId(keyValue);
            }
            return R.ok("修改成功");
        }
    }
    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId, String keyValue) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/culture/production/Update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("production");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
        return mv;
    }
    //真删假删
    @RequestMapping("/DeleteForm")
    public Object delete(CultureProduction tblHomeCultureProduction, String keyValue, String isshow) {
        if(StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)){
            tblHomeCultureProduction.setId(keyValue);
            tblHomeCultureProduction.setIsdel("0");
            tblHomeCultureProductionService.updateById(tblHomeCultureProduction);
//            fileEntityService.deleteByYwId(keyValue);
//            return R.ok("删除成功");
        }
        return R.ok("删除成功");
    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue){
        CultureProduction tblHomeCultureProduction = tblHomeCultureProductionService.getById(keyValue);
        return R.ok().put("data",tblHomeCultureProduction);
    }

    @RequestMapping("/GetTree")
    public Object GetTree(String communityid) {
        List<Tree> villageList = tblHomeCultureProductionService.getTree(communityid);
        return R.ok().put("data",villageList);
    }
}

