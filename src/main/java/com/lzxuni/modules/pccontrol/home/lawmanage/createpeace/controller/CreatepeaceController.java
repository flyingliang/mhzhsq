package com.lzxuni.modules.pccontrol.home.lawmanage.createpeace.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.lawmanage.createpeace.entity.Createpeace;
import com.lzxuni.modules.pccontrol.home.lawmanage.createpeace.service.CreatepeaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 * 首页--综治管理--平安创建 前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-07-24
 */
@RestController
@RequestMapping("/tblHomeLawmanageCreatepeace")
public class CreatepeaceController extends BaseController {

    @Autowired
    private CreatepeaceService tblHomeLawmanageCreatepeaceService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/createpeace/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Createpeace tblHomeLawmanageCreatepeace, String Keyword) {
        tblHomeLawmanageCreatepeace.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        tblHomeLawmanageCreatepeace.setId(Keyword);
        PageData pageData = getPageData(tblHomeLawmanageCreatepeaceService.queryPage(pageParameter, tblHomeLawmanageCreatepeace));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/GetTree")
    public Object GetTree(String communityid) {
        List<Tree> villageList = tblHomeLawmanageCreatepeaceService.getTree(communityid);
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/createpeace/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    public Object SaveForm(Createpeace tblHomeLawmanageCreatepeace, String keyValue, String isshow, String sjs) throws Exception{
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            tblHomeLawmanageCreatepeace.setId(ywId);
            tblHomeLawmanageCreatepeaceService.save(tblHomeLawmanageCreatepeace);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), ywId, "平安创建新闻图片", "sj", null);
            }
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            tblHomeLawmanageCreatepeaceService.updateById(tblHomeLawmanageCreatepeace);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), keyValue, "平安创建新闻图片", "sj", null);
                tblHomeLawmanageCreatepeace.setId(keyValue);
            }
            return R.ok("修改成功");
        }

    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue) throws Exception{
        Createpeace tblHomeLawmanageCreatepeace = tblHomeLawmanageCreatepeaceService.getById(keyValue);
        return R.ok().put("data",tblHomeLawmanageCreatepeace);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/createpeace/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("sjs");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
        return mv;
    }

    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        tblHomeLawmanageCreatepeaceService.removeById(keyValue);
        return R.ok("删除成功");
    }


}

