package com.lzxuni.modules.pccontrol.home.life.affair.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.affair.entity.Affair;
import com.lzxuni.modules.pccontrol.home.life.medicine.entity.Medicine;

import java.util.Map;

public interface AffairService extends IService<Affair> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Affair tblHomeLawmanageLegislation);

    //查询
    PageInfo<Affair> queryPage2(PageParameter pageParameter, Affair homeLifeAffair);
     //查询
    PageInfo<Affair> queryPage3(PageParameter pageParameter, Affair homeLifeAffair);
    //保存
    void SaveForm(Affair homeLifeAffair);
}
