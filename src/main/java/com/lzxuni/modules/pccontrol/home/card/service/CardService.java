package com.lzxuni.modules.pccontrol.home.card.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.card.entity.Card;

import java.util.Map;

/**
 * 办证 服务类
 * Created by 潘云明
 * 2019/7/16
 */
public interface CardService extends IService<Card> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Card entity);


}
