package com.lzxuni.modules.pccontrol.home.aged.organization.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.aged.organization.entity.AgedOrganization;

import java.util.Map;

/**
 * <p>
 * 养老管理--养老机构表 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-07-22
 */
public interface AgedOrganizationService extends IService<AgedOrganization> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, AgedOrganization tblHomeAgedOrganization);
    PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, AgedOrganization aged, String ywType);
}
