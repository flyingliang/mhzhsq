package com.lzxuni.modules.pccontrol.home.volunteer.vractivity.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.volunteer.vractivity.entity.VolunteerActivity;
import com.lzxuni.modules.pccontrol.home.volunteer.vractivity.service.VolunteerActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * <p>
 * 志愿管理-->志愿者活动表 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-07-29
 */
@RestController
@RequestMapping("/tblHomeVolunteerActivity")
public class VolunteerActivityController extends BaseController {
    @Autowired
    private FileEntityService fileEntityService;
    @Autowired
    private VolunteerActivityService tblHomeVolunteerActivityService;


    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "志愿管理-志愿者活动管理", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/volunteer/activity/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, VolunteerActivity tblHomeVolunteerActivity) {
        tblHomeVolunteerActivity.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblHomeVolunteerActivityService.queryPage(pageParameter, tblHomeVolunteerActivity));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/volunteer/activity/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3, module = "志愿管理-志愿者活动管理", operateType = "操作")
    public Object insertDo(VolunteerActivity tblHomeVolunteerActivity, String keyValue, String isshow, String stylepics) throws Exception {
        System.out.println("###参数列表：keyValue："+keyValue+",isshow:"+isshow);
        String typeName="社区志愿图片";
        String ywType="vractivity";
        if (StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)) {//展示
            tblHomeVolunteerActivity.setId(keyValue);
            tblHomeVolunteerActivity.setIsshow(isshow);
            tblHomeVolunteerActivityService.updateById(tblHomeVolunteerActivity);
            return R.ok("展示成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)) {//取消展示
            tblHomeVolunteerActivity.setId(keyValue);
            tblHomeVolunteerActivity.setIsshow(isshow);
            tblHomeVolunteerActivityService.updateById(tblHomeVolunteerActivity);
            return R.ok("取消展示成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)) {//删除
            tblHomeVolunteerActivity.setId(keyValue);
            tblHomeVolunteerActivity.setIsdel("0");
            tblHomeVolunteerActivityService.updateById(tblHomeVolunteerActivity);
            return R.ok("删除成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "4".equals(isshow)) {//返回图片数量
            FileEntity fileBeanCustom = new FileEntity();
            fileBeanCustom.setYwId(keyValue);
            fileBeanCustom.setYwType(ywType);
            return R.ok().put("data",fileEntityService.queryNumByFileEntity(fileBeanCustom));
        } else if (StringUtils.isNotEmpty(keyValue)) {
            fileEntityService.deleteByYwId(keyValue);
            if(!StringUtils.isEmpty(stylepics) && !"&amp;nbsp;".equals(stylepics)) {
                fileEntityService.insert(stylepics.replace("&quot;", "\""), keyValue, typeName, ywType, null);
            }
            tblHomeVolunteerActivity.setId(keyValue);
            tblHomeVolunteerActivityService.updateById(tblHomeVolunteerActivity);
            return R.ok("修改成功");
        }else{
            String ywId = UuidUtil.get32UUID();
            if (!StringUtils.isEmpty(stylepics) && !"&amp;nbsp;".equals(stylepics)) {
                fileEntityService.insert(stylepics.replace("&quot;", "\""), ywId, typeName, ywType, null);
            }
            tblHomeVolunteerActivity.setId(ywId);
            tblHomeVolunteerActivity.setIsdel("1");
            tblHomeVolunteerActivity.setIsshow("1");
            tblHomeVolunteerActivity.setState("0");
            tblHomeVolunteerActivityService.save(tblHomeVolunteerActivity);
            return R.ok("保存成功");
        }


    }
}

