package com.lzxuni.modules.pccontrol.home.party.report.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import com.lzxuni.modules.pccontrol.home.party.report.entity.PartyReport;
import com.lzxuni.modules.pccontrol.home.party.report.service.PartyReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 党建管理-->共驻共建管理-->社区报到管理 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
@RestController
@RequestMapping("/tblHomePartyReport")
public class PartyReportController extends BaseController {

    @Autowired
    private PartyReportService tblHomePartyReportService;


    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "党建管理-共驻共建管理-社会报到管理", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/party/report/index");
        return mv;
    }
    //文化管理-文化作品的查询
    @RequestMapping("/GetList")
    public Object GetList(String pagination, PartyReport tblHomePartyReport) throws SQLException {
        tblHomePartyReport.setIsdel("1");
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(tblHomePartyReportService.queryPage(pageParameter, tblHomePartyReport));
        return R.ok().put("data", pageData);
    }
    //添加、修改
    @RequestMapping("/SaveForm")
    public Object insertDo(String keyValue, PartyReport tblHomePartyReport, String isshow) throws Exception{

        if(StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)){
            tblHomePartyReport.setId(keyValue);
            tblHomePartyReport.setIsshow(isshow);
            tblHomePartyReportService.updateById(tblHomePartyReport);
            return R.ok("审核成功");
        }else if(StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)){
            tblHomePartyReport.setId(keyValue);
            tblHomePartyReport.setIsshow(isshow);
            tblHomePartyReportService.updateById(tblHomePartyReport);
            return R.ok("取消审核通过成功");
        }else
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            tblHomePartyReport.setId(ywId);
            tblHomePartyReport.setDate(new Date());
            tblHomePartyReportService.save(tblHomePartyReport);
            return R.ok("保存成功");
        }else{
            tblHomePartyReportService.updateById(tblHomePartyReport);
            return R.ok("修改成功");
        }
    }
    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update() throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/party/report/Update");
        return mv;
    }
    //真删假删
    @RequestMapping("/DeleteForm")
    public Object delete(PartyReport tblHomePartyReport, String keyValue, String isshow) {
        if(StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)){
            tblHomePartyReport.setId(keyValue);
            tblHomePartyReport.setIsdel("0");
            tblHomePartyReportService.updateById(tblHomePartyReport);
        }
        return R.ok("删除成功");
    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue){
        PartyReport tblHomePartyReport = tblHomePartyReportService.getById(keyValue);
        return R.ok().put("data",tblHomePartyReport);
    }

    @RequestMapping("/GetTree")
    public Object GetTree(String communityid) {
        List<Tree> villageList = tblHomePartyReportService.getTree(communityid);
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/getPost")
    public Object getPost(String pagination, CultureIntelligent tblHomeCultureIntelligent) {
        List<Tree> companyList = tblHomePartyReportService.getPost();
        return R.ok().put("data",companyList);
    }

    @RequestMapping("/getCompany")
    public Object getCompany(String pagination, CultureIntelligent tblHomeCultureIntelligent) {
        List<Tree> companyList = tblHomePartyReportService.getCompany();
        return R.ok().put("data",companyList);
    }
}

