package com.lzxuni.modules.pccontrol.home.life.affair.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.forum.entity.Comment;
import com.lzxuni.modules.pccontrol.home.life.affair.entity.Affair;
import com.lzxuni.modules.pccontrol.home.life.medicine.entity.Medicine;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AffairMapper extends BaseMapper<Affair> {

    List<Affair> selectListByopenid(Affair demo);
    List<Affair> selectListAll(Affair demo);

    List<Map<String,Object>> selectListByCondition(Affair tblHomeLawmanageLegislation);
}
