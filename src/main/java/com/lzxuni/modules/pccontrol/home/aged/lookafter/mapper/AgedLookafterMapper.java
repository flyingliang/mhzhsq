package com.lzxuni.modules.pccontrol.home.aged.lookafter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.aged.lookafter.entity.AgedLookafter;

/**
 * <p>
 * 养老管理--日间照料表 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-07-22
 */
public interface AgedLookafterMapper extends BaseMapper<AgedLookafter> {

}
