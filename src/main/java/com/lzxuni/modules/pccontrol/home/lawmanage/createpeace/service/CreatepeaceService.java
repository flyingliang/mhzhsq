package com.lzxuni.modules.pccontrol.home.lawmanage.createpeace.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.createpeace.entity.Createpeace;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治管理--平安创建 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-24
 */
public interface CreatepeaceService extends IService<Createpeace> {

    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Createpeace tblHomeLawmanageCreatepeace);
    //保存
    void SaveForm(Createpeace tblHomeLawmanageCreatepeace);

    List<Tree> getTree(String communityid);

    //查询
    PageInfo<Createpeace> selectListall(PageParameter pageParameter, Createpeace demo);
    //查询
    List<Createpeace> selectListthree(Createpeace demo);
}
