package com.lzxuni.modules.pccontrol.home.party.campaign.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.party.campaign.entity.PartyCampaign;

import java.util.Map;

/**
 * <p>
 * 党建管理-->共驻共建管理-->参加活动管理 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-08-06
 */
public interface PartyCampaignService extends IService<PartyCampaign> {
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, PartyCampaign tblHomePartyCampaign);
}
