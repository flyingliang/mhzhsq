package com.lzxuni.modules.pccontrol.home.party.campaign.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 党建管理-->共驻共建管理-->参加活动管理
 * </p>
 *
 * @author fcd
 * @since 2019-08-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_party_campaign")
public class PartyCampaign implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 姓名
     */
    private String campaignname;

    /**
     * 电话
     */
    private String campaignphone;

    /**
     * 小区id
     */
    private String villageid;

    /**
     * 参加理由
     */
    private String content;

    /**
     * 活动id
     */
    private String ceactivityid;

    /**
     * 建立时间
     */
    private Date date;

    /**
     * 1未删除，0删除
     */
    private String isdel;

    /**
     * 展示与不展示
     */
    private String isshow;
}
