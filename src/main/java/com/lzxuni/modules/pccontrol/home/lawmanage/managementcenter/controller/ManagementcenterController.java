package com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.entity.Managementcenter;
import com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.service.ManagementcenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@RestController
@RequestMapping("/pccontrol/home/lawmanage/managementcenter")
public class ManagementcenterController extends BaseController {

    @Autowired
    private ManagementcenterService tblHomeLawmanageManagementcenterService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/managementcenter/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Managementcenter tblHomeLawmanageManagementcenter, String Keyword) {
        tblHomeLawmanageManagementcenter.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblHomeLawmanageManagementcenterService.queryPage(pageParameter, tblHomeLawmanageManagementcenter));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/GetTree")
    public Object GetTree() {
        List<Tree> villageList = tblHomeLawmanageManagementcenterService.getTree();
        System.out.println("111111111111)))))"+villageList.toString());
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/managementcenter/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    public Object SaveForm(Managementcenter tblHomeLawmanageManagementcenter, String keyValue, String isshow, String sjs,String lcs) throws Exception{
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), ywId, "综治社区图片", "sj", null);
            }
            if(!StringUtils.isEmpty(lcs) && !"&amp;nbsp;".equals(lcs)) {
                fileEntityService.insert(lcs.replace("&quot;", "\""), ywId, "综治流程图片", "lc", null);
            }
            tblHomeLawmanageManagementcenter.setId(ywId);
            tblHomeLawmanageManagementcenterService.save(tblHomeLawmanageManagementcenter);
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), keyValue, "综治社区图片", "sj", null);
            }
            if(!StringUtils.isEmpty(lcs) && !"&amp;nbsp;".equals(lcs)) {
                fileEntityService.insert(lcs.replace("&quot;", "\""), keyValue, "综治流程图片", "lc", null);

            }
            tblHomeLawmanageManagementcenter.setId(keyValue);
            tblHomeLawmanageManagementcenterService.updateById(tblHomeLawmanageManagementcenter);
            return R.ok("修改成功");
        }

    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue) throws Exception{
        Managementcenter tblHomeLawmanageManagementcenter = tblHomeLawmanageManagementcenterService.getById(keyValue);
        return R.ok().put("data",tblHomeLawmanageManagementcenter);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/managementcenter/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("sjs");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
        fileBeanCustom.setYwType("lcs");
        mv.addObject("lcsNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",ywId);
        return mv;
    }

    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        tblHomeLawmanageManagementcenterService.removeById(keyValue);
        return R.ok("删除成功");
    }

}

