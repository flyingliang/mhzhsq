package com.lzxuni.modules.pccontrol.home.lawmanage.safety.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.entity.Legislation;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.mapper.LegislationMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.service.LegislationService;
import com.lzxuni.modules.pccontrol.home.lawmanage.safety.entity.Safety;
import com.lzxuni.modules.pccontrol.home.lawmanage.safety.mapper.SafetyMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.safety.service.SafetyService;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--平安创建条例 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
@Service
public class SafetyServiceImpl extends ServiceImpl<SafetyMapper, Safety> implements SafetyService {

    @Autowired
    private SafetyMapper tblHomeLawmanageLegislationMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Safety tblHomeLawmanageLegislation) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeLawmanageLegislationMapper.selectListByCondition(tblHomeLawmanageLegislation);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Safety tblHomeLawmanageLegislation) {
        String uuid = UuidUtil.get32UUID();
        tblHomeLawmanageLegislation.setId(uuid);
        this.save(tblHomeLawmanageLegislation);
    }

    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = tblHomeLawmanageLegislationMapper.getTree(communityid);
        return list;
    }
    @Override
    public PageInfo<Safety> queryPage3(PageParameter pageParameter, Safety demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Safety> homeLifeAffairs = tblHomeLawmanageLegislationMapper.selectListAll(demo);
        PageInfo<Safety> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }
}
