package com.lzxuni.modules.pccontrol.home.party.style.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 主页_党建_作风建设
 * Created by 潘云明
 * 2019/7/16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_party_style")
public class Style implements Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1L;
    @TableId
    /** 主键，uuid */
    private String id;

    /** 标题 */
    private String title;

    /** 社区编号 */
    private String communityid;

    /** 内容 */
    private String content;



    /**1表示展示，1隐藏 */
    private String isshow;

    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除*/
    private String isdel;

}
