package com.lzxuni.modules.pccontrol.home.party.report.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.party.report.entity.PartyReport;
import com.lzxuni.modules.pccontrol.home.party.report.mapper.PartyReportMapper;
import com.lzxuni.modules.pccontrol.home.party.report.service.PartyReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 党建管理-->共驻共建管理-->社区报到管理 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
@Service
public class PartyReportServiceImpl extends ServiceImpl<PartyReportMapper, PartyReport> implements PartyReportService {

    @Autowired
    private PartyReportMapper tblHomePartyReportMapper;
    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, PartyReport tblHomePartyReport) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomePartyReportMapper.selectListByCondition(tblHomePartyReport);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }
    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = tblHomePartyReportMapper.getTree(communityid);
        return list;
    }

    @Override
    public List<Tree> getPost() {
        List<Tree> list = tblHomePartyReportMapper.getPost();
        return list;
    }

    @Override
    public List<Tree> getCompany() {
        List<Tree> list = tblHomePartyReportMapper.getCompany();
        return list;
    }

}
