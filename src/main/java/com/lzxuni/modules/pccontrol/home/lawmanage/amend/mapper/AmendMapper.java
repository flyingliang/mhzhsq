package com.lzxuni.modules.pccontrol.home.lawmanage.amend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.amend.entity.Amend;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区矫正条例 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
public interface AmendMapper extends BaseMapper<Amend> {
    //社区联动
    List<Map<String,Object>> selectListByCondition(Amend tblHomeLawmanageLegislation);
    List<Tree> getTree(@Param("communityid") String communityid);
    List<Amend> selectListAll(Amend demo);
}
