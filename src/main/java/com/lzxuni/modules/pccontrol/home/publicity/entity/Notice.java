package com.lzxuni.modules.pccontrol.home.publicity.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 基础管理-->通知公告管理
 * </p>
 *
 * @author fcd
 * @since 2019-08-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_notice")
public class Notice implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 标题
     */
    private String publicitytitle;

    /**
     * 发布时间
     */
    private Date releasetime;

    /**
     * 发布部门
     */
    private String department;

    /**
     * 作者
     */
    private String author;

    /**
     * 浏览量
     */
    private String pageview;

    /**
     * 内容
     */
    private String content;

    /**
     * 展示与不展示
     */
    private String isshow;

    /**
     * 创建时间
     */
    private Date date;

    /**
     * 账号
     */
    private String account;

    /**
     * 0为删除1未删除
     */
    private String isdel;
}
