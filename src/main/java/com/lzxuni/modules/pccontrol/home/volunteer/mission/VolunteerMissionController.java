package com.lzxuni.modules.pccontrol.home.volunteer.mission;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.volunteer.mien.entity.VolunteerMien;
import com.lzxuni.modules.pccontrol.home.volunteer.mien.service.VolunteerMienService;
import com.lzxuni.modules.pccontrol.home.volunteer.vractivity.entity.VolunteerActivity;
import com.lzxuni.modules.pccontrol.home.volunteer.vractivity.service.VolunteerActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 * 志愿管理-->任务领取 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-07-29
 */
@RestController
@RequestMapping("/tblHomeVolunteerMission")
public class VolunteerMissionController extends BaseController {
    @Autowired
    private VolunteerActivityService tblHomeVolunteerActivityService;
    @Autowired
    private VolunteerMienService volunteerMienService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "志愿管理--任务领取管理", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/volunteer/mission/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, VolunteerActivity tblHomeVolunteerActivity) {
        tblHomeVolunteerActivity.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(volunteerMienService.selectMienList(pageParameter));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/volunteer/mission/form");
        return mv;
    }

    @RequestMapping("/FormAchieve")
    public ModelAndView FormAchieve() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/volunteer/mission/achieve");
        return mv;
    }

    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3, module = "志愿管理-任务领取管理", operateType = "操作")
    public Object insertDo(VolunteerActivity tblHomeVolunteerActivity, String keyValue, String isshow,String StateValue) throws Exception {
        System.out.println("###参数列表：keyValue："+keyValue+",isshow:"+isshow);
        String typeName="社区志愿图片";
        String ywType="vractivity";
        if (StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)) {//展示
            tblHomeVolunteerActivity.setId(keyValue);
            tblHomeVolunteerActivity.setIsshow(isshow);
            tblHomeVolunteerActivity.setState("1");
            tblHomeVolunteerActivityService.updateById(tblHomeVolunteerActivity);
            return R.ok("加入成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)) {//取消展示
            tblHomeVolunteerActivity.setId(keyValue);
            tblHomeVolunteerActivity.setIsshow(isshow);
            tblHomeVolunteerActivity.setState("0");
            tblHomeVolunteerActivityService.updateById(tblHomeVolunteerActivity);
            return R.ok("取消加入成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)) {//删除
            tblHomeVolunteerActivity.setId(keyValue);
            tblHomeVolunteerActivity.setIsdel("0");
            tblHomeVolunteerActivityService.updateById(tblHomeVolunteerActivity);
            return R.ok("删除成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "4".equals(isshow)) {//返回图片数量
            FileEntity fileBeanCustom = new FileEntity();
            fileBeanCustom.setYwId(keyValue);
            fileBeanCustom.setYwType(ywType);
            return R.ok().put("data",null);
        } else if (StringUtils.isNotEmpty(keyValue)) {
                tblHomeVolunteerActivity.setId(keyValue);
                tblHomeVolunteerActivity.setState("1");
                tblHomeVolunteerActivityService.updateById(tblHomeVolunteerActivity);
                return R.ok("接取成功");

        }else{
//            String ywId = UuidUtil.get32UUID();
//            tblHomeVolunteerActivity.setId(ywId);
//            tblHomeVolunteerActivity.setIsdel("1");
//            tblHomeVolunteerActivity.setState("0");
//            tblHomeVolunteerActivityService.save(tblHomeVolunteerActivity);
            return R.ok("取消加入失败");
        }


    }
    @RequestMapping("/SaveForms")
    @SysLog(categoryId = 3, module = "志愿管理-任务领取管理", operateType = "操作")
    public Object insertDos(String keyValue,String isshow,String award,String openid) throws Exception {
        VolunteerMien volunteerMien=new VolunteerMien();
        int i = Integer.parseInt(award);
        volunteerMien.setId(keyValue);
        volunteerMien.setState("2");
        volunteerMien.setAward(i);
            volunteerMienService.updateById(volunteerMien);
            tblHomeVolunteerActivityService.updateAward(award,openid);
            return R.ok("完成成功");
    }

    @RequestMapping("/SaveFormss")
    @SysLog(categoryId = 3, module = "志愿管理-任务领取管理", operateType = "操作")
    public Object insertDoss(VolunteerActivity tblHomeVolunteerActivity, String keyValue, String isshow,String award,String joinid) throws Exception {
            tblHomeVolunteerActivity.setId(keyValue);
            tblHomeVolunteerActivity.setIsshow(isshow);
            tblHomeVolunteerActivity.setState("1");
            tblHomeVolunteerActivityService.updateById(tblHomeVolunteerActivity);
            tblHomeVolunteerActivityService.updateAwards(award,joinid);
            return R.ok("取消完成成功");
    }

    @RequestMapping("/GetTree")
    public Object GetTree() {
        List<Tree> companyList = tblHomeVolunteerActivityService.getTree();
        return R.ok().put("data",companyList);
    }
}

