package com.lzxuni.modules.pccontrol.home.card.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.constant.LogConstant;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.card.entity.Card;
import com.lzxuni.modules.pccontrol.home.card.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 办证 前端控制器
 * Created by 潘云明
 * 2019/7/16
 */

@RestController
@RequestMapping("/pccontrol/home/card")
public class CardController extends BaseController {
    @Autowired
    private CardService cardService;

    @RequestMapping("/ZXBZ_Index")
    @SysLog(categoryId = 2, module = "查看在线办证", operateType = "访问")
    public ModelAndView ZXBZ_Index() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/card/carz_ZXBZ_index");
        return mv;
    }

    @RequestMapping("/ZXBZ_GetList")
    public Object ZXBZ_GetList(String pagination) {
        Card entity=new Card();
        entity.setIspass("0");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(cardService.queryPage(pageParameter, entity));
        return R.ok().put("data", pageData);
    }


    @RequestMapping("/ZXBZ_Form")
    public ModelAndView ZXBZ_Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/card/carz_ZXBZ_form");
        return mv;
    }


    @SysLog(categoryId = LogConstant.OPERATEID, module = "查看在线办证", operateType = LogConstant.INSERT)
    @RequestMapping("/ZXBZ_SaveForm")
    public Object ZXBZ_SaveForm(String keyValue, String ispass,String content) {
        if (StringUtils.isNotEmpty(keyValue)) {
            Card card = new Card();
            card.setId(keyValue);
            card.setIspass(ispass);
            card.setContent(content);
            cardService.updateById(card);
            return R.ok("修改成功");
        }
        return R.ok("修改成功");
    }



}
