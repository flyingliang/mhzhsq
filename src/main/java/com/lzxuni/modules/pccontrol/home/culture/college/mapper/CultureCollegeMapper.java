package com.lzxuni.modules.pccontrol.home.culture.college.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.lzxuni.modules.pccontrol.home.culture.college.entity.CultureCollege;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理-市民学院表 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-07-17
 */
public interface CultureCollegeMapper extends BaseMapper<CultureCollege> {
    List<Map<String,Object>> selectListByCondition(@Param("cultureCollege")CultureCollege tblHomeCultureCollege);

    List<Map<String, Object>> selectListByConditionAndImg(@Param("cultureCollege")CultureCollege cultureCollege,@Param("ywType") String ywType);
}
