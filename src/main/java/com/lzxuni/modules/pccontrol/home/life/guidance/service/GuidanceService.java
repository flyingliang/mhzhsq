package com.lzxuni.modules.pccontrol.home.life.guidance.service;

import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.guidance.entity.Guidance;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 生活服务-政务大厅-办事指南 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-11
 */
public interface GuidanceService extends IService<Guidance> {

    //查询
    PageInfo<Guidance> queryPage(PageParameter pageParameter, Guidance tblHomeLifeGuidance);
     //查询
    List<Guidance> findGuideList();
    //保存
    void SaveForm(Guidance tblHomeLifeGuidance);
}
