package com.lzxuni.modules.pccontrol.home.lawmanage.contradiction.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.lawmanage.contradiction.entity.Contradiction;
import com.lzxuni.modules.pccontrol.home.lawmanage.contradiction.service.ContradictionService;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.entity.Legislation;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.service.LegislationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 * 首页--综治--社区矛盾调解条例 前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
@RestController
@RequestMapping("/tblHomeLawmanageContradiction")
public class ContradictionController extends BaseController {

    @Autowired
    private ContradictionService contradictionService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/contradiction/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Contradiction tblHomeLawmanageLegislation, String Keyword) {
        tblHomeLawmanageLegislation.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(contradictionService.queryPage(pageParameter, tblHomeLawmanageLegislation));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/GetTree")
    public Object GetTree(String communityid) {
        List<Tree> villageList = contradictionService.getTree(communityid);
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/contradiction/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    public Object SaveForm(Contradiction tblHomeLawmanageLegislation, String keyValue, String isshow, String sjs) throws Exception{
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            tblHomeLawmanageLegislation.setId(ywId);
            contradictionService.save(tblHomeLawmanageLegislation);
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            contradictionService.updateById(tblHomeLawmanageLegislation);
            return R.ok("修改成功");
        }

    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue) throws Exception{
        Contradiction tblHomeLawmanageLegislation = contradictionService.getById(keyValue);
        return R.ok().put("data",tblHomeLawmanageLegislation);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/contradiction/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("sjs");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
        return mv;
    }

    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        contradictionService.removeById(keyValue);
        return R.ok("删除成功");
    }

}

