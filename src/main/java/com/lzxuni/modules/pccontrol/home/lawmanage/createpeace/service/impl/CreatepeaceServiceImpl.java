package com.lzxuni.modules.pccontrol.home.lawmanage.createpeace.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.createpeace.entity.Createpeace;
import com.lzxuni.modules.pccontrol.home.lawmanage.createpeace.mapper.CreatepeaceMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.createpeace.service.CreatepeaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治管理--平安创建 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-24
 */
@Service
public class CreatepeaceServiceImpl extends ServiceImpl<CreatepeaceMapper, Createpeace> implements CreatepeaceService {

    @Autowired
    private CreatepeaceMapper tblHomeLawmanageCreatepeaceMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Createpeace tblHomeLawmanageCreatepeace) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeLawmanageCreatepeaceMapper.selectListByCondition(tblHomeLawmanageCreatepeace);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Createpeace tblHomeLawmanageCreatepeace) {
        String uuid = UuidUtil.get32UUID();
        tblHomeLawmanageCreatepeace.setId(uuid);
        this.save(tblHomeLawmanageCreatepeace);
    }

    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = tblHomeLawmanageCreatepeaceMapper.getTree(communityid);
        return list;
    }

    @Override
    public PageInfo<Createpeace> selectListall(PageParameter pageParameter, Createpeace demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Createpeace> homeLifeAffairs = tblHomeLawmanageCreatepeaceMapper.selectListall(demo);
        PageInfo<Createpeace> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }

    @Override
    public List<Createpeace> selectListthree( Createpeace demo) {

        List<Createpeace> homeLifeAffairs = tblHomeLawmanageCreatepeaceMapper.selectListthree(demo);

        return homeLifeAffairs;
    }



}
