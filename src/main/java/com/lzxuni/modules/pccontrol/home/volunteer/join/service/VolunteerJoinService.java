package com.lzxuni.modules.pccontrol.home.volunteer.join.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import com.lzxuni.modules.pccontrol.home.volunteer.join.entity.VolunteerJoin;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 志愿服务--在线加入表 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-07-24
 */
public interface VolunteerJoinService extends IService<VolunteerJoin> {
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, VolunteerJoin tblHomeVolunteerJoin);

    List<Tree> getTree();

    PageInfo<Map<String,Object>> queryList(PageParameter pageParameter);


    //查询
    PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, VolunteerJoin volunteerJoin, String ywType);

    String selectJoinId(String openid);
}
