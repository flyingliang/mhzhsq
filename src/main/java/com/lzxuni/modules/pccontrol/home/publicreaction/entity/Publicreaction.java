package com.lzxuni.modules.pccontrol.home.publicreaction.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lzxuni.modules.common.entity.FileEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 基础管理-->社情反应管理
 * </p>
 *
 * @author fcd
 * @since 2019-08-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_publicreaction")
public class Publicreaction implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 标题
     */
    private String problemtitle;

    /**
     * 反映问题
     */
    private String problem;

    /**
     * 问题类别
     */
    private String problemtype;

    /**
     * 姓名
     */
    private String personnelname;

    /**
     * 小区id
     */
    private String villageid;

    /**
     * 联系电话
     */
    private String contactnumber;

    /**
     * 展示与不展示
     */
    private String isshow;

    /**
     * 创建时间
     */
    private Date date;

    /**
     * 0为删除
     */
    private String isdel;
    /**
     * 微信id
     */
    private String openid;
    /**
     * 微信名
     */
    private String nickname;

    //回复
    private String content;

    @TableField(exist = false)
    private List<FileEntity> fileEntities;

    @TableField(exist = false)
    private String urlPath;//web访问路径

    @TableField(exist = false)
    private String urlsPath;//缩略图访问路径


}
