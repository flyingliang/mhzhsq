package com.lzxuni.modules.pccontrol.home.lawmanage.legalaid.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.legalaid.entity.Legalaid;
import com.lzxuni.modules.pccontrol.home.lawmanage.legalaid.mapper.LegalaidMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.legalaid.service.LegalaidService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-29
 */
@Service
public class LegalaidServiceImpl extends ServiceImpl<LegalaidMapper, Legalaid> implements LegalaidService {


    @Autowired
    private LegalaidMapper tblHomeLawmanageLegalaidMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Legalaid tblHomeLawmanageLegalaid) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeLawmanageLegalaidMapper.selectListByCondition(tblHomeLawmanageLegalaid);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Legalaid tblHomeLawmanageLegalaid) {
        String uuid = UuidUtil.get32UUID();
        tblHomeLawmanageLegalaid.setId(uuid);
        this.save(tblHomeLawmanageLegalaid);
    }

    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = tblHomeLawmanageLegalaidMapper.getTree(communityid);
        return list;
    }

}
