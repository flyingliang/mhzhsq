package com.lzxuni.modules.pccontrol.home.lawmanage.onlineconsultation.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.lawmanage.onlineconsultation.entity.Onlineconsultation;
import com.lzxuni.modules.pccontrol.home.lawmanage.onlineconsultation.mapper.OnlineconsultationMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.onlineconsultation.service.OnlineconsultationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.pccontrol.home.life.medicine.entity.Medicine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 首页--综治管理--在线咨询管理 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@Service
public class OnlineconsultationServiceImpl extends ServiceImpl<OnlineconsultationMapper, Onlineconsultation> implements OnlineconsultationService {

    @Autowired
    private OnlineconsultationMapper tblHomeLawmanageOnlineconsultationMapper;

    @Override
    public PageInfo<Onlineconsultation> queryPage(PageParameter pageParameter, Onlineconsultation tblHomeLawmanageOnlineconsultation) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Onlineconsultation> tblHomeLawmanageOnlineconsultationList = tblHomeLawmanageOnlineconsultationMapper.selectList(new QueryWrapper<Onlineconsultation>().eq("isDel", "1"));
        PageInfo<Onlineconsultation> pageInfo = new PageInfo<>(tblHomeLawmanageOnlineconsultationList);
        return pageInfo;
    }

    @Override
    public PageInfo<Onlineconsultation> queryPage2(PageParameter pageParameter, Onlineconsultation demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Onlineconsultation> homeLifeAffairs = tblHomeLawmanageOnlineconsultationMapper.selectListByopenid(demo);
        PageInfo<Onlineconsultation> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }
    @Override
    public PageInfo<Onlineconsultation> queryPage3(PageParameter pageParameter, Onlineconsultation demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Onlineconsultation> homeLifeAffairs = tblHomeLawmanageOnlineconsultationMapper.selectListAll(demo);
        PageInfo<Onlineconsultation> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }
    @Override
    public void SaveForm(Onlineconsultation tblHomeLawmanageOnlineconsultation) {
        String newsZzyhDomesticId = UuidUtil.get32UUID();
        tblHomeLawmanageOnlineconsultation.setId(newsZzyhDomesticId);
        this.save(tblHomeLawmanageOnlineconsultation);
    }

}
