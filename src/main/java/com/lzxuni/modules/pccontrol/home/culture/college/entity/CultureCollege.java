package com.lzxuni.modules.pccontrol.home.culture.college.entity;


import com.baomidou.mybatisplus.annotation.TableName;


import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 文化管理-市民学院表
 * </p>
 *
 * @author fcd
 * @since 2019-07-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_culture_college")
public class CultureCollege implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 课堂标题
     */
    private String title;

    /**
     * 授课教师
     */
    private String teacher;

    /**
     * 授课时间
     */
    private Date teachingtime;

    /**
     * 授课地点
     */
    private String communityid;

    /**
     * 授课内容
     */
    private String content;

    /**
     * 1表示展示，0隐藏
     */
    private String isshow;

    /**
     * 添加时间
     */
    private Date date;

    /**
     * 1未删除，0删除
     */
    private String isdel;
    /**
     * 类型
     */
   private String  collegetype;
}
