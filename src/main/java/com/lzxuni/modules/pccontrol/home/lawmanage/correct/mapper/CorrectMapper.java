package com.lzxuni.modules.pccontrol.home.lawmanage.correct.mapper;

import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.correct.entity.Correct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区矫正 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-07-25
 */
public interface CorrectMapper extends BaseMapper<Correct> {

    //社区联动
    List<Map<String,Object>> selectListByCondition(Correct tblHomeLawmanageCorrect);
    List<Tree> getTree(@Param("communityid") String communityid);

    List<Correct> selectListthree(Correct demo);
    List<Correct> selectListall(Correct demo);
}
