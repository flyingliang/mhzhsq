package com.lzxuni.modules.pccontrol.home.card.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.card.entity.Card;

import java.util.List;
import java.util.Map;

/**
 * 办证 Mapper 接口
 * Created by 潘云明
 * 2019/7/16
 */
public interface CardMapper extends BaseMapper<Card> {
    List<Map<String,Object>> selectListByCondition(Card entity);
}
