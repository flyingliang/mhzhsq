package com.lzxuni.modules.pccontrol.home.party.ceactivity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.mapper.FileEntityMapper;
import com.lzxuni.modules.pccontrol.basics.community.entity.Community;
import com.lzxuni.modules.pccontrol.basics.community.mapper.CommunityMapper;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import com.lzxuni.modules.pccontrol.home.party.ceactivity.entity.PartyCeactivity;
import com.lzxuni.modules.pccontrol.home.party.ceactivity.mapper.PartyCeactivityMapper;
import com.lzxuni.modules.pccontrol.home.party.ceactivity.service.PartyCeactivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 党建管理-->共驻共建管理-->创建活动管理 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
@Service
public class PartyCeactivityServiceImpl extends ServiceImpl<PartyCeactivityMapper, PartyCeactivity> implements PartyCeactivityService {

    @Autowired
    private PartyCeactivityMapper tblHomePartyCeactivityMapper;
    @Autowired
    private FileEntityMapper fileEntityMapper;
    @Autowired
    private CommunityMapper communityMapper;

    @Override
    public PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, PartyCeactivity tblHomePartyCeactivity) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblHomePartyCeactivityMapper.selectListByCondition(tblHomePartyCeactivity);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }

    @Override
    public PageInfo<PartyCeactivity> queryPageAndImg(PageParameter pageParameter, PartyCeactivity partyCeactivity, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<PartyCeactivity> paList = tblHomePartyCeactivityMapper.selectqueryAll(partyCeactivity);
        PageInfo<PartyCeactivity> pageInfo = new PageInfo<>(paList);
        return pageInfo;
    }

    @Override
    public List<PartyCeactivity> queryListAll(String id) {
        return tblHomePartyCeactivityMapper.queryListAll(id);
    }

}
