package com.lzxuni.modules.pccontrol.home.lawmanage.legalaid.service;

import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.legalaid.entity.Legalaid;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-29
 */
public interface LegalaidService extends IService<Legalaid> {

    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Legalaid tblHomeLawmanageLegalaid);
    //保存
    void SaveForm( Legalaid tblHomeLawmanageLegalaid);

    List<Tree> getTree(String communityid);
}
