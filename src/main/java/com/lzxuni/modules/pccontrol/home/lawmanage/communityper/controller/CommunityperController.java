package com.lzxuni.modules.pccontrol.home.lawmanage.communityper.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.lawmanage.communityper.entity.Communityper;
import com.lzxuni.modules.pccontrol.home.lawmanage.communityper.service.CommunityperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 * 首页--综治--社区服务人员表 前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-08-20
 */
@RestController
@RequestMapping("/pccontrol/home/lawmanage/communityper")
public class CommunityperController extends BaseController {

    @Autowired
    private CommunityperService communityperService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/communityper/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Communityper communityper, String Keyword) {
        communityper.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(communityperService.queryPage(pageParameter, communityper));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/GetTree")
    public Object GetTree() {
        List<Tree> villageList = communityperService.getTree();
        System.out.println("111111111111)))))"+villageList.toString());
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/communityper/form");
        return mv;
    }

    //添加综治中心-->社区工作人员
    @RequestMapping("/SaveForm")
    public Object SaveForm(Communityper communityper, String keyValue, String isshow, String sjs,String lcs) throws Exception{
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), ywId, "社区工作人员照片", "sj", null);
            }
            communityper.setId(ywId);
            communityper.setNumberid("1");
            communityperService.save(communityper);
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), keyValue, "社区工作人员照片", "sj", null);
            }
            communityper.setId(keyValue);
            communityperService.updateById(communityper);
            return R.ok("修改成功");
        }

    }

    //添加社区警务室-->警务员
    @RequestMapping("/SaveFormPo")
    public Object SaveFormPo(Communityper communityper, String keyValue, String isshow, String sjs,String lcs) throws Exception{
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), ywId, "警务员照片", "sj", null);
            }
            communityper.setId(ywId);
            communityper.setNumberid("2");//警务员
            communityperService.save(communityper);
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), keyValue, "警务员照片", "sj", null);
            }
            communityper.setId(keyValue);
            communityperService.updateById(communityper);
            return R.ok("修改成功");
        }

    }

    //法律调解员
    @RequestMapping("/SaveFormAi")
    public Object SaveFormAi(Communityper communityper, String keyValue, String isshow, String sjs,String lcs) throws Exception{
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), ywId, "法律调解员照片", "sj", null);
            }
            communityper.setId(ywId);
            communityper.setNumberid("3");//法律调解员
            communityperService.save(communityper);
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), keyValue, "法律调解员照片", "sj", null);
            }
            communityper.setId(keyValue);
            communityperService.updateById(communityper);
            return R.ok("修改成功");
        }

    }

    //人民调解员
    @RequestMapping("/SaveFormRm")
    public Object SaveFormRm(Communityper communityper, String keyValue, String isshow, String sjs,String lcs) throws Exception{
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), ywId, "法律调解员照片", "sj", null);
            }
            communityper.setId(ywId);
            communityper.setNumberid("4");//人民调解员
            communityperService.save(communityper);
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), keyValue, "法律调解员照片", "sj", null);
            }
            communityper.setId(keyValue);
            communityperService.updateById(communityper);
            return R.ok("修改成功");
        }

    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue) throws Exception{
        Communityper communityper = communityperService.getById(keyValue);
        return R.ok().put("data",communityper);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/communityper/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("sjs");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
        return mv;
    }

    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        communityperService.removeById(keyValue);
        return R.ok("删除成功");
    }

    @RequestMapping("/FormPo")
    public ModelAndView FormPo() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/communityper/formpo");
        return mv;
    }

    @RequestMapping("/FormAi")
    public ModelAndView FormAi() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/communityper/formai");
        return mv;
    }

    @RequestMapping("/FormRm")
    public ModelAndView FormRm() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/communityper/formrm");
        return mv;
    }

}

