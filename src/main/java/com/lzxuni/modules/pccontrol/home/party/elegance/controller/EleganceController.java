package com.lzxuni.modules.pccontrol.home.party.elegance.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.party.elegance.entity.Elegance;
import com.lzxuni.modules.pccontrol.home.party.elegance.service.EleganceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 主页_党建_群团风采 前端控制器
 * Created by 潘云明
 * 2019/7/18
 */

@RestController
@RequestMapping("/pccontrol/home/party/elegance")
public class EleganceController extends BaseController {
    @Autowired
    private EleganceService service;

    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看群团风采", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/party/elegance/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Elegance entity) {
        System.out.println("￥￥￥￥￥￥￥￥￥￥"+pagination);
        entity.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(service.queryPage(pageParameter, entity));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/party/elegance/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3, module = "群团风采", operateType = "操作")
    public Object insertDo(Elegance entity, String keyValue, String isshow, String elegancepics) throws Exception {
        System.out.println("###参数列表：keyValue："+keyValue+",isshow:"+isshow);
        String typeName="群团风采图片";
        String ywType="elegancepic";
        if (StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)) {//展示
            entity.setId(keyValue);
            entity.setIsshow(isshow);
            service.updateById(entity);
            return R.ok("展示成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)) {//取消展示
            entity.setId(keyValue);
            entity.setIsshow(isshow);
            service.updateById(entity);
            return R.ok("取消展示成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)) {//删除
            entity.setId(keyValue);
            entity.setIsdel("0");
            service.updateById(entity);
            return R.ok("删除成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "4".equals(isshow)) {//返回图片数量
            FileEntity fileBeanCustom = new FileEntity();
            fileBeanCustom.setYwId(keyValue);
            fileBeanCustom.setYwType(ywType);
            return R.ok().put("data",fileEntityService.queryNumByFileEntity(fileBeanCustom));
        } else if (StringUtils.isNotEmpty(keyValue)) {
            fileEntityService.deleteByYwId(keyValue);
            if(!StringUtils.isEmpty(elegancepics) && !"&amp;nbsp;".equals(elegancepics)) {
                fileEntityService.insert(elegancepics.replace("&quot;", "\""), keyValue, typeName, ywType, null);
            }
            entity.setId(keyValue);
            service.updateById(entity);
            return R.ok("修改成功");
        } else {
            String ywId = UuidUtil.get32UUID();
            if (!StringUtils.isEmpty(elegancepics) && !"&amp;nbsp;".equals(elegancepics)) {
                fileEntityService.insert(elegancepics.replace("&quot;", "\""), ywId, typeName, ywType, null);
            }
            entity.setId(ywId);
            service.save(entity);
            return R.ok("保存成功");
        }


    }

}
