package com.lzxuni.modules.pccontrol.home.life.shop.mapper;

import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.life.shop.entity.Shop;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-07-19
 */
public interface ShopMapper extends BaseMapper<Shop> {

    //社区联动
    List<Map<String,Object>> selectListByCondition(Shop tblHomeLifeShop);
    List<Tree> getTree(@Param("communityid") String communityid);
    List<Shop> selectshopList(Shop demo);

    //商家类别联动
    List<Map<String,Object>> selectListByCondition1(Shop tblHomeLifeShop);
    Shop selectshopbyid(Shop tblHomeLifeShop);
//    List<Tree> getTree1();

    List<Tree> getTree1(@Param("shoptypeid") String shoptypeid);
}
