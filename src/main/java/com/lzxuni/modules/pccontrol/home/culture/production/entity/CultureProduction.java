package com.lzxuni.modules.pccontrol.home.culture.production.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 文化管理-文化作品表
 * </p>
 *
 * @author fcd
 * @since 2019-07-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_culture_production")
public class CultureProduction implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * uuid做为唯一主键
     */
    @TableId
    private String id;

    /**
     * 作品标题
     */
    private String productiontitle;

    /**
     * 作者姓名
     */
    private String productionname;

    /**
     * 作者电话
     */
    private String productionphone;

    /**
     * 选择小区
     */
    private String villageid;

    /**
     * 作品描述
     */
    private String productiondescribe;

    /**
     * 时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date date;
    /**
     * 是否展示
     */
    private String isshow;

    /** 1未删除，0删除*/
    private String isdel;

    /** 加入人微信id */
    private String openid;

    /** 加入人微信名 */
    private String nickname;
}
