package com.lzxuni.modules.pccontrol.home.life.shoptype.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.life.shoptype.entity.Shoptype;
import com.lzxuni.modules.pccontrol.home.life.shoptype.service.ShoptypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;

/**
 * <p>
 * 首页--生活管理--商家类别 前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-07-17
 */
@RestController
@RequestMapping("/pccontrol/home/life/shoptype")
public class ShoptypeController extends BaseController {

    @Autowired
    private ShoptypeService tblHomeLifeShoptypeService;

    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/shoptype/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Shoptype tblHomeLifeShoptype, String Keyword) throws SQLException {
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        tblHomeLifeShoptype.setShopname(Keyword);
        PageData pageData = getPageData(tblHomeLifeShoptypeService.queryPage(pageParameter, tblHomeLifeShoptype));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView insert() throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/shoptype/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    public Object insertDo(String keyValue, Shoptype tblHomeLifeShoptype, String isShow, String sjs) throws Exception{
        if(StringUtils.isNotEmpty(keyValue) && "1".equals(isShow)){
            tblHomeLifeShoptype.setId(keyValue);
            tblHomeLifeShoptype.setIsshow(isShow);
            tblHomeLifeShoptypeService.updateById(tblHomeLifeShoptype);
            return R.ok("展示成功");
        }else if(StringUtils.isNotEmpty(keyValue) && "0".equals(isShow)){
            tblHomeLifeShoptype.setId(keyValue);
            tblHomeLifeShoptype.setIsshow(isShow);
            tblHomeLifeShoptypeService.updateById(tblHomeLifeShoptype);
            return R.ok("取消展示成功");
        }else if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            System.out.println("++++"+sjs);
            tblHomeLifeShoptype.setId(ywId);
            tblHomeLifeShoptypeService.save(tblHomeLifeShoptype);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), ywId, "商家类别图片", "sj", null);
            }
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            tblHomeLifeShoptypeService.updateById(tblHomeLifeShoptype);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), keyValue, "商家类别图片", "sj", null);
                tblHomeLifeShoptype.setId(keyValue);
            }
            return R.ok("修改成功");
        }

    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue) throws Exception{
        Shoptype tblHomeLifeShoptype = tblHomeLifeShoptypeService.getById(keyValue);
        return R.ok().put("data",tblHomeLifeShoptype);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/life/shoptype/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("sjs");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
        return mv;
    }


    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        tblHomeLifeShoptypeService.removeById(keyValue);
        return R.ok("删除成功");
    }
}

