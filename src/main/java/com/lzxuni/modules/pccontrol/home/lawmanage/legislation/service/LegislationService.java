package com.lzxuni.modules.pccontrol.home.lawmanage.legislation.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.entity.Legislation;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
public interface LegislationService extends IService<Legislation> {

    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Legislation tblHomeLawmanageLegislation);
    //查询
    PageInfo<Legislation> queryPage3(PageParameter pageParameter, Legislation demo);
    //保存
    void SaveForm(Legislation tblHomeLawmanageLegislation);

    List<Tree> getTree(String communityid);
}
