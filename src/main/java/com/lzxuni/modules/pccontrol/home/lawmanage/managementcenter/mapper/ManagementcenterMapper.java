package com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.managementcenter.entity.Managementcenter;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
public interface ManagementcenterMapper extends BaseMapper<Managementcenter> {

    //社区联动
    List<Map<String,Object>> selectListByCondition(Managementcenter tblHomeLawmanageManagementcenter);
    List<Tree> getTree();
    List<Managementcenter> selectListAll(Managementcenter demo);

    Managementcenter queryBymcId(Managementcenter demo);
}
