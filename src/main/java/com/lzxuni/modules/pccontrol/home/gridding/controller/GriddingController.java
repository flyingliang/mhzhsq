package com.lzxuni.modules.pccontrol.home.gridding.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.gridding.entity.Gridding;
import com.lzxuni.modules.pccontrol.home.gridding.service.GriddingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 基础管理-->我的网格管理 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-08-08
 */
@RestController
@RequestMapping("/tblHomeGridding")
public class GriddingController extends BaseController {


    @Autowired
    private GriddingService tblHomeGriddingService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "基础管理-我的网格管理", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/gridding/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Gridding tblHomeGridding) {
        tblHomeGridding.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblHomeGriddingService.queryPage(pageParameter, tblHomeGridding));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/gridding/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3, module = "基础管理-我的网格管理", operateType = "操作")
    public Object insertDo(Gridding tblHomeGridding, String keyValue, String isshow) throws Exception {
        if (StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)) {//展示
            tblHomeGridding.setId(keyValue);
            tblHomeGridding.setIsshow(isshow);
            tblHomeGriddingService.updateById(tblHomeGridding);
            return R.ok("审核成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)) {//取消展示
            tblHomeGridding.setId(keyValue);
            tblHomeGridding.setIsshow(isshow);
            tblHomeGriddingService.updateById(tblHomeGridding);
            return R.ok("取消审核成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)) {//删除
            tblHomeGridding.setId(keyValue);
            tblHomeGridding.setIsdel("0");
            tblHomeGriddingService.updateById(tblHomeGridding);
            return R.ok("删除成功");
        } else if (StringUtils.isNotEmpty(keyValue)) {
            tblHomeGridding.setId(keyValue);
            tblHomeGriddingService.updateById(tblHomeGridding);
            return R.ok("修改成功");
        } else {
            String ywId = UuidUtil.get32UUID();
            tblHomeGridding.setId(ywId);
            tblHomeGridding.setIsdel("1");
            tblHomeGridding.setDate(new Date());
            tblHomeGriddingService.save(tblHomeGridding);
            return R.ok("保存成功");
        }
    }

    @RequestMapping("/GetTree")
    public Object GetTree(String communityid) {
        List<Tree> villageList = tblHomeGriddingService.getTree(communityid);
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/GetTreev")
    public Object GetTreev(String villageid) {
        List<Tree> villageList = tblHomeGriddingService.getTreev(villageid);
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/GetTreevillage")
    public Object GetTreevillage() {
        List<Tree> villageList = tblHomeGriddingService.getTreevillage();
        return R.ok().put("data",villageList);
    }
}

