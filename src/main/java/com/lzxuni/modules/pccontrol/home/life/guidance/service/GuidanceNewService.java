package com.lzxuni.modules.pccontrol.home.life.guidance.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.guidance.entity.GuidanceNew;

import java.util.List;

/**
 * <p>
 * 生活服务-政务大厅-办事指南 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-11
 */
public interface GuidanceNewService extends IService<GuidanceNew> {

    //查询
    PageInfo<GuidanceNew> queryPage(PageParameter pageParameter, GuidanceNew demo);
     //查询
    List<GuidanceNew> findGuideList();
    //保存
    void SaveForm(GuidanceNew demo);
}
