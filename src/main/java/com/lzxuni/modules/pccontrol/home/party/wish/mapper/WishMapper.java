package com.lzxuni.modules.pccontrol.home.party.wish.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.party.wish.entity.Wish;

import java.util.List;
import java.util.Map;

/**
 * 主页_党建_心愿 Mapper 接口
 * Created by 潘云明
 * 2019/7/17
 */
public interface WishMapper extends BaseMapper<Wish> {
    List<Map<String,Object>> selectListByCondition(Wish wish);

}
