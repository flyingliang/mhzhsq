package com.lzxuni.modules.pccontrol.home.lawmanage.legislation.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.entity.Legislation;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.mapper.LegislationMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.service.LegislationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@Service
public class LegislationServiceImpl extends ServiceImpl<LegislationMapper, Legislation> implements LegislationService {

    @Autowired
    private LegislationMapper tblHomeLawmanageLegislationMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Legislation tblHomeLawmanageLegislation) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeLawmanageLegislationMapper.selectListByCondition(tblHomeLawmanageLegislation);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Legislation tblHomeLawmanageLegislation) {
        String uuid = UuidUtil.get32UUID();
        tblHomeLawmanageLegislation.setId(uuid);
        this.save(tblHomeLawmanageLegislation);
    }

    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = tblHomeLawmanageLegislationMapper.getTree(communityid);
        return list;
    }
    @Override
    public PageInfo<Legislation> queryPage3(PageParameter pageParameter, Legislation demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Legislation> homeLifeAffairs = tblHomeLawmanageLegislationMapper.selectListAll(demo);
        PageInfo<Legislation> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }
}
