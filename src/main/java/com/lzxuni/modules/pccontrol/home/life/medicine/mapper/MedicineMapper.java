package com.lzxuni.modules.pccontrol.home.life.medicine.mapper;

import com.lzxuni.modules.pccontrol.home.life.medicine.entity.Medicine;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
public interface MedicineMapper extends BaseMapper<Medicine> {
    List<Medicine> selectListByopenid(Medicine demo);
    List<Medicine> selectListAll(Medicine demo);
    List<Map<String,Object>> selectListByCondition(Medicine tblHomeLawmanageLegislation);
}
