package com.lzxuni.modules.pccontrol.home.party.study.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.lzxuni.modules.pccontrol.home.party.study.entity.PartyStudy;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 党建管理-->在线学习管理 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-08-07
 */
public interface PartyStudyMapper extends BaseMapper<PartyStudy> {
    List<Map<String,Object>> selectListByCondition(PartyStudy tblHomePartyStudy);
}
