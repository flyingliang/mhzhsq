package com.lzxuni.modules.pccontrol.home.life.companytype.mapper;

import com.lzxuni.modules.pccontrol.home.life.companytype.entity.Companytype;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页--生活服务--单位类别 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
public interface CompanytypeMapper extends BaseMapper<Companytype> {

}
