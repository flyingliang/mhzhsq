package com.lzxuni.modules.pccontrol.home.culture.member.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 文化管理-文艺团体表
 * </p>
 *
 * @author fcd
 * @since 2019-07-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_culture_member")
public class CultureMember implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * uuid作为主键
     */
    @TableId
    private String id;

    /**
     * 团体
     */
    private String groupid;

    /**
     * 成员姓名
     */
    private String membername;

    /**
     * 成员年龄
     */
    private String memberage;

    /**
     * 成员性别
     */
    private String membersex;

    /**
     * 成员电话
     */
    private String memberphone;

    /**
     * 小区
     */
    private String villageid;

    /**
     * 单位
     */
    private String unit;

    /**
     * 加入理由
     */
    private String reason;

    /**
     * 创建时间
     */
    private Date date;

    /**
     * 1表示展示，0隐藏
     */
    private String isshow;

    /**
     * 1未删除，0删除
     */
    private String isdel;
    /** 加入人微信id */
    private String openid;

    /** 加入人微信名 */
    private String nickname;


}
