package com.lzxuni.modules.pccontrol.home.party.style.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.party.style.entity.Style;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 主页_党建_作风建设 Mapper 接口
 * Created by 潘云明
 * 2019/7/16
 */
public interface StyleMapper extends BaseMapper<Style> {
    List<Map<String,Object>> selectListByCondition(Style style);

    List<Map<String,Object>> selectListByConditionAndImg(@Param("style")Style style,@Param("ywType")String ywType);

}
