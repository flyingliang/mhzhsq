package com.lzxuni.modules.pccontrol.home.lawmanage.amend.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.amend.entity.Amend;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区矫正条例 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
public interface AmendService extends IService<Amend>{
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Amend tblHomeLawmanageLegislation);
    //查询
    PageInfo<Amend> queryPage3(PageParameter pageParameter, Amend demo);
    //保存
    void SaveForm(Amend tblHomeLawmanageLegislation);

    List<Tree> getTree(String communityid);
}
