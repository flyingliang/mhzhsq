package com.lzxuni.modules.pccontrol.home.party.wish.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lzxuni.modules.common.entity.FileEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 主页_党建_心愿
 * Created by 潘云明
 * 2019/7/17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_party_wish")
public class Wish implements Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1L;
    @TableId
    /** 主键，uuid */
    private String id;

    /** 心愿名称 */
    private String wishname;

    /** 求愿人 */
    private String people;

    /** 详细信息 */
    private String content;

    /** 小区名 */
    private String villageid;

    /** 联系电话 */
    private String phone;

    /** 实现人 */
    private String realizename;

    /** 实现人联系电话 */
    private String realizephone;

    /** 实现人寄语 */
    private String realizecontent;

    /** 1表示实现，0未实现 */
    private String isrealize;

    /** openid */
    private String openid;

    /** 微信名 */
    private String nickname;
 /** 实现人openid */
    private String sxopenid;

    /** 实现微信名 */
    private String sxnickname;

    /** 1表示展示，0隐藏 */
    private String isshow;

    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除 */
    private String isdel;



    @TableField(exist = false)
    private String communityname;
    @TableField(exist = false)
    private List<FileEntity> fileEntities;

    @TableField(exist = false)
    private String urlPath;//web访问路径

    @TableField(exist = false)
    private String urlsPath;//缩略图访问路径


}
