package com.lzxuni.modules.pccontrol.home.volunteer.join.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import com.lzxuni.modules.pccontrol.home.volunteer.join.entity.VolunteerJoin;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 志愿服务--在线加入表 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-07-24
 */
public interface VolunteerJoinMapper extends BaseMapper<VolunteerJoin> {
    List<Map<String,Object>> selectListByCondition(VolunteerJoin tblHomeVolunteerJoin);
    List<Tree> getTree();
    List<Map<String,Object>> selectList();

    List<Map<String, Object>> selectListByConditionAndImg(@Param("volunteerJoin") VolunteerJoin volunteerJoin, @Param("ywType") String ywType);
    String selectJoinId(@Param("openid") String openid);
}
