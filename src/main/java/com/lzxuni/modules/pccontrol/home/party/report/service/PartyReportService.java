package com.lzxuni.modules.pccontrol.home.party.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.party.report.entity.PartyReport;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 党建管理-->共驻共建管理-->社区报到管理 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
public interface PartyReportService extends IService<PartyReport> {
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, PartyReport tblHomePartyReport);
    List<Tree> getPost();

    List<Tree> getTree(String communityid);
    List<Tree> getCompany();

}
