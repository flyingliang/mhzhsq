package com.lzxuni.modules.pccontrol.home.party.study.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.party.study.entity.PartyStudy;

import java.util.Map;

/**
 * <p>
 * 党建管理-->在线学习管理 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-08-07
 */
public interface PartyStudyService extends IService<PartyStudy> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, PartyStudy tblHomePartyStudy);

    PageInfo<PartyStudy> queryPageAndImg(PageParameter pageParameter, PartyStudy partyStudy, String ywType);

    PageInfo<PartyStudy> queryPageSzxt(PageParameter pageParameter, PartyStudy partyStudy, String ywType);

    PageInfo<PartyStudy> queryPageTtxw(PageParameter pageParameter, PartyStudy partyStudy, String ywType);
}
