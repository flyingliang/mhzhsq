package com.lzxuni.modules.pccontrol.home.lawmanage.policeroom.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.policeroom.entity.Policeroom;
import com.lzxuni.modules.pccontrol.home.lawmanage.policeroom.mapper.PoliceroomMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.policeroom.service.PoliceroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-31
 */
@Service
public class PoliceroomServiceImpl extends ServiceImpl<PoliceroomMapper, Policeroom> implements PoliceroomService {

    @Autowired
    private PoliceroomMapper tblHomeLawmanagePoliceroomMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Policeroom tblHomeLawmanagePoliceroom) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeLawmanagePoliceroomMapper.selectListByCondition(tblHomeLawmanagePoliceroom);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Policeroom tblHomeLawmanagePoliceroom) {
        String uuid = UuidUtil.get32UUID();
        tblHomeLawmanagePoliceroom.setId(uuid);
        this.save(tblHomeLawmanagePoliceroom);
    }
    @Override
    public List<Tree> getTree() {
        List<Tree> list = tblHomeLawmanagePoliceroomMapper.getTree();
        return list;
    }

    @Override
    public List<Policeroom> queryPage3(Policeroom demo) {

        List<Policeroom> homeLifeAffairs = tblHomeLawmanagePoliceroomMapper.selectListAll(demo);

        return homeLifeAffairs;
    }

    public  Policeroom queryBymcId(Policeroom demmo){
        return tblHomeLawmanagePoliceroomMapper.queryBymcId(demmo);
    }


}
