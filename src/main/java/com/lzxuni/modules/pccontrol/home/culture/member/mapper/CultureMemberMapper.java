package com.lzxuni.modules.pccontrol.home.culture.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.member.entity.CultureMember;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理-文艺团体表 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-07-18
 */
public interface CultureMemberMapper extends BaseMapper<CultureMember> {
    List<Map<String,Object>> selectListByCondition(CultureMember tblHomeCultureMember);
    List<Tree> getTree(@Param("communityid") String communityid);
    List<Tree> getGroup();
}
