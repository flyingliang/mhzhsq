package com.lzxuni.modules.pccontrol.home.aged.lookafter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.aged.lookafter.entity.AgedLookafter;

import java.sql.SQLException;

/**
 * <p>
 * 养老管理--日间照料表 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-07-22
 */
public interface AgedLookafterService extends IService<AgedLookafter> {
    PageInfo<AgedLookafter> queryPage(PageParameter pageParameter, AgedLookafter tblHomeAgedLookafter) throws SQLException;

    PageInfo<AgedLookafter> queryPageAndImg(PageParameter pageParameter, AgedLookafter agedLookafter, String ywType) throws Exception;
}
