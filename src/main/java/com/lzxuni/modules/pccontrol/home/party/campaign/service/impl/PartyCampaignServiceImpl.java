package com.lzxuni.modules.pccontrol.home.party.campaign.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.party.campaign.entity.PartyCampaign;
import com.lzxuni.modules.pccontrol.home.party.campaign.mapper.PartyCampaignMapper;
import com.lzxuni.modules.pccontrol.home.party.campaign.service.PartyCampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 党建管理-->共驻共建管理-->参加活动管理 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-08-06
 */
@Service
public class PartyCampaignServiceImpl extends ServiceImpl<PartyCampaignMapper, PartyCampaign> implements PartyCampaignService {
   @Autowired
   private PartyCampaignMapper tblHomePartyCampaignMapper;
    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, PartyCampaign tblHomePartyCampaign) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomePartyCampaignMapper.selectListByCondition(tblHomePartyCampaign);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }
}
