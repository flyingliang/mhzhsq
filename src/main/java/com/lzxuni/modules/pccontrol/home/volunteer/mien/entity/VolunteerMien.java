package com.lzxuni.modules.pccontrol.home.volunteer.mien.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 志愿服务--志愿者风采表
 * </p>
 *
 * @author fcd
 * @since 2019-07-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_volunteer_mien")
public class VolunteerMien implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private String id;


    /**
     * 活动标题
     */
    private String title;

    /**
     * 志愿币数量
     */
    private Integer award;

    /**
     * 活动id
     */
    private String ayid;

    /**
     * 志愿者id
     */
    private String joinid;

    /**
     * 微信ID
     */
    private String openid;

    /**
     * 添加时间
     */
    private Date date;

    /**
     * 1未删除，0删除
     */
    private String isdel;

    /**
     * 状态
     */
    private String state;
}
