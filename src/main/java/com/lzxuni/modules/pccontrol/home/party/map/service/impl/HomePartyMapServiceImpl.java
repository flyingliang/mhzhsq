package com.lzxuni.modules.pccontrol.home.party.map.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.party.map.entity.HomePartyMap;
import com.lzxuni.modules.pccontrol.home.party.map.mapper.HomePartyMapMapper;
import com.lzxuni.modules.pccontrol.home.party.map.service.HomePartyMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 主页_党建_地图 服务实现类
 * Created by 潘云明
 * 2019/7/8 14:58
 */
@Service
public class HomePartyMapServiceImpl extends ServiceImpl<HomePartyMapMapper, HomePartyMap> implements HomePartyMapService {
    @Autowired
    private HomePartyMapMapper homePartyMapMapper;
    @Override
    public PageInfo<HomePartyMap> queryPage(PageParameter pageParameter, HomePartyMap homePartyMap) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<HomePartyMap> homePartyMapList = homePartyMapMapper.selectList(new QueryWrapper<HomePartyMap>().eq("isdel", "1"));
        PageInfo<HomePartyMap> pageInfo = new PageInfo<>(homePartyMapList);
        return pageInfo;
    }

    @Override
    public void SaveForm(HomePartyMap homePartyMap) {
        String newsZzyhDomesticId = UuidUtil.get32UUID();
        homePartyMap.setId(newsZzyhDomesticId);
        this.save(homePartyMap);
    }

    @Override
    public List<HomePartyMap> queryList() {
        List<HomePartyMap> homePartyMapList = homePartyMapMapper.selectList(new QueryWrapper<HomePartyMap>().eq("isdel", "1").eq("isshow","1"));
        return homePartyMapList;
    }

}
