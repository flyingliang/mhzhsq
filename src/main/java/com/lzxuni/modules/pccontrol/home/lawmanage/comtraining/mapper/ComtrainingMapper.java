package com.lzxuni.modules.pccontrol.home.lawmanage.comtraining.mapper;

import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.comtraining.entity.Comtraining;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--警务室警训 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
public interface ComtrainingMapper extends BaseMapper<Comtraining> {

    //社区联动
    List<Map<String,Object>> selectListByCondition(Comtraining communityper);
    List<Tree> getTree();
    List<Comtraining> selectListAll(Comtraining demo);
    List<Comtraining> selectListthree(Comtraining demo);

    Comtraining queryBymcId(Comtraining demo);
}
