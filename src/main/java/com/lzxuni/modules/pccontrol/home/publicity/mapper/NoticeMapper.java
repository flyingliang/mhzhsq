package com.lzxuni.modules.pccontrol.home.publicity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.publicity.entity.Notice;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理-->通知公告管理 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-08-08
 */
public interface NoticeMapper extends BaseMapper<Notice> {
    List<Map<String,Object>> selectListByCondition(Notice demo);
    List<Notice> selectListAll(Notice demo);
    List<Notice> selectListfour(Notice demo);
    void pageviewPlusOne(Notice demo);
}
