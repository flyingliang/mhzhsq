package com.lzxuni.modules.pccontrol.home.lawmanage.conflictmediation.service;

import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.conflictmediation.entity.Conflictmediation;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区矛盾调解 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-29
 */
public interface ConflictmediationService extends IService<Conflictmediation> {

    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Conflictmediation tblHomeLawmanageConflictmediation);
    //保存
    void SaveForm(Conflictmediation tblHomeLawmanageConflictmediation);

    List<Tree> getTree(String communityid);
}
