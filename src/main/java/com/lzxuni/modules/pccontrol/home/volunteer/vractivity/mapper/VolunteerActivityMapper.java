package com.lzxuni.modules.pccontrol.home.volunteer.vractivity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import com.lzxuni.modules.pccontrol.home.volunteer.vractivity.entity.VolunteerActivity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 志愿管理-->志愿者活动表 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-07-29
 */
public interface VolunteerActivityMapper extends BaseMapper<VolunteerActivity> {
    List<Map<String,Object>> selectListByCondition(VolunteerActivity tblHomeVolunteerActivity);
    List<Tree> getTree();
    List<Map<String,Object>> selectList(VolunteerActivity tblHomeVolunteerActivity);
    void updateAward(String award,String openid);
    void updateAwards(String award,String joinid);
    List<Map<String, Object>> selectListByConditionAndImg(@Param("volunteerActivity") VolunteerActivity volunteerActivity, @Param("ywType") String ywType);

    List<Map<String,Object>> selectTask(@Param("volunteerActivity")VolunteerActivity volunteerActivity);

    List<Map<String,Object>> selectTasks(@Param("openid")String openid);
}
