package com.lzxuni.modules.pccontrol.home.culture.intelligent.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理-社区达人表 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-07-15
 */
public interface CultureIntelligentService extends IService<CultureIntelligent> {
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, CultureIntelligent tblHomeCultureIntelligent);

    List<Tree> getTree();


    //查询
    PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, CultureIntelligent cultureIntelligent, String ywType);


}
