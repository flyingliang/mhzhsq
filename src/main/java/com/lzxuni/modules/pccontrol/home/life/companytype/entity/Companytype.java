package com.lzxuni.modules.pccontrol.home.life.companytype.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 首页--生活服务--单位类别
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_life_companytype")
public class Companytype extends Model<Companytype> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键UUID
     */
    @TableId
    private String id;

    /**
     * 单位类别名称
     */
    private String companytypename;

    /**
     * 1表示展示，0隐藏
     */
    private String isshow;

    /**
     * 建立时间
     */
    private LocalDateTime date;

    /**
     * 1未删除，0删除
     */
    private String isdel;

}
