package com.lzxuni.modules.pccontrol.home.life.affair.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_life_affair")
public class Affair implements Serializable {
    /** 版本号 */
        private static final long serialVersionUID = 5720807211536928416L;

    /*  */
    @TableId
    /** uuid 主键 */
    private String id;

    /** 提问人姓名 */
    private String tname;

    /** 提问人电话 */
    private String tphone;

    /** 住址 */
    private String taddress;

    /** 提问人问题内容 */
    private String tcontent;

    /** 客服姓名 */
    private String kname;

    /** 客服回复 */
    private String content;

    /** 提问人微信id */
    private String openid;

    /** 提问人微信名 */
    private String nickname;

    /** 1表示展示，0隐藏 */
    private String isshow;

    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除 */
    private String isdel;


    /** 回复图片 */
    @TableField(exist=false)
    private String imgurl;
    /** 业务类型 */
    @TableField(exist=false)
    private String ywtype;
    /** 微信头像 */
    @TableField(exist=false)
    private String headimgurl;

}
