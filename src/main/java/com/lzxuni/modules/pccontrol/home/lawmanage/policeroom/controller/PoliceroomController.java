package com.lzxuni.modules.pccontrol.home.lawmanage.policeroom.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.lawmanage.policeroom.entity.Policeroom;
import com.lzxuni.modules.pccontrol.home.lawmanage.policeroom.service.PoliceroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-07-31
 */
@RestController
@RequestMapping("/tblHomeLawmanagePoliceroom")
public class PoliceroomController extends BaseController {

    @Autowired
    private PoliceroomService tblHomeLawmanagePoliceroomService;
    @Autowired
    private FileEntityService fileEntityService;
    @RequestMapping("/Index")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/policeroom/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblHomeLawmanagePoliceroomService.queryPage(pageParameter, null));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/policeroom/form");
        return mv;
    }

//    //查看跳转页
//    @RequestMapping("/FormLook")
//    public ModelAndView FormLook(String keyValue) throws Exception{
//        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/police/form");
//        FileEntity fileBeanCustom = new FileEntity();
//        fileBeanCustom.setYwId(keyValue);
//        fileBeanCustom.setYwType("kefus");
//        mv.addObject("kefuNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
//        return mv;
//    }

    //新增修改
    @RequestMapping("/SaveForm")
    public Object insertDo(Policeroom tblHomeLawmanagePoliceroom, String keyValue, String sjs) throws Exception{

        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), ywId, "社区警务室照片", "sj", null);
            }
            tblHomeLawmanagePoliceroom.setId(ywId);
            tblHomeLawmanagePoliceroomService.save(tblHomeLawmanagePoliceroom);
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            tblHomeLawmanagePoliceroomService.updateById(tblHomeLawmanagePoliceroom);
            if(!StringUtils.isEmpty(sjs) && !"&amp;nbsp;".equals(sjs)) {
                fileEntityService.insert(sjs.replace("&quot;", "\""), keyValue, "社区警务室照片", "sj", null);
                tblHomeLawmanagePoliceroom.setId(keyValue);
            }
            return R.ok("修改成功");
        }

    }

    //图片ID返回给实体类  Update页传回
    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue){
        Policeroom tblHomeLawmanagePoliceroom = tblHomeLawmanagePoliceroomService.getById(keyValue);
        return R.ok().put("data",tblHomeLawmanagePoliceroom);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/policeroom/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("mjs");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
        return mv;
    }

    @RequestMapping("/GetTree")
    public Object GetTree() {
        List<Tree> villageList = tblHomeLawmanagePoliceroomService.getTree();
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        tblHomeLawmanagePoliceroomService.removeById(keyValue);
        return R.ok("删除成功");
    }
}

