package com.lzxuni.modules.pccontrol.home.lawmanage.onlineconsultation.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.lawmanage.onlineconsultation.entity.Onlineconsultation;
import com.lzxuni.modules.pccontrol.home.lawmanage.onlineconsultation.service.OnlineconsultationService;
import com.lzxuni.modules.pccontrol.home.life.affair.controller.AffairController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * <p>
 * 首页--综治管理--在线咨询管理 前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@RestController
@RequestMapping("/pccontrol/home/lawmanage/onlineconsultation")
public class OnlineconsultationController extends BaseController {

    @Autowired
    private OnlineconsultationService tblHomeLawmanageOnlineconsultationService;
    @Autowired
    private FileEntityService fileEntityService;
    @RequestMapping("/Index")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/onlineconsultation/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, AffairController homeLifeAffairController) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblHomeLawmanageOnlineconsultationService.queryPage(pageParameter, null));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/onlineconsultation/Insert");
        return mv;
    }

    //查看跳转页
    @RequestMapping("/FormLook")
    public ModelAndView FormLook(String keyValue) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/onlineconsultation/form");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(keyValue);
        fileBeanCustom.setYwType("kefus");
        mv.addObject("kefuNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
        mv.addObject("id",keyValue);
        return mv;
    }

    //新增修改
    @RequestMapping("/SaveForm")
    public Object insertDo(Onlineconsultation tblHomeLawmanageOnlineconsultation, String isshow, String keyValue, String kefus) throws Exception{

        if(StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)){
            tblHomeLawmanageOnlineconsultation.setId(keyValue);
            tblHomeLawmanageOnlineconsultation.setIsshow(isshow);
            tblHomeLawmanageOnlineconsultationService.updateById(tblHomeLawmanageOnlineconsultation);
            return R.ok("展示成功");
        }else if(StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)){
            tblHomeLawmanageOnlineconsultation.setId(keyValue);
            tblHomeLawmanageOnlineconsultation.setIsshow(isshow);
            tblHomeLawmanageOnlineconsultationService.updateById(tblHomeLawmanageOnlineconsultation);
            return R.ok("取消展示成功");
        }else if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            tblHomeLawmanageOnlineconsultation.setId(ywId);
            tblHomeLawmanageOnlineconsultationService.save(tblHomeLawmanageOnlineconsultation);
            if(!StringUtils.isEmpty(kefus) && !"&amp;nbsp;".equals(kefus)) {
                fileEntityService.insert(kefus.replace("&quot;", "\""), ywId, "客服照片", "kefu", null);
            }
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            tblHomeLawmanageOnlineconsultationService.updateById(tblHomeLawmanageOnlineconsultation);
            if(!StringUtils.isEmpty(kefus) && !"&amp;nbsp;".equals(kefus)) {
                fileEntityService.insert(kefus.replace("&quot;", "\""), keyValue, "客服照片", "kefu", null);
                tblHomeLawmanageOnlineconsultation.setId(keyValue);
            }
            return R.ok("修改成功");
        }

    }

    //图片ID返回给实体类  Update页传回
    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue){
        Onlineconsultation tblHomeLawmanageOnlineconsultation = tblHomeLawmanageOnlineconsultationService.getById(keyValue);
        return R.ok().put("data",tblHomeLawmanageOnlineconsultation);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/onlineconsultation/Update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("kefus");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
        return mv;
    }


    //真删假删
    @RequestMapping("/DeleteForm")
    public Object delete(Onlineconsultation tblHomeLawmanageOnlineconsultation, String keyValue, String isshow, String sjlbs) {
        if(StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)){
            tblHomeLawmanageOnlineconsultation.setId(keyValue);
            tblHomeLawmanageOnlineconsultation.setIsdel("0");
            tblHomeLawmanageOnlineconsultationService.updateById(tblHomeLawmanageOnlineconsultation);
            fileEntityService.deleteByYwId(keyValue);
//            return R.ok("删除成功");
        }
        return R.ok("删除成功");
    }

}

