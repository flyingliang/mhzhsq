package com.lzxuni.modules.pccontrol.home.party.ceactivity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.party.ceactivity.entity.PartyCeactivity;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 党建管理-->共驻共建管理-->创建活动管理 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
public interface PartyCeactivityService extends IService<PartyCeactivity> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, PartyCeactivity tblHomePartyCeactivity);

    PageInfo<PartyCeactivity> queryPageAndImg(PageParameter pageParameter, PartyCeactivity partyCeactivity, String ywType);

    List<PartyCeactivity> queryListAll(String id);
}
