package com.lzxuni.modules.pccontrol.home.lawmanage.regulations.mapper;

import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.comtraining.entity.Comtraining;
import com.lzxuni.modules.pccontrol.home.lawmanage.regulations.entity.Regulations;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--法律法规管理 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-08-22
 */
public interface RegulationsMapper extends BaseMapper<Regulations>  {
    //社区联动
    List<Map<String,Object>> selectListByCondition(Regulations communityper);
    List<Tree> getTree();
    List<Regulations> selectListAll(Regulations demo);
    List<Regulations> selectListthree(Regulations demo);

    Regulations queryBymcId(Regulations demo);
}
