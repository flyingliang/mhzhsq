package com.lzxuni.modules.pccontrol.home.culture.production.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.production.entity.CultureProduction;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理-文化作品表 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-07-16
 */
public interface CultureProductionMapper extends BaseMapper<CultureProduction>  {
    List<Map<String,Object>> selectListByCondition(CultureProduction tblHomeCultureProduction);
    List<Tree> getTree(@Param("communityid") String communityid);

    List<Map<String, Object>> selectListByConditionAndImg(@Param("cultureProduction") CultureProduction cultureProduction, @Param("ywType") String ywType);
}
