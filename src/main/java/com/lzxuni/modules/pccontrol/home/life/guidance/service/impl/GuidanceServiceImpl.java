package com.lzxuni.modules.pccontrol.home.life.guidance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.guidance.entity.Guidance;
import com.lzxuni.modules.pccontrol.home.life.guidance.mapper.GuidanceMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzxuni.modules.pccontrol.home.life.guidance.service.GuidanceService;
import com.lzxuni.modules.system.mapper.BaseAreaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 生活服务-政务大厅-办事指南 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-11
 */
@Service
public class GuidanceServiceImpl extends ServiceImpl<GuidanceMapper, Guidance> implements GuidanceService {

    @Autowired
    private GuidanceMapper tblHomeLifeGuidanceMapper;
    @Autowired
    private BaseAreaMapper areaMapper;

    @Override
    public PageInfo<Guidance> queryPage(PageParameter pageParameter, Guidance tblHomeLifeGuidance) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Guidance> tblHomeLifeGuidanceList = tblHomeLifeGuidanceMapper.selectList(new QueryWrapper<Guidance>().eq("isDel", "1"));
        PageInfo<Guidance> pageInfo = new PageInfo<>(tblHomeLifeGuidanceList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Guidance tblHomeLifeGuidance) {
        String id = UuidUtil.get32UUID();
        tblHomeLifeGuidance.setId(id);
        this.save(tblHomeLifeGuidance);
    }


    @Override
    public List<Guidance> findGuideList() {

        List<Guidance> tblHomeLifeGuidanceList = tblHomeLifeGuidanceMapper.selectList(new QueryWrapper<Guidance>().eq("isDel", "1").eq("isshow", "1"));

        return tblHomeLifeGuidanceList;
    }




}