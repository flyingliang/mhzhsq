package com.lzxuni.modules.pccontrol.home.party.elegance.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.party.elegance.entity.Elegance;

/**
 * 主页_党建_群团风采 Mapper 接口
 * Created by 潘云明
 * 2019/7/18
 */
public interface EleganceMapper extends BaseMapper<Elegance> {

}
