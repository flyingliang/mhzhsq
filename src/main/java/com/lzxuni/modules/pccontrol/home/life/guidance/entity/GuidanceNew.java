package com.lzxuni.modules.pccontrol.home.life.guidance.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 生活服务-政务大厅-办事指南
 * </p>
 *
 * @author gyl
 * @since 2019-10-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_life_guidance_new")
public class GuidanceNew implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键uuid
     */
    @TableId
    private String id;

    /**
     * 办事指南
     */
    private String guide;
    /**
     * 排序
     */
    private String sort;

    /**
     * 内容
     */
    private String content;

    /** 1表示展示，0隐藏 */
    private String isshow;

    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除 */
    private String isdel;

}
