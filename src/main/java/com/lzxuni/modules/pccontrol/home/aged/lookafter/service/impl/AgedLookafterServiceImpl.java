package com.lzxuni.modules.pccontrol.home.aged.lookafter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.mapper.FileEntityMapper;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.aged.lookafter.entity.AgedLookafter;
import com.lzxuni.modules.pccontrol.home.aged.lookafter.mapper.AgedLookafterMapper;
import com.lzxuni.modules.pccontrol.home.aged.lookafter.service.AgedLookafterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>
 * 养老管理--日间照料表 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-07-22
 */
@Service
public class AgedLookafterServiceImpl extends ServiceImpl<AgedLookafterMapper, AgedLookafter> implements AgedLookafterService {

//    @Autowired
//    private AgedLookafterService agedLookafterService;
    @Autowired
    private FileEntityService fileEntityService;
    @Autowired
    private FileEntityMapper fileEntityMapper;


    @Override
    public PageInfo<AgedLookafter> queryPage(PageParameter pageParameter, AgedLookafter tblHomeAgedLookafter) throws SQLException {
        PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                pageParameter.getSidx() + " " + pageParameter.getSord());
        List<AgedLookafter> list;

        if (StringUtils.isNotEmpty(tblHomeAgedLookafter.getTitle())) {
            list = baseMapper.selectList(new QueryWrapper<AgedLookafter>().like("title", tblHomeAgedLookafter.getTitle()));
        }else {
            list = baseMapper.selectList(new QueryWrapper<AgedLookafter>().eq("isdel","1"));
        }
        PageInfo<AgedLookafter> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public PageInfo<AgedLookafter> queryPageAndImg(PageParameter pageParameter, AgedLookafter agedLookafter, String ywType) throws Exception{
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
//        List<AgedLookafter> styleList = agedLookafterMapper.selectListByConditionAndImg(agedLookafter,ywType);
        List<AgedLookafter> styleList = baseMapper.selectList(new QueryWrapper<AgedLookafter>().eq("isshow","1"));
        if(styleList != null && styleList.size() >0){
            for (int i=0;i<styleList.size();i++ ){
                FileEntity fileEntity = new FileEntity();
                String id = styleList.get(i).getId();
                fileEntity.setYwId(id);
                fileEntity.setYwType(ywType);
                List<FileEntity> filelist = fileEntityMapper.selectList(new QueryWrapper<>(fileEntity));
                if(filelist != null && filelist.size() != 0){
                    styleList.get(i).setUrlsPath(filelist.get(0).getUrlsPath());
                    styleList.get(i).setUrlPath(filelist.get(0).getUrlPath());
                }
            }
        }
        PageInfo<AgedLookafter> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }
}
