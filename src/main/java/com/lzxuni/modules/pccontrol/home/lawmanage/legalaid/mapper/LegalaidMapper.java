package com.lzxuni.modules.pccontrol.home.lawmanage.legalaid.mapper;

import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.legalaid.entity.Legalaid;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-07-29
 */
public interface LegalaidMapper extends BaseMapper<Legalaid> {

    //社区联动
    List<Map<String,Object>> selectListByCondition(Legalaid tblHomeLawmanageLegalaid);
    List<Tree> getTree(@Param("communityid") String communityid);
}
