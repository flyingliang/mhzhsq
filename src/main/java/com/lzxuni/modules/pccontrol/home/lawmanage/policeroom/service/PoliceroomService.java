package com.lzxuni.modules.pccontrol.home.lawmanage.policeroom.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.policeroom.entity.Policeroom;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-31
 */
public interface PoliceroomService extends IService<Policeroom> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Policeroom tblHomeLawmanagePoliceroom);
    //保存
    void SaveForm(Policeroom tblHomeLawmanagePoliceroom);

    List<Tree> getTree();

    //查询
    List<Policeroom> queryPage3(Policeroom demo);

    Policeroom queryBymcId(Policeroom demmo);
}
