package com.lzxuni.modules.pccontrol.home.lawmanage.onlineconsultation.service;

import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.lawmanage.onlineconsultation.entity.Onlineconsultation;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzxuni.modules.pccontrol.home.life.medicine.entity.Medicine;

/**
 * <p>
 * 首页--综治管理--在线咨询管理 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
public interface OnlineconsultationService extends IService<Onlineconsultation> {

    //查询
    PageInfo<Onlineconsultation> queryPage(PageParameter pageParameter, Onlineconsultation tblHomeLawmanageOnlineconsultation);
    //查询
    PageInfo<Onlineconsultation> queryPage2(PageParameter pageParameter, Onlineconsultation demo);
    //查询
    PageInfo<Onlineconsultation> queryPage3(PageParameter pageParameter, Onlineconsultation demo);
    //保存
    void SaveForm(Onlineconsultation tblHomeLawmanageOnlineconsultation);
}
