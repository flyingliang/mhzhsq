package com.lzxuni.modules.pccontrol.home.card.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 办证
 * Created by 潘云明
 * 2019/7/16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_card_zxbz")
public class CardZXBZ implements Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1L;

    /** 主键，uuid */
    @TableId
    private String id;

    /** 社区id */
    private String communityid;

    /** 现居住地 */
    private String address;

    /** 办证信息 */
    private String cardinfo;


}
