package com.lzxuni.modules.pccontrol.home.lawmanage.legislation.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.entity.Legislation;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.service.LegislationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@RestController
@RequestMapping("/pccontrol/home/lawmanage/legislation")
public class LegislationController extends BaseController {

    @Autowired
    private LegislationService tblHomeLawmanageLegislationService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/legislation/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Legislation tblHomeLawmanageLegislation, String Keyword) {
        tblHomeLawmanageLegislation.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblHomeLawmanageLegislationService.queryPage(pageParameter, tblHomeLawmanageLegislation));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/GetTree")
    public Object GetTree(String communityid) {
        List<Tree> villageList = tblHomeLawmanageLegislationService.getTree(communityid);
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/legislation/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    public Object SaveForm(Legislation tblHomeLawmanageLegislation, String keyValue, String isshow, String sjs) throws Exception{
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            tblHomeLawmanageLegislation.setId(ywId);
            tblHomeLawmanageLegislationService.save(tblHomeLawmanageLegislation);
            return R.ok("保存成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            tblHomeLawmanageLegislationService.updateById(tblHomeLawmanageLegislation);
            return R.ok("修改成功");
        }

    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue) throws Exception{
        Legislation tblHomeLawmanageLegislation = tblHomeLawmanageLegislationService.getById(keyValue);
        return R.ok().put("data",tblHomeLawmanageLegislation);
    }

    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update(String ywId) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/lawmanage/legislation/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(ywId);
        fileBeanCustom.setYwType("sjs");
        mv.addObject("picNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
//        mv.addObject("id",keyValue);
        return mv;
    }

    @RequestMapping("/DeleteForm")
    public Object delete(String keyValue) {
        tblHomeLawmanageLegislationService.removeById(keyValue);
        return R.ok("删除成功");
    }

}

