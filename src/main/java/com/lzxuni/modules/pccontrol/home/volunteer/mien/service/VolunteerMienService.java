package com.lzxuni.modules.pccontrol.home.volunteer.mien.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.volunteer.mien.entity.VolunteerMien;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 志愿服务--志愿者风采表 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-07-24
 */
public interface VolunteerMienService extends IService<VolunteerMien> {
    PageInfo<VolunteerMien> queryPage(PageParameter pageParameter, VolunteerMien tblHomeVolunteerMien) throws SQLException;
    PageInfo<Map<String,Object>> queryList(PageParameter pageParameter);
    PageInfo<Map<String,Object>> queryListTwo(PageParameter pageParameter);
    PageInfo<Map<String,Object>> selectMienList(PageParameter pageParameter);
}
