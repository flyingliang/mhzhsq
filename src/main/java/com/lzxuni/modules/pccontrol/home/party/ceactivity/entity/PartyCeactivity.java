package com.lzxuni.modules.pccontrol.home.party.ceactivity.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lzxuni.modules.common.entity.FileEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 党建管理-->共驻共建管理-->创建活动管理
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_party_ceactivity")
public class PartyCeactivity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId
    private String id;

    /**
     * 活动名称
     */
    private String activityname;

    /**
     * 报名人数
     */
    private Integer willnum;

    /**
     * 开始时间
     */
    private Date starttime;

    /**
     * 结束时间
     */
    private Date endtime;

    /**
     * 发布单位
     */
    private String releaseunit;

    /**
     * 联系电话
     */
    private String contactnumber;

    /**
     * 社区id
     */
    private String communityid;

    /**
     * 活动内容
     */
    private String content;

    /**
     * 1表示展示，0隐藏
     */
    private String isshow;

    /**
     * 用户id
     */
    private String openid;

    /**
     * 添加时间
     */
    private Date date;

    /**
     * 1未删除，0删除
     */
    private String isdel;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    private String status;

    //社区名称
    @TableField(exist = false)
    private String communityname;

    @TableField(exist = false)
    private List<FileEntity> fileEntities;

    @TableField(exist = false)
    private String urlPath;//web访问路径

    @TableField(exist = false)
    private String urlsPath;//缩略图访问路径

}
