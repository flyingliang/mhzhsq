package com.lzxuni.modules.pccontrol.home.lawmanage.communityper.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.communityper.entity.Communityper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区服务人员表 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-20
 */
public interface CommunityperService extends IService<Communityper> {

    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Communityper communityper);
    //保存
    void SaveForm(Communityper communityper);

    List<Tree> getTree();
    //查询
    List<Communityper> queryPage3( Communityper demo);
    List<Communityper> selectpeopleAll( Communityper demo);
    List<Communityper> selectpeopleAll2( Communityper demo);

    Communityper queryBymcId(Communityper demmo);
}
