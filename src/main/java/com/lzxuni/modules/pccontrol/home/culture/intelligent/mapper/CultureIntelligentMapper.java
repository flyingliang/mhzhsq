package com.lzxuni.modules.pccontrol.home.culture.intelligent.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.culture.intelligent.entity.CultureIntelligent;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理-社区达人表 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-07-15
 */
public interface CultureIntelligentMapper extends BaseMapper<CultureIntelligent>  {
    List<Map<String,Object>> selectListByCondition(CultureIntelligent tblHomeCultureIntelligent);
    List<Tree> getTree();

    List<Map<String, Object>> selectListByConditionAndImg(@Param("cultureIntelligent") CultureIntelligent cultureIntelligent, @Param("ywType") String ywType);

}
