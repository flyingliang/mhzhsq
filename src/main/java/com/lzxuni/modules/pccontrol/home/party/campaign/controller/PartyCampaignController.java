package com.lzxuni.modules.pccontrol.home.party.campaign.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.party.campaign.entity.PartyCampaign;
import com.lzxuni.modules.pccontrol.home.party.campaign.service.PartyCampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;
import java.util.Date;

/**
 * <p>
 * 党建管理-->共驻共建管理-->参加活动管理 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-08-06
 */
@RestController
@RequestMapping("/tblHomePartyCampaign")
public class PartyCampaignController extends BaseController {
    @Autowired
    private PartyCampaignService tblHomePartyCampaignService;
    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "党建管理-共驻共建管理-参加活动管理", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/home/party/campaign/index");
        return mv;
    }
    //文化管理-文化作品的查询
    @RequestMapping("/GetList")
    public Object GetList(String pagination, PartyCampaign tblHomePartyCampaign) throws SQLException {
        tblHomePartyCampaign.setIsdel("1");
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        PageData pageData = getPageData(tblHomePartyCampaignService.queryPage(pageParameter, tblHomePartyCampaign));
        return R.ok().put("data", pageData);
    }

    //添加、修改
    @RequestMapping("/SaveForm")
    public Object insertDo(String keyValue, PartyCampaign tblHomePartyCampaign, String isshow) throws Exception{

        if(StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)){
            tblHomePartyCampaign.setId(keyValue);
            tblHomePartyCampaign.setIsshow(isshow);
            tblHomePartyCampaignService.updateById(tblHomePartyCampaign);
            return R.ok("就加入成功");
        }else if(StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)){
            tblHomePartyCampaign.setId(keyValue);
            tblHomePartyCampaign.setIsshow(isshow);
            tblHomePartyCampaignService.updateById(tblHomePartyCampaign);
            return R.ok("取消加入成功");
        }else
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            tblHomePartyCampaign.setId(ywId);
            tblHomePartyCampaign.setDate(new Date());
            tblHomePartyCampaignService.save(tblHomePartyCampaign);
            return R.ok("保存成功");
        }else{
            tblHomePartyCampaignService.updateById(tblHomePartyCampaign);
            return R.ok("修改成功");
        }
    }
    //图片回显
    @RequestMapping("/Update")
    public ModelAndView update() throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/home/party/campaign/Update");
        return mv;
    }
    //真删假删
    @RequestMapping("/DeleteForm")
    public Object delete(PartyCampaign tblHomePartyCampaign, String keyValue, String isshow) {
        if(StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)){
            tblHomePartyCampaign.setId(keyValue);
            tblHomePartyCampaign.setIsdel("0");
            tblHomePartyCampaignService.updateById(tblHomePartyCampaign);
        }
        return R.ok("删除成功");
    }

    @RequestMapping("GetEntity")
    public Object  GetEntity(String keyValue){
        PartyCampaign tblHomePartyCampaign = tblHomePartyCampaignService.getById(keyValue);
        return R.ok().put("data",tblHomePartyCampaign);
    }
}

