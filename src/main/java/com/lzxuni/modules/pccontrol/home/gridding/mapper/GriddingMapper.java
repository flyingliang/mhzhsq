package com.lzxuni.modules.pccontrol.home.gridding.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.gridding.entity.Gridding;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理-->我的网格管理 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-08-08
 */
public interface GriddingMapper extends BaseMapper<Gridding> {
    List<Map<String,Object>> selectListByCondition(Gridding tblHomeGridding);
    List<Tree> getTree(@Param("communityid") String communityid);
    List<Tree> getTreev(@Param("villageid") String villageid);
    List<Tree> getTreevillage();
}
