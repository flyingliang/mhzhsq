package com.lzxuni.modules.pccontrol.home.party.ceactivity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.party.ceactivity.entity.PartyCeactivity;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 党建管理-->共驻共建管理-->创建活动管理 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
public interface PartyCeactivityMapper extends BaseMapper<PartyCeactivity> {
    List<Map<String,Object>> selectListByCondition(PartyCeactivity tblHomePartyCeactivity);

    List<PartyCeactivity> selectqueryAll(PartyCeactivity partyCeactivity);

    List<PartyCeactivity> queryListAll(String id);
}
