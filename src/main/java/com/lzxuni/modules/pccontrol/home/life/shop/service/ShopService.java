package com.lzxuni.modules.pccontrol.home.life.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.life.shop.entity.Shop;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-19
 */
public interface ShopService extends IService<Shop> {

    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Shop tblHomeLifeShop);
    //保存
    void SaveForm(Shop tblHomeLifeShop);
    Shop selectshopbyid(Shop tblHomeLifeShop);
    PageInfo<Shop> selectshopList(PageParameter pageParameter, Shop demo);


    List<Tree> getTree(String communityid);

    List<Tree> getTree1(String shoptypeid);
}
