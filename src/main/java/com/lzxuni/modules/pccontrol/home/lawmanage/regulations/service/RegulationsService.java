package com.lzxuni.modules.pccontrol.home.lawmanage.regulations.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.regulations.entity.Regulations;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--法律法规管理 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-22
 */
public interface RegulationsService extends IService<Regulations>  {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Regulations communityper);
    //保存
    void SaveForm(Regulations communityper);

    List<Tree> getTree();
    //查询
    List<Regulations> queryPage3( Regulations demo);
    List<Regulations> selectListthree( Regulations demo);

    Regulations queryBymcId(Regulations demmo);

    //查询
    PageInfo<Regulations> queryPage4(PageParameter pageParameter, Regulations demo);
}
