package com.lzxuni.modules.pccontrol.home.party.wish.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.mapper.FileEntityMapper;
import com.lzxuni.modules.pccontrol.home.party.wish.entity.Wish;
import com.lzxuni.modules.pccontrol.home.party.wish.mapper.WishMapper;
import com.lzxuni.modules.pccontrol.home.party.wish.service.WishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 主页_党建_心愿 服务实现类
 * Created by 潘云明
 * 2019/7/17
 */
@Service
public class WishServiceImpl extends ServiceImpl<WishMapper, Wish> implements WishService {
    @Autowired
    private WishMapper styleMapper;
    @Autowired
    private FileEntityMapper fileEntityMapper;

    @Override
    public PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Wish wish) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = styleMapper.selectListByCondition(wish);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }
//分页
    @Override
    public PageInfo<Wish> queryPageAndImg(PageParameter pageParameter, Wish wish, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Wish> wishList = baseMapper.selectList(new QueryWrapper<Wish>().eq("isshow","1").eq("isrealize","0"));
//        for (int i=0;i<wishList.size();i++ ){
//            FileEntity fileEntity = new FileEntity();
//            String id = wishList.get(i).getId();
//            fileEntity.setYwId(id);
//            fileEntity.setYwType(ywType);
//            List<FileEntity> filelist = fileEntityMapper.selectList(new QueryWrapper<>(fileEntity));
//            if(filelist != null && filelist.size() != 0){
//                wishList.get(i).setUrlsPath(filelist.get(0).getUrlsPath());
//                wishList.get(i).setUrlPath(filelist.get(0).getUrlPath());
//            }
//        }
        PageInfo<Wish> pageInfo = new PageInfo<>(wishList);
        return pageInfo;
    }

    @Override
    public PageInfo<Wish> sxqueryPageAndImg(PageParameter pageParameter, Wish wish, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Wish> wishListsx = baseMapper.selectList(new QueryWrapper<Wish>().eq("isshow","1").eq("isrealize","1"));
        if(wishListsx != null && wishListsx.size() >0) {
            for (int i = 0; i < wishListsx.size(); i++) {
                FileEntity fileEntity = new FileEntity();
                String id = wishListsx.get(i).getId();
                fileEntity.setYwId(id);
                fileEntity.setYwType(ywType);
                List<FileEntity> filelist = fileEntityMapper.selectList(new QueryWrapper<>(fileEntity));
                if (filelist != null && filelist.size() != 0) {
                    wishListsx.get(i).setUrlsPath(filelist.get(0).getUrlsPath());
                    wishListsx.get(i).setUrlPath(filelist.get(0).getUrlPath());
                }
            }
        }
        PageInfo<Wish> pageInfo = new PageInfo<>(wishListsx);
        return pageInfo;
    }

    @Override
    public PageInfo<Wish> sxqueryPageYinyWish(PageParameter pageParameter, Wish wish, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        String openid = wish.getOpenid();
        List<Wish> tinywishList = baseMapper.selectList(new QueryWrapper<Wish>().eq("isshow","1").eq("openid",openid));
        if(tinywishList != null && tinywishList.size() >0) {
            for (int i = 0; i < tinywishList.size(); i++) {
                FileEntity fileEntity = new FileEntity();
                String id = tinywishList.get(i).getId();
                fileEntity.setYwId(id);
                fileEntity.setYwType(ywType);
                List<FileEntity> filelist = fileEntityMapper.selectList(new QueryWrapper<>(fileEntity));
                if (filelist != null && filelist.size() != 0) {
                    tinywishList.get(i).setUrlsPath(filelist.get(0).getUrlsPath());
                    tinywishList.get(i).setUrlPath(filelist.get(0).getUrlPath());
                }
            }
        }
        PageInfo<Wish> pageInfo = new PageInfo<>(tinywishList);
        return pageInfo;
    }


}
