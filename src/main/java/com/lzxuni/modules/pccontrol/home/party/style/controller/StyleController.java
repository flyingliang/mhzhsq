package com.lzxuni.modules.pccontrol.home.party.style.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.home.party.style.entity.Style;
import com.lzxuni.modules.pccontrol.home.party.style.service.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 主页_党建_作风建设 前端控制器
 * Created by 潘云明
 * 2019/7/16
 */

@RestController
@RequestMapping("/pccontrol/home/party/style")
public class StyleController extends BaseController {
    @Autowired
    private StyleService styleService;

    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看作风建设", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/party/style/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Style style) {
        style.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(styleService.queryPage(pageParameter, style));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/home/party/style/form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3, module = "作风建设", operateType = "操作")
    public Object insertDo(Style style, String keyValue, String isshow, String stylepics) throws Exception {
        System.out.println("###参数列表：keyValue："+keyValue+",isshow:"+isshow);
        String typeName="党建作风建设图片";
        String ywType="stylepic";
        if (StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)) {//展示
            style.setId(keyValue);
            style.setIsshow(isshow);
            styleService.updateById(style);
            return R.ok("展示成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)) {//取消展示
            style.setId(keyValue);
            style.setIsshow(isshow);
            styleService.updateById(style);
            return R.ok("取消展示成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)) {//删除
            style.setId(keyValue);
            style.setIsdel("0");
            styleService.updateById(style);
            return R.ok("删除成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "4".equals(isshow)) {//返回图片数量
            FileEntity fileBeanCustom = new FileEntity();
            fileBeanCustom.setYwId(keyValue);
            fileBeanCustom.setYwType(ywType);
            return R.ok().put("data",fileEntityService.queryNumByFileEntity(fileBeanCustom));
        } else if (StringUtils.isNotEmpty(keyValue)) {
            fileEntityService.deleteByYwId(keyValue);
            if(!StringUtils.isEmpty(stylepics) && !"&amp;nbsp;".equals(stylepics)) {
                fileEntityService.insert(stylepics.replace("&quot;", "\""), keyValue, typeName, ywType, null);
            }
            style.setId(keyValue);
            styleService.updateById(style);
            return R.ok("修改成功");
        } else {
            String ywId = UuidUtil.get32UUID();
            if (!StringUtils.isEmpty(stylepics) && !"&amp;nbsp;".equals(stylepics)) {
                fileEntityService.insert(stylepics.replace("&quot;", "\""), ywId, typeName, ywType, null);
            }
            style.setId(ywId);
            styleService.save(style);
            return R.ok("保存成功");
        }


    }

}
