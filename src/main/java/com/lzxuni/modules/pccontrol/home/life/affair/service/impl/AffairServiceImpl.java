package com.lzxuni.modules.pccontrol.home.life.affair.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.home.life.affair.entity.Affair;
import com.lzxuni.modules.pccontrol.home.life.affair.mapper.AffairMapper;
import com.lzxuni.modules.pccontrol.home.life.affair.service.AffairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AffairServiceImpl extends ServiceImpl<AffairMapper, Affair> implements AffairService {
    @Autowired
    private AffairMapper homeLifeAffairMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Affair homePartyMap) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> homeLifeAffairs = homeLifeAffairMapper.selectListByCondition(homePartyMap);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }

   @Override
    public PageInfo<Affair> queryPage2(PageParameter pageParameter, Affair demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Affair> homeLifeAffairs = homeLifeAffairMapper.selectListByopenid(demo);
        PageInfo<Affair> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }

     @Override
    public PageInfo<Affair> queryPage3(PageParameter pageParameter, Affair demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Affair> homeLifeAffairs = homeLifeAffairMapper.selectListAll(demo);
        PageInfo<Affair> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }

    @Override
    public void SaveForm(Affair homeLifeAffair) {
        String newsZzyhDomesticId = UuidUtil.get32UUID();
        homeLifeAffair.setId(newsZzyhDomesticId);
        this.save(homeLifeAffair);
    }

}
