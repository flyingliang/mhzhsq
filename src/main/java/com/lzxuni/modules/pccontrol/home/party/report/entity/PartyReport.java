package com.lzxuni.modules.pccontrol.home.party.report.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 党建管理-->共驻共建管理-->社区报到管理
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_home_party_report")
public class PartyReport implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * uuid作为主键
     */
    @TableId
    private String id;

    /**
     * 姓名
     */
    private String reportname;

    /**
     * 电话
     */
    private String reportphone;

    /**
     * 住址
     */
    private String address;

    /**
     * 身份证号
     */
    private String idnumber;

    /**
     * 单位id
     */
    private String unitid;
    /**
     * 生活管理->单位管理id
     */
    private String companyid;
    /**
     * 岗位id
     */
    private String postid;

    /**
     * 加入理由
     */
    private String content;

    /**
     * 创建时间
     */
    private Date date;

    /**
     * 1表示展示，0隐藏
     */
    private String isshow;

    /**
     * 1未删除，0删除
     */
    private String isdel;

    /**
     * 用户id
     */
    private String openid;

    /** 小区名 */
    @TableField(exist = false)
    private String villagename;

    @TableField(exist = false)
    private String urlPath;//web访问路径

    @TableField(exist = false)
    private String urlsPath;//缩略图访问路径

}
