package com.lzxuni.modules.pccontrol.home.lawmanage.aidcase.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.aidcase.entity.Aidcase;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--援助案例 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
public interface AidcaseService extends IService<Aidcase>  {

    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Aidcase communityper);
    //保存
    void SaveForm(Aidcase communityper);

    List<Tree> getTree();
    //查询
    List<Aidcase> querylist6( Aidcase demo);

    Aidcase queryBymcId(Aidcase demmo);
}
