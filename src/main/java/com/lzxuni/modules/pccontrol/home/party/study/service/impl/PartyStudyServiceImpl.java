package com.lzxuni.modules.pccontrol.home.party.study.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.mapper.FileEntityMapper;
import com.lzxuni.modules.pccontrol.basics.community.entity.Community;
import com.lzxuni.modules.pccontrol.basics.community.mapper.CommunityMapper;
import com.lzxuni.modules.pccontrol.home.party.study.entity.PartyStudy;
import com.lzxuni.modules.pccontrol.home.party.study.mapper.PartyStudyMapper;
import com.lzxuni.modules.pccontrol.home.party.study.service.PartyStudyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 党建管理-->在线学习管理 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-08-07
 */
@Service
public class PartyStudyServiceImpl extends ServiceImpl<PartyStudyMapper, PartyStudy> implements PartyStudyService {
    @Autowired
    private PartyStudyMapper tblHomePartyStudyMapper;
    @Autowired
    private FileEntityMapper fileEntityMapper;
    @Autowired
    private CommunityMapper communityMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, PartyStudy tblHomePartyStudy) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> styleList = tblHomePartyStudyMapper.selectListByCondition(tblHomePartyStudy);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }

    @Override
    public PageInfo<PartyStudy> queryPageAndImg(PageParameter pageParameter, PartyStudy partyStudy, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<PartyStudy> partyList = tblHomePartyStudyMapper.selectList(new QueryWrapper<PartyStudy>().eq("isshow", "1").eq("isdel", "1").eq("studytype", "党章党规"));
//        if(partyList != null && partyList.size() !=0){
//            for (int i=0;i<partyList.size();i++){
//                partyList.get(i).setContent(StringUtils.removeHtml(partyList.get(i).getContent()));
//                String communityid = partyList.get(i).getCommunityid();
//                List<Community> commList = communityMapper.selectList(new QueryWrapper<Community>().eq("id", communityid));
//                if(commList != null && commList.size() >0){
//                    partyList.get(i).setCommunityname(commList.get(i).getCommunityname());
//                }
//            }
//        }
        PageInfo<PartyStudy> pageInfo = new PageInfo<>(partyList);
        return pageInfo;
    }

    @Override
    public PageInfo<PartyStudy> queryPageSzxt(PageParameter pageParameter, PartyStudy partyStudy, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<PartyStudy> szctpartyList = tblHomePartyStudyMapper.selectList(new QueryWrapper<PartyStudy>().eq("isshow", "1").eq("isdel", "1").eq("studytype", "时政词条"));
        PageInfo<PartyStudy> pageInfo = new PageInfo<>(szctpartyList);
        return pageInfo;
    }

    @Override
    public PageInfo<PartyStudy> queryPageTtxw(PageParameter pageParameter, PartyStudy partyStudy, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<PartyStudy> ttxwpartyList = tblHomePartyStudyMapper.selectList(new QueryWrapper<PartyStudy>().eq("isshow", "1").eq("isdel", "1").eq("studytype", "头条新闻"));
        if (ttxwpartyList != null && ttxwpartyList.size() > 0) {
            for (int i = 0; i < ttxwpartyList.size(); i++) {
                FileEntity fileEntity = new FileEntity();
                String id = ttxwpartyList.get(i).getId();
                fileEntity.setYwId(id);
                fileEntity.setYwType(ywType);
                List<FileEntity> fileList = fileEntityMapper.selectList(new QueryWrapper<>(fileEntity));
                if (fileList != null && fileList.size() != 0) {
                    ttxwpartyList.get(i).setUrlsPath(fileList.get(0).getUrlsPath());
                    ttxwpartyList.get(i).setUrlPath(fileList.get(0).getUrlPath());
                }
                String communityid = ttxwpartyList.get(i).getCommunityid();
                List<Community> communityList = communityMapper.selectList(new QueryWrapper<Community>().eq("id", communityid));
                if (communityList != null && communityList.size() > 0) {
                    ttxwpartyList.get(i).setCommunityname(communityList.get(0).getCommunityname());
                }
            }
        }
        PageInfo<PartyStudy> pageInfo = new PageInfo<>(ttxwpartyList);
        return pageInfo;
    }
}
