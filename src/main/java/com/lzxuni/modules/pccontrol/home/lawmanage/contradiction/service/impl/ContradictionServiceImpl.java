package com.lzxuni.modules.pccontrol.home.lawmanage.contradiction.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.contradiction.entity.Contradiction;
import com.lzxuni.modules.pccontrol.home.lawmanage.contradiction.mapper.ContradictionMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.contradiction.service.ContradictionService;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.entity.Legislation;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.mapper.LegislationMapper;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.service.LegislationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区矛盾调解条例 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
@Service
public class ContradictionServiceImpl extends ServiceImpl<ContradictionMapper, Contradiction> implements ContradictionService {

    @Autowired
    private ContradictionMapper tblHomeLawmanageLegislationMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Contradiction tblHomeLawmanageLegislation) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeLawmanageLegislationMapper.selectListByCondition(tblHomeLawmanageLegislation);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Contradiction tblHomeLawmanageLegislation) {
        String uuid = UuidUtil.get32UUID();
        tblHomeLawmanageLegislation.setId(uuid);
        this.save(tblHomeLawmanageLegislation);
    }

    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = tblHomeLawmanageLegislationMapper.getTree(communityid);
        return list;
    }
    @Override
    public PageInfo<Contradiction> queryPage3(PageParameter pageParameter, Contradiction demo) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Contradiction> homeLifeAffairs = tblHomeLawmanageLegislationMapper.selectListAll(demo);
        PageInfo<Contradiction> pageInfo = new PageInfo<>(homeLifeAffairs);
        return pageInfo;
    }
}
