package com.lzxuni.modules.pccontrol.home.life.guidance.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.life.guidance.entity.GuidanceNew;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 生活服务-政务大厅-办事指南 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-07-11
 */
@Mapper
public interface GuidanceNewMapper extends BaseMapper<GuidanceNew> {

}
