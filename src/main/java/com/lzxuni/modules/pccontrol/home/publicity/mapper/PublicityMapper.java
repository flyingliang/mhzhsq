package com.lzxuni.modules.pccontrol.home.publicity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.publicity.entity.Publicity;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理-->政务公开管理 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-08-08
 */
public interface PublicityMapper extends BaseMapper<Publicity> {
    List<Map<String,Object>> selectListByCondition(Publicity tblHomePublicity);
    List<Publicity> selectListAll(Publicity tblHomePublicity);
    List<Publicity> selectListfour(Publicity tblHomePublicity);
    void pageviewPlusOne(Publicity tblHomePublicity);
}
