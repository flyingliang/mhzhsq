package com.lzxuni.modules.pccontrol.home.party.campaign.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.home.party.campaign.entity.PartyCampaign;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 党建管理-->共驻共建管理-->参加活动管理 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-08-06
 */
public interface PartyCampaignMapper extends BaseMapper<PartyCampaign> {
    List<Map<String,Object>> selectListByCondition(PartyCampaign tblHomePartyCampaign);
}
