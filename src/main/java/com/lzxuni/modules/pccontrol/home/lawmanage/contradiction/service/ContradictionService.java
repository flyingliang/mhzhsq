package com.lzxuni.modules.pccontrol.home.lawmanage.contradiction.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.contradiction.entity.Contradiction;
import com.lzxuni.modules.pccontrol.home.lawmanage.legislation.entity.Legislation;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治--社区矛盾调解条例 服务类
 * </p>
 *
 * @author Lhl
 * @since 2019-08-21
 */
public interface ContradictionService extends IService<Contradiction> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Contradiction tblHomeLawmanageLegislation);
    //查询
    PageInfo<Contradiction> queryPage3(PageParameter pageParameter, Contradiction demo);
    //保存
    void SaveForm(Contradiction tblHomeLawmanageLegislation);

    List<Tree> getTree(String communityid);
}
