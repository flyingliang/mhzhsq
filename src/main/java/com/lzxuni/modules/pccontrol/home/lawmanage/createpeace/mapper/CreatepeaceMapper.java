package com.lzxuni.modules.pccontrol.home.lawmanage.createpeace.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.lawmanage.createpeace.entity.Createpeace;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--综治管理--平安创建 Mapper 接口
 * </p>
 *
 * @author Lhl
 * @since 2019-07-24
 */
public interface CreatepeaceMapper extends BaseMapper<Createpeace> {

    //社区联动
    List<Map<String,Object>> selectListByCondition(Createpeace tblHomeLawmanageCreatepeace);
    List<Tree> getTree(@Param("communityid") String communityid);

    List<Createpeace> selectListthree(Createpeace demo);
    List<Createpeace> selectListall(Createpeace demo);
}
