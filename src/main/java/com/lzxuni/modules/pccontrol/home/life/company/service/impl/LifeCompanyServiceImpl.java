package com.lzxuni.modules.pccontrol.home.life.company.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.home.life.company.entity.LifeCompany;
import com.lzxuni.modules.pccontrol.home.life.company.mapper.LifeCompanyMapper;
import com.lzxuni.modules.pccontrol.home.life.company.service.LifeCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页--生活服务--单位管理 服务实现类
 * </p>
 *
 * @author Lhl
 * @since 2019-07-22
 */
@Service
public class LifeCompanyServiceImpl extends ServiceImpl<LifeCompanyMapper, LifeCompany> implements LifeCompanyService {

    @Autowired
    private LifeCompanyMapper tblHomeLifeCompanyMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, LifeCompany tblHomeLifeCompany) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblHomeLifeCompanyMapper.selectListByCondition(tblHomeLifeCompany);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

     @Override
    public PageInfo<LifeCompany> queryPage2(PageParameter pageParameter, LifeCompany tblHomeLifeCompany) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<LifeCompany> villageList = tblHomeLifeCompanyMapper.selectdemoList(tblHomeLifeCompany);
        PageInfo<LifeCompany> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(LifeCompany tblHomeLifeCompany) {
        String uuid = UuidUtil.get32UUID();
        tblHomeLifeCompany.setId(uuid);
        this.save(tblHomeLifeCompany);
    }

    @Override
    public LifeCompany selectcompanybyid(LifeCompany tblHomeLifeShop) {
        return tblHomeLifeCompanyMapper.selectcompanybyid(tblHomeLifeShop);
    }

    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = tblHomeLifeCompanyMapper.getTree(communityid);
        return list;
    }

}
