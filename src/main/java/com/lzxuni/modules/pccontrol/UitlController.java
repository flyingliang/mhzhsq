package com.lzxuni.modules.pccontrol;

import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.wechat.config.controller.WeChatController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * 主页_党建_心愿 前端控制器
 * Created by 潘云明
 * 2019/7/17
 */

@RestController
@RequestMapping("/pccontrol/util")
public class UitlController extends WeChatController {


    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/queryImgUrl")
    public Object queryImgUrl(String keyValue, String ywType) throws Exception {
        System.out.println("查询图片列表###参数列表：keyValue：" + keyValue + ",ywType:" + ywType);
        if (StringUtils.isNotEmpty(keyValue)) {//返回图片数量
            FileEntity fileBeanCustom = new FileEntity();
            fileBeanCustom.setYwId(keyValue);
            fileBeanCustom.setYwType(ywType);
            return R.ok().put("data", fileEntityService.queryListByFileEntity(fileBeanCustom));
        }

        return null;
    }

    /**
     * 显示二维码
     * @param response
     * @param keyValue
     * @throws Exception
     */
    @RequestMapping("/qrCode")
    public void qrCode(HttpServletResponse response,String type, String keyValue) throws Exception {
        String url=URL+"/wechat/qrView?type="+type+"&id="+keyValue;
        try {
            BitMatrix qRcodeImg = QRCodeUtil.generateQRCodeStream(url, response);
            MatrixToImageWriter.writeToStream(qRcodeImg, "png", response.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
