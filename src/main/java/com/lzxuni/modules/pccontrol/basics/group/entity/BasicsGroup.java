package com.lzxuni.modules.pccontrol.basics.group.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 文化管理-文艺团体信息表
 * </p>
 *
 * @author fcd
 * @since 2019-07-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_basics_group")
public class BasicsGroup implements Serializable  {

    private static final long serialVersionUID = 1L;

    /**
     * uuid作为主键
     */
    @TableId
    private String id;

    /**
     * 文艺团体名称
     */
    private String groupname;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 团体介绍
     */
    private String introduce;

    /**
     * 1未删除，0删除
     */
    private String isdel;

    /**
     * 建立时间
     */
    private Date date;

}
