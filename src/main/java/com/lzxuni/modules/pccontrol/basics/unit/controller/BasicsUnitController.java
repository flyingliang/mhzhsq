package com.lzxuni.modules.pccontrol.basics.unit.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.constant.LogConstant;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.unit.entity.BasicsUnit;
import com.lzxuni.modules.pccontrol.basics.unit.service.BasicsUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 * 基础管理--单位管理 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-08-01
 */
@RestController
@RequestMapping("/tblBasicsUnit")
public class BasicsUnitController extends BaseController {
    @Autowired
    private BasicsUnitService tblBasicsUnitService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看单位", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/basics/unit/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, BasicsUnit tblBasicsUnit) {
        tblBasicsUnit.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblBasicsUnitService.queryPage(pageParameter, tblBasicsUnit));
        return R.ok().put("data", pageData);
    }


    @RequestMapping("/GetTree")
    public Object GetTree(String pagination, BasicsUnit tblBasicsUnit) {
        List<Tree> companyList = tblBasicsUnitService.getTree();
        return R.ok().put("data",companyList);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/basics/unit/form");
        return mv;
    }


    @SysLog(categoryId = LogConstant.OPERATEID,module = "查看单位",operateType = LogConstant.INSERT)
    @RequestMapping("/SaveForm")
    public Object SaveForm(BasicsUnit tblBasicsUnit, String keyValue, String isshow){
        if(StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)){
            tblBasicsUnit.setId(keyValue);
            tblBasicsUnit.setIsdel("0");
            tblBasicsUnitService.updateById(tblBasicsUnit);
            return R.ok("删除成功");
        }else if(StringUtils.isNotEmpty(keyValue)){
            tblBasicsUnit.setId(keyValue);
            tblBasicsUnitService.updateById(tblBasicsUnit);
            return R.ok("修改成功");
        }else{
            tblBasicsUnitService.SaveForm(tblBasicsUnit);
            return R.ok("保存成功");
        }

    }
}

