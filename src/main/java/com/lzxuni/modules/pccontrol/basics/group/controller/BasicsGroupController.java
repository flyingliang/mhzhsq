package com.lzxuni.modules.pccontrol.basics.group.controller;


import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.basics.group.entity.BasicsGroup;
import com.lzxuni.modules.pccontrol.basics.group.service.BasicsGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * <p>
 * 文化管理-文艺团体信息表 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-07-18
 */
@RestController
@RequestMapping("/tblBasicsGroup")
public class BasicsGroupController extends BaseController {
    @Autowired
    private BasicsGroupService tblBasicsGroupService;
    @Autowired
    private FileEntityService fileEntityService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看团体管理", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/basics/group/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, BasicsGroup tblBasicsGroup) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblBasicsGroupService.queryPage(pageParameter, null));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Insert")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/basics/group/insert");
        return mv;
    }
    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3,module = "修改图片",operateType = "操作")
    public Object insertDo(BasicsGroup tblBasicsGroup, String keyValue, String featurepics) throws Exception{
        System.out.println("keyValue###"+keyValue);
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            if(!StringUtils.isEmpty(featurepics) && !"&amp;nbsp;".equals(featurepics)) {
                fileEntityService.insert(featurepics.replace("&quot;", "\""), ywId, "文艺团体照片", "group", null);
            }
            tblBasicsGroup.setId(ywId);
            tblBasicsGroup.setDate(new Date());
            tblBasicsGroupService.SaveForm(tblBasicsGroup);
            return R.ok("保存成功");
        }else if(StringUtils.isNotEmpty(keyValue) && (keyValue.substring(0, 1).equals("-"))){
            tblBasicsGroup.setId(keyValue.substring(1, keyValue.length()));
            tblBasicsGroup.setIsdel("0");
            tblBasicsGroupService.updateById(tblBasicsGroup);
            return R.ok("删除成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            if(!StringUtils.isEmpty(featurepics) && !"&amp;nbsp;".equals(featurepics)) {
                fileEntityService.insert(featurepics.replace("&quot;", "\""), keyValue, "文艺团体照片", "group", null);
            }
            tblBasicsGroup.setId(keyValue);
            tblBasicsGroupService.updateById(tblBasicsGroup);

            return R.ok("修改成功");
        }

    }

    @RequestMapping("/Update")
    public ModelAndView update(String keyValue) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/basics/group/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(keyValue);
        fileBeanCustom.setYwType("group");
        mv.addObject("leaderpicsNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
        mv.addObject("id",keyValue);
        return mv;
    }
}

