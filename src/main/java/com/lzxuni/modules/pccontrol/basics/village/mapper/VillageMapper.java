package com.lzxuni.modules.pccontrol.basics.village.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.village.entity.Village;

import java.util.List;
import java.util.Map;

/**
 * 基础_小区 Mapper 接口
 * Created by 潘云明
 * 2019/7/11
 */
public interface VillageMapper extends BaseMapper<Village> {
    List<Map<String,Object>> selectListByCondition(Village village);
    List<Tree> getTree();
    List<Village> selectVillage();
}
