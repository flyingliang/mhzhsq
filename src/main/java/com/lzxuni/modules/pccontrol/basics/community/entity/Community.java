package com.lzxuni.modules.pccontrol.basics.community.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 基础 _社区
 * Created by 潘云明
 * 2019/7/10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_basics_community")
public class Community implements Serializable {


    /** 版本号 */
    private static final long serialVersionUID = 2122970119242718886L;

    /** 主键id */
    private String id;

    /** 社区名称 */
    private String communityname;

    /** 社区介绍 */
    private String text;

    /** 特色项目 */
    private String feature;

    /** 电话 */
    private String phone;

    /** 领导姓名 */
    private String leadername;

    /** 领导职务 */
    private String duty;

    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除 */
    private String isdel;

}
