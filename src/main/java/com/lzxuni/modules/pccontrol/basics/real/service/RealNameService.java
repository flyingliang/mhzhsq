package com.lzxuni.modules.pccontrol.basics.real.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.real.entity.RealName;

import java.util.Map;

/**
 * 基础管理_实名认证 服务类
 * Created by 潘云明
 * 2019/7/5 16:08
 */
public interface RealNameService extends IService<RealName> {
    //查询
	PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, RealName entity);



//	 //查询
//	List<Province> queryList();
//	//保存
//    void SaveForm(Province entity);

}
