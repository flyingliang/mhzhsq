package com.lzxuni.modules.pccontrol.basics.community.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.community.entity.Community;
import com.lzxuni.modules.pccontrol.basics.community.mapper.CommunityMapper;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 基础 _社区 服务实现类
 * Created by 潘云明
 * 2019/7/8 14:58
 */
@Service
public class CommunityServiceImpl extends ServiceImpl<CommunityMapper, Community> implements CommunityService {
    @Autowired
    private CommunityMapper communityMapper;
    @Override
    public PageInfo<Community> queryPage(PageParameter pageParameter, Community community) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Community> communityList = communityMapper.selectList(new QueryWrapper<Community>().eq("isDel", "1"));
        PageInfo<Community> pageInfo = new PageInfo<>(communityList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Community community) {
        this.save(community);
    }

    @Override
    public List<Community> queryLList() {
        List<Community> communityList = communityMapper.selectList(new QueryWrapper<Community>().eq("isDel", "1"));
        return communityList;
    }


    @Override
    public List<Map<String, Object>> queryOne(String id) {
        List<Map<String, Object>> communityList = communityMapper.selectListByCondition(id);
        return communityList;
    }
}
