package com.lzxuni.modules.pccontrol.basics.unit.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.unit.entity.BasicsUnit;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理--单位管理 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-08-01
 */
public interface BasicsUnitMapper extends BaseMapper<BasicsUnit> {
    List<Map<String,Object>> selectListByCondition(BasicsUnit tblBasicsUnit);
    List<Tree> getTree();
}
