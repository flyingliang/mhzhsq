package com.lzxuni.modules.pccontrol.basics.address.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.address.entity.City;
import com.lzxuni.modules.pccontrol.basics.address.mapper.CityMapper;
import com.lzxuni.modules.pccontrol.basics.address.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 基础_小区 服务实现类
 * Created by 潘云明
 * 2019/7/11
 */
@Service
public class CityServiceImpl extends ServiceImpl<CityMapper, City> implements CityService {
    @Autowired
    private CityMapper cityMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, City city) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = cityMapper.selectListByCondition(city);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(City city) {
        String uuid = UuidUtil.get32UUID();
        city.setId(uuid);
        this.save(city);
    }

    @Override
    public List<Tree> getTree() {
        List<Tree> list = cityMapper.getTree();
//        getTreeDg(list);
        return list;
    }



    @Override
    public List<City> queryList() {
        List<City> cityList = cityMapper.selectList(new QueryWrapper<City>().eq("isDel", "1"));

        return cityList;
    }

}
