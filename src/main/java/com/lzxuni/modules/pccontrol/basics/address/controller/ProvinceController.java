package com.lzxuni.modules.pccontrol.basics.address.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.constant.LogConstant;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.address.entity.Province;
import com.lzxuni.modules.pccontrol.basics.address.service.ProvinceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 基础管理_地址管理_省 前端控制器
 * Created by 潘云明
 * 2019/7/10
 */

@RestController
@RequestMapping("/pccontrol/basics/address/province")
public class ProvinceController extends BaseController {
    @Autowired
    private ProvinceService service;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看地址管理_省", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/basics/address/province_index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Province entity) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(service.queryPage(pageParameter, null));
        return R.ok().put("data", pageData);
    }


    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/basics/address/province_form");
        return mv;
    }


    @SysLog(categoryId = LogConstant.OPERATEID,module = "查看地址管理_省",operateType = LogConstant.INSERT)
    @RequestMapping("/SaveForm")
    public Object SaveForm(Province entity, String keyValue, String isshow){
        if(StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)){
            entity.setId(keyValue);
            entity.setIsdel("0");
            service.updateById(entity);
            return R.ok("删除成功");
        }else if(StringUtils.isNotEmpty(keyValue)){
            entity.setId(keyValue);
            service.updateById(entity);
            return R.ok("修改成功");
        }else{
            service.SaveForm(entity);
            return R.ok("保存成功");
        }

    }
//    @SysLog(categoryId = LogConstant.OPERATEID,module = "新闻媒体-ZZYH-境内",operateType = LogConstant.DELETE)
//    @RequestMapping("/DeleteForm")
//    public Object DeleteForm(String keyValue){
//        newsZzyhDomesticService.removeById(keyValue);
//        return R.ok("删除成功");
//    }


}
