package com.lzxuni.modules.pccontrol.basics.address.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 基础管理_地址管理_区
 * Created by 潘云明
 * 2019/8/19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_basics_address_region")
public class Region implements Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1L;
    @TableId
    /** 主键，uuid */
    private String id;

    /** 市编号 */
    private String cityid;

    /** 区名 */
    private String regionname;


    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除*/
    private String isdel;

}
