package com.lzxuni.modules.pccontrol.basics.address.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.address.entity.Region;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 基础管理_地址管理_区 Mapper 接口
 * Created by 潘云明
 * 2019/7/15
 */
public interface RegionMapper extends BaseMapper<Region> {
    List<Map<String,Object>> selectListByCondition(Region region);
    List<Tree> getTree(@Param("provinceid") String provinceid);
}
