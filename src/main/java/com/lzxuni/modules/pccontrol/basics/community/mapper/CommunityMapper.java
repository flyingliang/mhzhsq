package com.lzxuni.modules.pccontrol.basics.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.basics.community.entity.Community;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 基础 _社区 Mapper 接口
 * Created by 潘云明
 * 2019/7/8 15:01
 */
public interface CommunityMapper extends BaseMapper<Community> {
    List<Map<String,Object>> selectListByCondition(@Param("id") String id);
}
