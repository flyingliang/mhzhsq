package com.lzxuni.modules.pccontrol.basics.village.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.village.entity.Village;

import java.util.List;
import java.util.Map;

/**
 * 基础_小区 服务类
 * Created by 潘云明
 * 2019/7/11
 */
public interface VillageService extends IService<Village> {
    //查询
	PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Village village);
	//保存
    void SaveForm(Village village);

    List<Tree> getTree();

    List<Village> selectVillage();

}
