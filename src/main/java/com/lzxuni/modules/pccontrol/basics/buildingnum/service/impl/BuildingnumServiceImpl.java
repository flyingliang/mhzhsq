package com.lzxuni.modules.pccontrol.basics.buildingnum.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.buildingnum.entity.Buildingnum;
import com.lzxuni.modules.pccontrol.basics.buildingnum.mapper.BuildingnumMapper;
import com.lzxuni.modules.pccontrol.basics.buildingnum.service.BuildingnumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 基基础_小区楼号 服务实现类
 * Created by 潘云明
 * 2019/7/15
 */
@Service
public class BuildingnumServiceImpl extends ServiceImpl<BuildingnumMapper, Buildingnum> implements BuildingnumService {
    @Autowired
    private BuildingnumMapper buildingnumMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Buildingnum buildingnum) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = buildingnumMapper.selectListByCondition(buildingnum);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Buildingnum buildingnum) {
        String uuid = UuidUtil.get32UUID();
        buildingnum.setId(uuid);
        this.save(buildingnum);
    }
    //查小区用的
    @Override
    public List<Tree> getTree(String communityid) {
        List<Tree> list = buildingnumMapper.getTree(communityid);
        return list;
    }

    @Override
    public List<Tree> getBuildingNumTree() {
        List<Tree> list = buildingnumMapper.getBuildingNumTree();
        return list;
    }


}
