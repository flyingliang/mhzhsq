package com.lzxuni.modules.pccontrol.basics.post.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.post.entity.BasicsPost;

/**
 * <p>
 * 基础管理-->岗位管理 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
public interface BasicsPostService extends IService<BasicsPost> {
    //查询
    PageInfo<BasicsPost> queryPage(PageParameter pageParameter, BasicsPost tblBasicsPost);
    //保存
    void SaveForm(BasicsPost tblBasicsPost);
}
