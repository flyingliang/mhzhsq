package com.lzxuni.modules.pccontrol.basics.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.group.entity.BasicsGroup;
import com.lzxuni.modules.pccontrol.home.culture.member.entity.CultureMember;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * <p>
 * 文化管理-文艺团体信息表 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-07-18
 */
public interface BasicsGroupService extends IService<BasicsGroup> {
    //查询
    PageInfo<BasicsGroup> queryPage(PageParameter pageParameter, BasicsGroup tblBasicsGroup);
    //保存
    void SaveForm(BasicsGroup tblBasicsGroup);


    //查询
    PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, BasicsGroup basicsGroup ,@Param("ywType") String ywType);
    //查询
    PageInfo<Map<String,Object>> queryPageAndImgs(PageParameter pageParameter, BasicsGroup basicsGroup ,@Param("ywType") String ywType);


    List<BasicsGroup> selectgroup();
}
