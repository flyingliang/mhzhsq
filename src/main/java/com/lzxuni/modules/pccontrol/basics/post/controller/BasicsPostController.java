package com.lzxuni.modules.pccontrol.basics.post.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.basics.post.entity.BasicsPost;
import com.lzxuni.modules.pccontrol.basics.post.service.BasicsPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * <p>
 * 基础管理-->岗位管理 前端控制器
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
@RestController
@RequestMapping("/tblBasicsPost")
public class BasicsPostController extends BaseController {
    @Autowired
    private BasicsPostService tblBasicsPostService;
    @Autowired
    private FileEntityService fileEntityService;
    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看岗位管理", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/basics/post/index");
        return mv;
    }

    @RequestMapping("GetList")
    public Object GetList(String pagination, BasicsPost tblBasicsPost) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(tblBasicsPostService.queryPage(pageParameter, null));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Insert")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/basics/post/insert");
        return mv;
    }
    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3,module = "修改图片",operateType = "操作")
    public Object insertDo(BasicsPost tblBasicsPost, String keyValue, String postrepics) throws Exception{
        System.out.println("keyValue###"+keyValue);
        if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            if(!StringUtils.isEmpty(postrepics) && !"&amp;nbsp;".equals(postrepics)) {
                fileEntityService.insert(postrepics.replace("&quot;", "\""), ywId, "社区岗位照片", "posterpic", null);
            }
            tblBasicsPost.setId(ywId);
            tblBasicsPost.setDate(new Date());
            tblBasicsPostService.SaveForm(tblBasicsPost);
            return R.ok("保存成功");
        }else if(StringUtils.isNotEmpty(keyValue) && (keyValue.substring(0, 1).equals("-"))){

            tblBasicsPost.setId(keyValue.substring(1, keyValue.length()));
            tblBasicsPost.setIsdel("0");
            tblBasicsPostService.updateById(tblBasicsPost);
            return R.ok("删除成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            if(!StringUtils.isEmpty(postrepics) && !"&amp;nbsp;".equals(postrepics)) {
                fileEntityService.insert(postrepics.replace("&quot;", "\""), keyValue, "社区岗位照片", "posterpic", null);
            }
            tblBasicsPost.setId(keyValue);
            tblBasicsPostService.updateById(tblBasicsPost);
            return R.ok("修改成功");
        }

    }
    @RequestMapping("/Update")
    public ModelAndView update(String keyValue) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/basics/post/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(keyValue);
        fileBeanCustom.setYwType("postrepics");
        mv.addObject("postrepicsNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
        mv.addObject("id",keyValue);
        return mv;
    }
}

