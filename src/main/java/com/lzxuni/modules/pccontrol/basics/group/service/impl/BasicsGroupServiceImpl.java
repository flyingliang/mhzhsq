package com.lzxuni.modules.pccontrol.basics.group.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.group.entity.BasicsGroup;
import com.lzxuni.modules.pccontrol.basics.group.mapper.BasicsGroupMapper;
import com.lzxuni.modules.pccontrol.basics.group.service.BasicsGroupService;
import com.lzxuni.modules.pccontrol.home.culture.member.entity.CultureMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理-文艺团体信息表 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-07-18
 */
@Service
public class BasicsGroupServiceImpl extends ServiceImpl<BasicsGroupMapper, BasicsGroup> implements BasicsGroupService {

    @Autowired
    private BasicsGroupMapper tblBasicsGroupMapper;

    @Override
    public PageInfo<BasicsGroup> queryPage(PageParameter pageParameter, BasicsGroup tblBasicsGroup) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<BasicsGroup> communityList = tblBasicsGroupMapper.selectList(new QueryWrapper<BasicsGroup>().eq("isdel", "1"));
        PageInfo<BasicsGroup> pageInfo = new PageInfo<>(communityList);
        return pageInfo;
    }

    @Override
    public void SaveForm(BasicsGroup tblBasicsGroup) {
        tblBasicsGroup.setIsdel("1");
        this.save(tblBasicsGroup);
    }

    @Override
    public PageInfo<Map<String,Object>> queryPageAndImg(PageParameter pageParameter, BasicsGroup basicsGroup, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblBasicsGroupMapper.selectListByConditionAndImg(basicsGroup,ywType);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }

    @Override
    public PageInfo<Map<String,Object>> queryPageAndImgs(PageParameter pageParameter, BasicsGroup basicsGroup, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = tblBasicsGroupMapper.selectListByConditionAndImgs(basicsGroup,ywType);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }

    public List<BasicsGroup> selectgroup(){
        return tblBasicsGroupMapper.selectgroup();
    };
}
