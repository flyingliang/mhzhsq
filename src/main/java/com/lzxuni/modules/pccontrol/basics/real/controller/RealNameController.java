package com.lzxuni.modules.pccontrol.basics.real.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.constant.LogConstant;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.real.entity.RealName;
import com.lzxuni.modules.pccontrol.basics.real.service.RealNameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 基础管理_实名认证 前端控制器
 * Created by 潘云明
 * 2019/7/10
 */

@RestController
@RequestMapping("/pccontrol/basics/realname")
public class RealNameController extends BaseController {
    @Autowired
    private RealNameService service;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看实名认证", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/basics/real/realname_index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination) {
        RealName entity=new RealName();
        entity.setIspass("0");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(service.queryPage(pageParameter, entity));
        return R.ok().put("data", pageData);
    }


    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/basics/real/realname_form");
        return mv;
    }


    @SysLog(categoryId = LogConstant.OPERATEID, module = "查看地址管理_省", operateType = LogConstant.INSERT)
    @RequestMapping("/SaveForm")
    public Object SaveForm(String keyValue, String ispass) {
        if (StringUtils.isNotEmpty(keyValue)) {
            RealName realName = new RealName();
            realName.setId(keyValue);
            realName.setIspass(ispass);
            service.updateById(realName);
            return R.ok("修改成功");
        }
        return R.ok("修改成功");
    }


}
