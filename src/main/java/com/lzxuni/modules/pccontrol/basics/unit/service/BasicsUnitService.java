package com.lzxuni.modules.pccontrol.basics.unit.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.unit.entity.BasicsUnit;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理--单位管理 服务类
 * </p>
 *
 * @author fcd
 * @since 2019-08-01
 */
public interface BasicsUnitService extends IService<BasicsUnit> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, BasicsUnit tblBasicsUnit);
    //保存
    void SaveForm(BasicsUnit tblBasicsUnit);

    List<Tree> getTree();
}
