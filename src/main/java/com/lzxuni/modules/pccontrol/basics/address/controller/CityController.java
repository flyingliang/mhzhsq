package com.lzxuni.modules.pccontrol.basics.address.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.constant.LogConstant;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.address.entity.City;
import com.lzxuni.modules.pccontrol.basics.address.service.CityService;
import com.lzxuni.modules.pccontrol.basics.village.entity.Village;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 基础管理_地址管理_市 前端控制器
 * Created by 潘云明
 * 2019/7/11
 */

@RestController
@RequestMapping("/pccontrol/basics/address/city")
public class CityController extends BaseController {
    @Autowired
    private CityService service;
    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看市县管理", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/basics/address/city_index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, City city) {
        city.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(service.queryPage(pageParameter, city));
        return R.ok().put("data", pageData);
    }


    @RequestMapping("/GetTree")
    public Object GetTree(String pagination, Village village) {
        List<Tree> companyList = service.getTree();
        return R.ok().put("data",companyList);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/basics/address/city_form");
        return mv;
    }


    @SysLog(categoryId = LogConstant.OPERATEID,module = "操作市县管理",operateType = LogConstant.INSERT)
    @RequestMapping("/SaveForm")
    public Object SaveForm(City city, String keyValue, String isshow){
        if(StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)){
            city.setId(keyValue);
            city.setIsdel("0");
            service.updateById(city);
            return R.ok("删除成功");
        }else if(StringUtils.isNotEmpty(keyValue)){
            city.setId(keyValue);
            service.updateById(city);
            return R.ok("修改成功");
        }else{
            service.SaveForm(city);
            return R.ok("保存成功");
        }

    }
}
