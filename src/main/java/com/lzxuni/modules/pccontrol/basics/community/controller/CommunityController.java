package com.lzxuni.modules.pccontrol.basics.community.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.FileEntity;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.basics.community.entity.Community;
import com.lzxuni.modules.pccontrol.basics.community.service.CommunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 基础 _社区 前端控制器
 * Created by 潘云明
 * 2019/7/10
 */

@RestController
@RequestMapping("/pccontrol/basics/community")
public class CommunityController extends BaseController {
    @Autowired
    private CommunityService communityService;

    @Autowired
    private FileEntityService fileEntityService;
    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看社区管理", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/basics/community/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Community community) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(communityService.queryPage(pageParameter, null));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Insert")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/basics/community/insert");
        return mv;
    }
    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3,module = "修改图片",operateType = "操作")
    public Object insertDo(Community community,String keyValue, String featurepics, String leaderpics) throws Exception{
System.out.println("keyValue###"+keyValue);
if(StringUtils.isEmpty(keyValue)){
            String ywId = UuidUtil.get32UUID();
            if(!StringUtils.isEmpty(featurepics) && !"&amp;nbsp;".equals(featurepics)) {
                fileEntityService.insert(featurepics.replace("&quot;", "\""), ywId, "社区特色图片", "featurepic", null);
            }
            if(!StringUtils.isEmpty(leaderpics) && !"&amp;nbsp;".equals(leaderpics)) {
                fileEntityService.insert(leaderpics.replace("&quot;", "\""), ywId, "社区领导照片", "leaderpic", null);
            }
            community.setId(ywId);
            communityService.SaveForm(community);
            return R.ok("保存成功");
        }else if(StringUtils.isNotEmpty(keyValue) && (keyValue.substring(0, 1).equals("-"))){

            community.setId(keyValue.substring(1, keyValue.length()));
            community.setIsdel("0");
            communityService.updateById(community);
            return R.ok("删除成功");
        }else{
            fileEntityService.deleteByYwId(keyValue);
            if(!StringUtils.isEmpty(featurepics) && !"&amp;nbsp;".equals(featurepics)) {
                fileEntityService.insert(featurepics.replace("&quot;", "\""), keyValue, "社区特色图片", "featurepic", null);
            }
            if(!StringUtils.isEmpty(leaderpics) && !"&amp;nbsp;".equals(leaderpics)) {
                fileEntityService.insert(leaderpics.replace("&quot;", "\""), keyValue, "社区领导照片", "leaderpic", null);
            }
            community.setId(keyValue);
            communityService.updateById(community);

            return R.ok("修改成功");
        }

    }
//    @SysLog(categoryId = LogConstant.OPERATEID,module = "新闻媒体-ZZYH-境内",operateType = LogConstant.DELETE)
//    @RequestMapping("/DeleteForm")
//    public Object DeleteForm(String keyValue){
//        newsZzyhDomesticService.removeById(keyValue);
//        return R.ok("删除成功");
//    }


    @RequestMapping("/Update")
    public ModelAndView update(String keyValue) throws Exception{
        ModelAndView mv = new ModelAndView("/pccontrol/basics/community/update");
        FileEntity fileBeanCustom = new FileEntity();
        fileBeanCustom.setYwId(keyValue);
        fileBeanCustom.setYwType("featurepics");
        mv.addObject("featurepicsNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
        fileBeanCustom.setYwType("leaderpics");
        mv.addObject("leaderpicsNum", fileEntityService.queryNumByFileEntity(fileBeanCustom));
        mv.addObject("id",keyValue);
        return mv;
    }
}
