package com.lzxuni.modules.pccontrol.basics.real.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.real.entity.RealCar;
import com.lzxuni.modules.pccontrol.basics.real.mapper.RealCarMapper;
import com.lzxuni.modules.pccontrol.basics.real.service.RealCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 基础管理_实名认证 服务实现类
 * Created by 潘云明
 * 2019/7/8 14:58
 */
@Service
public class RealCarServiceImpl extends ServiceImpl<RealCarMapper, RealCar> implements RealCarService {
    @Autowired
    private RealCarMapper mapper;
    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, RealCar entity) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = mapper.selectListByCondition(entity);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }



}
