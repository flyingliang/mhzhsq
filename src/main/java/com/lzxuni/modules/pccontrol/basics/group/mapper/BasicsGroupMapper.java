package com.lzxuni.modules.pccontrol.basics.group.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.basics.group.entity.BasicsGroup;
import com.lzxuni.modules.pccontrol.home.culture.member.entity.CultureMember;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文化管理-文艺团体信息表 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-07-18
 */
public interface BasicsGroupMapper extends BaseMapper<BasicsGroup> {
    List<Map<String, Object>> selectListByConditionAndImg(@Param("basicsGroup") BasicsGroup basicsGroup ,@Param("ywType") String ywType);

    List<Map<String, Object>> selectListByConditionAndImgs(@Param("basicsGroup") BasicsGroup basicsGroup ,@Param("ywType") String ywType);

    List<BasicsGroup> selectgroup();
}
