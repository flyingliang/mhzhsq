package com.lzxuni.modules.pccontrol.basics.unit.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.unit.entity.BasicsUnit;
import com.lzxuni.modules.pccontrol.basics.unit.mapper.BasicsUnitMapper;
import com.lzxuni.modules.pccontrol.basics.unit.service.BasicsUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 基础管理--单位管理 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-08-01
 */
@Service
public class BasicsUnitServiceImpl extends ServiceImpl<BasicsUnitMapper, BasicsUnit> implements BasicsUnitService {

    @Autowired
    private BasicsUnitMapper tblBasicsUnitMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, BasicsUnit tblBasicsUnit) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = tblBasicsUnitMapper.selectListByCondition(tblBasicsUnit);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(BasicsUnit tblBasicsUnit) {
        String uuid = UuidUtil.get32UUID();
        tblBasicsUnit.setId(uuid);
        this.save(tblBasicsUnit);
    }

    @Override
    public List<Tree> getTree() {
        List<Tree> list = tblBasicsUnitMapper.getTree();
        return list;
    }
}
