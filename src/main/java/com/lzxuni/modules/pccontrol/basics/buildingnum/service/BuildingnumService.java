package com.lzxuni.modules.pccontrol.basics.buildingnum.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.buildingnum.entity.Buildingnum;

import java.util.List;
import java.util.Map;

/**
 * 基础_小区楼号 服务类
 * Created by 潘云明
 * 2019/7/15
 */
public interface BuildingnumService extends IService<Buildingnum> {
    //查询
    PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Buildingnum buildingnum);

    //保存
    void SaveForm(Buildingnum buildingnum);

    //查小区用的
    List<Tree> getTree(String communityid);

    List<Tree> getBuildingNumTree();

}
