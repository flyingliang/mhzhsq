package com.lzxuni.modules.pccontrol.basics.post.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.basics.post.entity.BasicsPost;

/**
 * <p>
 * 基础管理-->岗位管理 Mapper 接口
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
public interface BasicsPostMapper extends BaseMapper<BasicsPost> {

}
