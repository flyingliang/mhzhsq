package com.lzxuni.modules.pccontrol.basics.post.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 基础管理-->岗位管理
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_basics_post")
public class BasicsPost implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId
    private String id;

    /**
     * 岗位名称
     */
    private String postname;

    /**
     * 岗位介绍
     */
    private String content;

    /**
     * 建立时间
     */
    private Date date;

    /**
     * 1未删除，0删除
     */
    private String isdel;

}
