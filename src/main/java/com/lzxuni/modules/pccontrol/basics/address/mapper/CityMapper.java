package com.lzxuni.modules.pccontrol.basics.address.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.address.entity.City;

import java.util.List;
import java.util.Map;

/**
 * 基础_小区 Mapper 接口
 * Created by 潘云明
 * 2019/7/11
 */
public interface CityMapper extends BaseMapper<City> {
    List<Map<String,Object>> selectListByCondition(City city);
    List<Tree> getTree();
    List<City> selectCity();
}
