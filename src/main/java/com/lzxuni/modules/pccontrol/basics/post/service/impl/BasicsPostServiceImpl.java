package com.lzxuni.modules.pccontrol.basics.post.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.post.entity.BasicsPost;
import com.lzxuni.modules.pccontrol.basics.post.mapper.BasicsPostMapper;
import com.lzxuni.modules.pccontrol.basics.post.service.BasicsPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 基础管理-->岗位管理 服务实现类
 * </p>
 *
 * @author fcd
 * @since 2019-08-02
 */
@Service
public class BasicsPostServiceImpl extends ServiceImpl<BasicsPostMapper, BasicsPost> implements BasicsPostService {
    @Autowired
    private BasicsPostMapper tblBasicsPostMapper;
    @Override
    public PageInfo<BasicsPost> queryPage(PageParameter pageParameter, BasicsPost tblBasicsPost) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<BasicsPost> communityList = tblBasicsPostMapper.selectList(new QueryWrapper<BasicsPost>().eq("isDel", "1"));
        PageInfo<BasicsPost> pageInfo = new PageInfo<>(communityList);
        return pageInfo;
    }

    @Override
    public void SaveForm(BasicsPost tblBasicsPost) {
        this.save(tblBasicsPost);
    }

}
