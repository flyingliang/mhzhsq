package com.lzxuni.modules.pccontrol.basics.address.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.address.entity.Province;

import java.util.List;

/**
 * 基础管理_地址管理_省 服务类
 * Created by 潘云明
 * 2019/7/5 16:08
 */
public interface ProvinceService extends IService<Province> {
    //查询
	PageInfo<Province> queryPage(PageParameter pageParameter,Province entity);
	 //查询
	List<Province> queryList();
	//保存
    void SaveForm(Province entity);

}
