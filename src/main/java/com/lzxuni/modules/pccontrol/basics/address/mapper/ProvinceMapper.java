package com.lzxuni.modules.pccontrol.basics.address.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.basics.address.entity.Province;

/**
 * 基础管理_地址管理_省 Mapper 接口
 * Created by 潘云明
 * 2019/7/8 15:01
 */
public interface ProvinceMapper extends BaseMapper<Province> {
}
