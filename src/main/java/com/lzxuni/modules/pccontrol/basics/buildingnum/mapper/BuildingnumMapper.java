package com.lzxuni.modules.pccontrol.basics.buildingnum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.buildingnum.entity.Buildingnum;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 基础_小区楼号 Mapper 接口
 * Created by 潘云明
 * 2019/7/15
 */
public interface BuildingnumMapper extends BaseMapper<Buildingnum> {
    List<Map<String,Object>> selectListByCondition(Buildingnum buildingnum);
    List<Tree> getTree(@Param("communityid") String communityid);
    List<Tree> getBuildingNumTree();
}
