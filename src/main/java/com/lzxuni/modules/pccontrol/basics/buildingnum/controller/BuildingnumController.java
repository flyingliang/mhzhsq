package com.lzxuni.modules.pccontrol.basics.buildingnum.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.constant.LogConstant;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.buildingnum.entity.Buildingnum;
import com.lzxuni.modules.pccontrol.basics.buildingnum.service.BuildingnumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 基础_小区楼号 前端控制器
 * Created by 潘云明
 * 2019/7/15
 */

@RestController
@RequestMapping("/pccontrol/basics/buildingnum")
public class BuildingnumController extends BaseController {
    @Autowired
    private BuildingnumService buildingnumService;
    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看小区楼号", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/basics/buildingnum/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Buildingnum buildingnum) {
        buildingnum.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(buildingnumService.queryPage(pageParameter, buildingnum));
        return R.ok().put("data", pageData);
    }


    @RequestMapping("/GetTree")
    public Object GetTree(String communityid) {
        List<Tree> villageList = buildingnumService.getTree(communityid);
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/basics/buildingnum/form");
        return mv;
    }


    @SysLog(categoryId = LogConstant.OPERATEID,module = "查看小区楼号",operateType = LogConstant.INSERT)
    @RequestMapping("/SaveForm")
    public Object SaveForm(Buildingnum buildingnum, String keyValue, String isshow){
        if(StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)){
            buildingnum.setId(keyValue);
            buildingnum.setIsdel("0");
            buildingnumService.updateById(buildingnum);
            return R.ok("删除成功");
        }else if(StringUtils.isNotEmpty(keyValue)){
            buildingnum.setId(keyValue);
            buildingnumService.updateById(buildingnum);
            return R.ok("修改成功");
        }else{
            buildingnumService.SaveForm(buildingnum);
            return R.ok("保存成功");
        }

    }
}
