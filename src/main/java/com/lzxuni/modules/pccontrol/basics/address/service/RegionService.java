package com.lzxuni.modules.pccontrol.basics.address.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.address.entity.Region;

import java.util.List;
import java.util.Map;

/**
 * 基础管理_地址管理_区 服务类
 * Created by 潘云明
 * 2019/7/15
 */
public interface RegionService extends IService<Region> {
    //查询
	PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Region region);
	//保存
    void SaveForm(Region region);

    List<Tree> getTree(String provinceid);
    //查询
    List<Region> queryList();
}
