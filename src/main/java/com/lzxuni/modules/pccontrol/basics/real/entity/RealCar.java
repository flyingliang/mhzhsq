package com.lzxuni.modules.pccontrol.basics.real.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 基础管理_车辆认证
 * Created by 潘云明
 * 2019/8/19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_basics_realcar")
public class RealCar implements Serializable {


    /** 版本号 */
    private static final long serialVersionUID = 2122970119242718886L;

    /** 主键id */
    private String id;

    /** 姓名 */
    private String name;

    /** 身份号码 */
    private String idnum;

    /** 车牌号 */
    private String carlicense;

    /** 车型号 */
    private String carsize;

    /** 驾龄 */
    private String drivingage;

    /** 车龄 */
    private String carage;

    /** 车价 */
    private String money;

    /** openid */
    private String openid;

    /** 微信名 */
    private String nickname;

    /** 1通过，0未通过，-1未审核 */
    private String ispass;

    /** 建立时间 */
    private Date date;

}
