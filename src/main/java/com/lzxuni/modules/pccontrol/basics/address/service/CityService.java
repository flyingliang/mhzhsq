package com.lzxuni.modules.pccontrol.basics.address.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.address.entity.City;

import java.util.List;
import java.util.Map;

/**
 * 基础_小区 服务类
 * Created by 潘云明
 * 2019/7/11
 */
public interface CityService extends IService<City> {
    //查询
    PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, City city);
    //保存
    void SaveForm(City city);

    List<Tree> getTree();


    //查询
    List<City> queryList();

}
