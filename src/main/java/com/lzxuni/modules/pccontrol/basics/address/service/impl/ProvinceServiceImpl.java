package com.lzxuni.modules.pccontrol.basics.address.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.address.entity.Province;
import com.lzxuni.modules.pccontrol.basics.address.mapper.ProvinceMapper;
import com.lzxuni.modules.pccontrol.basics.address.service.ProvinceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 基础管理_地址管理_省 服务实现类
 * Created by 潘云明
 * 2019/7/8 14:58
 */
@Service
public class ProvinceServiceImpl extends ServiceImpl<ProvinceMapper, Province> implements ProvinceService {
    @Autowired
    private ProvinceMapper mapper;
    @Override
    public PageInfo<Province> queryPage(PageParameter pageParameter, Province community) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Province> communityList = mapper.selectList(new QueryWrapper<Province>().eq("isDel", "1"));
        PageInfo<Province> pageInfo = new PageInfo<>(communityList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Province community) {
        this.save(community);
    }

    @Override
    public List<Province> queryList() {

        List<Province> communityList = mapper.selectList(new QueryWrapper<Province>().eq("isDel", "1"));

        return communityList;
    }
}
