package com.lzxuni.modules.pccontrol.basics.village.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.constant.LogConstant;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.village.entity.Village;
import com.lzxuni.modules.pccontrol.basics.village.service.VillageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 基础_小区 前端控制器
 * Created by 潘云明
 * 2019/7/11
 */

@RestController
@RequestMapping("/pccontrol/basics/village")
public class VillageController extends BaseController {
    @Autowired
    private VillageService villageService;
    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看小区", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/basics/village/index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Village village) {
        village.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(villageService.queryPage(pageParameter, village));
        return R.ok().put("data", pageData);
    }


    @RequestMapping("/GetTree")
    public Object GetTree(String pagination, Village village) {
        List<Tree> companyList = villageService.getTree();
        return R.ok().put("data",companyList);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/basics/village/form");
        return mv;
    }


    @SysLog(categoryId = LogConstant.OPERATEID,module = "查看小区",operateType = LogConstant.INSERT)
    @RequestMapping("/SaveForm")
    public Object SaveForm(Village village, String keyValue, String isshow){
        if(StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)){
            village.setId(keyValue);
            village.setIsdel("0");
            villageService.updateById(village);
            return R.ok("删除成功");
        }else if(StringUtils.isNotEmpty(keyValue)){
            village.setId(keyValue);
            villageService.updateById(village);
            return R.ok("修改成功");
        }else{
            villageService.SaveForm(village);
            return R.ok("保存成功");
        }

    }
//    @SysLog(categoryId = LogConstant.OPERATEID,module = "新闻媒体-ZZYH-境内",operateType = LogConstant.DELETE)
//    @RequestMapping("/DeleteForm")
//    public Object DeleteForm(String keyValue){
//        newsZzyhDomesticService.removeById(keyValue);
//        return R.ok("删除成功");
//    }
}
