package com.lzxuni.modules.pccontrol.basics.community.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.basics.community.entity.Community;

import java.util.List;
import java.util.Map;

/**
 * 基础 _社区 服务类
 * Created by 潘云明
 * 2019/7/5 16:08
 */
public interface CommunityService extends IService<Community> {
    //查询
	PageInfo<Community> queryPage(PageParameter pageParameter, Community community);
	 //查询
	List<Community> queryLList();
	//保存
    void SaveForm(Community community);

//微信查询单独
	List<Map<String, Object>> queryOne(String id);

}
