package com.lzxuni.modules.pccontrol.basics.real.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.basics.real.entity.RealCar;

import java.util.List;
import java.util.Map;

/**
 * 基础管理_实名认证 Mapper 接口
 * Created by 潘云明
 * 2019/7/8 15:01
 */
public interface RealCarMapper extends BaseMapper<RealCar> {
    List<Map<String,Object>> selectListByCondition(RealCar entity);
}
