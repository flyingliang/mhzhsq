package com.lzxuni.modules.pccontrol.basics.address.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 基础管理_地址管理_省
 * Created by 潘云明
 * 2019/8/19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_basics_address_province")
public class Province implements Serializable {


    /** 版本号 */
    private static final long serialVersionUID = 2122970119242718886L;

    /** 主键id */
    private String id;

    /** 省名称 */
    private String provincename;


    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除 */
    private String isdel;

}
