package com.lzxuni.modules.pccontrol.basics.village.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.village.entity.Village;
import com.lzxuni.modules.pccontrol.basics.village.mapper.VillageMapper;
import com.lzxuni.modules.pccontrol.basics.village.service.VillageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 基础_小区 服务实现类
 * Created by 潘云明
 * 2019/7/11
 */
@Service
public class VillageServiceImpl extends ServiceImpl<VillageMapper, Village> implements VillageService {
    @Autowired
    private VillageMapper villageMapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Village village) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = villageMapper.selectListByCondition(village);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Village village) {
        String uuid = UuidUtil.get32UUID();
        village.setId(uuid);
        this.save(village);
    }

    @Override
    public List<Tree> getTree() {
        List<Tree> list = villageMapper.getTree();
//        getTreeDg(list);
        return list;
    }

//    /*私有方法区域*/
//    private void getTreeDg(List<Tree> ztreeList) {
//        for (int i = 0; i < ztreeList.size(); i++) {
//            Tree tree = ztreeList.get(i);
//            List<Tree> treeList = baseMapper.queryListByParentId(tree.getId());
//            if (treeList != null && treeList.size() > 0) {
//                tree.setChildNodes(treeList);
//                tree.setHasChildren(true);
//                tree.setIsexpand(true);
//            }
//            getTreeDg(treeList);
//        }
//    }


    public List<Village> selectVillage(){
     return villageMapper.selectVillage();
    }

}
