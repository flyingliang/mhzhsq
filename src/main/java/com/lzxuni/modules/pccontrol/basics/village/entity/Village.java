package com.lzxuni.modules.pccontrol.basics.village.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 基础_小区 实体类
 * Created by 潘云明
 * 2019/7/11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TBL_BASICS_VILLAGE")
public class Village implements Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1L;
    @TableId
    /** 主键，uuid */
    private String id;

    /** 社区编号 */
    private String communityid;

    /** 小区名 */
    private String villagename;


    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除*/
    private String isdel;

}
