package com.lzxuni.modules.pccontrol.basics.address.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.constant.LogConstant;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.address.entity.Region;
import com.lzxuni.modules.pccontrol.basics.address.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 基础管理_地址管理_区前端控制器
 * Created by 潘云明
 * 2019/7/15
 */

@RestController
@RequestMapping("/pccontrol/basics/address/region")
public class RegionController extends BaseController {
    @Autowired
    private RegionService service;
    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看小区楼号", operateType = "访问")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView("/pccontrol/basics/address/region_index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Region region) {
        region.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(service.queryPage(pageParameter, region));
        return R.ok().put("data", pageData);
    }


    @RequestMapping("/GetTree")
    public Object GetTree(String provinceid) {
        List<Tree> villageList = service.getTree(provinceid);
        return R.ok().put("data",villageList);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/basics/address/region_form");
        return mv;
    }


    @SysLog(categoryId = LogConstant.OPERATEID,module = "查看小区楼号",operateType = LogConstant.INSERT)
    @RequestMapping("/SaveForm")
    public Object SaveForm(Region region, String keyValue, String isshow){
        if(StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)){
            region.setId(keyValue);
            region.setIsdel("0");
            service.updateById(region);
            return R.ok("删除成功");
        }else if(StringUtils.isNotEmpty(keyValue)){
            region.setId(keyValue);
            service.updateById(region);
            return R.ok("修改成功");
        }else{
            service.SaveForm(region);
            return R.ok("保存成功");
        }

    }
}
