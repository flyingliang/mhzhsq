package com.lzxuni.modules.pccontrol.basics.address.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.common.utils.UuidUtil;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.entity.Tree;
import com.lzxuni.modules.pccontrol.basics.address.entity.Region;
import com.lzxuni.modules.pccontrol.basics.address.mapper.RegionMapper;
import com.lzxuni.modules.pccontrol.basics.address.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 基础管理_地址管理_区 服务实现类
 * Created by 潘云明
 * 2019/7/15
 */
@Service
public class RegionServiceImpl extends ServiceImpl<RegionMapper, Region> implements RegionService {
    @Autowired
    private RegionMapper mapper;

    @Override
    public PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Region region) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> villageList = mapper.selectListByCondition(region);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(villageList);
        return pageInfo;
    }

    @Override
    public void SaveForm(Region region) {
        String uuid = UuidUtil.get32UUID();
        region.setId(uuid);
        this.save(region);
    }

    @Override
    public List<Tree> getTree(String provinceid) {
        List<Tree> list = mapper.getTree(provinceid);
        return list;
    }

    @Override
    public List<Region> queryList() {
        List<Region> regionList = mapper.selectList(new QueryWrapper<Region>().eq("isDel", "1"));

        return regionList;
    }
}
