package com.lzxuni.modules.pccontrol.basics.wechatuser;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.wechat.config.entity.WechatUserInfo;
import com.lzxuni.modules.wechat.config.service.WeChatUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 主页_签到 前端控制器
 * Created by 潘云明
 * 2019/7/18
 */

@RestController
@RequestMapping("/pccontrol/basics/wechatuser")
public class WechatUserInfoController extends BaseController {
    @Autowired
    private WeChatUserInfoService service;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "微信用户管理", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/basics/wechatuser/index");
        return mv;
    }


    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3, module = "微信用户管理", operateType = "操作")
    public Object insertDo(WechatUserInfo entity, String keyValue, String isshow) throws Exception {
        System.out.println("###参数列表：keyValue：" + keyValue + ",isshow:" + isshow);
        if (StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)) {//展示
            entity.setOpenid(keyValue);
            entity.setIsboodbye("0");
            service.updateById(entity);
            return R.ok("禁用成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)) {//取消展示
            entity.setOpenid(keyValue);
            entity.setIsboodbye("1");
            service.updateById(entity);
            return R.ok("取消禁用成功");
        }
        return R.ok();


    }


    @RequestMapping("/GetList")
    public Object GetGoodList(String pagination, String openid) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(service.queryPage(pageParameter));
        return R.ok().put("data", pageData);
    }
}
