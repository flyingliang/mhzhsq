package com.lzxuni.modules.pccontrol.forum.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.forum.entity.Comment;
import com.lzxuni.modules.pccontrol.forum.mapper.CommentMapper;
import com.lzxuni.modules.pccontrol.forum.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 主页_党建_帖子管理 服务实现类
 * Created by 潘云明
 * 2019/7/16
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {
    @Autowired
    private CommentMapper mapper;
    @Override
    public PageInfo<Map<String,Object>> queryPage(PageParameter pageParameter, Comment entity) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = mapper.selectListByCondition(entity);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }

    @Override
    public PageInfo<Map<String,Object>> selectListByConditionAndImg(PageParameter pageParameter, Comment entity,String ywType,String goodOpenId) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = mapper.selectListByConditionAndImg(entity,ywType,goodOpenId);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }


    @Override
    public void addGoodNum(String id) {
        mapper.addGoodNum(id);
    }

    @Override
    public void cutGoodNum(String id) {
        mapper.cutGoodNum(id);
    }

}
