package com.lzxuni.modules.pccontrol.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.forum.entity.Comment;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 主页_党建_帖字管理 Mapper 接口
 * Created by 潘云明
 * 2019/7/18
 */
public interface CommentMapper extends BaseMapper<Comment> {
    List<Map<String, Object>> selectListByCondition(Comment entity);


    List<Map<String, Object>> selectListByConditionAndImg(@Param("comment") Comment entity, @Param("ywType") String ywType, @Param("goodOpenId") String goodOpenId);

    //微信加赞
    void addGoodNum(@Param("id") String id);

    //微信减赞
    void cutGoodNum(@Param("id") String id);

}
