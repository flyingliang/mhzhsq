package com.lzxuni.modules.pccontrol.forum.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.forum.entity.Comment;
import com.lzxuni.modules.pccontrol.forum.service.SignInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 主页_签到 前端控制器
 * Created by 潘云明
 * 2019/7/18
 */

@RestController
@RequestMapping("/pccontrol/signin")
public class SignInController extends BaseController {
    @Autowired
    private SignInService signInService;

    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "积分签到管理", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/forum/signin_index");
        return mv;
    }

    @RequestMapping("/GetList")
    public Object GetList(String pagination, Comment entity) {
        entity.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(signInService.selectListByCondition(pageParameter));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3, module = "签到管理", operateType = "操作")
    public Object insertDo(String keyValue, String isshow) throws Exception {
        System.out.println("###签到管理：keyValue：" + keyValue + ",isshow:" + isshow);
        if (StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)) {//删除
            signInService.removeByOpenId(keyValue);
            return R.ok("删除成功");
        }
        return R.ok();
    }

    @RequestMapping("/signByOpenIdIndex")
    @SysLog(categoryId = 2, module = "查看签到列表", operateType = "访问")
    public ModelAndView GoodListForm() {
        ModelAndView mv = new ModelAndView("/pccontrol/forum/sign_info");
        return mv;
    }




    @RequestMapping("/GetsignByOpenIList")
    public Object GetGoodList(String pagination, String openid) {
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(signInService.queryPage(pageParameter, openid));
        return R.ok().put("data", pageData);
    }
}
