package com.lzxuni.modules.pccontrol.forum.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.forum.entity.Posts;

import java.util.Map;

/**
 * 主页_党建_点赞管理 服务类
 * Created by 潘云明
 * 2019/7/16
 */
public interface PostsService extends IService<Posts> {
    //查询

    PageInfo<Posts> queryPage(PageParameter pageParameter, Posts entity);


    //微信查询

    PageInfo<Map<String, Object>> selectListByCondition(PageParameter pageParameter,  String id);
}
