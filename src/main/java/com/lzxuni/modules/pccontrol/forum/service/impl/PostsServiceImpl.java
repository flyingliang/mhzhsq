package com.lzxuni.modules.pccontrol.forum.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.forum.entity.Posts;
import com.lzxuni.modules.pccontrol.forum.mapper.PostsMapper;
import com.lzxuni.modules.pccontrol.forum.service.PostsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 主页_党建_点赞 服务实现类
 * Created by 潘云明
 * 2019/7/16
 */
@Service
public class PostsServiceImpl extends ServiceImpl<PostsMapper, Posts> implements PostsService {
    @Autowired
    private PostsMapper mapper;
    @Override
    public PageInfo<Posts> queryPage(PageParameter pageParameter, Posts eneity) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Posts> goodList = mapper.selectList(new QueryWrapper<Posts>().eq("commentid", eneity.getCommentid()));
        PageInfo<Posts> pageInfo = new PageInfo<>(goodList);
        return pageInfo;
    }


    @Override
    public PageInfo<Map<String,Object>> selectListByCondition(PageParameter pageParameter,String id) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = mapper.selectListByCondition(id,"1");
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }


}
