package com.lzxuni.modules.pccontrol.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.forum.entity.SignIn;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 主页_签到 Mapper 接口
 * Created by 潘云明
 * 2019/7/18
 */
public interface SignInMapper extends BaseMapper<SignIn> {

    List<Map<String, Object>> selectListByCondition();

    void removeByOpenId(@Param("openId") String openId);

    //查询签到列表100
    List<SignIn> queryCountList();

    //查询可用积分
    String quertCountNum(@Param("openid") String openid);
}
