package com.lzxuni.modules.pccontrol.forum.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 论坛_点赞管理
 * Created by 潘云明
 * 2019/7/18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_forum_good")
public class Good implements Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1L;
    @TableId
    /** 主键，uuid */
    private String id;

    /** 帖子id */
    private String commentid;

    /** openid */
    private String openid;

    /** 微信名 */
    private String nickname;


    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除*/
    private String isdel;

}
