package com.lzxuni.modules.pccontrol.forum.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.forum.entity.Good;

/**
 * 主页_党建_点赞管理 服务类
 * Created by 潘云明
 * 2019/7/16
 */
public interface GoodService extends IService<Good> {
    //查询

    PageInfo<Good> queryPage(PageParameter pageParameter, Good entity);

    //微信删除点赞
    void removeByCondition( Good entity);
}
