package com.lzxuni.modules.pccontrol.forum.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.forum.entity.Posts;
import com.lzxuni.modules.pccontrol.forum.service.PostsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 论坛_回复管理 前端控制器
 * Created by 潘云明
 * 2019/7/18
 */

@RestController
@RequestMapping("/pccontrol/forum")
public class PostsController extends BaseController {
    @Autowired
    private PostsService service;

    @Autowired
    private FileEntityService fileEntityService;


    @RequestMapping("/PostsIndex")
    @SysLog(categoryId = 2, module = "查看帖子回复列表", operateType = "访问")
    public ModelAndView GoodListForm() {
        ModelAndView mv = new ModelAndView("/pccontrol/forum/postsList");
        return mv;
    }


    @RequestMapping("/GetPostsList")
    public Object GetGoodList(String pagination, Posts entity) {
        entity.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(service.queryPage(pageParameter, entity));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Posts/SaveForm")
    @SysLog(categoryId = 3, module = "帖子管理", operateType = "操作")
    public Object insertDo(Posts entity, String keyValue, String isshow) throws Exception {
        System.out.println("回复管理###参数列表：keyValue：" + keyValue + ",isshow:" + isshow);
        if (StringUtils.isNotEmpty(keyValue) && "1".equals(isshow)) {//展示
            entity.setId(keyValue);
            entity.setIsshow(isshow);
            service.updateById(entity);
            return R.ok("展示成功");
        } else if (StringUtils.isNotEmpty(keyValue) && "0".equals(isshow)) {//取消展示
            entity.setId(keyValue);
            entity.setIsshow(isshow);
            service.updateById(entity);
            return R.ok("取消展示成功");
        } else {
            return R.ok("");
        }
    }


}
