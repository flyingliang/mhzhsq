package com.lzxuni.modules.pccontrol.forum.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.forum.entity.SignIn;
import com.lzxuni.modules.pccontrol.forum.mapper.SignInMapper;
import com.lzxuni.modules.pccontrol.forum.service.SignInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 主页_签到 服务实现类
 * Created by 潘云明
 * 2019/7/16
 */
@Service
public class SignInServiceImpl extends ServiceImpl<SignInMapper, SignIn> implements SignInService {
    @Autowired
    private SignInMapper mapper;


    @Override
    public PageInfo<Map<String, Object>> selectListByCondition(PageParameter pageParameter) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String, Object>> styleList = mapper.selectListByCondition();
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }


    public void removeByOpenId(String openId) {
        mapper.removeByOpenId(openId);
    }


    public PageInfo<SignIn> queryPage(PageParameter pageParameter, String openid) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        } else {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<SignIn> signInList = mapper.selectList(new QueryWrapper<SignIn>().eq("openid", openid));
        PageInfo<SignIn> pageInfo = new PageInfo<>(signInList);
        return pageInfo;
    }

    @Override
    public String queryDate(SignIn entity) {
        String result = "";
        List<SignIn> signInList = mapper.selectList(new QueryWrapper<SignIn>().eq("openid", entity.getOpenid()));

        for (SignIn signIn : signInList) {
            result += "\"" + signIn.getDate().getTime() / 1000 + "\",";
        }
        if (result.length() > 1)
            result = result.substring(0, result.length() - 1);
        return result;
    }


    @Override
    public Map<String, Object> queryCountNum(SignIn entity) {
        Map<String, Object> map = new HashMap<>();
        List<SignIn> signInList = mapper.selectList(new QueryWrapper<SignIn>().eq("openid", entity.getOpenid()));
        map.put("dayCount", signInList.size());
        Long l = new Long("0");
        for (SignIn signIn : signInList) {
            l += signIn.getNum();
        }
        map.put("numCount", l);
        return map;
    }

    @Override
    public List<SignIn> queryCountList() {
        List<SignIn> signInList = mapper.queryCountList();
        return signInList;
    }


    @Override
    public String quertUseCountNum(String openid){
        return mapper.quertCountNum(openid);
    }
}
