package com.lzxuni.modules.pccontrol.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.forum.entity.Good;
import org.apache.ibatis.annotations.Param;

/**
 * 主页_党建_点赞管理 Mapper 接口
 * Created by 潘云明
 * 2019/7/18
 */
public interface GoodMapper extends BaseMapper<Good> {

    void removeByCondition(@Param("good")Good good);
}
