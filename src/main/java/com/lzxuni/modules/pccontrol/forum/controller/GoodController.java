package com.lzxuni.modules.pccontrol.forum.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.common.service.FileEntityService;
import com.lzxuni.modules.pccontrol.forum.entity.Good;
import com.lzxuni.modules.pccontrol.forum.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 论坛_点赞管理 前端控制器
 * Created by 潘云明
 * 2019/7/18
 */

@RestController
@RequestMapping("/pccontrol/forum")
public class GoodController extends BaseController {
    @Autowired
    private GoodService service;

    @Autowired
    private FileEntityService fileEntityService;







    @RequestMapping("/GoodIndex")
    @SysLog(categoryId = 2, module = "查看帖子点赞列表", operateType = "访问")
    public ModelAndView GoodListForm() {
        ModelAndView mv = new ModelAndView("/pccontrol/forum/goodList");
        return mv;
    }




    @RequestMapping("/GetGoodList")
    public Object GetGoodList(String pagination, Good entity) {
        entity.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(service.queryPage(pageParameter, entity));
        return R.ok().put("data", pageData);
    }




}
