package com.lzxuni.modules.pccontrol.forum.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.forum.entity.Comment;

import java.util.Map;

/**
 * 主页_党建_帖子管理 服务类
 * Created by 潘云明
 * 2019/7/16
 */
public interface CommentService extends IService<Comment> {
    //查询
    PageInfo<Map<String, Object>> queryPage(PageParameter pageParameter, Comment entity);

    //微信
    PageInfo<Map<String, Object>> selectListByConditionAndImg(PageParameter pageParameter, Comment entity, String ywType, String goodOpenId);

    //微信加赞
    void addGoodNum(String id);

    //微信减赞
    void cutGoodNum(String id);
}
