package com.lzxuni.modules.pccontrol.forum.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.forum.entity.Good;
import com.lzxuni.modules.pccontrol.forum.mapper.GoodMapper;
import com.lzxuni.modules.pccontrol.forum.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 主页_党建_点赞 服务实现类
 * Created by 潘云明
 * 2019/7/16
 */
@Service
public class GoodServiceImpl extends ServiceImpl<GoodMapper, Good> implements GoodService {
    @Autowired
    private GoodMapper mapper;
    @Override
    public PageInfo<Good> queryPage(PageParameter pageParameter, Good eneity) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Good> goodList = mapper.selectList(new QueryWrapper<Good>().eq("commentid", eneity.getCommentid()));
        PageInfo<Good> pageInfo = new PageInfo<>(goodList);
        return pageInfo;
    }

    @Override
    public void removeByCondition(Good eneity) {
        mapper.removeByCondition(eneity);
    }



}
