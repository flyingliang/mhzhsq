package com.lzxuni.modules.pccontrol.forum.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 主页_党建_群团风采
 * Created by 潘云明
 * 2019/7/18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_forum_comment")
public class Comment implements Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1L;
    @TableId
    /** 主键，uuid */
    private String id;

    /** 标题 */
    private String title;

    /** 内容 */
    private String content;

    /** 过期天数 */
    private String overday;

    /** 类型 */
    private String type;

    /** openid */
    private String openid;

    /** 微信名 */
    private String nickname;

    /** 点赞数 */
    private int goodnum;

    /** 回复数 */
    private int postsnum_1;

    /**1表示展示，1隐藏 */
    private String isshow;

    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除*/
    private String isdel;

}
