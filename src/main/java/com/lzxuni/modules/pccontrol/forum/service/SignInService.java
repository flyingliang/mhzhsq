package com.lzxuni.modules.pccontrol.forum.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.forum.entity.SignIn;

import java.util.List;
import java.util.Map;

/**
 * 主页_签到 服务类
 * Created by 潘云明
 * 2019/7/16
 */
public interface SignInService extends IService<SignIn> {

    //查询
    PageInfo<Map<String, Object>> selectListByCondition(PageParameter pageParameter);

    void removeByOpenId(String openId);

    PageInfo<SignIn> queryPage(PageParameter pageParameter,String openid);

    //微信查询
   String queryDate(SignIn entity);
    //微信查询自己总分
    Map<String,Object> queryCountNum(SignIn entity);

    //查询签到列表100
    List<SignIn> queryCountList();

    //查询可用积分
    String quertUseCountNum(String openid);
}
