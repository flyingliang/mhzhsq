package com.lzxuni.modules.pccontrol.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.forum.entity.Posts;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 主页_党建_回复管理 Mapper 接口
 * Created by 潘云明
 * 2019/7/18
 */
public interface PostsMapper extends BaseMapper<Posts> {

    List<Map<String, Object>> selectListByCondition(@Param("id") String id,@Param("isshow")String isshow);

}
