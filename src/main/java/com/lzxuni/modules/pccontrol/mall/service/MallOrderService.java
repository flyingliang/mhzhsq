package com.lzxuni.modules.pccontrol.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.mall.entity.MallOrder;

import java.util.Map;

/**
 * 订单管理 服务类
 * Created by 潘云明
 * 2019/7/16
 */
public interface MallOrderService extends IService<MallOrder> {




    PageInfo<Map<String, Object>> selectListByConditionAndImg(PageParameter pageParameter, MallOrder entity);


    void cutNum(String id);

}
