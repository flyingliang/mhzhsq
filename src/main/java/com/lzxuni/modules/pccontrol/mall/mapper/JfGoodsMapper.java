package com.lzxuni.modules.pccontrol.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.mall.entity.JfGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 积分商城 Mapper 接口
 * Created by 潘云明
 * 2019/7/18
 */
public interface JfGoodsMapper extends BaseMapper<JfGoods> {

    List<Map<String, Object>> selectListByConditionAndImg(@Param("jfGoods") JfGoods entity, @Param("ywType") String ywType);

}
