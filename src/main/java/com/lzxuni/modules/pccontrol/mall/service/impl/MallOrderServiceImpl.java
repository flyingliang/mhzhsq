package com.lzxuni.modules.pccontrol.mall.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.mall.entity.MallOrder;
import com.lzxuni.modules.pccontrol.mall.mapper.MallOrderMapper;
import com.lzxuni.modules.pccontrol.mall.service.MallOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 订单管理 服务实现类
 * Created by 潘云明
 * 2019/7/16
 */
@Service
public class MallOrderServiceImpl extends ServiceImpl<MallOrderMapper, MallOrder> implements MallOrderService {
    @Autowired
    private MallOrderMapper mapper;



    @Override
    public PageInfo<Map<String,Object>> selectListByConditionAndImg(PageParameter pageParameter, MallOrder entity) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = mapper.selectListByConditionAndImg(entity);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }

    @Override
    public void cutNum(String id){
        mapper.cutNum(id);
    }

}
