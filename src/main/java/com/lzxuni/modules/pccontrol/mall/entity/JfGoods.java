package com.lzxuni.modules.pccontrol.mall.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 积分商城
 * Created by 潘云明
 * 2019/7/18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_mall_goods")
public class JfGoods implements Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1L;
    @TableId
    /** 主键，uuid */
    private String id;

    /** 名字 */
    private String name;

    /** 积分 */
    private String num;

    /** 商品简介 */
    private String content;

    /** 兑换说明 */
    private String tips;

    /** 库存 */
    private Integer stock;

    /** 1表示展示，0隐藏 */
    private String isshow;

    /** 建立时间 */
    private Date date;

    /** 1未删除，0删除 */
    private String isdel;

}
