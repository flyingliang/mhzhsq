package com.lzxuni.modules.pccontrol.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzxuni.modules.pccontrol.mall.entity.MallOrder;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 订单管理 Mapper 接口
 * Created by 潘云明
 * 2019/7/18
 */
public interface MallOrderMapper extends BaseMapper<MallOrder> {

    List<Map<String, Object>> selectListByConditionAndImg(@Param("mallOrder") MallOrder entity );


    void cutNum(@Param("id")String id);
}
