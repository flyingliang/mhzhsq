package com.lzxuni.modules.pccontrol.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.mall.entity.JfGoods;

import java.util.Map;

/**
 * 积分商城 服务类
 * Created by 潘云明
 * 2019/7/16
 */
public interface JfGoodsService extends IService<JfGoods> {
    //查询

    PageInfo<JfGoods> queryPage(PageParameter pageParameter, JfGoods entity);


    //微信
    PageInfo<Map<String, Object>> selectListByConditionAndImg(PageParameter pageParameter, JfGoods entity, String ywType);

}
