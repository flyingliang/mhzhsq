package com.lzxuni.modules.pccontrol.mall.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单管理
 * Created by 潘云明
 * 2019/7/18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tbl_mall_order")
public class MallOrder implements Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1L;
    @TableId
    /** 主键，uuid */
    private String id;

    /** 商品表id */
    private String goodsid;

    /** 收货地址 */
    private String address;

    /** 备注 */
    private String content;

    /** 状态  0已下单、1已发货，2已完成 */
    private String status;

    /** openid */
    private String openid;

    /** 微信名 */
    private String nickname;

    /** 发货备注 */
    private String fhcontent;

    /** 建立时间 创建订单时间 */
    private Date date;

    /** 1未删除，0删除 */
    private String isdel;

    /** 发货时间 */
    private Date postdate;

    /** 完成时间 */
    private Date overdate;

}
