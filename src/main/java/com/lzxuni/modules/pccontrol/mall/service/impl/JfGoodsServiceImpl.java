package com.lzxuni.modules.pccontrol.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.mall.entity.JfGoods;
import com.lzxuni.modules.pccontrol.mall.mapper.JfGoodsMapper;
import com.lzxuni.modules.pccontrol.mall.service.JfGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 积分商城 服务实现类
 * Created by 潘云明
 * 2019/7/16
 */
@Service
public class JfGoodsServiceImpl extends ServiceImpl<JfGoodsMapper, JfGoods> implements JfGoodsService {
    @Autowired
    private JfGoodsMapper mapper;
    @Override
    public PageInfo<JfGoods> queryPage(PageParameter pageParameter, JfGoods eneity) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<JfGoods> goodList = mapper.selectList(new QueryWrapper<JfGoods>().eq("isDel", "1"));
        PageInfo<JfGoods> pageInfo = new PageInfo<>(goodList);
        return pageInfo;
    }



    @Override
    public PageInfo<Map<String,Object>> selectListByConditionAndImg(PageParameter pageParameter, JfGoods entity, String ywType) {
        if (StringUtils.isNotEmpty(pageParameter.getSidx())) {
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows()).setOrderBy(
                    pageParameter.getSidx() + " " + pageParameter.getSord());
        }else{
            PageHelper.startPage(pageParameter.getPage(), pageParameter.getRows());
        }
        List<Map<String,Object>> styleList = mapper.selectListByConditionAndImg(entity,ywType);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<>(styleList);
        return pageInfo;
    }



}
