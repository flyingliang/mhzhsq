package com.lzxuni.modules.pccontrol.mall.controller;

import com.alibaba.fastjson.JSON;
import com.lzxuni.common.annotation.SysLog;
import com.lzxuni.common.utils.R;
import com.lzxuni.common.utils.StringUtils;
import com.lzxuni.modules.common.controller.BaseController;
import com.lzxuni.modules.common.entity.PageData;
import com.lzxuni.modules.common.entity.PageParameter;
import com.lzxuni.modules.pccontrol.mall.entity.MallOrder;
import com.lzxuni.modules.pccontrol.mall.service.MallOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * 订单管理 前端控制器
 * Created by 潘云明
 * 2019/7/18
 */

@RestController
@RequestMapping("/pccontrol/mall/order")
public class MallOrderController extends BaseController {


    @Autowired
    private MallOrderService mallOrderService;



    @RequestMapping("/Index")
    @SysLog(categoryId = 2, module = "查看订单管理", operateType = "访问")
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView("/pccontrol/mall/order_index");
        System.out.println("333234234");
        return mv;
    }
    @RequestMapping("/GetOrderList")
    public Object GetGoodList(String pagination, MallOrder entity) {
        System.out.println("1111111");
        entity.setIsdel("1");
        //1.Json转换成实体类
        PageParameter pageParameter = JSON.parseObject(pagination, PageParameter.class);
        //调用父类的一个方法，传进一个参数pageParameter进行查询操作将在业务层将结果直接封装进PageInfo中然后将PageInfo数据封装进pageData中，返回到页面中
        PageData pageData = getPageData(mallOrderService.selectListByConditionAndImg(pageParameter, entity));
        return R.ok().put("data", pageData);
    }

    @RequestMapping("/Form")
    public ModelAndView Form() {
        ModelAndView mv = new ModelAndView("/pccontrol/mall/order_form");
        return mv;
    }

    @RequestMapping("/SaveForm")
    @SysLog(categoryId = 3, module = "积分商城订单管理", operateType = "操作")
    public Object insertDo( String keyValue,String fhcontent, String isshow) throws Exception {
        System.out.println("###参数列表：keyValue：" + keyValue + ",isshow:" + isshow);
        MallOrder entity = new MallOrder();
         if (StringUtils.isNotEmpty(keyValue) && "3".equals(isshow)) {//删除
             entity.setId(keyValue);
             entity.setIsdel("0");
             mallOrderService.updateById(entity);
            return R.ok("删除成功");
        }  else {
             entity.setId(keyValue);
             entity.setPostdate(new Date());
             entity.setStatus("2");
             entity.setFhcontent(fhcontent);
             mallOrderService.updateById(entity);
             mallOrderService.updateById(entity);
            return R.ok("发货成功");
        }

    }



}
