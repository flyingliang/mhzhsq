package com.lzxuni;

import com.lzxuni.datasources.DynamicDataSourceConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Import;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class, SecurityAutoConfiguration.class})
@MapperScan(basePackages = {"com.lzxuni.modules.**/.mapper"})
@Import({DynamicDataSourceConfig.class})
@ServletComponentScan
public class MhzhsqApplication {


    public static void main(String[] args) {
        SpringApplication.run(MhzhsqApplication.class, args);
    }

}
