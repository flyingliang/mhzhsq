package com.lzxuni.config;

import com.lzxuni.common.xss.XssFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.DispatcherType;

/**
 * Filter配置
 *
 * @author 孙志强
 * @date 2017-04-21 21:56
 */
@Configuration
public class FilterConfig {
    @Bean
    public FilterRegistrationBean xssFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setDispatcherTypes(DispatcherType.REQUEST);
        registration.setFilter(new XssFilter());
        registration.addUrlPatterns("/*");
        registration.setName("xssFilter");
        registration.setOrder(Integer.MAX_VALUE);
        return registration;
    }

    @Bean
    public FilterRegistrationBean staticResourceFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setDispatcherTypes(DispatcherType.REQUEST);
        registration.setFilter(staticResourceFilter());
        registration.addUrlPatterns("/resource/*");
        registration.setName("staticResourceFilter");
        registration.setOrder(Integer.MAX_VALUE);
        return registration;
    }

    @Bean(name = "staticResourceFilter")
    public StaticResourceFilter staticResourceFilter() {
        return new StaticResourceFilter();
    }

}
