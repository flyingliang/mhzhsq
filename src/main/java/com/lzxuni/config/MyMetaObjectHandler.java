package com.lzxuni.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;

//import com.baomidou.mybatisplus.mapper.MetaObjectHandler;
//import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author:孙志强
 * @create:2018-12-07 10:28
 * @Modified BY:
 **/

@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(MyMetaObjectHandler.class);

	@Override
	public void insertFill(MetaObject metaObject) {
		LOGGER.info("start insert fill ....");
		Object createDate = this.getFieldValByName("createDate", metaObject);
		if(createDate==null){
			this.setFieldValByName("createDate", new Date(), metaObject);
		}
	}

	@Override
	public void updateFill(MetaObject metaObject) {
		LOGGER.info("start update fill ....");
		Object enabledMark = this.getFieldValByName("enabledMark", metaObject);
		Object deleteMark = this.getFieldValByName("deleteMark", metaObject);
		Object modifyDate = this.getFieldValByName("modifyDate", metaObject);
		if(enabledMark==null){
			this.setFieldValByName("enabledMark", 1, metaObject);
		}
		if(deleteMark==null){
			this.setFieldValByName("deleteMark", 0, metaObject);
		}
		if(modifyDate==null){
			this.setFieldValByName("modifyDate", new Date(), metaObject);
		}
	}
}

